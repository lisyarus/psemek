#pragma once

#include <iostream>

namespace psemek::log
{

	enum class level
	{
		debug,
		info,
		warning,
		error,
	};

	inline std::ostream & operator << (std::ostream & s, level l)
	{
		switch (l)
		{
		case level::debug: s << "debug"; break;
		case level::info: s << "info"; break;
		case level::warning: s << "warn"; break;
		case level::error: s << "error"; break;
		default: s << "(unknown)"; break;
		}
		return s;
	}

	inline bool operator < (level l1, level l2)
	{
		return static_cast<int>(l1) < static_cast<int>(l2);
	}

	inline bool operator > (level l1, level l2)
	{
		return l2 < l1;
	}

	inline bool operator <= (level l1, level l2)
	{
		return !(l2 < l1);
	}

	inline bool operator >= (level l1, level l2)
	{
		return !(l1 < l2);
	}

}
