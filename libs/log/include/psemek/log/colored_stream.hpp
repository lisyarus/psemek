#pragma once

#include <psemek/io/stream.hpp>

namespace psemek::log
{

	std::unique_ptr<io::ostream> colored_stream(std::unique_ptr<io::ostream> stream, std::string open, std::string close);

}
