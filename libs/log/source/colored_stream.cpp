#include <psemek/log/colored_stream.hpp>

namespace psemek::log
{

	namespace
	{

		struct colored_stream_impl
			: io::ostream
		{
			colored_stream_impl(std::unique_ptr<io::ostream> stream, std::string open, std::string close)
				: stream_(std::move(stream))
				, open_(std::move(open))
				, close_(std::move(close))
			{}

			std::size_t write(char const * p, std::size_t size) override
			{
				std::size_t result = 0;
				stream_->write(open_.data(), open_.size());
				result += stream_->write(p, size);
				stream_->write(close_.data(), close_.size());
				return result;
			}

			void flush() override
			{
				stream_->flush();
			}

		private:
			std::unique_ptr<io::ostream> stream_;
			std::string open_;
			std::string close_;
		};

	}

	std::unique_ptr<io::ostream> colored_stream(std::unique_ptr<io::ostream> stream, std::string open, std::string close)
	{
		return std::make_unique<colored_stream_impl>(std::move(stream), std::move(open), std::move(close));
	}

}
