#pragma once

#include <psemek/math/interval.hpp>

#include <limits>
#include <cmath>

namespace psemek::random
{

	template <typename T>
	struct uniform_real_distribution
	{
		static_assert(std::is_floating_point_v<T>);

		using result_type = T;

		uniform_real_distribution()
			: range_{T(0), T(1)}
		{}

		uniform_real_distribution(T min, T max)
			: range_{min, max}
		{}

		uniform_real_distribution(math::interval<T> range)
			: range_(range)
		{}

		uniform_real_distribution(uniform_real_distribution const &) = default;

		template <typename RNG>
		T operator()(RNG && rng);

		math::interval<T> range() const { return range_; }

	private:
		math::interval<T> range_;

		template <typename RNG>
		T generate(RNG & rng);
	};

	template <typename T>
	template <typename RNG>
	T uniform_real_distribution<T>::operator()(RNG && rng)
	{
		return generate(rng) * range_.length() + range_.min;
	}

	template <typename T>
	template <typename RNG>
	T uniform_real_distribution<T>::generate(RNG & rng)
	{
		// see https://en.cppreference.com/w/cpp/numeric/random/generate_canonical

		static constexpr std::size_t digits = std::numeric_limits<T>::digits;
		static T const limit = std::pow<T>(2, digits);

		T const R = static_cast<T>(rng.max()) - static_cast<T>(rng.min()) + 1;

		T mult = T(1);

		T sum = T(0);

		while (mult < limit)
		{
			T inc = static_cast<T>(rng() - rng.min());
			sum += inc * mult;
			mult *= R;
		}

		return sum / mult;
	}

}
