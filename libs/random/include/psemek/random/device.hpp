#pragma once

#include <boost/nondet_random.hpp>

namespace psemek::random
{

	using device = boost::random_device;

	// check that random_device::result_type is basically std::uint32_t
	static_assert(std::is_integral_v<device::result_type>);
	static_assert(std::is_unsigned_v<device::result_type>);
	static_assert(sizeof(device::result_type) == 4);

}
