#pragma once

#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>
#include <psemek/random/uniform.hpp>

namespace psemek::random
{

	template <typename T, std::size_t N>
	struct uniform_box_point_distribution
	{
		using result_type = math::point<T, N>;

		uniform_box_point_distribution(math::box<T, N> const & b)
		{
			for (std::size_t i = 0; i < N; ++i)
			{
				d_[i] = uniform_distribution<T>{b[i]};
			}
		}

		uniform_box_point_distribution() = default;

		template <typename RNG>
		auto operator()(RNG && rng)
		{
			math::point<T, N> p;
			for (std::size_t i = 0; i < N; ++i)
				p[i] = d_[i](rng);
			return p;
		}

		math::box<T, N> box() const
		{
			math::box<T, N> result;
			for (std::size_t i = 0; i < N; ++i)
				result[i] = d_[i].range();
			return result;
		}

	private:
		uniform_distribution<T> d_[N];
	};

}
