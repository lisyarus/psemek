#pragma once

#include <psemek/random/uniform.hpp>

#include <cmath>

namespace psemek::random
{

	template <typename T>
	struct exponential_distribution
	{
		exponential_distribution(T lambda)
			: lambda_(lambda)
		{}

		template <typename RNG>
		T operator()(RNG & rng)
		{
			T const y = uniform<T>(rng);
			return - std::log(1 - y) / lambda_;
		}

	private:
		T lambda_;
	};

}
