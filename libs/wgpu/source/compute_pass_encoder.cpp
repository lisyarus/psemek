#include <psemek/wgpu/compute_pass_encoder.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void compute_pass_encoder::set_bind_group(std::uint32_t group_index, bind_group const & group, std::vector<std::uint32_t> const & dynamic_offsets)
	{
		wgpuComputePassEncoderSetBindGroup((WGPUComputePassEncoder)get(), group_index, (WGPUBindGroup)group.get(), dynamic_offsets.size(), dynamic_offsets.data());
	}

	void compute_pass_encoder::set_pipeline(compute_pipeline const & pipeline)
	{
		wgpuComputePassEncoderSetPipeline((WGPUComputePassEncoder)get(), (WGPUComputePipeline)pipeline.get());
	}

	void compute_pass_encoder::dispatch_workgroups(math::vector<std::uint32_t, 3> const & workgroup_count)
	{
		wgpuComputePassEncoderDispatchWorkgroups((WGPUComputePassEncoder)get(), workgroup_count[0], workgroup_count[1], workgroup_count[2]);
	}

	void compute_pass_encoder::dispatch_workgroups_indirect(buffer const & indirect_buffer, std::uint64_t offset)
	{
		wgpuComputePassEncoderDispatchWorkgroupsIndirect((WGPUComputePassEncoder)get(), (WGPUBuffer)indirect_buffer.get(), offset);
	}

	void compute_pass_encoder::insert_debug_marker(std::string const & marker_label)
	{
		wgpuComputePassEncoderInsertDebugMarker((WGPUComputePassEncoder)get(), marker_label.data());
	}

	void compute_pass_encoder::push_debug_group(std::string const & group_label)
	{
		wgpuComputePassEncoderPushDebugGroup((WGPUComputePassEncoder)get(), group_label.data());
	}

	void compute_pass_encoder::pop_debug_group()
	{
		wgpuComputePassEncoderPopDebugGroup((WGPUComputePassEncoder)get());
	}

	void compute_pass_encoder::end()
	{
		wgpuComputePassEncoderEnd((WGPUComputePassEncoder)get());
	}

	void compute_pass_encoder::set_label(std::string const & label)
	{
		wgpuComputePassEncoderSetLabel((WGPUComputePassEncoder)get(), label.data());
	}

	void compute_pass_encoder::reference(void * ptr)
	{
		wgpuComputePassEncoderReference((WGPUComputePassEncoder)ptr);
	}

	void compute_pass_encoder::release(void * ptr)
	{
		wgpuComputePassEncoderRelease((WGPUComputePassEncoder)ptr);
	}

}
