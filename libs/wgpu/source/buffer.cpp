#include <psemek/wgpu/buffer.hpp>
#include <psemek/wgpu/external/webgpu.h>

#include <memory>

namespace psemek::wgpu
{

	void buffer::destroy()
	{
		wgpuBufferDestroy((WGPUBuffer)get());
	}

	std::uint64_t buffer::get_size()
	{
		return wgpuBufferGetSize((WGPUBuffer)get());
	}

	buffer::usage buffer::get_usage()
	{
		return (usage)wgpuBufferGetUsage((WGPUBuffer)get());
	}

	buffer::map_state buffer::get_map_state()
	{
		return (map_state)wgpuBufferGetMapState((WGPUBuffer)get());
	}

	void const * buffer::get_const_mapped_range(std::size_t offset, std::size_t size)
	{
		return wgpuBufferGetConstMappedRange((WGPUBuffer)get(), offset, size);
	}

	void * buffer::get_mapped_range(std::size_t offset, std::size_t size)
	{
		return wgpuBufferGetMappedRange((WGPUBuffer)get(), offset, size);
	}

	void buffer::map_async(map_mode flags, std::size_t offset, std::size_t size, map_callback const & callback)
	{
		auto userdata = new map_callback(callback);

		auto real_callback = [](WGPUBufferMapAsyncStatus status, void * userdata)
		{
			std::unique_ptr<map_callback> callback((map_callback *)userdata);
			if (*callback) (*callback)((map_async_status)status);
		};

		wgpuBufferMapAsync((WGPUBuffer)get(), (WGPUMapModeFlags)flags, offset, size, real_callback, userdata);
	}

	void buffer::unmap()
	{
		wgpuBufferUnmap((WGPUBuffer)get());
	}

	void buffer::set_label(std::string const & label)
	{
		wgpuBufferSetLabel((WGPUBuffer)get(), label.data());
	}

	void buffer::reference(void * ptr)
	{
		wgpuBufferReference((WGPUBuffer)ptr);
	}

	void buffer::release(void * ptr)
	{
		wgpuBufferRelease((WGPUBuffer)ptr);
	}

}
