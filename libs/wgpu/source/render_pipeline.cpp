#include <psemek/wgpu/render_pipeline.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	bind_group_layout render_pipeline::get_bind_group_layout(std::uint32_t index)
	{
		return bind_group_layout(wgpuRenderPipelineGetBindGroupLayout((WGPURenderPipeline)get(), index));
	}

	void render_pipeline::set_label(std::string const & label)
	{
		wgpuRenderPipelineSetLabel((WGPURenderPipeline)get(), label.data());
	}

	void render_pipeline::reference(void * ptr)
	{
		wgpuRenderPipelineReference((WGPURenderPipeline)ptr);
	}

	void render_pipeline::release(void * ptr)
	{
		wgpuRenderPipelineRelease((WGPURenderPipeline)ptr);
	}

}
