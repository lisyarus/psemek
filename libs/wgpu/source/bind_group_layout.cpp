#include <psemek/wgpu/bind_group_layout.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void bind_group_layout::set_label(std::string const & label)
	{
		wgpuBindGroupLayoutSetLabel((WGPUBindGroupLayout)get(), label.data());
	}

	void bind_group_layout::reference(void * ptr)
	{
		wgpuBindGroupLayoutReference((WGPUBindGroupLayout)ptr);
	}

	void bind_group_layout::release(void * ptr)
	{
		wgpuBindGroupLayoutRelease((WGPUBindGroupLayout)ptr);
	}

}
