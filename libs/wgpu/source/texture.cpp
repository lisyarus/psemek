#include <psemek/wgpu/texture.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	texture_view texture::create_view(texture_view::descriptor const & desc)
	{
		WGPUTextureViewDescriptor descriptor = {};
		descriptor.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(desc.chain);
		descriptor.label = desc.label.data();
		descriptor.format = (WGPUTextureFormat)desc.format;
		descriptor.dimension = (WGPUTextureViewDimension)desc.dimension;
		descriptor.baseMipLevel = desc.base_mip_level;
		descriptor.mipLevelCount = desc.mip_level_count;
		descriptor.baseArrayLayer = desc.base_array_layer;
		descriptor.arrayLayerCount = desc.array_layer_count;
		descriptor.aspect = (WGPUTextureAspect)desc.aspect;

		return texture_view(wgpuTextureCreateView((WGPUTexture)get(), &descriptor));
	}

	void texture::destroy()
	{
		wgpuTextureDestroy((WGPUTexture)get());
	}

	std::uint32_t texture::get_width()
	{
		return wgpuTextureGetWidth((WGPUTexture)get());
	}

	std::uint32_t texture::get_height()
	{
		return wgpuTextureGetHeight((WGPUTexture)get());
	}

	std::uint32_t texture::get_depth()
	{
		return wgpuTextureGetDepthOrArrayLayers((WGPUTexture)get());
	}

	texture::dimension texture::get_dimension()
	{
		return (dimension)wgpuTextureGetDimension((WGPUTexture)get());
	}

	texture::format texture::get_format()
	{
		return (format)wgpuTextureGetFormat((WGPUTexture)get());
	}

	std::uint32_t texture::get_mip_level_count()
	{
		return wgpuTextureGetMipLevelCount((WGPUTexture)get());
	}

	texture::usage texture::get_usage()
	{
		return (usage)wgpuTextureGetUsage((WGPUTexture)get());
	}

	void texture::set_label(std::string const & label)
	{
		wgpuTextureSetLabel((WGPUTexture)get(), label.data());
	}

	void texture::reference(void * ptr)
	{
		wgpuTextureReference((WGPUTexture)ptr);
	}

	void texture::release(void * ptr)
	{
		wgpuTextureRelease((WGPUTexture)ptr);
	}

}
