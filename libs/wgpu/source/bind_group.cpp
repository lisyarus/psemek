#include <psemek/wgpu/bind_group.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void bind_group::set_label(std::string const & label)
	{
		wgpuBindGroupSetLabel((WGPUBindGroup)get(), label.data());
	}

	void bind_group::reference(void * ptr)
	{
		wgpuBindGroupReference((WGPUBindGroup)ptr);
	}

	void bind_group::release(void * ptr)
	{
		wgpuBindGroupRelease((WGPUBindGroup)ptr);
	}

}
