#include <psemek/wgpu/render_bundle.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void render_bundle::set_label(std::string const & label)
	{
		wgpuRenderBundleSetLabel((WGPURenderBundle)get(), label.data());
	}

	void render_bundle::reference(void * ptr)
	{
		wgpuRenderBundleReference((WGPURenderBundle)ptr);
	}

	void render_bundle::release(void * ptr)
	{
		wgpuRenderBundleRelease((WGPURenderBundle)ptr);
	}

}
