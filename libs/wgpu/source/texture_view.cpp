#include <psemek/wgpu/texture_view.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void texture_view::set_label(std::string const & label)
	{
		wgpuTextureViewSetLabel((WGPUTextureView)get(), label.data());
	}

	void texture_view::reference(void * ptr)
	{
		wgpuTextureViewReference((WGPUTextureView)ptr);
	}

	void texture_view::release(void * ptr)
	{
		wgpuTextureViewRelease((WGPUTextureView)ptr);
	}

}
