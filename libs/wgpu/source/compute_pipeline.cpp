#include <psemek/wgpu/compute_pipeline.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	bind_group_layout compute_pipeline::get_bind_group_layout(std::uint32_t index)
	{
		return bind_group_layout(wgpuComputePipelineGetBindGroupLayout((WGPUComputePipeline)get(), index));
	}

	void compute_pipeline::set_label(std::string const & label)
	{
		wgpuComputePipelineSetLabel((WGPUComputePipeline)get(), label.data());
	}

	void compute_pipeline::reference(void * ptr)
	{
		wgpuComputePipelineReference((WGPUComputePipeline)ptr);
	}

	void compute_pipeline::release(void * ptr)
	{
		wgpuComputePipelineRelease((WGPUComputePipeline)ptr);
	}

}
