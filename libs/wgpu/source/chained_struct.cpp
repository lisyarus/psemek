#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu::detail
{

	void * fill_chain(std::vector<chained_struct> const & chain)
	{
		WGPUChainedStruct * first = nullptr;
		WGPUChainedStruct * current = nullptr;

		for (auto const & s : chain)
		{
			auto p = (WGPUChainedStruct *)s.ptr();
			if (!first)
			{
				first = p;
				current = p;
			}
			else
			{
				current->next = p;
				current = p;
			}
		}

		if (current)
			current->next = nullptr;

		return first;
	}

}
