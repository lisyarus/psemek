#include <psemek/wgpu/queue.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void queue::submit(std::vector<command_buffer> const & commands)
	{
		static_assert(sizeof(WGPUCommandBuffer) == sizeof(command_buffer));
		wgpuQueueSubmit((WGPUQueue)get(), commands.size(), (WGPUCommandBuffer*)commands.data());
	}

	void queue::write_buffer(buffer const & buffer, std::uint64_t offset, util::span<char const> data)
	{
		wgpuQueueWriteBuffer((WGPUQueue)get(), (WGPUBuffer)buffer.get(), offset, data.data(), data.size());
	}

	void queue::write_texture(image_copy_texture const & dest, util::span<char const> data, texture::data_layout const & data_layout, math::vector<std::uint32_t, 3> const & write_size)
	{
		WGPUImageCopyTexture image_copy_texture = {};
		image_copy_texture.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(dest.chain);
		image_copy_texture.texture = (WGPUTexture)dest.texture.get();
		image_copy_texture.mipLevel = dest.mip_level;
		image_copy_texture.origin = {dest.origin[0], dest.origin[1], dest.origin[2]};
		image_copy_texture.aspect = (WGPUTextureAspect)dest.aspect;

		WGPUTextureDataLayout texture_data_layout = {};
		texture_data_layout.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(data_layout.chain);
		texture_data_layout.offset = data_layout.offset;
		texture_data_layout.bytesPerRow = data_layout.bytes_per_row;
		texture_data_layout.rowsPerImage = data_layout.rows_per_image;

		WGPUExtent3D extent = {write_size[0], write_size[1], write_size[2]};

		wgpuQueueWriteTexture((WGPUQueue)get(), &image_copy_texture, data.data(), data.size(), &texture_data_layout, &extent);
	}

	void queue::on_submitted_work_done(work_done_callback const & callback)
	{
		auto userdata = new work_done_callback(callback);

		auto real_callback = [](WGPUQueueWorkDoneStatus status, void * userdata)
		{
			std::unique_ptr<work_done_callback> callback((work_done_callback*)userdata);
			if (*callback) (*callback)((work_done_status)status);
		};

		wgpuQueueOnSubmittedWorkDone((WGPUQueue)get(), real_callback, userdata);
	}

	void queue::set_label(std::string const & label)
	{
		wgpuQueueSetLabel((WGPUQueue)get(), label.data());
	}

	void queue::reference(void * ptr)
	{
		wgpuQueueReference((WGPUQueue)ptr);
	}

	void queue::release(void * ptr)
	{
		wgpuQueueRelease((WGPUQueue)ptr);
	}

}
