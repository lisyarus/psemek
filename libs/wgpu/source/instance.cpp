#include <psemek/wgpu/instance.hpp>
#include <psemek/wgpu/external/webgpu.h>
#include <psemek/wgpu/external/wgpu.h>

namespace psemek::wgpu
{

	namespace
	{

		struct extras
		{};

		detail::chained_struct_ptr to_chained_struct(extras const &)
		{
			WGPUInstanceExtras chained = {};
			chained.chain.sType = (WGPUSType)WGPUSType_InstanceExtras;
			chained.backends = WGPUInstanceBackend_Primary;
			return detail::make_chained_struct(chained);
		}

	}

	instance instance::create(descriptor const & desc)
	{
		std::vector<chained_struct> chain = desc.chain;
		chain.push_back(extras{});

		WGPUInstanceDescriptor descriptor = {};
		descriptor.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(chain);
		return instance(wgpuCreateInstance(&descriptor));
	}

	void instance::process_events()
	{
		wgpuInstanceProcessEvents((WGPUInstance)get());
	}

	surface instance::create_surface(surface::descriptor const & desc)
	{
		WGPUSurfaceDescriptor descriptor = {};
		descriptor.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(desc.chain);
		descriptor.label = desc.label.data();
		return surface{wgpuInstanceCreateSurface((WGPUInstance)get(), &descriptor)};
	}

	void instance::request_adapter(adapter::request_options const & request_options, adapter::request_callback callback)
	{
		WGPURequestAdapterOptions options;
		options.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(request_options.chain);
		options.compatibleSurface = (WGPUSurface)request_options.compatible_surface.get();
		options.powerPreference = (WGPUPowerPreference)request_options.power_preference;
		options.backendType = (WGPUBackendType)request_options.backend_type;
		options.forceFallbackAdapter = request_options.force_fallback_adapter;

		auto userdata = new adapter::request_callback(callback);

		auto real_callback = [](WGPURequestAdapterStatus status, WGPUAdapter adapter, char const * message, void * userdata)
		{
			std::unique_ptr<adapter::request_callback> callback{(adapter::request_callback *)userdata};
			if (*callback) (*callback)(adapter::request_status{status}, wgpu::adapter(adapter), message ? message : "");
		};

		wgpuInstanceRequestAdapter((WGPUInstance)get(), &options, real_callback, userdata);
	}

	void instance::reference(void * ptr)
	{
		wgpuInstanceReference((WGPUInstance)ptr);
	}

	void instance::release(void * ptr)
	{
		wgpuInstanceRelease((WGPUInstance)ptr);
	}

}
