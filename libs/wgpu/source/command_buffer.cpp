#include <psemek/wgpu/command_buffer.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void command_buffer::set_label(std::string const & label)
	{
		wgpuCommandBufferSetLabel((WGPUCommandBuffer)get(), label.data());
	}

	void command_buffer::reference(void * ptr)
	{
		wgpuCommandBufferReference((WGPUCommandBuffer)ptr);
	}

	void command_buffer::release(void * ptr)
	{
		wgpuCommandBufferRelease((WGPUCommandBuffer)ptr);
	}

}
