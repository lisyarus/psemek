#include <psemek/wgpu/pipeline_layout.hpp>
#include <psemek/wgpu/external/webgpu.h>
#include <psemek/wgpu/external/wgpu.h>

namespace psemek::wgpu
{

	void pipeline_layout::set_label(std::string const & label)
	{
		wgpuPipelineLayoutSetLabel((WGPUPipelineLayout)get(), label.data());
	}

	void pipeline_layout::reference(void * ptr)
	{
		wgpuPipelineLayoutReference((WGPUPipelineLayout)ptr);
	}

	void pipeline_layout::release(void * ptr)
	{
		wgpuPipelineLayoutRelease((WGPUPipelineLayout)ptr);
	}

	detail::chained_struct_ptr to_chained_struct(pipeline_layout::extras && value)
	{
		WGPUPipelineLayoutExtras chained = {};
		chained.chain.sType = (WGPUSType)WGPUSType_PipelineLayoutExtras;
		static_assert(sizeof(WGPUPushConstantRange) == sizeof(push_constant_range));
		chained.pushConstantRangeCount = value.ranges.size();
		chained.pushConstantRanges = (WGPUPushConstantRange const *)value.ranges.data();
		return detail::make_chained_struct(chained, std::move(value));
	}

}
