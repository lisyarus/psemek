#include <psemek/wgpu/query_set.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void query_set::destroy()
	{
		wgpuQuerySetDestroy((WGPUQuerySet)get());
	}

	std::uint32_t query_set::get_count()
	{
		return wgpuQuerySetGetCount((WGPUQuerySet)get());
	}

	query_set::type query_set::get_type()
	{
		return (type)wgpuQuerySetGetType((WGPUQuerySet)get());
	}

	void query_set::set_label(std::string const & label)
	{
		wgpuQuerySetSetLabel((WGPUQuerySet)get(), label.data());
	}

	void query_set::reference(void * ptr)
	{
		wgpuQuerySetReference((WGPUQuerySet)ptr);
	}

	void query_set::release(void * ptr)
	{
		wgpuQuerySetRelease((WGPUQuerySet)ptr);
	}

}
