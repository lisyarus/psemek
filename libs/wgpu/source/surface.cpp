#include <psemek/wgpu/surface.hpp>
#include <psemek/wgpu/adapter.hpp>
#include <psemek/wgpu/external/webgpu.h>
#include <psemek/wgpu/external/wgpu.h>

namespace psemek::wgpu
{

	void surface::configure(configuration const & conf)
	{
		WGPUSurfaceConfiguration config = {};

		config.nextInChain = (WGPUChainedStruct const *)detail::fill_chain(conf.chain);
		config.device = (WGPUDevice)conf.device.get();
		config.format = (WGPUTextureFormat)conf.format;
		config.usage = (WGPUTextureUsageFlags)conf.usage;
		config.viewFormatCount = conf.view_formats.size();
		config.viewFormats = (WGPUTextureFormat const *)conf.view_formats.data();
		config.alphaMode = (WGPUCompositeAlphaMode)conf.alpha_mode;
		config.width = conf.width;
		config.height = conf.height;
		config.presentMode = (WGPUPresentMode)conf.present_mode;
		wgpuSurfaceConfigure((WGPUSurface)get(), &config);
	}

	surface::capabilities surface::get_capabilities(adapter const & adapter)
	{
		WGPUSurfaceCapabilities caps = {};
		wgpuSurfaceGetCapabilities((WGPUSurface)get(), (WGPUAdapter)adapter.get(), &caps);

		capabilities result;
		// TODO: support out chain
		result.formats.assign((wgpu::texture::format *)(caps.formats), (wgpu::texture::format *)(caps.formats) + caps.formatCount);
		result.present_modes.assign((present_mode *)(caps.presentModes), (present_mode *)(caps.presentModes) + caps.presentModeCount);
		result.alpha_modes.assign((composite_alpha_mode *)(caps.alphaModes), (composite_alpha_mode *)(caps.alphaModes) + caps.alphaModeCount);
		return result;
	}

	surface::current_texture surface::get_current_texture()
	{
		WGPUSurfaceTexture tex;
		wgpuSurfaceGetCurrentTexture((WGPUSurface)get(), &tex);

		return {
			.texture = wgpu::texture(tex.texture),
			.suboptimal = tex.suboptimal,
			.status = (enum current_texture::status)tex.status,
		};
	}

	wgpu::texture::format surface::get_preferred_format(adapter const & adapter)
	{
		return (wgpu::texture::format)wgpuSurfaceGetPreferredFormat((WGPUSurface)get(), (WGPUAdapter)adapter.get());
	}

	void surface::present()
	{
		wgpuSurfacePresent((WGPUSurface)get());
	}

	void surface::unconfigure()
	{
		wgpuSurfaceUnconfigure((WGPUSurface)get());
	}

	void surface::reference(void * ptr)
	{
		wgpuSurfaceReference((WGPUSurface)ptr);
	}

	void surface::release(void * ptr)
	{
		wgpuSurfaceRelease((WGPUSurface)ptr);
	}

	detail::chained_struct_ptr to_chained_struct(surface::configuration::extras const & value)
	{
		WGPUSurfaceConfigurationExtras chained = {};
		chained.chain.sType = (WGPUSType)WGPUSType_SurfaceConfigurationExtras;
		chained.desiredMaximumFrameLatency = value.desired_maximum_frame_latency;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_android_native_window const & value)
	{
		WGPUSurfaceDescriptorFromAndroidNativeWindow chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromAndroidNativeWindow;
		chained.window = value.window;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_canvas_html_selected const & value)
	{
		WGPUSurfaceDescriptorFromCanvasHTMLSelector chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromCanvasHTMLSelector;
		chained.selector = value.selector.data();
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_metal_layer const & value)
	{
		WGPUSurfaceDescriptorFromMetalLayer chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromMetalLayer;
		chained.layer = value.layer;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_wayland_surface const & value)
	{
		WGPUSurfaceDescriptorFromWaylandSurface chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromWaylandSurface;
		chained.display = value.display;
		chained.surface = value.surface;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_windows_hwnd const & value)
	{
		WGPUSurfaceDescriptorFromWindowsHWND chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromWindowsHWND;
		chained.hinstance = value.hinstance;
		chained.hwnd = value.hwnd;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_xcb_window const & value)
	{
		WGPUSurfaceDescriptorFromXcbWindow chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromXcbWindow;
		chained.connection = value.connection;
		chained.window = value.window;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(surface::from_xlib_window const & value)
	{
		WGPUSurfaceDescriptorFromXlibWindow chained = {};
		chained.chain.sType = WGPUSType_SurfaceDescriptorFromXlibWindow;
		chained.display = value.display;
		chained.window = value.window;
		return detail::make_chained_struct(chained);
	}

}
