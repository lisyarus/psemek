#include <psemek/wgpu/logging.hpp>
#include <psemek/wgpu/external/wgpu.h>
#include <psemek/log/log.hpp>
#include <psemek/util/enum.hpp>

namespace psemek::wgpu
{

	namespace
	{

		log::level to_psemek_log_level(WGPULogLevel level)
		{
			switch (level)
			{
			case WGPULogLevel_Off: return log::level::error;
			case WGPULogLevel_Error: return log::level::error;
			case WGPULogLevel_Warn: return log::level::warning;
			case WGPULogLevel_Info: return log::level::info;
			case WGPULogLevel_Debug: return log::level::debug;
			case WGPULogLevel_Trace: return log::level::debug;
			default: return log::level::error;
			}
		}

		WGPULogLevel to_wgpu_log_level(log::level level)
		{
			switch (level)
			{
			case log::level::debug: return WGPULogLevel_Debug;
			case log::level::info: return WGPULogLevel_Info;
			case log::level::warning: return WGPULogLevel_Warn;
			case log::level::error: return WGPULogLevel_Error;
			default: return WGPULogLevel_Error;
			}
		}

	}

	void setup_logging(log::level level)
	{
		wgpuSetLogLevel(to_wgpu_log_level(level));
		wgpuSetLogCallback([](WGPULogLevel level, char const * message, void * /* userdata */){
			log::log(to_psemek_log_level(level)) << "WebGPU: " << message;
		}, nullptr);
	}

}
