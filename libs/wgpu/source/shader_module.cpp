#include <psemek/wgpu/shader_module.hpp>
#include <psemek/wgpu/external/webgpu.h>
#include <psemek/wgpu/external/wgpu.h>

#include <memory>

namespace psemek::wgpu
{

	void shader_module::get_compilation_info(compilation_info_callback const & callback)
	{
		auto userdata = new compilation_info_callback(callback);

		auto real_callback = [](WGPUCompilationInfoRequestStatus status, WGPUCompilationInfo const * info, void * userdata)
		{
			std::unique_ptr<compilation_info_callback> callback((compilation_info_callback *)userdata);

			// TODO support chained struct
			compilation_info cinfo;
			if (info)
			{
				for (std::size_t i = 0; i < info->messageCount; ++i)
				{
					auto & message = cinfo.messages.emplace_back();
					auto & message_in = info->messages[i];

					message.message = message_in.message;
					message.type = (compilation_message_type)message_in.type;
					message.line_num = message_in.lineNum;
					message.line_pos = message_in.linePos;
					message.offset = message_in.offset;
					message.length = message_in.length;
					message.utf16_line_pos = message_in.utf16LinePos;
					message.utf16_offset = message_in.utf16Offset;
					message.utf16_length = message_in.utf16Length;
				}
			}

			if (*callback) (*callback)((compilation_info_request_status)status, cinfo);
		};

		wgpuShaderModuleGetCompilationInfo((WGPUShaderModule)get(), real_callback, userdata);
	}

	void shader_module::set_label(std::string const & label)
	{
		wgpuShaderModuleSetLabel((WGPUShaderModule)get(), label.data());
	}

	void shader_module::reference(void * ptr)
	{
		wgpuShaderModuleReference((WGPUShaderModule)ptr);
	}

	void shader_module::release(void * ptr)
	{
		wgpuShaderModuleRelease((WGPUShaderModule)ptr);
	}

	detail::chained_struct_ptr to_chained_struct(shader_module::spirv_descriptor const & value)
	{
		WGPUShaderModuleSPIRVDescriptor chained = {};
		chained.chain.sType = WGPUSType_ShaderModuleSPIRVDescriptor;
		chained.codeSize = value.code.size(); // TODO: is it in bytes or in words?
		chained.code = value.code.data();
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(shader_module::wgsl_descriptor const & value)
	{
		WGPUShaderModuleWGSLDescriptor chained = {};
		chained.chain.sType = WGPUSType_ShaderModuleWGSLDescriptor;
		chained.code = value.code;
		return detail::make_chained_struct(chained);
	}

	detail::chained_struct_ptr to_chained_struct(shader_module::glsl_descriptor && value)
	{
		WGPUShaderModuleGLSLDescriptor chained = {};
		chained.chain.sType = (WGPUSType)WGPUSType_ShaderModuleGLSLDescriptor;
		chained.stage = (WGPUShaderStage)value.stage;
		chained.code = value.code;
		chained.defineCount = value.defines.size();
		static_assert(sizeof(WGPUShaderDefine) == sizeof(shader_module::define));
		chained.defines = (WGPUShaderDefine *)value.defines.data();
		return detail::make_chained_struct(chained, std::move(value));
	}

}
