#include <psemek/wgpu/sampler.hpp>
#include <psemek/wgpu/external/webgpu.h>

namespace psemek::wgpu
{

	void sampler::set_label(std::string const & label)
	{
		wgpuSamplerSetLabel((WGPUSampler)get(), label.data());
	}

	void sampler::reference(void * ptr)
	{
		wgpuSamplerReference((WGPUSampler)ptr);
	}

	void sampler::release(void * ptr)
	{
		wgpuSamplerRelease((WGPUSampler)ptr);
	}

}
