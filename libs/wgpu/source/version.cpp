#include <psemek/wgpu/version.hpp>
#include <psemek/wgpu/external/wgpu.h>

#include <cstdint>

namespace psemek::wgpu
{

	std::ostream & operator << (std::ostream & os, version const & version)
	{
		os << version.major << '.' << version.minor << '.' << version.revision << '.' << version.build;
		return os;
	}

	version get_version()
	{
		std::uint32_t value = wgpuGetVersion();
		return {
			.major    = (value >> 24) & 0xffu,
			.minor    = (value >> 16) & 0xffu,
			.revision = (value >>  8) & 0xffu,
			.build    = (value >>  0) & 0xffu,
		};
	}

}
