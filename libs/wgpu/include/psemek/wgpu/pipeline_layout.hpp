#pragma once

#include <psemek/wgpu/detail/object.hpp>
#include <psemek/wgpu/bind_group_layout.hpp>
#include <psemek/wgpu/chained_struct.hpp>

#include <string>
#include <vector>

namespace psemek::wgpu
{

	struct push_constant_range
	{
		shader_stage stages;
		std::uint32_t start;
		std::uint32_t end;
	};

	struct pipeline_layout
		: detail::object<pipeline_layout>
	{
		using detail::object<pipeline_layout>::object;

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
			std::vector<bind_group_layout> layouts;
		};

		struct extras
		{
			std::vector<push_constant_range> ranges;
		};

		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit pipeline_layout(void * ptr)
			: detail::object<pipeline_layout>(ptr)
		{}

		friend struct device;
	};

	detail::chained_struct_ptr to_chained_struct(pipeline_layout::extras && value);

}
