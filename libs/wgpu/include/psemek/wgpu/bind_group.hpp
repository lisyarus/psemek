#pragma once

#include <psemek/wgpu/bind_group_layout.hpp>
#include <psemek/wgpu/buffer.hpp>
#include <psemek/wgpu/sampler.hpp>
#include <psemek/wgpu/texture_view.hpp>
#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <string>
#include <vector>
#include <cstdint>

namespace psemek::wgpu
{

	struct bind_group
		: detail::object<bind_group>
	{
		using detail::object<bind_group>::object;

		struct entry
		{
			std::vector<chained_struct> chain = {};
			std::uint32_t binding;
			struct buffer buffer = {};
			std::uint64_t offset = 0;
			std::uint64_t size = 0;
			struct sampler sampler = {};
			struct texture_view texture_view = {};
		};

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
			bind_group_layout layout;
			std::vector<entry> entries;
		};

		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit bind_group(void * ptr)
			: detail::object<bind_group>(ptr)
		{}

		friend struct device;
	};

}
