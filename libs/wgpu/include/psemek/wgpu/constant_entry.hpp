#pragma once

#include <psemek/wgpu/chained_struct.hpp>

#include <string>
#include <vector>

namespace psemek::wgpu
{

	struct constant_entry
	{
		std::vector<chained_struct> chain;
		std::string key;
		double value;
	};

}
