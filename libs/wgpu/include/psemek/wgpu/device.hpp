#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/queue.hpp>
#include <psemek/wgpu/bind_group.hpp>
#include <psemek/wgpu/bind_group_layout.hpp>
#include <psemek/wgpu/buffer.hpp>
#include <psemek/wgpu/command_encoder.hpp>
#include <psemek/wgpu/compute_pipeline.hpp>
#include <psemek/wgpu/pipeline_layout.hpp>
#include <psemek/wgpu/query_set.hpp>
#include <psemek/wgpu/render_bundle_encoder.hpp>
#include <psemek/wgpu/render_pipeline.hpp>
#include <psemek/wgpu/sampler.hpp>
#include <psemek/wgpu/shader_module.hpp>
#include <psemek/wgpu/texture.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <cstdint>
#include <optional>
#include <functional>

namespace psemek::wgpu
{

	enum class feature : std::uint32_t {
		undefined = 0x00000000,
		depth_clip_control = 0x00000001,
		depth32float_stencil8 = 0x00000002,
		timestamp_query = 0x00000003,
		texture_compression_bc = 0x00000004,
		texture_compression_etc2 = 0x00000005,
		texture_compression_astc = 0x00000006,
		indirect_first_instance = 0x00000007,
		shader_f16 = 0x00000008,
		rg11b10ufloat_renderable = 0x00000009,
		bgra8unorm_storage = 0x0000000a,
		float32_filterable = 0x0000000b,

		push_constants = 0x00030001,
		texture_adapter_specific_format_features = 0x00030002,
		multi_draw_indirect = 0x00030003,
		multi_draw_indirect_count = 0x00030004,
		vertex_writable_storage = 0x00030005,
		texture_binding_array = 0x00030006,
		sampled_texture_and_storage_buffer_array_non_uniform_indexing = 0x00030007,
		pipeline_statistics_query = 0x00030008,
	};

	struct limits
	{
		std::uint32_t max_texture_dimension_1D = 8192;
		std::uint32_t max_texture_dimension_2D = 8192;
		std::uint32_t max_texture_dimension_3D = 2048;
		std::uint32_t max_texture_array_layers = 256;
		std::uint32_t max_bind_groups = 4;
		std::uint32_t max_bind_groups_plus_vertex_buffers = 8;
		std::uint32_t max_bindings_per_bind_group = 1000;
		std::uint32_t max_dynamic_uniform_buffers_per_pipeline_layout = 8;
		std::uint32_t max_dynamic_storage_buffers_per_pipeline_layout = 4;
		std::uint32_t max_sampled_textures_per_shader_stage = 16;
		std::uint32_t max_samplers_per_shader_stage = 16;
		std::uint32_t max_storage_buffers_per_shader_stage = 8;
		std::uint32_t max_storage_textures_per_shader_stage = 4;
		std::uint32_t max_uniform_buffers_per_shader_stage = 12;
		std::uint64_t max_uniform_buffer_binding_size = 64 * 1024;
		std::uint64_t max_storage_buffer_binding_size = 128 * 1024 * 1024;
		std::uint32_t min_uniform_buffer_offset_alignment = 256;
		std::uint32_t min_storage_buffer_offset_alignment = 256;
		std::uint32_t max_vertex_buffers = 8;
		std::uint64_t max_buffer_size = 256 * 1024 * 1024;
		std::uint32_t max_vertex_attributes = 16;
		std::uint32_t max_vertex_buffer_array_stride = 2048;
		std::uint32_t max_inter_stage_shader_components = 60;
		std::uint32_t max_inter_stage_shader_variables = 15;
		std::uint32_t max_color_attachments = 1;
		std::uint32_t max_color_attachment_bytes_per_sample = 4;
		std::uint32_t max_compute_workgroup_storage_size = 16384;
		std::uint32_t max_compute_invocations_per_workgroup = 256;
		std::uint32_t max_compute_workgroup_size_x = 256;
		std::uint32_t max_compute_workgroup_size_y = 256;
		std::uint32_t max_compute_workgroup_size_z = 64;
		std::uint32_t max_compute_workgroups_per_dimension = 65535;
	};

	struct native_limits
	{
		std::uint32_t max_push_constant_size = 0;
		std::uint32_t max_non_sampler_bindings = 0;
	};

	enum class create_pipeline_async_status : std::uint32_t
	{
		success = 0x00000000,
		validation_error = 0x00000001,
		internal_error = 0x00000002,
		device_lost = 0x00000003,
		device_destroyed = 0x00000004,
		unknown = 0x00000005,
	};

	enum class error_filter : std::uint32_t
	{
		validation = 0x00000000,
		out_of_memory = 0x00000001,
		internal = 0x00000002,
	};

	enum class error_type : std::uint32_t
	{
		no_error = 0x00000000,
		validation = 0x00000001,
		out_of_memory = 0x00000002,
		internal = 0x00000003,
		unknown = 0x00000004,
		device_lost = 0x00000005,
	};

	struct device
		: detail::object<device>
	{
		using detail::object<device>::object;

		enum class lost_reason : std::uint32_t
		{
			undefined = 0x00000000,
			destroyed = 0x00000001,
		};

		using lost_callback = std::function<void(lost_reason, std::string const &)>;

		struct required_limits
		{
			std::vector<chained_struct> chain = {};
			wgpu::limits limits = {};
		};

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
			std::vector<feature> required_features = {};
			std::optional<struct required_limits> required_limits = {};
			queue::descriptor default_queue = {};
			device::lost_callback lost_callback = {};
		};

		enum class request_status : std::uint32_t
		{
			success = 0x00000000,
			error = 0x00000001,
			unknown = 0x00000002,
		};

		using request_callback = std::function<void(request_status, device, std::string const & message)>;
		using create_compute_pipeline_async_callback = std::function<void(create_pipeline_async_status, compute_pipeline, std::string const &)>;
		using create_render_pipeline_async_callback = std::function<void(create_pipeline_async_status, render_pipeline, std::string const &)>;
		using error_callback = std::function<void(error_type, std::string const &)>;

		queue get_queue();
		bind_group create_bind_group(bind_group::descriptor const & desc);
		bind_group_layout create_bind_group_layout(bind_group_layout::descriptor const & desc);
		buffer create_buffer(buffer::descriptor const & desc);
		command_encoder create_command_encoder(command_encoder::descriptor const & desc);
		compute_pipeline create_compute_pipeline(compute_pipeline::descriptor const & desc);
		void create_compute_pipeline_async(compute_pipeline::descriptor const & desc, create_compute_pipeline_async_callback const & callback);
		pipeline_layout create_pipeline_layout(pipeline_layout::descriptor const & desc);
		query_set create_query_set(query_set::descriptor const & desc);
		render_pipeline create_render_pipeline(render_pipeline::descriptor const & desc);
		void create_render_pipeline_async(render_pipeline::descriptor const & desc, create_render_pipeline_async_callback const & callback);
		render_bundle_encoder create_render_bundle_encoder(render_bundle_encoder::descriptor const & desc);
		sampler create_sampler(sampler::descriptor const & desc);
		shader_module create_shader_module(shader_module::descriptor const & desc);
		texture create_texture(texture::descriptor const & desc);
		void destroy();
		std::vector<feature> enumerate_features();
		limits get_limits();
		bool has_feature(feature feature);
		void push_error_scope(error_filter filter);
		void pop_error_scope(error_callback const & callback);
		void set_uncaptured_error_callback(error_callback const & callback);
		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit device(void * ptr)
			: detail::object<device>(ptr)
		{}

		friend struct adapter;
	};

	detail::chained_struct_ptr to_chained_struct(native_limits && value);

}
