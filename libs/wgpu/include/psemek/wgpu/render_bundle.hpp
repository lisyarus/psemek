#pragma once

#include <psemek/wgpu/detail/object.hpp>
#include <psemek/wgpu/chained_struct.hpp>

#include <string>
#include <vector>

namespace psemek::wgpu
{

	struct render_bundle
		: detail::object<render_bundle>
	{
		using detail::object<render_bundle>::object;

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
		};

		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit render_bundle(void * ptr)
			: detail::object<render_bundle>(ptr)
		{}

		friend struct render_bundle_encoder;
	};

}
