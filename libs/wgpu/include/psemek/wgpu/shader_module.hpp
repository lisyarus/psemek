#pragma once

#include <psemek/wgpu/shader_stage.hpp>
#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/pipeline_layout.hpp>
#include <psemek/wgpu/detail/object.hpp>
#include <psemek/util/span.hpp>

#include <cstdint>
#include <string>
#include <functional>
#include <vector>

namespace psemek::wgpu
{

	struct shader_module
		: detail::object<shader_module>
	{
		using detail::object<shader_module>::object;

		enum class compilation_info_request_status : std::uint32_t
		{
			success = 0x00000000,
			error = 0x00000001,
			device_lost = 0x00000002,
			unknown = 0x00000003,
		};

		enum class compilation_message_type : std::uint32_t
		{
			error = 0x00000000,
			warning = 0x00000001,
			info = 0x00000002,
		};

		struct compilation_message
		{
			std::vector<chained_struct> chain = {};
			std::string message = {};
			compilation_message_type type;
			std::uint64_t line_num;
			std::uint64_t line_pos;
			std::uint64_t offset;
			std::uint64_t length;
			std::uint64_t utf16_line_pos;
			std::uint64_t utf16_offset;
			std::uint64_t utf16_length;
		};

		struct compilation_info
		{
			std::vector<chained_struct> chain = {};
			std::vector<compilation_message> messages = {};
		};

		struct compilation_hint
		{
			std::vector<chained_struct> chain = {};
			std::string entry_point;
			pipeline_layout layout;
		};

		struct define
		{
			char const * name;
			char const * value;
		};

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
			std::vector<compilation_hint> hints = {};
		};

		struct spirv_descriptor
		{
			util::span<std::uint32_t const> code;
		};

		struct wgsl_descriptor
		{
			char const * code;
		};

		struct glsl_descriptor
		{
			shader_stage stage;
			char const * code;
			std::vector<define> defines = {};
		};

		using compilation_info_callback = std::function<void(compilation_info_request_status, compilation_info const &)>;

		void get_compilation_info(compilation_info_callback const & callback);
		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit shader_module(void * ptr)
			: detail::object<shader_module>(ptr)
		{}

		friend struct device;
	};

	detail::chained_struct_ptr to_chained_struct(shader_module::spirv_descriptor const & value);
	detail::chained_struct_ptr to_chained_struct(shader_module::wgsl_descriptor const & value);
	detail::chained_struct_ptr to_chained_struct(shader_module::glsl_descriptor && value);

}
