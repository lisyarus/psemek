#pragma once

#include <ostream>

namespace psemek::wgpu
{

	struct version
	{
		int major;
		int minor;
		int revision;
		int build;
	};

	std::ostream & operator << (std::ostream & os, version const & version);

	version get_version();

}
