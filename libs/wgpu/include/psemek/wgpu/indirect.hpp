#pragma once

#include <cstdint>

namespace psemek::wgpu
{

	struct draw_indirect_command
	{
		std::uint32_t vertex_count;
		std::uint32_t instance_count;
		std::uint32_t first_vertex;
		std::uint32_t first_instance;
	};

	struct draw_indexed_indirect_command
	{
		std::uint32_t index_count;
		std::uint32_t instance_count;
		std::uint32_t first_index;
		std::uint32_t base_vertex;
		std::uint32_t first_instance;
	};

}
