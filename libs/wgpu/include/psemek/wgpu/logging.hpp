#pragma once

#include <psemek/log/level.hpp>

namespace psemek::wgpu
{

	void setup_logging(log::level level);

}
