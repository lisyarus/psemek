#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <string>

namespace psemek::wgpu
{

	struct command_buffer
		: detail::object<command_buffer>
	{
		using detail::object<command_buffer>::object;

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
		};

		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit command_buffer(void * ptr)
			: detail::object<command_buffer>(ptr)
		{}

		friend struct command_encoder;
	};

}
