#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/surface.hpp>
#include <psemek/wgpu/device.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <functional>
#include <cstdint>

namespace psemek::wgpu
{

	enum class power_preference : std::uint32_t
	{
		undefined = 0x00000000,
		low       = 0x00000001,
		high      = 0x00000002,
	};

	enum class backend_type : std::uint32_t
	{
		undefined = 0x00000000,
		null      = 0x00000001,
		webgpu    = 0x00000002,
		d3d11     = 0x00000003,
		d3d12     = 0x00000004,
		metal     = 0x00000005,
		vulkan    = 0x00000006,
		opengl    = 0x00000007,
		opengles  = 0x00000008,
	};

	struct adapter
		: detail::object<adapter>
	{
		using detail::object<adapter>::object;

		struct request_options
		{
			std::vector<chained_struct> chain = {};
			surface compatible_surface = {};
			wgpu::power_preference power_preference = power_preference::high;
			wgpu::backend_type backend_type = backend_type::vulkan;
			bool force_fallback_adapter = false;
		};

		enum class request_status : std::uint32_t
		{
			success     = 0x00000000,
			unavailable = 0x00000001,
			error       = 0x00000002,
			unknown     = 0x00000003,
		};

		using request_callback = std::function<void(request_status, adapter, std::string)>;

		enum class type : std::uint32_t
		{
			discrete_gpu = 0x00000000,
			integrated_gpu = 0x00000001,
			cpu = 0x00000002,
			unknown = 0x00000003,
		};

		struct properties
		{
			std::uint32_t vendor_id;
			std::string vendor_name;
			std::string architecture;
			std::uint32_t device_id;
			std::string name;
			std::string driver_description;
			enum type adapter_type;
			wgpu::backend_type backend_type;
		};

		std::vector<feature> enumerate_features();
		limits get_limits();
		properties get_properties();
		bool has_feature(feature feature);
		void request_device(device::descriptor const & descriptor, device::request_callback const & callback);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit adapter(void * ptr)
			: detail::object<adapter>(ptr)
		{}

		friend struct instance;
	};

}
