#pragma once

#include <cstdint>

namespace psemek::wgpu
{

	enum class shader_stage : std::uint32_t
	{
		none = 0x00000000,
		vertex = 0x00000001,
		fragment = 0x00000002,
		compute = 0x00000004,
	};

	inline shader_stage operator | (shader_stage s1, shader_stage s2)
	{
		return static_cast<shader_stage>(static_cast<std::uint32_t>(s1) | static_cast<std::uint32_t>(s2));
	}

}
