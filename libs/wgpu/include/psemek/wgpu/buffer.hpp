#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <cstdint>
#include <string>
#include <functional>

namespace psemek::wgpu
{

	struct buffer
		: detail::object<buffer>
	{
		using detail::object<buffer>::object;

		enum class usage : std::uint32_t
		{
			none = 0x00000000,
			map_read = 0x00000001,
			map_write = 0x00000002,
			copy_src = 0x00000004,
			copy_dst = 0x00000008,
			index = 0x00000010,
			vertex = 0x00000020,
			uniform = 0x00000040,
			storage = 0x00000080,
			indirect = 0x00000100,
			query_resolve = 0x00000200,
		};

		enum class map_mode : std::uint32_t
		{
			none = 0x00000000,
			read = 0x00000001,
			write = 0x00000002,
		};

		enum class map_state : std::uint32_t
		{
			unmapped = 0x00000000,
			pending = 0x00000001,
			mapped = 0x00000002,
		};

		enum class map_async_status : std::uint32_t
		{
			success = 0x00000000,
			validation_error = 0x00000001,
			unknown = 0x00000002,
			device_lost = 0x00000003,
			destroyed_before_callback = 0x00000004,
			unmapped_before_callback = 0x00000005,
			mapping_already_pending = 0x00000006,
			offset_out_of_range = 0x00000007,
			size_out_of_range = 0x00000008,
		};

		struct descriptor {
			std::vector<chained_struct> chain = {};
			std::string label = {};
			enum usage usage;
			uint64_t size;
			bool mapped_at_creation = false;
		};

		using map_callback = std::function<void(map_async_status)>;

		void destroy();
		std::uint64_t get_size();
		usage get_usage();
		map_state get_map_state();
		void const * get_const_mapped_range(std::size_t offset, std::size_t size);
		void * get_mapped_range(std::size_t offset, std::size_t size);
		void map_async(map_mode flags, std::size_t offset, std::size_t size, map_callback const & callback);
		void unmap();
		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit buffer(void * ptr)
			: detail::object<buffer>(ptr)
		{}

		friend struct device;
	};

	inline buffer::usage operator | (buffer::usage u1, buffer::usage u2)
	{
		return static_cast<buffer::usage>(static_cast<std::uint32_t>(u1) | static_cast<std::uint32_t>(u2));
	}

}
