#pragma once

#include <psemek/wgpu/detail/object.hpp>

#include <cstdint>
#include <string>

namespace psemek::wgpu
{

	struct texture_view
		: detail::object<texture_view>
	{
		using detail::object<texture_view>::object;

		enum class dimension : std::uint32_t
		{
			undefined = 0x00000000,
			_1d = 0x00000001,
			_2d = 0x00000002,
			_2d_array = 0x00000003,
			cube = 0x00000004,
			cube_array = 0x00000005,
			_3d = 0x00000006,
		};

		// Defined in wgpu/texture.hpp to break circular dependencies
		struct descriptor;

		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit texture_view(void * ptr)
			: detail::object<texture_view>(ptr)
		{}

		friend struct texture;
	};

}
