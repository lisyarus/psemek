#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/surface.hpp>
#include <psemek/wgpu/adapter.hpp>
#include <psemek/wgpu/detail/object.hpp>

namespace psemek::wgpu
{

	struct instance
		: detail::object<instance>
	{
		using detail::object<instance>::object;

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
		};

		static instance create(descriptor const & desc);

		void process_events();
		surface create_surface(surface::descriptor const & desc);
		void request_adapter(adapter::request_options const & request_options, adapter::request_callback callback);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit instance(void * ptr)
			: detail::object<instance>(ptr)
		{}
	};

}
