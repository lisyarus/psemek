#pragma once

#include <psemek/wgpu/chained_struct.hpp>
#include <psemek/wgpu/detail/object.hpp>

#include <string>
#include <cstdint>

namespace psemek::wgpu
{

	struct query_set
		: detail::object<query_set>
	{
		using detail::object<query_set>::object;

		enum class type : std::uint32_t
		{
			occlusion = 0x00000000,
			timestamp = 0x00000001,
		};

		struct descriptor
		{
			std::vector<chained_struct> chain = {};
			std::string label = {};
			enum type type;
			std::uint32_t count;
		};

		void destroy();
		std::uint32_t get_count();
		type get_type();
		void set_label(std::string const & label);

		static void reference(void * ptr);
		static void release(void * ptr);

	private:
		explicit query_set(void * ptr)
			: detail::object<query_set>(ptr)
		{}

		friend struct device;
	};

}
