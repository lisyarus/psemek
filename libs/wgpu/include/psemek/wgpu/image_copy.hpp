#pragma once

#include <psemek/wgpu/texture.hpp>
#include <psemek/wgpu/buffer.hpp>
#include <psemek/math/point.hpp>
#include <psemek/wgpu/chained_struct.hpp>

#include <vector>

namespace psemek::wgpu
{

	struct image_copy_texture
	{
		std::vector<chained_struct> chain = {};
		struct texture texture;
		std::uint32_t mip_level = 0;
		math::point<std::uint32_t, 3> origin = {0, 0, 0};
		texture::aspect aspect = texture::aspect::all;
	};

	struct image_copy_buffer {
		std::vector<chained_struct> chain = {};
		texture::data_layout layout;
		struct buffer buffer;
	};

}
