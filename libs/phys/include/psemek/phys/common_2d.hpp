#pragma once

#include <psemek/math/point.hpp>

#include <vector>

namespace psemek::phys2d
{

	struct ball
	{
		float radius;
	};

	struct half_space
	{
		// defined by normal * p <= value
		math::vector<float, 2> normal;
		float value;
	};

	struct box
	{
		float width;
		float height;
	};

	struct convex_polygon
	{
		std::vector<math::point<float, 2>> points;
		math::point<float, 2> anchor;
	};

	struct material
	{
		float density;
		float elasticity;
		float friction;
	};

	struct static_state
	{
		math::point<float, 2> position = {0.f, 0.f};
		float rotation = 0.f; // in radians
	};

	struct dynamic_state
	{
		math::vector<float, 2> velocity = {0.f, 0.f};
		float angular_velocity = 0.f;
	};

	struct fixed_constraint
	{
		math::point<float, 2> point, target;
	};

	struct joint_constraint
	{
		math::point<float, 2> point1, point2;
	};

}
