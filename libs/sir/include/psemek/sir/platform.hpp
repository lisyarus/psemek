#pragma once

#include <bit>
#include <cstdint>

namespace psemek::sir
{

	// If the target platform is big endian or mixed endian, the library needs tweaking
	static_assert(std::endian::native == std::endian::little);

	// If the target platform has different sizes or alignments for basic types, the library needs tweaking
	static_assert(sizeof(char) == 1);
	static_assert(alignof(char) == 1);
	static_assert(sizeof(char8_t) == 1);
	static_assert(alignof(char8_t) == 1);
	static_assert(sizeof(char32_t) == 4);
	static_assert(alignof(char32_t) == 4);
	static_assert(alignof(std::uint8_t) == 1);
	static_assert(alignof(std::uint16_t) == 2);
	static_assert(alignof(std::uint32_t) == 4);
	static_assert(alignof(std::uint64_t) <= 8);
	static_assert(alignof(std::int8_t) == 1);
	static_assert(alignof(std::int16_t) == 2);
	static_assert(alignof(std::int32_t) == 4);
	static_assert(alignof(std::int64_t) <= 8);
	static_assert(sizeof(float) == 4);
	static_assert(alignof(float) == 4);
	static_assert(sizeof(double) == 8);
	static_assert(alignof(double) <= 8);

}
