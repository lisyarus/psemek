#pragma once

#include <psemek/sir/trivial.hpp>
#include <psemek/sir/container.hpp>
#include <psemek/util/array.hpp>

namespace psemek::sir
{

	template <typename Stream, typename T, std::size_t N>
	requires (is_ostream_v<Stream> && !is_custom_v<util::array<T, N>>)
	void write(Stream & s, util::array<T, N> const & x)
	{
		write(s, x.dims());
		if constexpr (is_trivial_v<T>)
		{
			write_contiguous(s, x.data(), x.size());
		}
		else
		{
			write_sequence(s, x.begin(), x.end());
		}
	}

	template <typename Stream, typename T, std::size_t N>
	requires (is_istream_v<Stream> && !is_custom_v<util::array<T, N>>)
	void read(Stream & s, util::array<T, N> & x)
	{
		std::array<std::size_t, N> dims;
		read(s, dims);
		x.resize(dims);
		if constexpr (is_trivial_v<T>)
		{
			read_contiguous(s, x.data(), x.size());
		}
		else
		{
			read_sequence(s, x.begin(), x.end());
		}
	}

}
