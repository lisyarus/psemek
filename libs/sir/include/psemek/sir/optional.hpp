#pragma once

#include <psemek/sir/platform.hpp>
#include <psemek/sir/stream.hpp>
#include <psemek/sir/trivial.hpp>

#include <optional>

namespace psemek::sir
{

	template <typename Stream, typename T>
	requires (is_ostream_v<Stream> && !is_custom_v<std::optional<T>>)
	void write(Stream & s, std::optional<T> const & x)
	{
		write(s, static_cast<bool>(x));
		if (x) write(s, *x);
	}

	template <typename Stream, typename T>
	requires (is_istream_v<Stream> && !is_custom_v<std::optional<T>>)
	void read(Stream & s, std::optional<T> & x)
	{
		bool initialized = false;
		read(s, initialized);
		if (initialized)
		{
			x.emplace();
			read(s, *x);
		}
	}

}
