#pragma once

#include <psemek/test/test.hpp>
#include <psemek/sir/container.hpp>

#include <iostream>
#include <functional>

using namespace psemek;

struct test_type
{
	std::uint32_t value;

	friend auto operator <=> (test_type const &, test_type const &) = default;
};

template <typename Stream>
void write(Stream & s, test_type const & x)
{
	write(s, x.value);
}

template <typename Stream>
void read(Stream & s, test_type & x)
{
	read(s, x.value);
}

static_assert(sir::is_custom_v<test_type>);

inline std::ostream & operator << (std::ostream & s, test_type const & x)
{
	return s << '{' << x.value << '}';
}

namespace std
{

	template <>
	struct hash<test_type>
	{
		std::size_t operator()(test_type const & x) const
		{
			return x.value;
		}
	};

}
