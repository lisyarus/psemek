#include <psemek/test/test.hpp>

#include <psemek/sir/struct.hpp>
#include <psemek/io/memory_stream.hpp>

#include "test_type.hpp"

#include <utility>
#include <tuple>

using namespace psemek;

namespace
{

	template <typename T>
	void test_value(T const & x)
	{
		char buffer[1024];

		{
			io::memory_ostream s{buffer, buffer + std::size(buffer)};
			sir::ostream os{s};
			write(os, x);
		}

		T x2;

		{
			io::memory_istream s{buffer, buffer + std::size(buffer)};
			sir::istream is{s};
			read(is, x2);
		}

		expect_equal(x, x2);
	}

}

test_case(sir_struct_pair_trivial)
{
	std::pair<char, std::uint32_t> p{'a', 1000000u};
	test_value(p);
}

test_case(sir_struct_pair_custom)
{
	std::pair<char, test_type> p{'a', {1000000u}};
	test_value(p);
}

test_case(sir_struct_tuple_trivial)
{
	std::tuple<char, std::uint32_t, float> p{'a', 1000000u, 3.1415f};
	test_value(p);
}

test_case(sir_struct_tuple_custom)
{
	std::tuple<char, test_type, float> p{'a', {1000000u}, 3.1415f};
	test_value(p);
}
