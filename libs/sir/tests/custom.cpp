#include <psemek/test/test.hpp>

#include <psemek/sir/container.hpp>
#include <psemek/io/memory_stream.hpp>

#include "test_type.hpp"

using namespace psemek;

test_case(sir_custom)
{
	char buffer[1024];

	test_type x;
	x.value = 42;
	
	{
		io::memory_ostream s{buffer, buffer + std::size(buffer)};
		sir::ostream os{s};
		write(os, x);
	}

	test_type x2;

	{
		io::memory_istream s{buffer, buffer + std::size(buffer)};
		sir::istream is{s};
		read(is, x2);
	}

	expect_equal(x, x2);
}
