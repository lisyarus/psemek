#pragma once

// Prevent inclusion of other OpenGL-related headers

#if defined(__glew_h__) || defined(__GLEW_H__)
#error Attempt to include auto-generated header after including glew.h
#endif
#if defined(__gl_h_) || defined(__GL_H__)
#error Attempt to include auto-generated header after including gl.h
#endif
#if defined(__glext_h_) || defined(__GLEXT_H_)
#error Attempt to include auto-generated header after including glext.h
#endif
#if defined(__gltypes_h_)
#error Attempt to include auto-generated header after gltypes.h
#endif
#if defined(__gl_ATI_h_)
#error Attempt to include auto-generated header after including glATI.h
#endif

#define __glew_h__
#define __GLEW_H__
#define __gl_h_
#define __GL_H__
#define __glext_h_
#define __GLEXT_H_
#define __gltypes_h_
#define __gl_ATI_h_

// Undefine some macros that may interfere with OpenGL ES constants

#undef ZERO
#undef NONE
#undef TRUE
#undef ONE
#undef LOAD
#undef MULT
#undef ADD
#undef LESS
#undef BACK
#undef LEFT
#undef AUX0
#undef AUX1
#undef AUX2
#undef AUX3
#undef EXP
#undef EXP2
#undef CW
#undef CCW
#undef FOG
#undef BYTE
#undef INT
#undef AND
#undef COPY
#undef NOOP
#undef XOR
#undef OR
#undef NOR
#undef NAND
#undef SET
#undef RED
#undef BLUE
#undef RGB
#undef RGBA
#undef LINE
#undef FILL
#undef FLAT
#undef KEEP
#undef INCR
#undef DECR
#undef S
#undef T
#undef R
#undef Q
#undef V2F
#undef V3F
#undef MIN
#undef MAX
#undef RGB4
#undef RGB5
#undef RGB8
#undef BGR
#undef BGRA
#undef RG
#undef R8
#undef R16
#undef RG8
#undef RG16
#undef R16F
#undef R32F
#undef R8I
#undef R8UI
#undef R16I
#undef R32I
#undef RG8I
#undef BOOL
#undef SRGB
#undef TYPE

// OpenGL ES type definitions

#include <KHR/khrplatform.h>
typedef unsigned int GLenum;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef void GLvoid;
typedef khronos_int8_t GLbyte;
typedef khronos_uint8_t GLubyte;
typedef khronos_int16_t GLshort;
typedef khronos_uint16_t GLushort;
typedef int GLint;
typedef unsigned int GLuint;
typedef khronos_int32_t GLclampx;
typedef int GLsizei;
typedef khronos_float_t GLfloat;
typedef khronos_float_t GLclampf;
typedef double GLdouble;
typedef double GLclampd;
typedef void *GLeglClientBufferEXT;
typedef void *GLeglImageOES;
typedef char GLchar;
typedef char GLcharARB;
#ifdef __APPLE__
typedef void *GLhandleARB;
#else
typedef unsigned int GLhandleARB;
#endif
typedef khronos_uint16_t GLhalf;
typedef khronos_uint16_t GLhalfARB;
typedef khronos_int32_t GLfixed;
typedef khronos_intptr_t GLintptr;
typedef khronos_intptr_t GLintptrARB;
typedef khronos_ssize_t GLsizeiptr;
typedef khronos_ssize_t GLsizeiptrARB;
typedef khronos_int64_t GLint64;
typedef khronos_int64_t GLint64EXT;
typedef khronos_uint64_t GLuint64;
typedef khronos_uint64_t GLuint64EXT;
typedef struct __GLsync *GLsync;
struct _cl_context;
struct _cl_event;
typedef void (GLDEBUGPROC)(GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message,const void *userParam);
typedef void (GLDEBUGPROCARB)(GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message,const void *userParam);
typedef void (GLDEBUGPROCKHR)(GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message,const void *userParam);
typedef void (GLDEBUGPROCAMD)(GLuint id,GLenum category,GLenum severity,GLsizei length,const GLchar *message,void *userParam);
typedef unsigned short GLhalfNV;
typedef GLintptr GLvdpauSurfaceNV;
typedef void (GLVULKANPROCNV)(void);

namespace gl
{

	namespace internal
	{

		// OpenGL ES 2.0

		extern void (*glActiveTexture)(GLenum texture);
		extern void (*glAttachShader)(GLuint program, GLuint shader);
		extern void (*glBindAttribLocation)(GLuint program, GLuint index, const GLchar *name);
		extern void (*glBindBuffer)(GLenum target, GLuint buffer);
		extern void (*glBindFramebuffer)(GLenum target, GLuint framebuffer);
		extern void (*glBindRenderbuffer)(GLenum target, GLuint renderbuffer);
		extern void (*glBindTexture)(GLenum target, GLuint texture);
		extern void (*glBlendColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
		extern void (*glBlendEquation)(GLenum mode);
		extern void (*glBlendEquationSeparate)(GLenum modeRGB, GLenum modeAlpha);
		extern void (*glBlendFunc)(GLenum sfactor, GLenum dfactor);
		extern void (*glBlendFuncSeparate)(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha);
		extern void (*glBufferData)(GLenum target, GLsizeiptr size, const void *data, GLenum usage);
		extern void (*glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const void *data);
		extern GLenum (*glCheckFramebufferStatus)(GLenum target);
		extern void (*glClear)(GLbitfield mask);
		extern void (*glClearColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
		extern void (*glClearDepthf)(GLfloat d);
		extern void (*glClearStencil)(GLint s);
		extern void (*glColorMask)(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);
		extern void (*glCompileShader)(GLuint shader);
		extern void (*glCompressedTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data);
		extern void (*glCompressedTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data);
		extern void (*glCopyTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border);
		extern void (*glCopyTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);
		extern GLuint (*glCreateProgram)();
		extern GLuint (*glCreateShader)(GLenum type);
		extern void (*glCullFace)(GLenum mode);
		extern void (*glDeleteBuffers)(GLsizei n, const GLuint *buffers);
		extern void (*glDeleteFramebuffers)(GLsizei n, const GLuint *framebuffers);
		extern void (*glDeleteProgram)(GLuint program);
		extern void (*glDeleteRenderbuffers)(GLsizei n, const GLuint *renderbuffers);
		extern void (*glDeleteShader)(GLuint shader);
		extern void (*glDeleteTextures)(GLsizei n, const GLuint *textures);
		extern void (*glDepthFunc)(GLenum func);
		extern void (*glDepthMask)(GLboolean flag);
		extern void (*glDepthRangef)(GLfloat n, GLfloat f);
		extern void (*glDetachShader)(GLuint program, GLuint shader);
		extern void (*glDisable)(GLenum cap);
		extern void (*glDisableVertexAttribArray)(GLuint index);
		extern void (*glDrawArrays)(GLenum mode, GLint first, GLsizei count);
		extern void (*glDrawElements)(GLenum mode, GLsizei count, GLenum type, const void *indices);
		extern void (*glEnable)(GLenum cap);
		extern void (*glEnableVertexAttribArray)(GLuint index);
		extern void (*glFinish)();
		extern void (*glFlush)();
		extern void (*glFramebufferRenderbuffer)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
		extern void (*glFramebufferTexture2D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
		extern void (*glFrontFace)(GLenum mode);
		extern void (*glGenBuffers)(GLsizei n, GLuint *buffers);
		extern void (*glGenerateMipmap)(GLenum target);
		extern void (*glGenFramebuffers)(GLsizei n, GLuint *framebuffers);
		extern void (*glGenRenderbuffers)(GLsizei n, GLuint *renderbuffers);
		extern void (*glGenTextures)(GLsizei n, GLuint *textures);
		extern void (*glGetActiveAttrib)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
		extern void (*glGetActiveUniform)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
		extern void (*glGetAttachedShaders)(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders);
		extern GLint (*glGetAttribLocation)(GLuint program, const GLchar *name);
		extern void (*glGetBooleanv)(GLenum pname, GLboolean *data);
		extern void (*glGetBufferParameteriv)(GLenum target, GLenum pname, GLint *params);
		extern GLenum (*glGetError)();
		extern void (*glGetFloatv)(GLenum pname, GLfloat *data);
		extern void (*glGetFramebufferAttachmentParameteriv)(GLenum target, GLenum attachment, GLenum pname, GLint *params);
		extern void (*glGetIntegerv)(GLenum pname, GLint *data);
		extern void (*glGetProgramiv)(GLuint program, GLenum pname, GLint *params);
		extern void (*glGetProgramInfoLog)(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
		extern void (*glGetRenderbufferParameteriv)(GLenum target, GLenum pname, GLint *params);
		extern void (*glGetShaderiv)(GLuint shader, GLenum pname, GLint *params);
		extern void (*glGetShaderInfoLog)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
		extern void (*glGetShaderPrecisionFormat)(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision);
		extern void (*glGetShaderSource)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source);
		extern const GLubyte *(*glGetString)(GLenum name);
		extern void (*glGetTexParameterfv)(GLenum target, GLenum pname, GLfloat *params);
		extern void (*glGetTexParameteriv)(GLenum target, GLenum pname, GLint *params);
		extern void (*glGetUniformfv)(GLuint program, GLint location, GLfloat *params);
		extern void (*glGetUniformiv)(GLuint program, GLint location, GLint *params);
		extern GLint (*glGetUniformLocation)(GLuint program, const GLchar *name);
		extern void (*glGetVertexAttribfv)(GLuint index, GLenum pname, GLfloat *params);
		extern void (*glGetVertexAttribiv)(GLuint index, GLenum pname, GLint *params);
		extern void (*glGetVertexAttribPointerv)(GLuint index, GLenum pname, void **pointer);
		extern void (*glHint)(GLenum target, GLenum mode);
		extern GLboolean (*glIsBuffer)(GLuint buffer);
		extern GLboolean (*glIsEnabled)(GLenum cap);
		extern GLboolean (*glIsFramebuffer)(GLuint framebuffer);
		extern GLboolean (*glIsProgram)(GLuint program);
		extern GLboolean (*glIsRenderbuffer)(GLuint renderbuffer);
		extern GLboolean (*glIsShader)(GLuint shader);
		extern GLboolean (*glIsTexture)(GLuint texture);
		extern void (*glLineWidth)(GLfloat width);
		extern void (*glLinkProgram)(GLuint program);
		extern void (*glPixelStorei)(GLenum pname, GLint param);
		extern void (*glPolygonOffset)(GLfloat factor, GLfloat units);
		extern void (*glReadPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels);
		extern void (*glReleaseShaderCompiler)();
		extern void (*glRenderbufferStorage)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
		extern void (*glSampleCoverage)(GLfloat value, GLboolean invert);
		extern void (*glScissor)(GLint x, GLint y, GLsizei width, GLsizei height);
		extern void (*glShaderBinary)(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length);
		extern void (*glShaderSource)(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
		extern void (*glStencilFunc)(GLenum func, GLint ref, GLuint mask);
		extern void (*glStencilFuncSeparate)(GLenum face, GLenum func, GLint ref, GLuint mask);
		extern void (*glStencilMask)(GLuint mask);
		extern void (*glStencilMaskSeparate)(GLenum face, GLuint mask);
		extern void (*glStencilOp)(GLenum fail, GLenum zfail, GLenum zpass);
		extern void (*glStencilOpSeparate)(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);
		extern void (*glTexImage2D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels);
		extern void (*glTexParameterf)(GLenum target, GLenum pname, GLfloat param);
		extern void (*glTexParameterfv)(GLenum target, GLenum pname, const GLfloat *params);
		extern void (*glTexParameteri)(GLenum target, GLenum pname, GLint param);
		extern void (*glTexParameteriv)(GLenum target, GLenum pname, const GLint *params);
		extern void (*glTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);
		extern void (*glUniform1f)(GLint location, GLfloat v0);
		extern void (*glUniform1fv)(GLint location, GLsizei count, const GLfloat *value);
		extern void (*glUniform1i)(GLint location, GLint v0);
		extern void (*glUniform1iv)(GLint location, GLsizei count, const GLint *value);
		extern void (*glUniform2f)(GLint location, GLfloat v0, GLfloat v1);
		extern void (*glUniform2fv)(GLint location, GLsizei count, const GLfloat *value);
		extern void (*glUniform2i)(GLint location, GLint v0, GLint v1);
		extern void (*glUniform2iv)(GLint location, GLsizei count, const GLint *value);
		extern void (*glUniform3f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
		extern void (*glUniform3fv)(GLint location, GLsizei count, const GLfloat *value);
		extern void (*glUniform3i)(GLint location, GLint v0, GLint v1, GLint v2);
		extern void (*glUniform3iv)(GLint location, GLsizei count, const GLint *value);
		extern void (*glUniform4f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
		extern void (*glUniform4fv)(GLint location, GLsizei count, const GLfloat *value);
		extern void (*glUniform4i)(GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
		extern void (*glUniform4iv)(GLint location, GLsizei count, const GLint *value);
		extern void (*glUniformMatrix2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUseProgram)(GLuint program);
		extern void (*glValidateProgram)(GLuint program);
		extern void (*glVertexAttrib1f)(GLuint index, GLfloat x);
		extern void (*glVertexAttrib1fv)(GLuint index, const GLfloat *v);
		extern void (*glVertexAttrib2f)(GLuint index, GLfloat x, GLfloat y);
		extern void (*glVertexAttrib2fv)(GLuint index, const GLfloat *v);
		extern void (*glVertexAttrib3f)(GLuint index, GLfloat x, GLfloat y, GLfloat z);
		extern void (*glVertexAttrib3fv)(GLuint index, const GLfloat *v);
		extern void (*glVertexAttrib4f)(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
		extern void (*glVertexAttrib4fv)(GLuint index, const GLfloat *v);
		extern void (*glVertexAttribPointer)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer);
		extern void (*glViewport)(GLint x, GLint y, GLsizei width, GLsizei height);

		// OpenGL ES 3.0

		extern void (*glReadBuffer)(GLenum src);
		extern void (*glDrawRangeElements)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices);
		extern void (*glTexImage3D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
		extern void (*glTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);
		extern void (*glCopyTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);
		extern void (*glCompressedTexImage3D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data);
		extern void (*glCompressedTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data);
		extern void (*glGenQueries)(GLsizei n, GLuint *ids);
		extern void (*glDeleteQueries)(GLsizei n, const GLuint *ids);
		extern GLboolean (*glIsQuery)(GLuint id);
		extern void (*glBeginQuery)(GLenum target, GLuint id);
		extern void (*glEndQuery)(GLenum target);
		extern void (*glGetQueryiv)(GLenum target, GLenum pname, GLint *params);
		extern void (*glGetQueryObjectuiv)(GLuint id, GLenum pname, GLuint *params);
		extern GLboolean (*glUnmapBuffer)(GLenum target);
		extern void (*glGetBufferPointerv)(GLenum target, GLenum pname, void **params);
		extern void (*glDrawBuffers)(GLsizei n, const GLenum *bufs);
		extern void (*glUniformMatrix2x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix3x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix2x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix4x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix3x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glUniformMatrix4x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glBlitFramebuffer)(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
		extern void (*glRenderbufferStorageMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
		extern void (*glFramebufferTextureLayer)(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer);
		extern void *(*glMapBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
		extern void (*glFlushMappedBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length);
		extern void (*glBindVertexArray)(GLuint array);
		extern void (*glDeleteVertexArrays)(GLsizei n, const GLuint *arrays);
		extern void (*glGenVertexArrays)(GLsizei n, GLuint *arrays);
		extern GLboolean (*glIsVertexArray)(GLuint array);
		extern void (*glGetIntegeri_v)(GLenum target, GLuint index, GLint *data);
		extern void (*glBeginTransformFeedback)(GLenum primitiveMode);
		extern void (*glEndTransformFeedback)();
		extern void (*glBindBufferRange)(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size);
		extern void (*glBindBufferBase)(GLenum target, GLuint index, GLuint buffer);
		extern void (*glTransformFeedbackVaryings)(GLuint program, GLsizei count, const GLchar *const*varyings, GLenum bufferMode);
		extern void (*glGetTransformFeedbackVarying)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, GLchar *name);
		extern void (*glVertexAttribIPointer)(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer);
		extern void (*glGetVertexAttribIiv)(GLuint index, GLenum pname, GLint *params);
		extern void (*glGetVertexAttribIuiv)(GLuint index, GLenum pname, GLuint *params);
		extern void (*glVertexAttribI4i)(GLuint index, GLint x, GLint y, GLint z, GLint w);
		extern void (*glVertexAttribI4ui)(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w);
		extern void (*glVertexAttribI4iv)(GLuint index, const GLint *v);
		extern void (*glVertexAttribI4uiv)(GLuint index, const GLuint *v);
		extern void (*glGetUniformuiv)(GLuint program, GLint location, GLuint *params);
		extern GLint (*glGetFragDataLocation)(GLuint program, const GLchar *name);
		extern void (*glUniform1ui)(GLint location, GLuint v0);
		extern void (*glUniform2ui)(GLint location, GLuint v0, GLuint v1);
		extern void (*glUniform3ui)(GLint location, GLuint v0, GLuint v1, GLuint v2);
		extern void (*glUniform4ui)(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
		extern void (*glUniform1uiv)(GLint location, GLsizei count, const GLuint *value);
		extern void (*glUniform2uiv)(GLint location, GLsizei count, const GLuint *value);
		extern void (*glUniform3uiv)(GLint location, GLsizei count, const GLuint *value);
		extern void (*glUniform4uiv)(GLint location, GLsizei count, const GLuint *value);
		extern void (*glClearBufferiv)(GLenum buffer, GLint drawbuffer, const GLint *value);
		extern void (*glClearBufferuiv)(GLenum buffer, GLint drawbuffer, const GLuint *value);
		extern void (*glClearBufferfv)(GLenum buffer, GLint drawbuffer, const GLfloat *value);
		extern void (*glClearBufferfi)(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil);
		extern const GLubyte *(*glGetStringi)(GLenum name, GLuint index);
		extern void (*glCopyBufferSubData)(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);
		extern void (*glGetUniformIndices)(GLuint program, GLsizei uniformCount, const GLchar *const*uniformNames, GLuint *uniformIndices);
		extern void (*glGetActiveUniformsiv)(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params);
		extern GLuint (*glGetUniformBlockIndex)(GLuint program, const GLchar *uniformBlockName);
		extern void (*glGetActiveUniformBlockiv)(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params);
		extern void (*glGetActiveUniformBlockName)(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName);
		extern void (*glUniformBlockBinding)(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding);
		extern void (*glDrawArraysInstanced)(GLenum mode, GLint first, GLsizei count, GLsizei instancecount);
		extern void (*glDrawElementsInstanced)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount);
		extern GLsync (*glFenceSync)(GLenum condition, GLbitfield flags);
		extern GLboolean (*glIsSync)(GLsync sync);
		extern void (*glDeleteSync)(GLsync sync);
		extern GLenum (*glClientWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout);
		extern void (*glWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout);
		extern void (*glGetInteger64v)(GLenum pname, GLint64 *data);
		extern void (*glGetSynciv)(GLsync sync, GLenum pname, GLsizei count, GLsizei *length, GLint *values);
		extern void (*glGetInteger64i_v)(GLenum target, GLuint index, GLint64 *data);
		extern void (*glGetBufferParameteri64v)(GLenum target, GLenum pname, GLint64 *params);
		extern void (*glGenSamplers)(GLsizei count, GLuint *samplers);
		extern void (*glDeleteSamplers)(GLsizei count, const GLuint *samplers);
		extern GLboolean (*glIsSampler)(GLuint sampler);
		extern void (*glBindSampler)(GLuint unit, GLuint sampler);
		extern void (*glSamplerParameteri)(GLuint sampler, GLenum pname, GLint param);
		extern void (*glSamplerParameteriv)(GLuint sampler, GLenum pname, const GLint *param);
		extern void (*glSamplerParameterf)(GLuint sampler, GLenum pname, GLfloat param);
		extern void (*glSamplerParameterfv)(GLuint sampler, GLenum pname, const GLfloat *param);
		extern void (*glGetSamplerParameteriv)(GLuint sampler, GLenum pname, GLint *params);
		extern void (*glGetSamplerParameterfv)(GLuint sampler, GLenum pname, GLfloat *params);
		extern void (*glVertexAttribDivisor)(GLuint index, GLuint divisor);
		extern void (*glBindTransformFeedback)(GLenum target, GLuint id);
		extern void (*glDeleteTransformFeedbacks)(GLsizei n, const GLuint *ids);
		extern void (*glGenTransformFeedbacks)(GLsizei n, GLuint *ids);
		extern GLboolean (*glIsTransformFeedback)(GLuint id);
		extern void (*glPauseTransformFeedback)();
		extern void (*glResumeTransformFeedback)();
		extern void (*glGetProgramBinary)(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary);
		extern void (*glProgramBinary)(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length);
		extern void (*glProgramParameteri)(GLuint program, GLenum pname, GLint value);
		extern void (*glInvalidateFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments);
		extern void (*glInvalidateSubFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height);
		extern void (*glTexStorage2D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
		extern void (*glTexStorage3D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
		extern void (*glGetInternalformativ)(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint *params);

		// OpenGL ES 3.1

		extern void (*glDispatchCompute)(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z);
		extern void (*glDispatchComputeIndirect)(GLintptr indirect);
		extern void (*glDrawArraysIndirect)(GLenum mode, const void *indirect);
		extern void (*glDrawElementsIndirect)(GLenum mode, GLenum type, const void *indirect);
		extern void (*glFramebufferParameteri)(GLenum target, GLenum pname, GLint param);
		extern void (*glGetFramebufferParameteriv)(GLenum target, GLenum pname, GLint *params);
		extern void (*glGetProgramInterfaceiv)(GLuint program, GLenum programInterface, GLenum pname, GLint *params);
		extern GLuint (*glGetProgramResourceIndex)(GLuint program, GLenum programInterface, const GLchar *name);
		extern void (*glGetProgramResourceName)(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name);
		extern void (*glGetProgramResourceiv)(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei count, GLsizei *length, GLint *params);
		extern GLint (*glGetProgramResourceLocation)(GLuint program, GLenum programInterface, const GLchar *name);
		extern void (*glUseProgramStages)(GLuint pipeline, GLbitfield stages, GLuint program);
		extern void (*glActiveShaderProgram)(GLuint pipeline, GLuint program);
		extern GLuint (*glCreateShaderProgramv)(GLenum type, GLsizei count, const GLchar *const*strings);
		extern void (*glBindProgramPipeline)(GLuint pipeline);
		extern void (*glDeleteProgramPipelines)(GLsizei n, const GLuint *pipelines);
		extern void (*glGenProgramPipelines)(GLsizei n, GLuint *pipelines);
		extern GLboolean (*glIsProgramPipeline)(GLuint pipeline);
		extern void (*glGetProgramPipelineiv)(GLuint pipeline, GLenum pname, GLint *params);
		extern void (*glProgramUniform1i)(GLuint program, GLint location, GLint v0);
		extern void (*glProgramUniform2i)(GLuint program, GLint location, GLint v0, GLint v1);
		extern void (*glProgramUniform3i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2);
		extern void (*glProgramUniform4i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
		extern void (*glProgramUniform1ui)(GLuint program, GLint location, GLuint v0);
		extern void (*glProgramUniform2ui)(GLuint program, GLint location, GLuint v0, GLuint v1);
		extern void (*glProgramUniform3ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2);
		extern void (*glProgramUniform4ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
		extern void (*glProgramUniform1f)(GLuint program, GLint location, GLfloat v0);
		extern void (*glProgramUniform2f)(GLuint program, GLint location, GLfloat v0, GLfloat v1);
		extern void (*glProgramUniform3f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
		extern void (*glProgramUniform4f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
		extern void (*glProgramUniform1iv)(GLuint program, GLint location, GLsizei count, const GLint *value);
		extern void (*glProgramUniform2iv)(GLuint program, GLint location, GLsizei count, const GLint *value);
		extern void (*glProgramUniform3iv)(GLuint program, GLint location, GLsizei count, const GLint *value);
		extern void (*glProgramUniform4iv)(GLuint program, GLint location, GLsizei count, const GLint *value);
		extern void (*glProgramUniform1uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value);
		extern void (*glProgramUniform2uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value);
		extern void (*glProgramUniform3uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value);
		extern void (*glProgramUniform4uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value);
		extern void (*glProgramUniform1fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value);
		extern void (*glProgramUniform2fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value);
		extern void (*glProgramUniform3fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value);
		extern void (*glProgramUniform4fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value);
		extern void (*glProgramUniformMatrix2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix2x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix3x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix2x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix4x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix3x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glProgramUniformMatrix4x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
		extern void (*glValidateProgramPipeline)(GLuint pipeline);
		extern void (*glGetProgramPipelineInfoLog)(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
		extern void (*glBindImageTexture)(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format);
		extern void (*glGetBooleani_v)(GLenum target, GLuint index, GLboolean *data);
		extern void (*glMemoryBarrier)(GLbitfield barriers);
		extern void (*glMemoryBarrierByRegion)(GLbitfield barriers);
		extern void (*glTexStorage2DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
		extern void (*glGetMultisamplefv)(GLenum pname, GLuint index, GLfloat *val);
		extern void (*glSampleMaski)(GLuint maskNumber, GLbitfield mask);
		extern void (*glGetTexLevelParameteriv)(GLenum target, GLint level, GLenum pname, GLint *params);
		extern void (*glGetTexLevelParameterfv)(GLenum target, GLint level, GLenum pname, GLfloat *params);
		extern void (*glBindVertexBuffer)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
		extern void (*glVertexAttribFormat)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
		extern void (*glVertexAttribIFormat)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset);
		extern void (*glVertexAttribBinding)(GLuint attribindex, GLuint bindingindex);
		extern void (*glVertexBindingDivisor)(GLuint bindingindex, GLuint divisor);

		// OpenGL ES 3.2

		extern void (*glBlendBarrier)();
		extern void (*glCopyImageSubData)(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth);
		extern void (*glDebugMessageControl)(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled);
		extern void (*glDebugMessageInsert)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf);
		extern void (*glDebugMessageCallback)(GLDEBUGPROC callback, const void *userParam);
		extern GLuint (*glGetDebugMessageLog)(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog);
		extern void (*glPushDebugGroup)(GLenum source, GLuint id, GLsizei length, const GLchar *message);
		extern void (*glPopDebugGroup)();
		extern void (*glObjectLabel)(GLenum identifier, GLuint name, GLsizei length, const GLchar *label);
		extern void (*glGetObjectLabel)(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label);
		extern void (*glObjectPtrLabel)(const void *ptr, GLsizei length, const GLchar *label);
		extern void (*glGetObjectPtrLabel)(const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label);
		extern void (*glGetPointerv)(GLenum pname, void **params);
		extern void (*glEnablei)(GLenum target, GLuint index);
		extern void (*glDisablei)(GLenum target, GLuint index);
		extern void (*glBlendEquationi)(GLuint buf, GLenum mode);
		extern void (*glBlendEquationSeparatei)(GLuint buf, GLenum modeRGB, GLenum modeAlpha);
		extern void (*glBlendFunci)(GLuint buf, GLenum src, GLenum dst);
		extern void (*glBlendFuncSeparatei)(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
		extern void (*glColorMaski)(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a);
		extern GLboolean (*glIsEnabledi)(GLenum target, GLuint index);
		extern void (*glDrawElementsBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex);
		extern void (*glDrawRangeElementsBaseVertex)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex);
		extern void (*glDrawElementsInstancedBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex);
		extern void (*glFramebufferTexture)(GLenum target, GLenum attachment, GLuint texture, GLint level);
		extern void (*glPrimitiveBoundingBox)(GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW);
		extern GLenum (*glGetGraphicsResetStatus)();
		extern void (*glReadnPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data);
		extern void (*glGetnUniformfv)(GLuint program, GLint location, GLsizei bufSize, GLfloat *params);
		extern void (*glGetnUniformiv)(GLuint program, GLint location, GLsizei bufSize, GLint *params);
		extern void (*glGetnUniformuiv)(GLuint program, GLint location, GLsizei bufSize, GLuint *params);
		extern void (*glMinSampleShading)(GLfloat value);
		extern void (*glPatchParameteri)(GLenum pname, GLint value);
		extern void (*glTexParameterIiv)(GLenum target, GLenum pname, const GLint *params);
		extern void (*glTexParameterIuiv)(GLenum target, GLenum pname, const GLuint *params);
		extern void (*glGetTexParameterIiv)(GLenum target, GLenum pname, GLint *params);
		extern void (*glGetTexParameterIuiv)(GLenum target, GLenum pname, GLuint *params);
		extern void (*glSamplerParameterIiv)(GLuint sampler, GLenum pname, const GLint *param);
		extern void (*glSamplerParameterIuiv)(GLuint sampler, GLenum pname, const GLuint *param);
		extern void (*glGetSamplerParameterIiv)(GLuint sampler, GLenum pname, GLint *params);
		extern void (*glGetSamplerParameterIuiv)(GLuint sampler, GLenum pname, GLuint *params);
		extern void (*glTexBuffer)(GLenum target, GLenum internalformat, GLuint buffer);
		extern void (*glTexBufferRange)(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size);
		extern void (*glTexStorage3DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations);

		// GL_ARB_compute_shader


		// GL_ARB_shader_image_load_store


		// GL_ARB_texture_filter_anisotropic


	} // namespace internal

	// OpenGL ES 2.0

	constexpr GLenum DEPTH_BUFFER_BIT = 0x00000100;
	constexpr GLenum STENCIL_BUFFER_BIT = 0x00000400;
	constexpr GLenum COLOR_BUFFER_BIT = 0x00004000;
	constexpr GLenum FALSE = 0;
	constexpr GLenum TRUE = 1;
	constexpr GLenum POINTS = 0x0000;
	constexpr GLenum LINES = 0x0001;
	constexpr GLenum LINE_LOOP = 0x0002;
	constexpr GLenum LINE_STRIP = 0x0003;
	constexpr GLenum TRIANGLES = 0x0004;
	constexpr GLenum TRIANGLE_STRIP = 0x0005;
	constexpr GLenum TRIANGLE_FAN = 0x0006;
	constexpr GLenum ZERO = 0;
	constexpr GLenum ONE = 1;
	constexpr GLenum SRC_COLOR = 0x0300;
	constexpr GLenum ONE_MINUS_SRC_COLOR = 0x0301;
	constexpr GLenum SRC_ALPHA = 0x0302;
	constexpr GLenum ONE_MINUS_SRC_ALPHA = 0x0303;
	constexpr GLenum DST_ALPHA = 0x0304;
	constexpr GLenum ONE_MINUS_DST_ALPHA = 0x0305;
	constexpr GLenum DST_COLOR = 0x0306;
	constexpr GLenum ONE_MINUS_DST_COLOR = 0x0307;
	constexpr GLenum SRC_ALPHA_SATURATE = 0x0308;
	constexpr GLenum FUNC_ADD = 0x8006;
	constexpr GLenum BLEND_EQUATION = 0x8009;
	constexpr GLenum BLEND_EQUATION_RGB = 0x8009;
	constexpr GLenum BLEND_EQUATION_ALPHA = 0x883D;
	constexpr GLenum FUNC_SUBTRACT = 0x800A;
	constexpr GLenum FUNC_REVERSE_SUBTRACT = 0x800B;
	constexpr GLenum BLEND_DST_RGB = 0x80C8;
	constexpr GLenum BLEND_SRC_RGB = 0x80C9;
	constexpr GLenum BLEND_DST_ALPHA = 0x80CA;
	constexpr GLenum BLEND_SRC_ALPHA = 0x80CB;
	constexpr GLenum CONSTANT_COLOR = 0x8001;
	constexpr GLenum ONE_MINUS_CONSTANT_COLOR = 0x8002;
	constexpr GLenum CONSTANT_ALPHA = 0x8003;
	constexpr GLenum ONE_MINUS_CONSTANT_ALPHA = 0x8004;
	constexpr GLenum BLEND_COLOR = 0x8005;
	constexpr GLenum ARRAY_BUFFER = 0x8892;
	constexpr GLenum ELEMENT_ARRAY_BUFFER = 0x8893;
	constexpr GLenum ARRAY_BUFFER_BINDING = 0x8894;
	constexpr GLenum ELEMENT_ARRAY_BUFFER_BINDING = 0x8895;
	constexpr GLenum STREAM_DRAW = 0x88E0;
	constexpr GLenum STATIC_DRAW = 0x88E4;
	constexpr GLenum DYNAMIC_DRAW = 0x88E8;
	constexpr GLenum BUFFER_SIZE = 0x8764;
	constexpr GLenum BUFFER_USAGE = 0x8765;
	constexpr GLenum CURRENT_VERTEX_ATTRIB = 0x8626;
	constexpr GLenum FRONT = 0x0404;
	constexpr GLenum BACK = 0x0405;
	constexpr GLenum FRONT_AND_BACK = 0x0408;
	constexpr GLenum TEXTURE_2D = 0x0DE1;
	constexpr GLenum CULL_FACE = 0x0B44;
	constexpr GLenum BLEND = 0x0BE2;
	constexpr GLenum DITHER = 0x0BD0;
	constexpr GLenum STENCIL_TEST = 0x0B90;
	constexpr GLenum DEPTH_TEST = 0x0B71;
	constexpr GLenum SCISSOR_TEST = 0x0C11;
	constexpr GLenum POLYGON_OFFSET_FILL = 0x8037;
	constexpr GLenum SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
	constexpr GLenum SAMPLE_COVERAGE = 0x80A0;
	constexpr GLenum NO_ERROR = 0;
	constexpr GLenum INVALID_ENUM = 0x0500;
	constexpr GLenum INVALID_VALUE = 0x0501;
	constexpr GLenum INVALID_OPERATION = 0x0502;
	constexpr GLenum OUT_OF_MEMORY = 0x0505;
	constexpr GLenum CW = 0x0900;
	constexpr GLenum CCW = 0x0901;
	constexpr GLenum LINE_WIDTH = 0x0B21;
	constexpr GLenum ALIASED_POINT_SIZE_RANGE = 0x846D;
	constexpr GLenum ALIASED_LINE_WIDTH_RANGE = 0x846E;
	constexpr GLenum CULL_FACE_MODE = 0x0B45;
	constexpr GLenum FRONT_FACE = 0x0B46;
	constexpr GLenum DEPTH_RANGE = 0x0B70;
	constexpr GLenum DEPTH_WRITEMASK = 0x0B72;
	constexpr GLenum DEPTH_CLEAR_VALUE = 0x0B73;
	constexpr GLenum DEPTH_FUNC = 0x0B74;
	constexpr GLenum STENCIL_CLEAR_VALUE = 0x0B91;
	constexpr GLenum STENCIL_FUNC = 0x0B92;
	constexpr GLenum STENCIL_FAIL = 0x0B94;
	constexpr GLenum STENCIL_PASS_DEPTH_FAIL = 0x0B95;
	constexpr GLenum STENCIL_PASS_DEPTH_PASS = 0x0B96;
	constexpr GLenum STENCIL_REF = 0x0B97;
	constexpr GLenum STENCIL_VALUE_MASK = 0x0B93;
	constexpr GLenum STENCIL_WRITEMASK = 0x0B98;
	constexpr GLenum STENCIL_BACK_FUNC = 0x8800;
	constexpr GLenum STENCIL_BACK_FAIL = 0x8801;
	constexpr GLenum STENCIL_BACK_PASS_DEPTH_FAIL = 0x8802;
	constexpr GLenum STENCIL_BACK_PASS_DEPTH_PASS = 0x8803;
	constexpr GLenum STENCIL_BACK_REF = 0x8CA3;
	constexpr GLenum STENCIL_BACK_VALUE_MASK = 0x8CA4;
	constexpr GLenum STENCIL_BACK_WRITEMASK = 0x8CA5;
	constexpr GLenum VIEWPORT = 0x0BA2;
	constexpr GLenum SCISSOR_BOX = 0x0C10;
	constexpr GLenum COLOR_CLEAR_VALUE = 0x0C22;
	constexpr GLenum COLOR_WRITEMASK = 0x0C23;
	constexpr GLenum UNPACK_ALIGNMENT = 0x0CF5;
	constexpr GLenum PACK_ALIGNMENT = 0x0D05;
	constexpr GLenum MAX_TEXTURE_SIZE = 0x0D33;
	constexpr GLenum MAX_VIEWPORT_DIMS = 0x0D3A;
	constexpr GLenum SUBPIXEL_BITS = 0x0D50;
	constexpr GLenum RED_BITS = 0x0D52;
	constexpr GLenum GREEN_BITS = 0x0D53;
	constexpr GLenum BLUE_BITS = 0x0D54;
	constexpr GLenum ALPHA_BITS = 0x0D55;
	constexpr GLenum DEPTH_BITS = 0x0D56;
	constexpr GLenum STENCIL_BITS = 0x0D57;
	constexpr GLenum POLYGON_OFFSET_UNITS = 0x2A00;
	constexpr GLenum POLYGON_OFFSET_FACTOR = 0x8038;
	constexpr GLenum TEXTURE_BINDING_2D = 0x8069;
	constexpr GLenum SAMPLE_BUFFERS = 0x80A8;
	constexpr GLenum SAMPLES = 0x80A9;
	constexpr GLenum SAMPLE_COVERAGE_VALUE = 0x80AA;
	constexpr GLenum SAMPLE_COVERAGE_INVERT = 0x80AB;
	constexpr GLenum NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
	constexpr GLenum COMPRESSED_TEXTURE_FORMATS = 0x86A3;
	constexpr GLenum DONT_CARE = 0x1100;
	constexpr GLenum FASTEST = 0x1101;
	constexpr GLenum NICEST = 0x1102;
	constexpr GLenum GENERATE_MIPMAP_HINT = 0x8192;
	constexpr GLenum BYTE = 0x1400;
	constexpr GLenum UNSIGNED_BYTE = 0x1401;
	constexpr GLenum SHORT = 0x1402;
	constexpr GLenum UNSIGNED_SHORT = 0x1403;
	constexpr GLenum INT = 0x1404;
	constexpr GLenum UNSIGNED_INT = 0x1405;
	constexpr GLenum FLOAT = 0x1406;
	constexpr GLenum FIXED = 0x140C;
	constexpr GLenum DEPTH_COMPONENT = 0x1902;
	constexpr GLenum ALPHA = 0x1906;
	constexpr GLenum RGB = 0x1907;
	constexpr GLenum RGBA = 0x1908;
	constexpr GLenum LUMINANCE = 0x1909;
	constexpr GLenum LUMINANCE_ALPHA = 0x190A;
	constexpr GLenum UNSIGNED_SHORT_4_4_4_4 = 0x8033;
	constexpr GLenum UNSIGNED_SHORT_5_5_5_1 = 0x8034;
	constexpr GLenum UNSIGNED_SHORT_5_6_5 = 0x8363;
	constexpr GLenum FRAGMENT_SHADER = 0x8B30;
	constexpr GLenum VERTEX_SHADER = 0x8B31;
	constexpr GLenum MAX_VERTEX_ATTRIBS = 0x8869;
	constexpr GLenum MAX_VERTEX_UNIFORM_VECTORS = 0x8DFB;
	constexpr GLenum MAX_VARYING_VECTORS = 0x8DFC;
	constexpr GLenum MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0x8B4D;
	constexpr GLenum MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0x8B4C;
	constexpr GLenum MAX_TEXTURE_IMAGE_UNITS = 0x8872;
	constexpr GLenum MAX_FRAGMENT_UNIFORM_VECTORS = 0x8DFD;
	constexpr GLenum SHADER_TYPE = 0x8B4F;
	constexpr GLenum DELETE_STATUS = 0x8B80;
	constexpr GLenum LINK_STATUS = 0x8B82;
	constexpr GLenum VALIDATE_STATUS = 0x8B83;
	constexpr GLenum ATTACHED_SHADERS = 0x8B85;
	constexpr GLenum ACTIVE_UNIFORMS = 0x8B86;
	constexpr GLenum ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
	constexpr GLenum ACTIVE_ATTRIBUTES = 0x8B89;
	constexpr GLenum ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A;
	constexpr GLenum SHADING_LANGUAGE_VERSION = 0x8B8C;
	constexpr GLenum CURRENT_PROGRAM = 0x8B8D;
	constexpr GLenum NEVER = 0x0200;
	constexpr GLenum LESS = 0x0201;
	constexpr GLenum EQUAL = 0x0202;
	constexpr GLenum LEQUAL = 0x0203;
	constexpr GLenum GREATER = 0x0204;
	constexpr GLenum NOTEQUAL = 0x0205;
	constexpr GLenum GEQUAL = 0x0206;
	constexpr GLenum ALWAYS = 0x0207;
	constexpr GLenum KEEP = 0x1E00;
	constexpr GLenum REPLACE = 0x1E01;
	constexpr GLenum INCR = 0x1E02;
	constexpr GLenum DECR = 0x1E03;
	constexpr GLenum INVERT = 0x150A;
	constexpr GLenum INCR_WRAP = 0x8507;
	constexpr GLenum DECR_WRAP = 0x8508;
	constexpr GLenum VENDOR = 0x1F00;
	constexpr GLenum RENDERER = 0x1F01;
	constexpr GLenum VERSION = 0x1F02;
	constexpr GLenum EXTENSIONS = 0x1F03;
	constexpr GLenum NEAREST = 0x2600;
	constexpr GLenum LINEAR = 0x2601;
	constexpr GLenum NEAREST_MIPMAP_NEAREST = 0x2700;
	constexpr GLenum LINEAR_MIPMAP_NEAREST = 0x2701;
	constexpr GLenum NEAREST_MIPMAP_LINEAR = 0x2702;
	constexpr GLenum LINEAR_MIPMAP_LINEAR = 0x2703;
	constexpr GLenum TEXTURE_MAG_FILTER = 0x2800;
	constexpr GLenum TEXTURE_MIN_FILTER = 0x2801;
	constexpr GLenum TEXTURE_WRAP_S = 0x2802;
	constexpr GLenum TEXTURE_WRAP_T = 0x2803;
	constexpr GLenum TEXTURE = 0x1702;
	constexpr GLenum TEXTURE_CUBE_MAP = 0x8513;
	constexpr GLenum TEXTURE_BINDING_CUBE_MAP = 0x8514;
	constexpr GLenum TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
	constexpr GLenum TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
	constexpr GLenum TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
	constexpr GLenum TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
	constexpr GLenum TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
	constexpr GLenum TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
	constexpr GLenum MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C;
	constexpr GLenum TEXTURE0 = 0x84C0;
	constexpr GLenum TEXTURE1 = 0x84C1;
	constexpr GLenum TEXTURE2 = 0x84C2;
	constexpr GLenum TEXTURE3 = 0x84C3;
	constexpr GLenum TEXTURE4 = 0x84C4;
	constexpr GLenum TEXTURE5 = 0x84C5;
	constexpr GLenum TEXTURE6 = 0x84C6;
	constexpr GLenum TEXTURE7 = 0x84C7;
	constexpr GLenum TEXTURE8 = 0x84C8;
	constexpr GLenum TEXTURE9 = 0x84C9;
	constexpr GLenum TEXTURE10 = 0x84CA;
	constexpr GLenum TEXTURE11 = 0x84CB;
	constexpr GLenum TEXTURE12 = 0x84CC;
	constexpr GLenum TEXTURE13 = 0x84CD;
	constexpr GLenum TEXTURE14 = 0x84CE;
	constexpr GLenum TEXTURE15 = 0x84CF;
	constexpr GLenum TEXTURE16 = 0x84D0;
	constexpr GLenum TEXTURE17 = 0x84D1;
	constexpr GLenum TEXTURE18 = 0x84D2;
	constexpr GLenum TEXTURE19 = 0x84D3;
	constexpr GLenum TEXTURE20 = 0x84D4;
	constexpr GLenum TEXTURE21 = 0x84D5;
	constexpr GLenum TEXTURE22 = 0x84D6;
	constexpr GLenum TEXTURE23 = 0x84D7;
	constexpr GLenum TEXTURE24 = 0x84D8;
	constexpr GLenum TEXTURE25 = 0x84D9;
	constexpr GLenum TEXTURE26 = 0x84DA;
	constexpr GLenum TEXTURE27 = 0x84DB;
	constexpr GLenum TEXTURE28 = 0x84DC;
	constexpr GLenum TEXTURE29 = 0x84DD;
	constexpr GLenum TEXTURE30 = 0x84DE;
	constexpr GLenum TEXTURE31 = 0x84DF;
	constexpr GLenum ACTIVE_TEXTURE = 0x84E0;
	constexpr GLenum REPEAT = 0x2901;
	constexpr GLenum CLAMP_TO_EDGE = 0x812F;
	constexpr GLenum MIRRORED_REPEAT = 0x8370;
	constexpr GLenum FLOAT_VEC2 = 0x8B50;
	constexpr GLenum FLOAT_VEC3 = 0x8B51;
	constexpr GLenum FLOAT_VEC4 = 0x8B52;
	constexpr GLenum INT_VEC2 = 0x8B53;
	constexpr GLenum INT_VEC3 = 0x8B54;
	constexpr GLenum INT_VEC4 = 0x8B55;
	constexpr GLenum BOOL = 0x8B56;
	constexpr GLenum BOOL_VEC2 = 0x8B57;
	constexpr GLenum BOOL_VEC3 = 0x8B58;
	constexpr GLenum BOOL_VEC4 = 0x8B59;
	constexpr GLenum FLOAT_MAT2 = 0x8B5A;
	constexpr GLenum FLOAT_MAT3 = 0x8B5B;
	constexpr GLenum FLOAT_MAT4 = 0x8B5C;
	constexpr GLenum SAMPLER_2D = 0x8B5E;
	constexpr GLenum SAMPLER_CUBE = 0x8B60;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_ENABLED = 0x8622;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_SIZE = 0x8623;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_STRIDE = 0x8624;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_TYPE = 0x8625;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_POINTER = 0x8645;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 0x889F;
	constexpr GLenum IMPLEMENTATION_COLOR_READ_TYPE = 0x8B9A;
	constexpr GLenum IMPLEMENTATION_COLOR_READ_FORMAT = 0x8B9B;
	constexpr GLenum COMPILE_STATUS = 0x8B81;
	constexpr GLenum INFO_LOG_LENGTH = 0x8B84;
	constexpr GLenum SHADER_SOURCE_LENGTH = 0x8B88;
	constexpr GLenum SHADER_COMPILER = 0x8DFA;
	constexpr GLenum SHADER_BINARY_FORMATS = 0x8DF8;
	constexpr GLenum NUM_SHADER_BINARY_FORMATS = 0x8DF9;
	constexpr GLenum LOW_FLOAT = 0x8DF0;
	constexpr GLenum MEDIUM_FLOAT = 0x8DF1;
	constexpr GLenum HIGH_FLOAT = 0x8DF2;
	constexpr GLenum LOW_INT = 0x8DF3;
	constexpr GLenum MEDIUM_INT = 0x8DF4;
	constexpr GLenum HIGH_INT = 0x8DF5;
	constexpr GLenum FRAMEBUFFER = 0x8D40;
	constexpr GLenum RENDERBUFFER = 0x8D41;
	constexpr GLenum RGBA4 = 0x8056;
	constexpr GLenum RGB5_A1 = 0x8057;
	constexpr GLenum RGB565 = 0x8D62;
	constexpr GLenum DEPTH_COMPONENT16 = 0x81A5;
	constexpr GLenum STENCIL_INDEX8 = 0x8D48;
	constexpr GLenum RENDERBUFFER_WIDTH = 0x8D42;
	constexpr GLenum RENDERBUFFER_HEIGHT = 0x8D43;
	constexpr GLenum RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
	constexpr GLenum RENDERBUFFER_RED_SIZE = 0x8D50;
	constexpr GLenum RENDERBUFFER_GREEN_SIZE = 0x8D51;
	constexpr GLenum RENDERBUFFER_BLUE_SIZE = 0x8D52;
	constexpr GLenum RENDERBUFFER_ALPHA_SIZE = 0x8D53;
	constexpr GLenum RENDERBUFFER_DEPTH_SIZE = 0x8D54;
	constexpr GLenum RENDERBUFFER_STENCIL_SIZE = 0x8D55;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
	constexpr GLenum COLOR_ATTACHMENT0 = 0x8CE0;
	constexpr GLenum DEPTH_ATTACHMENT = 0x8D00;
	constexpr GLenum STENCIL_ATTACHMENT = 0x8D20;
	constexpr GLenum NONE = 0;
	constexpr GLenum FRAMEBUFFER_COMPLETE = 0x8CD5;
	constexpr GLenum FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
	constexpr GLenum FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
	constexpr GLenum FRAMEBUFFER_INCOMPLETE_DIMENSIONS = 0x8CD9;
	constexpr GLenum FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
	constexpr GLenum FRAMEBUFFER_BINDING = 0x8CA6;
	constexpr GLenum RENDERBUFFER_BINDING = 0x8CA7;
	constexpr GLenum MAX_RENDERBUFFER_SIZE = 0x84E8;
	constexpr GLenum INVALID_FRAMEBUFFER_OPERATION = 0x0506;

	inline void ActiveTexture(GLenum texture){ return internal::glActiveTexture(texture); }
	inline void AttachShader(GLuint program, GLuint shader){ return internal::glAttachShader(program, shader); }
	inline void BindAttribLocation(GLuint program, GLuint index, const GLchar *name){ return internal::glBindAttribLocation(program, index, name); }
	inline void BindBuffer(GLenum target, GLuint buffer){ return internal::glBindBuffer(target, buffer); }
	inline void BindFramebuffer(GLenum target, GLuint framebuffer){ return internal::glBindFramebuffer(target, framebuffer); }
	inline void BindRenderbuffer(GLenum target, GLuint renderbuffer){ return internal::glBindRenderbuffer(target, renderbuffer); }
	inline void BindTexture(GLenum target, GLuint texture){ return internal::glBindTexture(target, texture); }
	inline void BlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){ return internal::glBlendColor(red, green, blue, alpha); }
	inline void BlendEquation(GLenum mode){ return internal::glBlendEquation(mode); }
	inline void BlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha){ return internal::glBlendEquationSeparate(modeRGB, modeAlpha); }
	inline void BlendFunc(GLenum sfactor, GLenum dfactor){ return internal::glBlendFunc(sfactor, dfactor); }
	inline void BlendFuncSeparate(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha){ return internal::glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha); }
	inline void BufferData(GLenum target, GLsizeiptr size, const void *data, GLenum usage){ return internal::glBufferData(target, size, data, usage); }
	inline void BufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const void *data){ return internal::glBufferSubData(target, offset, size, data); }
	inline GLenum CheckFramebufferStatus(GLenum target){ return internal::glCheckFramebufferStatus(target); }
	inline void Clear(GLbitfield mask){ return internal::glClear(mask); }
	inline void ClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){ return internal::glClearColor(red, green, blue, alpha); }
	inline void ClearDepthf(GLfloat d){ return internal::glClearDepthf(d); }
	inline void ClearStencil(GLint s){ return internal::glClearStencil(s); }
	inline void ColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha){ return internal::glColorMask(red, green, blue, alpha); }
	inline void CompileShader(GLuint shader){ return internal::glCompileShader(shader); }
	inline void CompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data){ return internal::glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data); }
	inline void CompressedTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data){ return internal::glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data); }
	inline void CopyTexImage2D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border){ return internal::glCopyTexImage2D(target, level, internalformat, x, y, width, height, border); }
	inline void CopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height){ return internal::glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height); }
	inline GLuint CreateProgram(){ return internal::glCreateProgram(); }
	inline GLuint CreateShader(GLenum type){ return internal::glCreateShader(type); }
	inline void CullFace(GLenum mode){ return internal::glCullFace(mode); }
	inline void DeleteBuffers(GLsizei n, const GLuint *buffers){ return internal::glDeleteBuffers(n, buffers); }
	inline void DeleteFramebuffers(GLsizei n, const GLuint *framebuffers){ return internal::glDeleteFramebuffers(n, framebuffers); }
	inline void DeleteProgram(GLuint program){ return internal::glDeleteProgram(program); }
	inline void DeleteRenderbuffers(GLsizei n, const GLuint *renderbuffers){ return internal::glDeleteRenderbuffers(n, renderbuffers); }
	inline void DeleteShader(GLuint shader){ return internal::glDeleteShader(shader); }
	inline void DeleteTextures(GLsizei n, const GLuint *textures){ return internal::glDeleteTextures(n, textures); }
	inline void DepthFunc(GLenum func){ return internal::glDepthFunc(func); }
	inline void DepthMask(GLboolean flag){ return internal::glDepthMask(flag); }
	inline void DepthRangef(GLfloat n, GLfloat f){ return internal::glDepthRangef(n, f); }
	inline void DetachShader(GLuint program, GLuint shader){ return internal::glDetachShader(program, shader); }
	inline void Disable(GLenum cap){ return internal::glDisable(cap); }
	inline void DisableVertexAttribArray(GLuint index){ return internal::glDisableVertexAttribArray(index); }
	inline void DrawArrays(GLenum mode, GLint first, GLsizei count){ return internal::glDrawArrays(mode, first, count); }
	inline void DrawElements(GLenum mode, GLsizei count, GLenum type, const void *indices){ return internal::glDrawElements(mode, count, type, indices); }
	inline void Enable(GLenum cap){ return internal::glEnable(cap); }
	inline void EnableVertexAttribArray(GLuint index){ return internal::glEnableVertexAttribArray(index); }
	inline void Finish(){ return internal::glFinish(); }
	inline void Flush(){ return internal::glFlush(); }
	inline void FramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer){ return internal::glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer); }
	inline void FramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level){ return internal::glFramebufferTexture2D(target, attachment, textarget, texture, level); }
	inline void FrontFace(GLenum mode){ return internal::glFrontFace(mode); }
	inline void GenBuffers(GLsizei n, GLuint *buffers){ return internal::glGenBuffers(n, buffers); }
	inline void GenerateMipmap(GLenum target){ return internal::glGenerateMipmap(target); }
	inline void GenFramebuffers(GLsizei n, GLuint *framebuffers){ return internal::glGenFramebuffers(n, framebuffers); }
	inline void GenRenderbuffers(GLsizei n, GLuint *renderbuffers){ return internal::glGenRenderbuffers(n, renderbuffers); }
	inline void GenTextures(GLsizei n, GLuint *textures){ return internal::glGenTextures(n, textures); }
	inline void GetActiveAttrib(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name){ return internal::glGetActiveAttrib(program, index, bufSize, length, size, type, name); }
	inline void GetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name){ return internal::glGetActiveUniform(program, index, bufSize, length, size, type, name); }
	inline void GetAttachedShaders(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders){ return internal::glGetAttachedShaders(program, maxCount, count, shaders); }
	inline GLint GetAttribLocation(GLuint program, const GLchar *name){ return internal::glGetAttribLocation(program, name); }
	inline void GetBooleanv(GLenum pname, GLboolean *data){ return internal::glGetBooleanv(pname, data); }
	inline void GetBufferParameteriv(GLenum target, GLenum pname, GLint *params){ return internal::glGetBufferParameteriv(target, pname, params); }
	inline GLenum GetError(){ return internal::glGetError(); }
	inline void GetFloatv(GLenum pname, GLfloat *data){ return internal::glGetFloatv(pname, data); }
	inline void GetFramebufferAttachmentParameteriv(GLenum target, GLenum attachment, GLenum pname, GLint *params){ return internal::glGetFramebufferAttachmentParameteriv(target, attachment, pname, params); }
	inline void GetIntegerv(GLenum pname, GLint *data){ return internal::glGetIntegerv(pname, data); }
	inline void GetProgramiv(GLuint program, GLenum pname, GLint *params){ return internal::glGetProgramiv(program, pname, params); }
	inline void GetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog){ return internal::glGetProgramInfoLog(program, bufSize, length, infoLog); }
	inline void GetRenderbufferParameteriv(GLenum target, GLenum pname, GLint *params){ return internal::glGetRenderbufferParameteriv(target, pname, params); }
	inline void GetShaderiv(GLuint shader, GLenum pname, GLint *params){ return internal::glGetShaderiv(shader, pname, params); }
	inline void GetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog){ return internal::glGetShaderInfoLog(shader, bufSize, length, infoLog); }
	inline void GetShaderPrecisionFormat(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision){ return internal::glGetShaderPrecisionFormat(shadertype, precisiontype, range, precision); }
	inline void GetShaderSource(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source){ return internal::glGetShaderSource(shader, bufSize, length, source); }
	inline const GLubyte *GetString(GLenum name){ return internal::glGetString(name); }
	inline void GetTexParameterfv(GLenum target, GLenum pname, GLfloat *params){ return internal::glGetTexParameterfv(target, pname, params); }
	inline void GetTexParameteriv(GLenum target, GLenum pname, GLint *params){ return internal::glGetTexParameteriv(target, pname, params); }
	inline void GetUniformfv(GLuint program, GLint location, GLfloat *params){ return internal::glGetUniformfv(program, location, params); }
	inline void GetUniformiv(GLuint program, GLint location, GLint *params){ return internal::glGetUniformiv(program, location, params); }
	inline GLint GetUniformLocation(GLuint program, const GLchar *name){ return internal::glGetUniformLocation(program, name); }
	inline void GetVertexAttribfv(GLuint index, GLenum pname, GLfloat *params){ return internal::glGetVertexAttribfv(index, pname, params); }
	inline void GetVertexAttribiv(GLuint index, GLenum pname, GLint *params){ return internal::glGetVertexAttribiv(index, pname, params); }
	inline void GetVertexAttribPointerv(GLuint index, GLenum pname, void **pointer){ return internal::glGetVertexAttribPointerv(index, pname, pointer); }
	inline void Hint(GLenum target, GLenum mode){ return internal::glHint(target, mode); }
	inline GLboolean IsBuffer(GLuint buffer){ return internal::glIsBuffer(buffer); }
	inline GLboolean IsEnabled(GLenum cap){ return internal::glIsEnabled(cap); }
	inline GLboolean IsFramebuffer(GLuint framebuffer){ return internal::glIsFramebuffer(framebuffer); }
	inline GLboolean IsProgram(GLuint program){ return internal::glIsProgram(program); }
	inline GLboolean IsRenderbuffer(GLuint renderbuffer){ return internal::glIsRenderbuffer(renderbuffer); }
	inline GLboolean IsShader(GLuint shader){ return internal::glIsShader(shader); }
	inline GLboolean IsTexture(GLuint texture){ return internal::glIsTexture(texture); }
	inline void LineWidth(GLfloat width){ return internal::glLineWidth(width); }
	inline void LinkProgram(GLuint program){ return internal::glLinkProgram(program); }
	inline void PixelStorei(GLenum pname, GLint param){ return internal::glPixelStorei(pname, param); }
	inline void PolygonOffset(GLfloat factor, GLfloat units){ return internal::glPolygonOffset(factor, units); }
	inline void ReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels){ return internal::glReadPixels(x, y, width, height, format, type, pixels); }
	inline void ReleaseShaderCompiler(){ return internal::glReleaseShaderCompiler(); }
	inline void RenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height){ return internal::glRenderbufferStorage(target, internalformat, width, height); }
	inline void SampleCoverage(GLfloat value, GLboolean invert){ return internal::glSampleCoverage(value, invert); }
	inline void Scissor(GLint x, GLint y, GLsizei width, GLsizei height){ return internal::glScissor(x, y, width, height); }
	inline void ShaderBinary(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length){ return internal::glShaderBinary(count, shaders, binaryFormat, binary, length); }
	inline void ShaderSource(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length){ return internal::glShaderSource(shader, count, string, length); }
	inline void StencilFunc(GLenum func, GLint ref, GLuint mask){ return internal::glStencilFunc(func, ref, mask); }
	inline void StencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask){ return internal::glStencilFuncSeparate(face, func, ref, mask); }
	inline void StencilMask(GLuint mask){ return internal::glStencilMask(mask); }
	inline void StencilMaskSeparate(GLenum face, GLuint mask){ return internal::glStencilMaskSeparate(face, mask); }
	inline void StencilOp(GLenum fail, GLenum zfail, GLenum zpass){ return internal::glStencilOp(fail, zfail, zpass); }
	inline void StencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass){ return internal::glStencilOpSeparate(face, sfail, dpfail, dppass); }
	inline void TexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels){ return internal::glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels); }
	inline void TexParameterf(GLenum target, GLenum pname, GLfloat param){ return internal::glTexParameterf(target, pname, param); }
	inline void TexParameterfv(GLenum target, GLenum pname, const GLfloat *params){ return internal::glTexParameterfv(target, pname, params); }
	inline void TexParameteri(GLenum target, GLenum pname, GLint param){ return internal::glTexParameteri(target, pname, param); }
	inline void TexParameteriv(GLenum target, GLenum pname, const GLint *params){ return internal::glTexParameteriv(target, pname, params); }
	inline void TexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels){ return internal::glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels); }
	inline void Uniform1f(GLint location, GLfloat v0){ return internal::glUniform1f(location, v0); }
	inline void Uniform1fv(GLint location, GLsizei count, const GLfloat *value){ return internal::glUniform1fv(location, count, value); }
	inline void Uniform1i(GLint location, GLint v0){ return internal::glUniform1i(location, v0); }
	inline void Uniform1iv(GLint location, GLsizei count, const GLint *value){ return internal::glUniform1iv(location, count, value); }
	inline void Uniform2f(GLint location, GLfloat v0, GLfloat v1){ return internal::glUniform2f(location, v0, v1); }
	inline void Uniform2fv(GLint location, GLsizei count, const GLfloat *value){ return internal::glUniform2fv(location, count, value); }
	inline void Uniform2i(GLint location, GLint v0, GLint v1){ return internal::glUniform2i(location, v0, v1); }
	inline void Uniform2iv(GLint location, GLsizei count, const GLint *value){ return internal::glUniform2iv(location, count, value); }
	inline void Uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2){ return internal::glUniform3f(location, v0, v1, v2); }
	inline void Uniform3fv(GLint location, GLsizei count, const GLfloat *value){ return internal::glUniform3fv(location, count, value); }
	inline void Uniform3i(GLint location, GLint v0, GLint v1, GLint v2){ return internal::glUniform3i(location, v0, v1, v2); }
	inline void Uniform3iv(GLint location, GLsizei count, const GLint *value){ return internal::glUniform3iv(location, count, value); }
	inline void Uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3){ return internal::glUniform4f(location, v0, v1, v2, v3); }
	inline void Uniform4fv(GLint location, GLsizei count, const GLfloat *value){ return internal::glUniform4fv(location, count, value); }
	inline void Uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3){ return internal::glUniform4i(location, v0, v1, v2, v3); }
	inline void Uniform4iv(GLint location, GLsizei count, const GLint *value){ return internal::glUniform4iv(location, count, value); }
	inline void UniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix2fv(location, count, transpose, value); }
	inline void UniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix3fv(location, count, transpose, value); }
	inline void UniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix4fv(location, count, transpose, value); }
	inline void UseProgram(GLuint program){ return internal::glUseProgram(program); }
	inline void ValidateProgram(GLuint program){ return internal::glValidateProgram(program); }
	inline void VertexAttrib1f(GLuint index, GLfloat x){ return internal::glVertexAttrib1f(index, x); }
	inline void VertexAttrib1fv(GLuint index, const GLfloat *v){ return internal::glVertexAttrib1fv(index, v); }
	inline void VertexAttrib2f(GLuint index, GLfloat x, GLfloat y){ return internal::glVertexAttrib2f(index, x, y); }
	inline void VertexAttrib2fv(GLuint index, const GLfloat *v){ return internal::glVertexAttrib2fv(index, v); }
	inline void VertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z){ return internal::glVertexAttrib3f(index, x, y, z); }
	inline void VertexAttrib3fv(GLuint index, const GLfloat *v){ return internal::glVertexAttrib3fv(index, v); }
	inline void VertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w){ return internal::glVertexAttrib4f(index, x, y, z, w); }
	inline void VertexAttrib4fv(GLuint index, const GLfloat *v){ return internal::glVertexAttrib4fv(index, v); }
	inline void VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer){ return internal::glVertexAttribPointer(index, size, type, normalized, stride, pointer); }
	inline void Viewport(GLint x, GLint y, GLsizei width, GLsizei height){ return internal::glViewport(x, y, width, height); }

	// OpenGL ES 3.0

	constexpr GLenum READ_BUFFER = 0x0C02;
	constexpr GLenum UNPACK_ROW_LENGTH = 0x0CF2;
	constexpr GLenum UNPACK_SKIP_ROWS = 0x0CF3;
	constexpr GLenum UNPACK_SKIP_PIXELS = 0x0CF4;
	constexpr GLenum PACK_ROW_LENGTH = 0x0D02;
	constexpr GLenum PACK_SKIP_ROWS = 0x0D03;
	constexpr GLenum PACK_SKIP_PIXELS = 0x0D04;
	constexpr GLenum COLOR = 0x1800;
	constexpr GLenum DEPTH = 0x1801;
	constexpr GLenum STENCIL = 0x1802;
	constexpr GLenum RED = 0x1903;
	constexpr GLenum RGB8 = 0x8051;
	constexpr GLenum RGBA8 = 0x8058;
	constexpr GLenum RGB10_A2 = 0x8059;
	constexpr GLenum TEXTURE_BINDING_3D = 0x806A;
	constexpr GLenum UNPACK_SKIP_IMAGES = 0x806D;
	constexpr GLenum UNPACK_IMAGE_HEIGHT = 0x806E;
	constexpr GLenum TEXTURE_3D = 0x806F;
	constexpr GLenum TEXTURE_WRAP_R = 0x8072;
	constexpr GLenum MAX_3D_TEXTURE_SIZE = 0x8073;
	constexpr GLenum UNSIGNED_INT_2_10_10_10_REV = 0x8368;
	constexpr GLenum MAX_ELEMENTS_VERTICES = 0x80E8;
	constexpr GLenum MAX_ELEMENTS_INDICES = 0x80E9;
	constexpr GLenum TEXTURE_MIN_LOD = 0x813A;
	constexpr GLenum TEXTURE_MAX_LOD = 0x813B;
	constexpr GLenum TEXTURE_BASE_LEVEL = 0x813C;
	constexpr GLenum TEXTURE_MAX_LEVEL = 0x813D;
	constexpr GLenum MIN = 0x8007;
	constexpr GLenum MAX = 0x8008;
	constexpr GLenum DEPTH_COMPONENT24 = 0x81A6;
	constexpr GLenum MAX_TEXTURE_LOD_BIAS = 0x84FD;
	constexpr GLenum TEXTURE_COMPARE_MODE = 0x884C;
	constexpr GLenum TEXTURE_COMPARE_FUNC = 0x884D;
	constexpr GLenum CURRENT_QUERY = 0x8865;
	constexpr GLenum QUERY_RESULT = 0x8866;
	constexpr GLenum QUERY_RESULT_AVAILABLE = 0x8867;
	constexpr GLenum BUFFER_MAPPED = 0x88BC;
	constexpr GLenum BUFFER_MAP_POINTER = 0x88BD;
	constexpr GLenum STREAM_READ = 0x88E1;
	constexpr GLenum STREAM_COPY = 0x88E2;
	constexpr GLenum STATIC_READ = 0x88E5;
	constexpr GLenum STATIC_COPY = 0x88E6;
	constexpr GLenum DYNAMIC_READ = 0x88E9;
	constexpr GLenum DYNAMIC_COPY = 0x88EA;
	constexpr GLenum MAX_DRAW_BUFFERS = 0x8824;
	constexpr GLenum DRAW_BUFFER0 = 0x8825;
	constexpr GLenum DRAW_BUFFER1 = 0x8826;
	constexpr GLenum DRAW_BUFFER2 = 0x8827;
	constexpr GLenum DRAW_BUFFER3 = 0x8828;
	constexpr GLenum DRAW_BUFFER4 = 0x8829;
	constexpr GLenum DRAW_BUFFER5 = 0x882A;
	constexpr GLenum DRAW_BUFFER6 = 0x882B;
	constexpr GLenum DRAW_BUFFER7 = 0x882C;
	constexpr GLenum DRAW_BUFFER8 = 0x882D;
	constexpr GLenum DRAW_BUFFER9 = 0x882E;
	constexpr GLenum DRAW_BUFFER10 = 0x882F;
	constexpr GLenum DRAW_BUFFER11 = 0x8830;
	constexpr GLenum DRAW_BUFFER12 = 0x8831;
	constexpr GLenum DRAW_BUFFER13 = 0x8832;
	constexpr GLenum DRAW_BUFFER14 = 0x8833;
	constexpr GLenum DRAW_BUFFER15 = 0x8834;
	constexpr GLenum MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49;
	constexpr GLenum MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A;
	constexpr GLenum SAMPLER_3D = 0x8B5F;
	constexpr GLenum SAMPLER_2D_SHADOW = 0x8B62;
	constexpr GLenum FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
	constexpr GLenum PIXEL_PACK_BUFFER = 0x88EB;
	constexpr GLenum PIXEL_UNPACK_BUFFER = 0x88EC;
	constexpr GLenum PIXEL_PACK_BUFFER_BINDING = 0x88ED;
	constexpr GLenum PIXEL_UNPACK_BUFFER_BINDING = 0x88EF;
	constexpr GLenum FLOAT_MAT2x3 = 0x8B65;
	constexpr GLenum FLOAT_MAT2x4 = 0x8B66;
	constexpr GLenum FLOAT_MAT3x2 = 0x8B67;
	constexpr GLenum FLOAT_MAT3x4 = 0x8B68;
	constexpr GLenum FLOAT_MAT4x2 = 0x8B69;
	constexpr GLenum FLOAT_MAT4x3 = 0x8B6A;
	constexpr GLenum SRGB = 0x8C40;
	constexpr GLenum SRGB8 = 0x8C41;
	constexpr GLenum SRGB8_ALPHA8 = 0x8C43;
	constexpr GLenum COMPARE_REF_TO_TEXTURE = 0x884E;
	constexpr GLenum MAJOR_VERSION = 0x821B;
	constexpr GLenum MINOR_VERSION = 0x821C;
	constexpr GLenum NUM_EXTENSIONS = 0x821D;
	constexpr GLenum RGBA32F = 0x8814;
	constexpr GLenum RGB32F = 0x8815;
	constexpr GLenum RGBA16F = 0x881A;
	constexpr GLenum RGB16F = 0x881B;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_INTEGER = 0x88FD;
	constexpr GLenum MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
	constexpr GLenum MIN_PROGRAM_TEXEL_OFFSET = 0x8904;
	constexpr GLenum MAX_PROGRAM_TEXEL_OFFSET = 0x8905;
	constexpr GLenum MAX_VARYING_COMPONENTS = 0x8B4B;
	constexpr GLenum TEXTURE_2D_ARRAY = 0x8C1A;
	constexpr GLenum TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
	constexpr GLenum R11F_G11F_B10F = 0x8C3A;
	constexpr GLenum UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
	constexpr GLenum RGB9_E5 = 0x8C3D;
	constexpr GLenum UNSIGNED_INT_5_9_9_9_REV = 0x8C3E;
	constexpr GLenum TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH = 0x8C76;
	constexpr GLenum TRANSFORM_FEEDBACK_BUFFER_MODE = 0x8C7F;
	constexpr GLenum MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0x8C80;
	constexpr GLenum TRANSFORM_FEEDBACK_VARYINGS = 0x8C83;
	constexpr GLenum TRANSFORM_FEEDBACK_BUFFER_START = 0x8C84;
	constexpr GLenum TRANSFORM_FEEDBACK_BUFFER_SIZE = 0x8C85;
	constexpr GLenum TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 0x8C88;
	constexpr GLenum RASTERIZER_DISCARD = 0x8C89;
	constexpr GLenum MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0x8C8A;
	constexpr GLenum MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0x8C8B;
	constexpr GLenum INTERLEAVED_ATTRIBS = 0x8C8C;
	constexpr GLenum SEPARATE_ATTRIBS = 0x8C8D;
	constexpr GLenum TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
	constexpr GLenum TRANSFORM_FEEDBACK_BUFFER_BINDING = 0x8C8F;
	constexpr GLenum RGBA32UI = 0x8D70;
	constexpr GLenum RGB32UI = 0x8D71;
	constexpr GLenum RGBA16UI = 0x8D76;
	constexpr GLenum RGB16UI = 0x8D77;
	constexpr GLenum RGBA8UI = 0x8D7C;
	constexpr GLenum RGB8UI = 0x8D7D;
	constexpr GLenum RGBA32I = 0x8D82;
	constexpr GLenum RGB32I = 0x8D83;
	constexpr GLenum RGBA16I = 0x8D88;
	constexpr GLenum RGB16I = 0x8D89;
	constexpr GLenum RGBA8I = 0x8D8E;
	constexpr GLenum RGB8I = 0x8D8F;
	constexpr GLenum RED_INTEGER = 0x8D94;
	constexpr GLenum RGB_INTEGER = 0x8D98;
	constexpr GLenum RGBA_INTEGER = 0x8D99;
	constexpr GLenum SAMPLER_2D_ARRAY = 0x8DC1;
	constexpr GLenum SAMPLER_2D_ARRAY_SHADOW = 0x8DC4;
	constexpr GLenum SAMPLER_CUBE_SHADOW = 0x8DC5;
	constexpr GLenum UNSIGNED_INT_VEC2 = 0x8DC6;
	constexpr GLenum UNSIGNED_INT_VEC3 = 0x8DC7;
	constexpr GLenum UNSIGNED_INT_VEC4 = 0x8DC8;
	constexpr GLenum INT_SAMPLER_2D = 0x8DCA;
	constexpr GLenum INT_SAMPLER_3D = 0x8DCB;
	constexpr GLenum INT_SAMPLER_CUBE = 0x8DCC;
	constexpr GLenum INT_SAMPLER_2D_ARRAY = 0x8DCF;
	constexpr GLenum UNSIGNED_INT_SAMPLER_2D = 0x8DD2;
	constexpr GLenum UNSIGNED_INT_SAMPLER_3D = 0x8DD3;
	constexpr GLenum UNSIGNED_INT_SAMPLER_CUBE = 0x8DD4;
	constexpr GLenum UNSIGNED_INT_SAMPLER_2D_ARRAY = 0x8DD7;
	constexpr GLenum BUFFER_ACCESS_FLAGS = 0x911F;
	constexpr GLenum BUFFER_MAP_LENGTH = 0x9120;
	constexpr GLenum BUFFER_MAP_OFFSET = 0x9121;
	constexpr GLenum DEPTH_COMPONENT32F = 0x8CAC;
	constexpr GLenum DEPTH32F_STENCIL8 = 0x8CAD;
	constexpr GLenum FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
	constexpr GLenum FRAMEBUFFER_DEFAULT = 0x8218;
	constexpr GLenum FRAMEBUFFER_UNDEFINED = 0x8219;
	constexpr GLenum DEPTH_STENCIL_ATTACHMENT = 0x821A;
	constexpr GLenum DEPTH_STENCIL = 0x84F9;
	constexpr GLenum UNSIGNED_INT_24_8 = 0x84FA;
	constexpr GLenum DEPTH24_STENCIL8 = 0x88F0;
	constexpr GLenum UNSIGNED_NORMALIZED = 0x8C17;
	constexpr GLenum DRAW_FRAMEBUFFER_BINDING = 0x8CA6;
	constexpr GLenum READ_FRAMEBUFFER = 0x8CA8;
	constexpr GLenum DRAW_FRAMEBUFFER = 0x8CA9;
	constexpr GLenum READ_FRAMEBUFFER_BINDING = 0x8CAA;
	constexpr GLenum RENDERBUFFER_SAMPLES = 0x8CAB;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
	constexpr GLenum MAX_COLOR_ATTACHMENTS = 0x8CDF;
	constexpr GLenum COLOR_ATTACHMENT1 = 0x8CE1;
	constexpr GLenum COLOR_ATTACHMENT2 = 0x8CE2;
	constexpr GLenum COLOR_ATTACHMENT3 = 0x8CE3;
	constexpr GLenum COLOR_ATTACHMENT4 = 0x8CE4;
	constexpr GLenum COLOR_ATTACHMENT5 = 0x8CE5;
	constexpr GLenum COLOR_ATTACHMENT6 = 0x8CE6;
	constexpr GLenum COLOR_ATTACHMENT7 = 0x8CE7;
	constexpr GLenum COLOR_ATTACHMENT8 = 0x8CE8;
	constexpr GLenum COLOR_ATTACHMENT9 = 0x8CE9;
	constexpr GLenum COLOR_ATTACHMENT10 = 0x8CEA;
	constexpr GLenum COLOR_ATTACHMENT11 = 0x8CEB;
	constexpr GLenum COLOR_ATTACHMENT12 = 0x8CEC;
	constexpr GLenum COLOR_ATTACHMENT13 = 0x8CED;
	constexpr GLenum COLOR_ATTACHMENT14 = 0x8CEE;
	constexpr GLenum COLOR_ATTACHMENT15 = 0x8CEF;
	constexpr GLenum COLOR_ATTACHMENT16 = 0x8CF0;
	constexpr GLenum COLOR_ATTACHMENT17 = 0x8CF1;
	constexpr GLenum COLOR_ATTACHMENT18 = 0x8CF2;
	constexpr GLenum COLOR_ATTACHMENT19 = 0x8CF3;
	constexpr GLenum COLOR_ATTACHMENT20 = 0x8CF4;
	constexpr GLenum COLOR_ATTACHMENT21 = 0x8CF5;
	constexpr GLenum COLOR_ATTACHMENT22 = 0x8CF6;
	constexpr GLenum COLOR_ATTACHMENT23 = 0x8CF7;
	constexpr GLenum COLOR_ATTACHMENT24 = 0x8CF8;
	constexpr GLenum COLOR_ATTACHMENT25 = 0x8CF9;
	constexpr GLenum COLOR_ATTACHMENT26 = 0x8CFA;
	constexpr GLenum COLOR_ATTACHMENT27 = 0x8CFB;
	constexpr GLenum COLOR_ATTACHMENT28 = 0x8CFC;
	constexpr GLenum COLOR_ATTACHMENT29 = 0x8CFD;
	constexpr GLenum COLOR_ATTACHMENT30 = 0x8CFE;
	constexpr GLenum COLOR_ATTACHMENT31 = 0x8CFF;
	constexpr GLenum FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
	constexpr GLenum MAX_SAMPLES = 0x8D57;
	constexpr GLenum HALF_FLOAT = 0x140B;
	constexpr GLenum MAP_READ_BIT = 0x0001;
	constexpr GLenum MAP_WRITE_BIT = 0x0002;
	constexpr GLenum MAP_INVALIDATE_RANGE_BIT = 0x0004;
	constexpr GLenum MAP_INVALIDATE_BUFFER_BIT = 0x0008;
	constexpr GLenum MAP_FLUSH_EXPLICIT_BIT = 0x0010;
	constexpr GLenum MAP_UNSYNCHRONIZED_BIT = 0x0020;
	constexpr GLenum RG = 0x8227;
	constexpr GLenum RG_INTEGER = 0x8228;
	constexpr GLenum R8 = 0x8229;
	constexpr GLenum RG8 = 0x822B;
	constexpr GLenum R16F = 0x822D;
	constexpr GLenum R32F = 0x822E;
	constexpr GLenum RG16F = 0x822F;
	constexpr GLenum RG32F = 0x8230;
	constexpr GLenum R8I = 0x8231;
	constexpr GLenum R8UI = 0x8232;
	constexpr GLenum R16I = 0x8233;
	constexpr GLenum R16UI = 0x8234;
	constexpr GLenum R32I = 0x8235;
	constexpr GLenum R32UI = 0x8236;
	constexpr GLenum RG8I = 0x8237;
	constexpr GLenum RG8UI = 0x8238;
	constexpr GLenum RG16I = 0x8239;
	constexpr GLenum RG16UI = 0x823A;
	constexpr GLenum RG32I = 0x823B;
	constexpr GLenum RG32UI = 0x823C;
	constexpr GLenum VERTEX_ARRAY_BINDING = 0x85B5;
	constexpr GLenum R8_SNORM = 0x8F94;
	constexpr GLenum RG8_SNORM = 0x8F95;
	constexpr GLenum RGB8_SNORM = 0x8F96;
	constexpr GLenum RGBA8_SNORM = 0x8F97;
	constexpr GLenum SIGNED_NORMALIZED = 0x8F9C;
	constexpr GLenum PRIMITIVE_RESTART_FIXED_INDEX = 0x8D69;
	constexpr GLenum COPY_READ_BUFFER = 0x8F36;
	constexpr GLenum COPY_WRITE_BUFFER = 0x8F37;
	constexpr GLenum COPY_READ_BUFFER_BINDING = 0x8F36;
	constexpr GLenum COPY_WRITE_BUFFER_BINDING = 0x8F37;
	constexpr GLenum UNIFORM_BUFFER = 0x8A11;
	constexpr GLenum UNIFORM_BUFFER_BINDING = 0x8A28;
	constexpr GLenum UNIFORM_BUFFER_START = 0x8A29;
	constexpr GLenum UNIFORM_BUFFER_SIZE = 0x8A2A;
	constexpr GLenum MAX_VERTEX_UNIFORM_BLOCKS = 0x8A2B;
	constexpr GLenum MAX_FRAGMENT_UNIFORM_BLOCKS = 0x8A2D;
	constexpr GLenum MAX_COMBINED_UNIFORM_BLOCKS = 0x8A2E;
	constexpr GLenum MAX_UNIFORM_BUFFER_BINDINGS = 0x8A2F;
	constexpr GLenum MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
	constexpr GLenum MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0x8A31;
	constexpr GLenum MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33;
	constexpr GLenum UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0x8A34;
	constexpr GLenum ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = 0x8A35;
	constexpr GLenum ACTIVE_UNIFORM_BLOCKS = 0x8A36;
	constexpr GLenum UNIFORM_TYPE = 0x8A37;
	constexpr GLenum UNIFORM_SIZE = 0x8A38;
	constexpr GLenum UNIFORM_NAME_LENGTH = 0x8A39;
	constexpr GLenum UNIFORM_BLOCK_INDEX = 0x8A3A;
	constexpr GLenum UNIFORM_OFFSET = 0x8A3B;
	constexpr GLenum UNIFORM_ARRAY_STRIDE = 0x8A3C;
	constexpr GLenum UNIFORM_MATRIX_STRIDE = 0x8A3D;
	constexpr GLenum UNIFORM_IS_ROW_MAJOR = 0x8A3E;
	constexpr GLenum UNIFORM_BLOCK_BINDING = 0x8A3F;
	constexpr GLenum UNIFORM_BLOCK_DATA_SIZE = 0x8A40;
	constexpr GLenum UNIFORM_BLOCK_NAME_LENGTH = 0x8A41;
	constexpr GLenum UNIFORM_BLOCK_ACTIVE_UNIFORMS = 0x8A42;
	constexpr GLenum UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = 0x8A43;
	constexpr GLenum UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = 0x8A44;
	constexpr GLenum UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46;
	constexpr unsigned INVALID_INDEX = 0xFFFFFFFF;
	constexpr GLenum MAX_VERTEX_OUTPUT_COMPONENTS = 0x9122;
	constexpr GLenum MAX_FRAGMENT_INPUT_COMPONENTS = 0x9125;
	constexpr GLenum MAX_SERVER_WAIT_TIMEOUT = 0x9111;
	constexpr GLenum OBJECT_TYPE = 0x9112;
	constexpr GLenum SYNC_CONDITION = 0x9113;
	constexpr GLenum SYNC_STATUS = 0x9114;
	constexpr GLenum SYNC_FLAGS = 0x9115;
	constexpr GLenum SYNC_FENCE = 0x9116;
	constexpr GLenum SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
	constexpr GLenum UNSIGNALED = 0x9118;
	constexpr GLenum SIGNALED = 0x9119;
	constexpr GLenum ALREADY_SIGNALED = 0x911A;
	constexpr GLenum TIMEOUT_EXPIRED = 0x911B;
	constexpr GLenum CONDITION_SATISFIED = 0x911C;
	constexpr GLenum WAIT_FAILED = 0x911D;
	constexpr GLenum SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
	constexpr unsigned long long TIMEOUT_IGNORED = 0xFFFFFFFFFFFFFFFF;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_DIVISOR = 0x88FE;
	constexpr GLenum ANY_SAMPLES_PASSED = 0x8C2F;
	constexpr GLenum ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
	constexpr GLenum SAMPLER_BINDING = 0x8919;
	constexpr GLenum RGB10_A2UI = 0x906F;
	constexpr GLenum TEXTURE_SWIZZLE_R = 0x8E42;
	constexpr GLenum TEXTURE_SWIZZLE_G = 0x8E43;
	constexpr GLenum TEXTURE_SWIZZLE_B = 0x8E44;
	constexpr GLenum TEXTURE_SWIZZLE_A = 0x8E45;
	constexpr GLenum GREEN = 0x1904;
	constexpr GLenum BLUE = 0x1905;
	constexpr GLenum INT_2_10_10_10_REV = 0x8D9F;
	constexpr GLenum TRANSFORM_FEEDBACK = 0x8E22;
	constexpr GLenum TRANSFORM_FEEDBACK_PAUSED = 0x8E23;
	constexpr GLenum TRANSFORM_FEEDBACK_ACTIVE = 0x8E24;
	constexpr GLenum TRANSFORM_FEEDBACK_BINDING = 0x8E25;
	constexpr GLenum PROGRAM_BINARY_RETRIEVABLE_HINT = 0x8257;
	constexpr GLenum PROGRAM_BINARY_LENGTH = 0x8741;
	constexpr GLenum NUM_PROGRAM_BINARY_FORMATS = 0x87FE;
	constexpr GLenum PROGRAM_BINARY_FORMATS = 0x87FF;
	constexpr GLenum COMPRESSED_R11_EAC = 0x9270;
	constexpr GLenum COMPRESSED_SIGNED_R11_EAC = 0x9271;
	constexpr GLenum COMPRESSED_RG11_EAC = 0x9272;
	constexpr GLenum COMPRESSED_SIGNED_RG11_EAC = 0x9273;
	constexpr GLenum COMPRESSED_RGB8_ETC2 = 0x9274;
	constexpr GLenum COMPRESSED_SRGB8_ETC2 = 0x9275;
	constexpr GLenum COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9276;
	constexpr GLenum COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9277;
	constexpr GLenum COMPRESSED_RGBA8_ETC2_EAC = 0x9278;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 0x9279;
	constexpr GLenum TEXTURE_IMMUTABLE_FORMAT = 0x912F;
	constexpr GLenum MAX_ELEMENT_INDEX = 0x8D6B;
	constexpr GLenum NUM_SAMPLE_COUNTS = 0x9380;
	constexpr GLenum TEXTURE_IMMUTABLE_LEVELS = 0x82DF;

	inline void ReadBuffer(GLenum src){ return internal::glReadBuffer(src); }
	inline void DrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices){ return internal::glDrawRangeElements(mode, start, end, count, type, indices); }
	inline void TexImage3D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels){ return internal::glTexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels); }
	inline void TexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels){ return internal::glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels); }
	inline void CopyTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height){ return internal::glCopyTexSubImage3D(target, level, xoffset, yoffset, zoffset, x, y, width, height); }
	inline void CompressedTexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data){ return internal::glCompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data); }
	inline void CompressedTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data){ return internal::glCompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data); }
	inline void GenQueries(GLsizei n, GLuint *ids){ return internal::glGenQueries(n, ids); }
	inline void DeleteQueries(GLsizei n, const GLuint *ids){ return internal::glDeleteQueries(n, ids); }
	inline GLboolean IsQuery(GLuint id){ return internal::glIsQuery(id); }
	inline void BeginQuery(GLenum target, GLuint id){ return internal::glBeginQuery(target, id); }
	inline void EndQuery(GLenum target){ return internal::glEndQuery(target); }
	inline void GetQueryiv(GLenum target, GLenum pname, GLint *params){ return internal::glGetQueryiv(target, pname, params); }
	inline void GetQueryObjectuiv(GLuint id, GLenum pname, GLuint *params){ return internal::glGetQueryObjectuiv(id, pname, params); }
	inline GLboolean UnmapBuffer(GLenum target){ return internal::glUnmapBuffer(target); }
	inline void GetBufferPointerv(GLenum target, GLenum pname, void **params){ return internal::glGetBufferPointerv(target, pname, params); }
	inline void DrawBuffers(GLsizei n, const GLenum *bufs){ return internal::glDrawBuffers(n, bufs); }
	inline void UniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix2x3fv(location, count, transpose, value); }
	inline void UniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix3x2fv(location, count, transpose, value); }
	inline void UniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix2x4fv(location, count, transpose, value); }
	inline void UniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix4x2fv(location, count, transpose, value); }
	inline void UniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix3x4fv(location, count, transpose, value); }
	inline void UniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glUniformMatrix4x3fv(location, count, transpose, value); }
	inline void BlitFramebuffer(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter){ return internal::glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter); }
	inline void RenderbufferStorageMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height){ return internal::glRenderbufferStorageMultisample(target, samples, internalformat, width, height); }
	inline void FramebufferTextureLayer(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer){ return internal::glFramebufferTextureLayer(target, attachment, texture, level, layer); }
	inline void *MapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access){ return internal::glMapBufferRange(target, offset, length, access); }
	inline void FlushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length){ return internal::glFlushMappedBufferRange(target, offset, length); }
	inline void BindVertexArray(GLuint array){ return internal::glBindVertexArray(array); }
	inline void DeleteVertexArrays(GLsizei n, const GLuint *arrays){ return internal::glDeleteVertexArrays(n, arrays); }
	inline void GenVertexArrays(GLsizei n, GLuint *arrays){ return internal::glGenVertexArrays(n, arrays); }
	inline GLboolean IsVertexArray(GLuint array){ return internal::glIsVertexArray(array); }
	inline void GetIntegeri_v(GLenum target, GLuint index, GLint *data){ return internal::glGetIntegeri_v(target, index, data); }
	inline void BeginTransformFeedback(GLenum primitiveMode){ return internal::glBeginTransformFeedback(primitiveMode); }
	inline void EndTransformFeedback(){ return internal::glEndTransformFeedback(); }
	inline void BindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size){ return internal::glBindBufferRange(target, index, buffer, offset, size); }
	inline void BindBufferBase(GLenum target, GLuint index, GLuint buffer){ return internal::glBindBufferBase(target, index, buffer); }
	inline void TransformFeedbackVaryings(GLuint program, GLsizei count, const GLchar *const*varyings, GLenum bufferMode){ return internal::glTransformFeedbackVaryings(program, count, varyings, bufferMode); }
	inline void GetTransformFeedbackVarying(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, GLchar *name){ return internal::glGetTransformFeedbackVarying(program, index, bufSize, length, size, type, name); }
	inline void VertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer){ return internal::glVertexAttribIPointer(index, size, type, stride, pointer); }
	inline void GetVertexAttribIiv(GLuint index, GLenum pname, GLint *params){ return internal::glGetVertexAttribIiv(index, pname, params); }
	inline void GetVertexAttribIuiv(GLuint index, GLenum pname, GLuint *params){ return internal::glGetVertexAttribIuiv(index, pname, params); }
	inline void VertexAttribI4i(GLuint index, GLint x, GLint y, GLint z, GLint w){ return internal::glVertexAttribI4i(index, x, y, z, w); }
	inline void VertexAttribI4ui(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w){ return internal::glVertexAttribI4ui(index, x, y, z, w); }
	inline void VertexAttribI4iv(GLuint index, const GLint *v){ return internal::glVertexAttribI4iv(index, v); }
	inline void VertexAttribI4uiv(GLuint index, const GLuint *v){ return internal::glVertexAttribI4uiv(index, v); }
	inline void GetUniformuiv(GLuint program, GLint location, GLuint *params){ return internal::glGetUniformuiv(program, location, params); }
	inline GLint GetFragDataLocation(GLuint program, const GLchar *name){ return internal::glGetFragDataLocation(program, name); }
	inline void Uniform1ui(GLint location, GLuint v0){ return internal::glUniform1ui(location, v0); }
	inline void Uniform2ui(GLint location, GLuint v0, GLuint v1){ return internal::glUniform2ui(location, v0, v1); }
	inline void Uniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2){ return internal::glUniform3ui(location, v0, v1, v2); }
	inline void Uniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3){ return internal::glUniform4ui(location, v0, v1, v2, v3); }
	inline void Uniform1uiv(GLint location, GLsizei count, const GLuint *value){ return internal::glUniform1uiv(location, count, value); }
	inline void Uniform2uiv(GLint location, GLsizei count, const GLuint *value){ return internal::glUniform2uiv(location, count, value); }
	inline void Uniform3uiv(GLint location, GLsizei count, const GLuint *value){ return internal::glUniform3uiv(location, count, value); }
	inline void Uniform4uiv(GLint location, GLsizei count, const GLuint *value){ return internal::glUniform4uiv(location, count, value); }
	inline void ClearBufferiv(GLenum buffer, GLint drawbuffer, const GLint *value){ return internal::glClearBufferiv(buffer, drawbuffer, value); }
	inline void ClearBufferuiv(GLenum buffer, GLint drawbuffer, const GLuint *value){ return internal::glClearBufferuiv(buffer, drawbuffer, value); }
	inline void ClearBufferfv(GLenum buffer, GLint drawbuffer, const GLfloat *value){ return internal::glClearBufferfv(buffer, drawbuffer, value); }
	inline void ClearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil){ return internal::glClearBufferfi(buffer, drawbuffer, depth, stencil); }
	inline const GLubyte *GetStringi(GLenum name, GLuint index){ return internal::glGetStringi(name, index); }
	inline void CopyBufferSubData(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size){ return internal::glCopyBufferSubData(readTarget, writeTarget, readOffset, writeOffset, size); }
	inline void GetUniformIndices(GLuint program, GLsizei uniformCount, const GLchar *const*uniformNames, GLuint *uniformIndices){ return internal::glGetUniformIndices(program, uniformCount, uniformNames, uniformIndices); }
	inline void GetActiveUniformsiv(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params){ return internal::glGetActiveUniformsiv(program, uniformCount, uniformIndices, pname, params); }
	inline GLuint GetUniformBlockIndex(GLuint program, const GLchar *uniformBlockName){ return internal::glGetUniformBlockIndex(program, uniformBlockName); }
	inline void GetActiveUniformBlockiv(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params){ return internal::glGetActiveUniformBlockiv(program, uniformBlockIndex, pname, params); }
	inline void GetActiveUniformBlockName(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName){ return internal::glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, length, uniformBlockName); }
	inline void UniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding){ return internal::glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding); }
	inline void DrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei instancecount){ return internal::glDrawArraysInstanced(mode, first, count, instancecount); }
	inline void DrawElementsInstanced(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount){ return internal::glDrawElementsInstanced(mode, count, type, indices, instancecount); }
	inline GLsync FenceSync(GLenum condition, GLbitfield flags){ return internal::glFenceSync(condition, flags); }
	inline GLboolean IsSync(GLsync sync){ return internal::glIsSync(sync); }
	inline void DeleteSync(GLsync sync){ return internal::glDeleteSync(sync); }
	inline GLenum ClientWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout){ return internal::glClientWaitSync(sync, flags, timeout); }
	inline void WaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout){ return internal::glWaitSync(sync, flags, timeout); }
	inline void GetInteger64v(GLenum pname, GLint64 *data){ return internal::glGetInteger64v(pname, data); }
	inline void GetSynciv(GLsync sync, GLenum pname, GLsizei count, GLsizei *length, GLint *values){ return internal::glGetSynciv(sync, pname, count, length, values); }
	inline void GetInteger64i_v(GLenum target, GLuint index, GLint64 *data){ return internal::glGetInteger64i_v(target, index, data); }
	inline void GetBufferParameteri64v(GLenum target, GLenum pname, GLint64 *params){ return internal::glGetBufferParameteri64v(target, pname, params); }
	inline void GenSamplers(GLsizei count, GLuint *samplers){ return internal::glGenSamplers(count, samplers); }
	inline void DeleteSamplers(GLsizei count, const GLuint *samplers){ return internal::glDeleteSamplers(count, samplers); }
	inline GLboolean IsSampler(GLuint sampler){ return internal::glIsSampler(sampler); }
	inline void BindSampler(GLuint unit, GLuint sampler){ return internal::glBindSampler(unit, sampler); }
	inline void SamplerParameteri(GLuint sampler, GLenum pname, GLint param){ return internal::glSamplerParameteri(sampler, pname, param); }
	inline void SamplerParameteriv(GLuint sampler, GLenum pname, const GLint *param){ return internal::glSamplerParameteriv(sampler, pname, param); }
	inline void SamplerParameterf(GLuint sampler, GLenum pname, GLfloat param){ return internal::glSamplerParameterf(sampler, pname, param); }
	inline void SamplerParameterfv(GLuint sampler, GLenum pname, const GLfloat *param){ return internal::glSamplerParameterfv(sampler, pname, param); }
	inline void GetSamplerParameteriv(GLuint sampler, GLenum pname, GLint *params){ return internal::glGetSamplerParameteriv(sampler, pname, params); }
	inline void GetSamplerParameterfv(GLuint sampler, GLenum pname, GLfloat *params){ return internal::glGetSamplerParameterfv(sampler, pname, params); }
	inline void VertexAttribDivisor(GLuint index, GLuint divisor){ return internal::glVertexAttribDivisor(index, divisor); }
	inline void BindTransformFeedback(GLenum target, GLuint id){ return internal::glBindTransformFeedback(target, id); }
	inline void DeleteTransformFeedbacks(GLsizei n, const GLuint *ids){ return internal::glDeleteTransformFeedbacks(n, ids); }
	inline void GenTransformFeedbacks(GLsizei n, GLuint *ids){ return internal::glGenTransformFeedbacks(n, ids); }
	inline GLboolean IsTransformFeedback(GLuint id){ return internal::glIsTransformFeedback(id); }
	inline void PauseTransformFeedback(){ return internal::glPauseTransformFeedback(); }
	inline void ResumeTransformFeedback(){ return internal::glResumeTransformFeedback(); }
	inline void GetProgramBinary(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary){ return internal::glGetProgramBinary(program, bufSize, length, binaryFormat, binary); }
	inline void ProgramBinary(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length){ return internal::glProgramBinary(program, binaryFormat, binary, length); }
	inline void ProgramParameteri(GLuint program, GLenum pname, GLint value){ return internal::glProgramParameteri(program, pname, value); }
	inline void InvalidateFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments){ return internal::glInvalidateFramebuffer(target, numAttachments, attachments); }
	inline void InvalidateSubFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height){ return internal::glInvalidateSubFramebuffer(target, numAttachments, attachments, x, y, width, height); }
	inline void TexStorage2D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height){ return internal::glTexStorage2D(target, levels, internalformat, width, height); }
	inline void TexStorage3D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth){ return internal::glTexStorage3D(target, levels, internalformat, width, height, depth); }
	inline void GetInternalformativ(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint *params){ return internal::glGetInternalformativ(target, internalformat, pname, count, params); }

	// OpenGL ES 3.1

	constexpr GLenum COMPUTE_SHADER = 0x91B9;
	constexpr GLenum MAX_COMPUTE_UNIFORM_BLOCKS = 0x91BB;
	constexpr GLenum MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0x91BC;
	constexpr GLenum MAX_COMPUTE_IMAGE_UNIFORMS = 0x91BD;
	constexpr GLenum MAX_COMPUTE_SHARED_MEMORY_SIZE = 0x8262;
	constexpr GLenum MAX_COMPUTE_UNIFORM_COMPONENTS = 0x8263;
	constexpr GLenum MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0x8264;
	constexpr GLenum MAX_COMPUTE_ATOMIC_COUNTERS = 0x8265;
	constexpr GLenum MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0x8266;
	constexpr GLenum MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0x90EB;
	constexpr GLenum MAX_COMPUTE_WORK_GROUP_COUNT = 0x91BE;
	constexpr GLenum MAX_COMPUTE_WORK_GROUP_SIZE = 0x91BF;
	constexpr GLenum COMPUTE_WORK_GROUP_SIZE = 0x8267;
	constexpr GLenum DISPATCH_INDIRECT_BUFFER = 0x90EE;
	constexpr GLenum DISPATCH_INDIRECT_BUFFER_BINDING = 0x90EF;
	constexpr GLenum COMPUTE_SHADER_BIT = 0x00000020;
	constexpr GLenum DRAW_INDIRECT_BUFFER = 0x8F3F;
	constexpr GLenum DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;
	constexpr GLenum MAX_UNIFORM_LOCATIONS = 0x826E;
	constexpr GLenum FRAMEBUFFER_DEFAULT_WIDTH = 0x9310;
	constexpr GLenum FRAMEBUFFER_DEFAULT_HEIGHT = 0x9311;
	constexpr GLenum FRAMEBUFFER_DEFAULT_SAMPLES = 0x9313;
	constexpr GLenum FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = 0x9314;
	constexpr GLenum MAX_FRAMEBUFFER_WIDTH = 0x9315;
	constexpr GLenum MAX_FRAMEBUFFER_HEIGHT = 0x9316;
	constexpr GLenum MAX_FRAMEBUFFER_SAMPLES = 0x9318;
	constexpr GLenum UNIFORM = 0x92E1;
	constexpr GLenum UNIFORM_BLOCK = 0x92E2;
	constexpr GLenum PROGRAM_INPUT = 0x92E3;
	constexpr GLenum PROGRAM_OUTPUT = 0x92E4;
	constexpr GLenum BUFFER_VARIABLE = 0x92E5;
	constexpr GLenum SHADER_STORAGE_BLOCK = 0x92E6;
	constexpr GLenum ATOMIC_COUNTER_BUFFER = 0x92C0;
	constexpr GLenum TRANSFORM_FEEDBACK_VARYING = 0x92F4;
	constexpr GLenum ACTIVE_RESOURCES = 0x92F5;
	constexpr GLenum MAX_NAME_LENGTH = 0x92F6;
	constexpr GLenum MAX_NUM_ACTIVE_VARIABLES = 0x92F7;
	constexpr GLenum NAME_LENGTH = 0x92F9;
	constexpr GLenum TYPE = 0x92FA;
	constexpr GLenum ARRAY_SIZE = 0x92FB;
	constexpr GLenum OFFSET = 0x92FC;
	constexpr GLenum BLOCK_INDEX = 0x92FD;
	constexpr GLenum ARRAY_STRIDE = 0x92FE;
	constexpr GLenum MATRIX_STRIDE = 0x92FF;
	constexpr GLenum IS_ROW_MAJOR = 0x9300;
	constexpr GLenum ATOMIC_COUNTER_BUFFER_INDEX = 0x9301;
	constexpr GLenum BUFFER_BINDING = 0x9302;
	constexpr GLenum BUFFER_DATA_SIZE = 0x9303;
	constexpr GLenum NUM_ACTIVE_VARIABLES = 0x9304;
	constexpr GLenum ACTIVE_VARIABLES = 0x9305;
	constexpr GLenum REFERENCED_BY_VERTEX_SHADER = 0x9306;
	constexpr GLenum REFERENCED_BY_FRAGMENT_SHADER = 0x930A;
	constexpr GLenum REFERENCED_BY_COMPUTE_SHADER = 0x930B;
	constexpr GLenum TOP_LEVEL_ARRAY_SIZE = 0x930C;
	constexpr GLenum TOP_LEVEL_ARRAY_STRIDE = 0x930D;
	constexpr GLenum LOCATION = 0x930E;
	constexpr GLenum VERTEX_SHADER_BIT = 0x00000001;
	constexpr GLenum FRAGMENT_SHADER_BIT = 0x00000002;
	constexpr GLenum ALL_SHADER_BITS = 0xFFFFFFFF;
	constexpr GLenum PROGRAM_SEPARABLE = 0x8258;
	constexpr GLenum ACTIVE_PROGRAM = 0x8259;
	constexpr GLenum PROGRAM_PIPELINE_BINDING = 0x825A;
	constexpr GLenum ATOMIC_COUNTER_BUFFER_BINDING = 0x92C1;
	constexpr GLenum ATOMIC_COUNTER_BUFFER_START = 0x92C2;
	constexpr GLenum ATOMIC_COUNTER_BUFFER_SIZE = 0x92C3;
	constexpr GLenum MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0x92CC;
	constexpr GLenum MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0x92D0;
	constexpr GLenum MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = 0x92D1;
	constexpr GLenum MAX_VERTEX_ATOMIC_COUNTERS = 0x92D2;
	constexpr GLenum MAX_FRAGMENT_ATOMIC_COUNTERS = 0x92D6;
	constexpr GLenum MAX_COMBINED_ATOMIC_COUNTERS = 0x92D7;
	constexpr GLenum MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0x92D8;
	constexpr GLenum MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0x92DC;
	constexpr GLenum ACTIVE_ATOMIC_COUNTER_BUFFERS = 0x92D9;
	constexpr GLenum UNSIGNED_INT_ATOMIC_COUNTER = 0x92DB;
	constexpr GLenum MAX_IMAGE_UNITS = 0x8F38;
	constexpr GLenum MAX_VERTEX_IMAGE_UNIFORMS = 0x90CA;
	constexpr GLenum MAX_FRAGMENT_IMAGE_UNIFORMS = 0x90CE;
	constexpr GLenum MAX_COMBINED_IMAGE_UNIFORMS = 0x90CF;
	constexpr GLenum IMAGE_BINDING_NAME = 0x8F3A;
	constexpr GLenum IMAGE_BINDING_LEVEL = 0x8F3B;
	constexpr GLenum IMAGE_BINDING_LAYERED = 0x8F3C;
	constexpr GLenum IMAGE_BINDING_LAYER = 0x8F3D;
	constexpr GLenum IMAGE_BINDING_ACCESS = 0x8F3E;
	constexpr GLenum IMAGE_BINDING_FORMAT = 0x906E;
	constexpr GLenum VERTEX_ATTRIB_ARRAY_BARRIER_BIT = 0x00000001;
	constexpr GLenum ELEMENT_ARRAY_BARRIER_BIT = 0x00000002;
	constexpr GLenum UNIFORM_BARRIER_BIT = 0x00000004;
	constexpr GLenum TEXTURE_FETCH_BARRIER_BIT = 0x00000008;
	constexpr GLenum SHADER_IMAGE_ACCESS_BARRIER_BIT = 0x00000020;
	constexpr GLenum COMMAND_BARRIER_BIT = 0x00000040;
	constexpr GLenum PIXEL_BUFFER_BARRIER_BIT = 0x00000080;
	constexpr GLenum TEXTURE_UPDATE_BARRIER_BIT = 0x00000100;
	constexpr GLenum BUFFER_UPDATE_BARRIER_BIT = 0x00000200;
	constexpr GLenum FRAMEBUFFER_BARRIER_BIT = 0x00000400;
	constexpr GLenum TRANSFORM_FEEDBACK_BARRIER_BIT = 0x00000800;
	constexpr GLenum ATOMIC_COUNTER_BARRIER_BIT = 0x00001000;
	constexpr GLenum ALL_BARRIER_BITS = 0xFFFFFFFF;
	constexpr GLenum IMAGE_2D = 0x904D;
	constexpr GLenum IMAGE_3D = 0x904E;
	constexpr GLenum IMAGE_CUBE = 0x9050;
	constexpr GLenum IMAGE_2D_ARRAY = 0x9053;
	constexpr GLenum INT_IMAGE_2D = 0x9058;
	constexpr GLenum INT_IMAGE_3D = 0x9059;
	constexpr GLenum INT_IMAGE_CUBE = 0x905B;
	constexpr GLenum INT_IMAGE_2D_ARRAY = 0x905E;
	constexpr GLenum UNSIGNED_INT_IMAGE_2D = 0x9063;
	constexpr GLenum UNSIGNED_INT_IMAGE_3D = 0x9064;
	constexpr GLenum UNSIGNED_INT_IMAGE_CUBE = 0x9066;
	constexpr GLenum UNSIGNED_INT_IMAGE_2D_ARRAY = 0x9069;
	constexpr GLenum IMAGE_FORMAT_COMPATIBILITY_TYPE = 0x90C7;
	constexpr GLenum IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = 0x90C8;
	constexpr GLenum IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = 0x90C9;
	constexpr GLenum READ_ONLY = 0x88B8;
	constexpr GLenum WRITE_ONLY = 0x88B9;
	constexpr GLenum READ_WRITE = 0x88BA;
	constexpr GLenum SHADER_STORAGE_BUFFER = 0x90D2;
	constexpr GLenum SHADER_STORAGE_BUFFER_BINDING = 0x90D3;
	constexpr GLenum SHADER_STORAGE_BUFFER_START = 0x90D4;
	constexpr GLenum SHADER_STORAGE_BUFFER_SIZE = 0x90D5;
	constexpr GLenum MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0x90D6;
	constexpr GLenum MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0x90DA;
	constexpr GLenum MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0x90DB;
	constexpr GLenum MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0x90DC;
	constexpr GLenum MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0x90DD;
	constexpr GLenum MAX_SHADER_STORAGE_BLOCK_SIZE = 0x90DE;
	constexpr GLenum SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0x90DF;
	constexpr GLenum SHADER_STORAGE_BARRIER_BIT = 0x00002000;
	constexpr GLenum MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0x8F39;
	constexpr GLenum DEPTH_STENCIL_TEXTURE_MODE = 0x90EA;
	constexpr GLenum STENCIL_INDEX = 0x1901;
	constexpr GLenum MIN_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5E;
	constexpr GLenum MAX_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5F;
	constexpr GLenum SAMPLE_POSITION = 0x8E50;
	constexpr GLenum SAMPLE_MASK = 0x8E51;
	constexpr GLenum SAMPLE_MASK_VALUE = 0x8E52;
	constexpr GLenum TEXTURE_2D_MULTISAMPLE = 0x9100;
	constexpr GLenum MAX_SAMPLE_MASK_WORDS = 0x8E59;
	constexpr GLenum MAX_COLOR_TEXTURE_SAMPLES = 0x910E;
	constexpr GLenum MAX_DEPTH_TEXTURE_SAMPLES = 0x910F;
	constexpr GLenum MAX_INTEGER_SAMPLES = 0x9110;
	constexpr GLenum TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
	constexpr GLenum TEXTURE_SAMPLES = 0x9106;
	constexpr GLenum TEXTURE_FIXED_SAMPLE_LOCATIONS = 0x9107;
	constexpr GLenum TEXTURE_WIDTH = 0x1000;
	constexpr GLenum TEXTURE_HEIGHT = 0x1001;
	constexpr GLenum TEXTURE_DEPTH = 0x8071;
	constexpr GLenum TEXTURE_INTERNAL_FORMAT = 0x1003;
	constexpr GLenum TEXTURE_RED_SIZE = 0x805C;
	constexpr GLenum TEXTURE_GREEN_SIZE = 0x805D;
	constexpr GLenum TEXTURE_BLUE_SIZE = 0x805E;
	constexpr GLenum TEXTURE_ALPHA_SIZE = 0x805F;
	constexpr GLenum TEXTURE_DEPTH_SIZE = 0x884A;
	constexpr GLenum TEXTURE_STENCIL_SIZE = 0x88F1;
	constexpr GLenum TEXTURE_SHARED_SIZE = 0x8C3F;
	constexpr GLenum TEXTURE_RED_TYPE = 0x8C10;
	constexpr GLenum TEXTURE_GREEN_TYPE = 0x8C11;
	constexpr GLenum TEXTURE_BLUE_TYPE = 0x8C12;
	constexpr GLenum TEXTURE_ALPHA_TYPE = 0x8C13;
	constexpr GLenum TEXTURE_DEPTH_TYPE = 0x8C16;
	constexpr GLenum TEXTURE_COMPRESSED = 0x86A1;
	constexpr GLenum SAMPLER_2D_MULTISAMPLE = 0x9108;
	constexpr GLenum INT_SAMPLER_2D_MULTISAMPLE = 0x9109;
	constexpr GLenum UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = 0x910A;
	constexpr GLenum VERTEX_ATTRIB_BINDING = 0x82D4;
	constexpr GLenum VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D5;
	constexpr GLenum VERTEX_BINDING_DIVISOR = 0x82D6;
	constexpr GLenum VERTEX_BINDING_OFFSET = 0x82D7;
	constexpr GLenum VERTEX_BINDING_STRIDE = 0x82D8;
	constexpr GLenum VERTEX_BINDING_BUFFER = 0x8F4F;
	constexpr GLenum MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
	constexpr GLenum MAX_VERTEX_ATTRIB_BINDINGS = 0x82DA;
	constexpr GLenum MAX_VERTEX_ATTRIB_STRIDE = 0x82E5;

	inline void DispatchCompute(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z){ return internal::glDispatchCompute(num_groups_x, num_groups_y, num_groups_z); }
	inline void DispatchComputeIndirect(GLintptr indirect){ return internal::glDispatchComputeIndirect(indirect); }
	inline void DrawArraysIndirect(GLenum mode, const void *indirect){ return internal::glDrawArraysIndirect(mode, indirect); }
	inline void DrawElementsIndirect(GLenum mode, GLenum type, const void *indirect){ return internal::glDrawElementsIndirect(mode, type, indirect); }
	inline void FramebufferParameteri(GLenum target, GLenum pname, GLint param){ return internal::glFramebufferParameteri(target, pname, param); }
	inline void GetFramebufferParameteriv(GLenum target, GLenum pname, GLint *params){ return internal::glGetFramebufferParameteriv(target, pname, params); }
	inline void GetProgramInterfaceiv(GLuint program, GLenum programInterface, GLenum pname, GLint *params){ return internal::glGetProgramInterfaceiv(program, programInterface, pname, params); }
	inline GLuint GetProgramResourceIndex(GLuint program, GLenum programInterface, const GLchar *name){ return internal::glGetProgramResourceIndex(program, programInterface, name); }
	inline void GetProgramResourceName(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name){ return internal::glGetProgramResourceName(program, programInterface, index, bufSize, length, name); }
	inline void GetProgramResourceiv(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei count, GLsizei *length, GLint *params){ return internal::glGetProgramResourceiv(program, programInterface, index, propCount, props, count, length, params); }
	inline GLint GetProgramResourceLocation(GLuint program, GLenum programInterface, const GLchar *name){ return internal::glGetProgramResourceLocation(program, programInterface, name); }
	inline void UseProgramStages(GLuint pipeline, GLbitfield stages, GLuint program){ return internal::glUseProgramStages(pipeline, stages, program); }
	inline void ActiveShaderProgram(GLuint pipeline, GLuint program){ return internal::glActiveShaderProgram(pipeline, program); }
	inline GLuint CreateShaderProgramv(GLenum type, GLsizei count, const GLchar *const*strings){ return internal::glCreateShaderProgramv(type, count, strings); }
	inline void BindProgramPipeline(GLuint pipeline){ return internal::glBindProgramPipeline(pipeline); }
	inline void DeleteProgramPipelines(GLsizei n, const GLuint *pipelines){ return internal::glDeleteProgramPipelines(n, pipelines); }
	inline void GenProgramPipelines(GLsizei n, GLuint *pipelines){ return internal::glGenProgramPipelines(n, pipelines); }
	inline GLboolean IsProgramPipeline(GLuint pipeline){ return internal::glIsProgramPipeline(pipeline); }
	inline void GetProgramPipelineiv(GLuint pipeline, GLenum pname, GLint *params){ return internal::glGetProgramPipelineiv(pipeline, pname, params); }
	inline void ProgramUniform1i(GLuint program, GLint location, GLint v0){ return internal::glProgramUniform1i(program, location, v0); }
	inline void ProgramUniform2i(GLuint program, GLint location, GLint v0, GLint v1){ return internal::glProgramUniform2i(program, location, v0, v1); }
	inline void ProgramUniform3i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2){ return internal::glProgramUniform3i(program, location, v0, v1, v2); }
	inline void ProgramUniform4i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3){ return internal::glProgramUniform4i(program, location, v0, v1, v2, v3); }
	inline void ProgramUniform1ui(GLuint program, GLint location, GLuint v0){ return internal::glProgramUniform1ui(program, location, v0); }
	inline void ProgramUniform2ui(GLuint program, GLint location, GLuint v0, GLuint v1){ return internal::glProgramUniform2ui(program, location, v0, v1); }
	inline void ProgramUniform3ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2){ return internal::glProgramUniform3ui(program, location, v0, v1, v2); }
	inline void ProgramUniform4ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3){ return internal::glProgramUniform4ui(program, location, v0, v1, v2, v3); }
	inline void ProgramUniform1f(GLuint program, GLint location, GLfloat v0){ return internal::glProgramUniform1f(program, location, v0); }
	inline void ProgramUniform2f(GLuint program, GLint location, GLfloat v0, GLfloat v1){ return internal::glProgramUniform2f(program, location, v0, v1); }
	inline void ProgramUniform3f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2){ return internal::glProgramUniform3f(program, location, v0, v1, v2); }
	inline void ProgramUniform4f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3){ return internal::glProgramUniform4f(program, location, v0, v1, v2, v3); }
	inline void ProgramUniform1iv(GLuint program, GLint location, GLsizei count, const GLint *value){ return internal::glProgramUniform1iv(program, location, count, value); }
	inline void ProgramUniform2iv(GLuint program, GLint location, GLsizei count, const GLint *value){ return internal::glProgramUniform2iv(program, location, count, value); }
	inline void ProgramUniform3iv(GLuint program, GLint location, GLsizei count, const GLint *value){ return internal::glProgramUniform3iv(program, location, count, value); }
	inline void ProgramUniform4iv(GLuint program, GLint location, GLsizei count, const GLint *value){ return internal::glProgramUniform4iv(program, location, count, value); }
	inline void ProgramUniform1uiv(GLuint program, GLint location, GLsizei count, const GLuint *value){ return internal::glProgramUniform1uiv(program, location, count, value); }
	inline void ProgramUniform2uiv(GLuint program, GLint location, GLsizei count, const GLuint *value){ return internal::glProgramUniform2uiv(program, location, count, value); }
	inline void ProgramUniform3uiv(GLuint program, GLint location, GLsizei count, const GLuint *value){ return internal::glProgramUniform3uiv(program, location, count, value); }
	inline void ProgramUniform4uiv(GLuint program, GLint location, GLsizei count, const GLuint *value){ return internal::glProgramUniform4uiv(program, location, count, value); }
	inline void ProgramUniform1fv(GLuint program, GLint location, GLsizei count, const GLfloat *value){ return internal::glProgramUniform1fv(program, location, count, value); }
	inline void ProgramUniform2fv(GLuint program, GLint location, GLsizei count, const GLfloat *value){ return internal::glProgramUniform2fv(program, location, count, value); }
	inline void ProgramUniform3fv(GLuint program, GLint location, GLsizei count, const GLfloat *value){ return internal::glProgramUniform3fv(program, location, count, value); }
	inline void ProgramUniform4fv(GLuint program, GLint location, GLsizei count, const GLfloat *value){ return internal::glProgramUniform4fv(program, location, count, value); }
	inline void ProgramUniformMatrix2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix2fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix3fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix4fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix2x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix2x3fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix3x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix3x2fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix2x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix2x4fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix4x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix4x2fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix3x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix3x4fv(program, location, count, transpose, value); }
	inline void ProgramUniformMatrix4x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value){ return internal::glProgramUniformMatrix4x3fv(program, location, count, transpose, value); }
	inline void ValidateProgramPipeline(GLuint pipeline){ return internal::glValidateProgramPipeline(pipeline); }
	inline void GetProgramPipelineInfoLog(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog){ return internal::glGetProgramPipelineInfoLog(pipeline, bufSize, length, infoLog); }
	inline void BindImageTexture(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format){ return internal::glBindImageTexture(unit, texture, level, layered, layer, access, format); }
	inline void GetBooleani_v(GLenum target, GLuint index, GLboolean *data){ return internal::glGetBooleani_v(target, index, data); }
	inline void MemoryBarrier(GLbitfield barriers){ return internal::glMemoryBarrier(barriers); }
	inline void MemoryBarrierByRegion(GLbitfield barriers){ return internal::glMemoryBarrierByRegion(barriers); }
	inline void TexStorage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations){ return internal::glTexStorage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations); }
	inline void GetMultisamplefv(GLenum pname, GLuint index, GLfloat *val){ return internal::glGetMultisamplefv(pname, index, val); }
	inline void SampleMaski(GLuint maskNumber, GLbitfield mask){ return internal::glSampleMaski(maskNumber, mask); }
	inline void GetTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint *params){ return internal::glGetTexLevelParameteriv(target, level, pname, params); }
	inline void GetTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat *params){ return internal::glGetTexLevelParameterfv(target, level, pname, params); }
	inline void BindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride){ return internal::glBindVertexBuffer(bindingindex, buffer, offset, stride); }
	inline void VertexAttribFormat(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset){ return internal::glVertexAttribFormat(attribindex, size, type, normalized, relativeoffset); }
	inline void VertexAttribIFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset){ return internal::glVertexAttribIFormat(attribindex, size, type, relativeoffset); }
	inline void VertexAttribBinding(GLuint attribindex, GLuint bindingindex){ return internal::glVertexAttribBinding(attribindex, bindingindex); }
	inline void VertexBindingDivisor(GLuint bindingindex, GLuint divisor){ return internal::glVertexBindingDivisor(bindingindex, divisor); }

	// OpenGL ES 3.2

	constexpr GLenum MULTISAMPLE_LINE_WIDTH_RANGE = 0x9381;
	constexpr GLenum MULTISAMPLE_LINE_WIDTH_GRANULARITY = 0x9382;
	constexpr GLenum MULTIPLY = 0x9294;
	constexpr GLenum SCREEN = 0x9295;
	constexpr GLenum OVERLAY = 0x9296;
	constexpr GLenum DARKEN = 0x9297;
	constexpr GLenum LIGHTEN = 0x9298;
	constexpr GLenum COLORDODGE = 0x9299;
	constexpr GLenum COLORBURN = 0x929A;
	constexpr GLenum HARDLIGHT = 0x929B;
	constexpr GLenum SOFTLIGHT = 0x929C;
	constexpr GLenum DIFFERENCE = 0x929E;
	constexpr GLenum EXCLUSION = 0x92A0;
	constexpr GLenum HSL_HUE = 0x92AD;
	constexpr GLenum HSL_SATURATION = 0x92AE;
	constexpr GLenum HSL_COLOR = 0x92AF;
	constexpr GLenum HSL_LUMINOSITY = 0x92B0;
	constexpr GLenum DEBUG_OUTPUT_SYNCHRONOUS = 0x8242;
	constexpr GLenum DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
	constexpr GLenum DEBUG_CALLBACK_FUNCTION = 0x8244;
	constexpr GLenum DEBUG_CALLBACK_USER_PARAM = 0x8245;
	constexpr GLenum DEBUG_SOURCE_API = 0x8246;
	constexpr GLenum DEBUG_SOURCE_WINDOW_SYSTEM = 0x8247;
	constexpr GLenum DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
	constexpr GLenum DEBUG_SOURCE_THIRD_PARTY = 0x8249;
	constexpr GLenum DEBUG_SOURCE_APPLICATION = 0x824A;
	constexpr GLenum DEBUG_SOURCE_OTHER = 0x824B;
	constexpr GLenum DEBUG_TYPE_ERROR = 0x824C;
	constexpr GLenum DEBUG_TYPE_DEPRECATED_BEHAVIOR = 0x824D;
	constexpr GLenum DEBUG_TYPE_UNDEFINED_BEHAVIOR = 0x824E;
	constexpr GLenum DEBUG_TYPE_PORTABILITY = 0x824F;
	constexpr GLenum DEBUG_TYPE_PERFORMANCE = 0x8250;
	constexpr GLenum DEBUG_TYPE_OTHER = 0x8251;
	constexpr GLenum DEBUG_TYPE_MARKER = 0x8268;
	constexpr GLenum DEBUG_TYPE_PUSH_GROUP = 0x8269;
	constexpr GLenum DEBUG_TYPE_POP_GROUP = 0x826A;
	constexpr GLenum DEBUG_SEVERITY_NOTIFICATION = 0x826B;
	constexpr GLenum MAX_DEBUG_GROUP_STACK_DEPTH = 0x826C;
	constexpr GLenum DEBUG_GROUP_STACK_DEPTH = 0x826D;
	constexpr GLenum BUFFER = 0x82E0;
	constexpr GLenum SHADER = 0x82E1;
	constexpr GLenum PROGRAM = 0x82E2;
	constexpr GLenum VERTEX_ARRAY = 0x8074;
	constexpr GLenum QUERY = 0x82E3;
	constexpr GLenum PROGRAM_PIPELINE = 0x82E4;
	constexpr GLenum SAMPLER = 0x82E6;
	constexpr GLenum MAX_LABEL_LENGTH = 0x82E8;
	constexpr GLenum MAX_DEBUG_MESSAGE_LENGTH = 0x9143;
	constexpr GLenum MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
	constexpr GLenum DEBUG_LOGGED_MESSAGES = 0x9145;
	constexpr GLenum DEBUG_SEVERITY_HIGH = 0x9146;
	constexpr GLenum DEBUG_SEVERITY_MEDIUM = 0x9147;
	constexpr GLenum DEBUG_SEVERITY_LOW = 0x9148;
	constexpr GLenum DEBUG_OUTPUT = 0x92E0;
	constexpr GLenum CONTEXT_FLAG_DEBUG_BIT = 0x00000002;
	constexpr GLenum STACK_OVERFLOW = 0x0503;
	constexpr GLenum STACK_UNDERFLOW = 0x0504;
	constexpr GLenum GEOMETRY_SHADER = 0x8DD9;
	constexpr GLenum GEOMETRY_SHADER_BIT = 0x00000004;
	constexpr GLenum GEOMETRY_VERTICES_OUT = 0x8916;
	constexpr GLenum GEOMETRY_INPUT_TYPE = 0x8917;
	constexpr GLenum GEOMETRY_OUTPUT_TYPE = 0x8918;
	constexpr GLenum GEOMETRY_SHADER_INVOCATIONS = 0x887F;
	constexpr GLenum LAYER_PROVOKING_VERTEX = 0x825E;
	constexpr GLenum LINES_ADJACENCY = 0x000A;
	constexpr GLenum LINE_STRIP_ADJACENCY = 0x000B;
	constexpr GLenum TRIANGLES_ADJACENCY = 0x000C;
	constexpr GLenum TRIANGLE_STRIP_ADJACENCY = 0x000D;
	constexpr GLenum MAX_GEOMETRY_UNIFORM_COMPONENTS = 0x8DDF;
	constexpr GLenum MAX_GEOMETRY_UNIFORM_BLOCKS = 0x8A2C;
	constexpr GLenum MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0x8A32;
	constexpr GLenum MAX_GEOMETRY_INPUT_COMPONENTS = 0x9123;
	constexpr GLenum MAX_GEOMETRY_OUTPUT_COMPONENTS = 0x9124;
	constexpr GLenum MAX_GEOMETRY_OUTPUT_VERTICES = 0x8DE0;
	constexpr GLenum MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0x8DE1;
	constexpr GLenum MAX_GEOMETRY_SHADER_INVOCATIONS = 0x8E5A;
	constexpr GLenum MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = 0x8C29;
	constexpr GLenum MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0x92CF;
	constexpr GLenum MAX_GEOMETRY_ATOMIC_COUNTERS = 0x92D5;
	constexpr GLenum MAX_GEOMETRY_IMAGE_UNIFORMS = 0x90CD;
	constexpr GLenum MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0x90D7;
	constexpr GLenum FIRST_VERTEX_CONVENTION = 0x8E4D;
	constexpr GLenum LAST_VERTEX_CONVENTION = 0x8E4E;
	constexpr GLenum UNDEFINED_VERTEX = 0x8260;
	constexpr GLenum PRIMITIVES_GENERATED = 0x8C87;
	constexpr GLenum FRAMEBUFFER_DEFAULT_LAYERS = 0x9312;
	constexpr GLenum MAX_FRAMEBUFFER_LAYERS = 0x9317;
	constexpr GLenum FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = 0x8DA8;
	constexpr GLenum FRAMEBUFFER_ATTACHMENT_LAYERED = 0x8DA7;
	constexpr GLenum REFERENCED_BY_GEOMETRY_SHADER = 0x9309;
	constexpr GLenum PRIMITIVE_BOUNDING_BOX = 0x92BE;
	constexpr GLenum CONTEXT_FLAG_ROBUST_ACCESS_BIT = 0x00000004;
	constexpr GLenum CONTEXT_FLAGS = 0x821E;
	constexpr GLenum LOSE_CONTEXT_ON_RESET = 0x8252;
	constexpr GLenum GUILTY_CONTEXT_RESET = 0x8253;
	constexpr GLenum INNOCENT_CONTEXT_RESET = 0x8254;
	constexpr GLenum UNKNOWN_CONTEXT_RESET = 0x8255;
	constexpr GLenum RESET_NOTIFICATION_STRATEGY = 0x8256;
	constexpr GLenum NO_RESET_NOTIFICATION = 0x8261;
	constexpr GLenum CONTEXT_LOST = 0x0507;
	constexpr GLenum SAMPLE_SHADING = 0x8C36;
	constexpr GLenum MIN_SAMPLE_SHADING_VALUE = 0x8C37;
	constexpr GLenum MIN_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5B;
	constexpr GLenum MAX_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5C;
	constexpr GLenum FRAGMENT_INTERPOLATION_OFFSET_BITS = 0x8E5D;
	constexpr GLenum PATCHES = 0x000E;
	constexpr GLenum PATCH_VERTICES = 0x8E72;
	constexpr GLenum TESS_CONTROL_OUTPUT_VERTICES = 0x8E75;
	constexpr GLenum TESS_GEN_MODE = 0x8E76;
	constexpr GLenum TESS_GEN_SPACING = 0x8E77;
	constexpr GLenum TESS_GEN_VERTEX_ORDER = 0x8E78;
	constexpr GLenum TESS_GEN_POINT_MODE = 0x8E79;
	constexpr GLenum ISOLINES = 0x8E7A;
	constexpr GLenum QUADS = 0x0007;
	constexpr GLenum FRACTIONAL_ODD = 0x8E7B;
	constexpr GLenum FRACTIONAL_EVEN = 0x8E7C;
	constexpr GLenum MAX_PATCH_VERTICES = 0x8E7D;
	constexpr GLenum MAX_TESS_GEN_LEVEL = 0x8E7E;
	constexpr GLenum MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E7F;
	constexpr GLenum MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E80;
	constexpr GLenum MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0x8E81;
	constexpr GLenum MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
	constexpr GLenum MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0x8E83;
	constexpr GLenum MAX_TESS_PATCH_COMPONENTS = 0x8E84;
	constexpr GLenum MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
	constexpr GLenum MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0x8E86;
	constexpr GLenum MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0x8E89;
	constexpr GLenum MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0x8E8A;
	constexpr GLenum MAX_TESS_CONTROL_INPUT_COMPONENTS = 0x886C;
	constexpr GLenum MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0x886D;
	constexpr GLenum MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E1E;
	constexpr GLenum MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E1F;
	constexpr GLenum MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0x92CD;
	constexpr GLenum MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0x92CE;
	constexpr GLenum MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0x92D3;
	constexpr GLenum MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0x92D4;
	constexpr GLenum MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0x90CB;
	constexpr GLenum MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0x90CC;
	constexpr GLenum MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0x90D8;
	constexpr GLenum MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0x90D9;
	constexpr GLenum PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED = 0x8221;
	constexpr GLenum IS_PER_PATCH = 0x92E7;
	constexpr GLenum REFERENCED_BY_TESS_CONTROL_SHADER = 0x9307;
	constexpr GLenum REFERENCED_BY_TESS_EVALUATION_SHADER = 0x9308;
	constexpr GLenum TESS_CONTROL_SHADER = 0x8E88;
	constexpr GLenum TESS_EVALUATION_SHADER = 0x8E87;
	constexpr GLenum TESS_CONTROL_SHADER_BIT = 0x00000008;
	constexpr GLenum TESS_EVALUATION_SHADER_BIT = 0x00000010;
	constexpr GLenum TEXTURE_BORDER_COLOR = 0x1004;
	constexpr GLenum CLAMP_TO_BORDER = 0x812D;
	constexpr GLenum TEXTURE_BUFFER = 0x8C2A;
	constexpr GLenum TEXTURE_BUFFER_BINDING = 0x8C2A;
	constexpr GLenum MAX_TEXTURE_BUFFER_SIZE = 0x8C2B;
	constexpr GLenum TEXTURE_BINDING_BUFFER = 0x8C2C;
	constexpr GLenum TEXTURE_BUFFER_DATA_STORE_BINDING = 0x8C2D;
	constexpr GLenum TEXTURE_BUFFER_OFFSET_ALIGNMENT = 0x919F;
	constexpr GLenum SAMPLER_BUFFER = 0x8DC2;
	constexpr GLenum INT_SAMPLER_BUFFER = 0x8DD0;
	constexpr GLenum UNSIGNED_INT_SAMPLER_BUFFER = 0x8DD8;
	constexpr GLenum IMAGE_BUFFER = 0x9051;
	constexpr GLenum INT_IMAGE_BUFFER = 0x905C;
	constexpr GLenum UNSIGNED_INT_IMAGE_BUFFER = 0x9067;
	constexpr GLenum TEXTURE_BUFFER_OFFSET = 0x919D;
	constexpr GLenum TEXTURE_BUFFER_SIZE = 0x919E;
	constexpr GLenum COMPRESSED_RGBA_ASTC_4x4 = 0x93B0;
	constexpr GLenum COMPRESSED_RGBA_ASTC_5x4 = 0x93B1;
	constexpr GLenum COMPRESSED_RGBA_ASTC_5x5 = 0x93B2;
	constexpr GLenum COMPRESSED_RGBA_ASTC_6x5 = 0x93B3;
	constexpr GLenum COMPRESSED_RGBA_ASTC_6x6 = 0x93B4;
	constexpr GLenum COMPRESSED_RGBA_ASTC_8x5 = 0x93B5;
	constexpr GLenum COMPRESSED_RGBA_ASTC_8x6 = 0x93B6;
	constexpr GLenum COMPRESSED_RGBA_ASTC_8x8 = 0x93B7;
	constexpr GLenum COMPRESSED_RGBA_ASTC_10x5 = 0x93B8;
	constexpr GLenum COMPRESSED_RGBA_ASTC_10x6 = 0x93B9;
	constexpr GLenum COMPRESSED_RGBA_ASTC_10x8 = 0x93BA;
	constexpr GLenum COMPRESSED_RGBA_ASTC_10x10 = 0x93BB;
	constexpr GLenum COMPRESSED_RGBA_ASTC_12x10 = 0x93BC;
	constexpr GLenum COMPRESSED_RGBA_ASTC_12x12 = 0x93BD;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_4x4 = 0x93D0;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_5x4 = 0x93D1;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_5x5 = 0x93D2;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_6x5 = 0x93D3;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_6x6 = 0x93D4;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_8x5 = 0x93D5;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_8x6 = 0x93D6;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_8x8 = 0x93D7;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_10x5 = 0x93D8;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_10x6 = 0x93D9;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_10x8 = 0x93DA;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_10x10 = 0x93DB;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_12x10 = 0x93DC;
	constexpr GLenum COMPRESSED_SRGB8_ALPHA8_ASTC_12x12 = 0x93DD;
	constexpr GLenum TEXTURE_CUBE_MAP_ARRAY = 0x9009;
	constexpr GLenum TEXTURE_BINDING_CUBE_MAP_ARRAY = 0x900A;
	constexpr GLenum SAMPLER_CUBE_MAP_ARRAY = 0x900C;
	constexpr GLenum SAMPLER_CUBE_MAP_ARRAY_SHADOW = 0x900D;
	constexpr GLenum INT_SAMPLER_CUBE_MAP_ARRAY = 0x900E;
	constexpr GLenum UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900F;
	constexpr GLenum IMAGE_CUBE_MAP_ARRAY = 0x9054;
	constexpr GLenum INT_IMAGE_CUBE_MAP_ARRAY = 0x905F;
	constexpr GLenum UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = 0x906A;
	constexpr GLenum TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
	constexpr GLenum TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
	constexpr GLenum SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910B;
	constexpr GLenum INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910C;
	constexpr GLenum UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910D;

	inline void BlendBarrier(){ return internal::glBlendBarrier(); }
	inline void CopyImageSubData(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth){ return internal::glCopyImageSubData(srcName, srcTarget, srcLevel, srcX, srcY, srcZ, dstName, dstTarget, dstLevel, dstX, dstY, dstZ, srcWidth, srcHeight, srcDepth); }
	inline void DebugMessageControl(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled){ return internal::glDebugMessageControl(source, type, severity, count, ids, enabled); }
	inline void DebugMessageInsert(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf){ return internal::glDebugMessageInsert(source, type, id, severity, length, buf); }
	inline void DebugMessageCallback(GLDEBUGPROC callback, const void *userParam){ return internal::glDebugMessageCallback(callback, userParam); }
	inline GLuint GetDebugMessageLog(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog){ return internal::glGetDebugMessageLog(count, bufSize, sources, types, ids, severities, lengths, messageLog); }
	inline void PushDebugGroup(GLenum source, GLuint id, GLsizei length, const GLchar *message){ return internal::glPushDebugGroup(source, id, length, message); }
	inline void PopDebugGroup(){ return internal::glPopDebugGroup(); }
	inline void ObjectLabel(GLenum identifier, GLuint name, GLsizei length, const GLchar *label){ return internal::glObjectLabel(identifier, name, length, label); }
	inline void GetObjectLabel(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label){ return internal::glGetObjectLabel(identifier, name, bufSize, length, label); }
	inline void ObjectPtrLabel(const void *ptr, GLsizei length, const GLchar *label){ return internal::glObjectPtrLabel(ptr, length, label); }
	inline void GetObjectPtrLabel(const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label){ return internal::glGetObjectPtrLabel(ptr, bufSize, length, label); }
	inline void GetPointerv(GLenum pname, void **params){ return internal::glGetPointerv(pname, params); }
	inline void Enablei(GLenum target, GLuint index){ return internal::glEnablei(target, index); }
	inline void Disablei(GLenum target, GLuint index){ return internal::glDisablei(target, index); }
	inline void BlendEquationi(GLuint buf, GLenum mode){ return internal::glBlendEquationi(buf, mode); }
	inline void BlendEquationSeparatei(GLuint buf, GLenum modeRGB, GLenum modeAlpha){ return internal::glBlendEquationSeparatei(buf, modeRGB, modeAlpha); }
	inline void BlendFunci(GLuint buf, GLenum src, GLenum dst){ return internal::glBlendFunci(buf, src, dst); }
	inline void BlendFuncSeparatei(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha){ return internal::glBlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha); }
	inline void ColorMaski(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a){ return internal::glColorMaski(index, r, g, b, a); }
	inline GLboolean IsEnabledi(GLenum target, GLuint index){ return internal::glIsEnabledi(target, index); }
	inline void DrawElementsBaseVertex(GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex){ return internal::glDrawElementsBaseVertex(mode, count, type, indices, basevertex); }
	inline void DrawRangeElementsBaseVertex(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex){ return internal::glDrawRangeElementsBaseVertex(mode, start, end, count, type, indices, basevertex); }
	inline void DrawElementsInstancedBaseVertex(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex){ return internal::glDrawElementsInstancedBaseVertex(mode, count, type, indices, instancecount, basevertex); }
	inline void FramebufferTexture(GLenum target, GLenum attachment, GLuint texture, GLint level){ return internal::glFramebufferTexture(target, attachment, texture, level); }
	inline void PrimitiveBoundingBox(GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW){ return internal::glPrimitiveBoundingBox(minX, minY, minZ, minW, maxX, maxY, maxZ, maxW); }
	inline GLenum GetGraphicsResetStatus(){ return internal::glGetGraphicsResetStatus(); }
	inline void ReadnPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data){ return internal::glReadnPixels(x, y, width, height, format, type, bufSize, data); }
	inline void GetnUniformfv(GLuint program, GLint location, GLsizei bufSize, GLfloat *params){ return internal::glGetnUniformfv(program, location, bufSize, params); }
	inline void GetnUniformiv(GLuint program, GLint location, GLsizei bufSize, GLint *params){ return internal::glGetnUniformiv(program, location, bufSize, params); }
	inline void GetnUniformuiv(GLuint program, GLint location, GLsizei bufSize, GLuint *params){ return internal::glGetnUniformuiv(program, location, bufSize, params); }
	inline void MinSampleShading(GLfloat value){ return internal::glMinSampleShading(value); }
	inline void PatchParameteri(GLenum pname, GLint value){ return internal::glPatchParameteri(pname, value); }
	inline void TexParameterIiv(GLenum target, GLenum pname, const GLint *params){ return internal::glTexParameterIiv(target, pname, params); }
	inline void TexParameterIuiv(GLenum target, GLenum pname, const GLuint *params){ return internal::glTexParameterIuiv(target, pname, params); }
	inline void GetTexParameterIiv(GLenum target, GLenum pname, GLint *params){ return internal::glGetTexParameterIiv(target, pname, params); }
	inline void GetTexParameterIuiv(GLenum target, GLenum pname, GLuint *params){ return internal::glGetTexParameterIuiv(target, pname, params); }
	inline void SamplerParameterIiv(GLuint sampler, GLenum pname, const GLint *param){ return internal::glSamplerParameterIiv(sampler, pname, param); }
	inline void SamplerParameterIuiv(GLuint sampler, GLenum pname, const GLuint *param){ return internal::glSamplerParameterIuiv(sampler, pname, param); }
	inline void GetSamplerParameterIiv(GLuint sampler, GLenum pname, GLint *params){ return internal::glGetSamplerParameterIiv(sampler, pname, params); }
	inline void GetSamplerParameterIuiv(GLuint sampler, GLenum pname, GLuint *params){ return internal::glGetSamplerParameterIuiv(sampler, pname, params); }
	inline void TexBuffer(GLenum target, GLenum internalformat, GLuint buffer){ return internal::glTexBuffer(target, internalformat, buffer); }
	inline void TexBufferRange(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size){ return internal::glTexBufferRange(target, internalformat, buffer, offset, size); }
	inline void TexStorage3DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations){ return internal::glTexStorage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations); }

	// GL_ARB_compute_shader

	constexpr GLenum UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER = 0x90EC;
	constexpr GLenum ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER = 0x90ED;


	// GL_ARB_shader_image_load_store

	constexpr GLenum MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = 0x8F39;
	constexpr GLenum IMAGE_1D = 0x904C;
	constexpr GLenum IMAGE_2D_RECT = 0x904F;
	constexpr GLenum IMAGE_1D_ARRAY = 0x9052;
	constexpr GLenum IMAGE_2D_MULTISAMPLE = 0x9055;
	constexpr GLenum IMAGE_2D_MULTISAMPLE_ARRAY = 0x9056;
	constexpr GLenum INT_IMAGE_1D = 0x9057;
	constexpr GLenum INT_IMAGE_2D_RECT = 0x905A;
	constexpr GLenum INT_IMAGE_1D_ARRAY = 0x905D;
	constexpr GLenum INT_IMAGE_2D_MULTISAMPLE = 0x9060;
	constexpr GLenum INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9061;
	constexpr GLenum UNSIGNED_INT_IMAGE_1D = 0x9062;
	constexpr GLenum UNSIGNED_INT_IMAGE_2D_RECT = 0x9065;
	constexpr GLenum UNSIGNED_INT_IMAGE_1D_ARRAY = 0x9068;
	constexpr GLenum UNSIGNED_INT_IMAGE_2D_MULTISAMPLE = 0x906B;
	constexpr GLenum UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x906C;
	constexpr GLenum MAX_IMAGE_SAMPLES = 0x906D;


	// GL_ARB_texture_filter_anisotropic

	constexpr GLenum TEXTURE_MAX_ANISOTROPY = 0x84FE;
	constexpr GLenum MAX_TEXTURE_MAX_ANISOTROPY = 0x84FF;


	namespace sys
	{

		bool initialize();
		const char * api();
		int major_version();
		int minor_version();

		bool has_extension(const char * name);
		bool ext_ARB_compute_shader();
		bool ext_ARB_shader_image_load_store();
		bool ext_ARB_texture_filter_anisotropic();

		const char * shader_prefix();

	} // namespace sys

} // namespace gl

#define PSEMEK_GLES 1

