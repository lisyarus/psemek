#include <psemek/gfx/gl.hpp>


#include <unordered_set>
#include <string>

#if defined(__APPLE__)
#include <dlfcn.h>
#elif defined(__sgi) || defined (__sun)
#include <dlfcn.h>
#include <stdio.h>
#elif defined(_WIN32)
#include <windows.h>
#elif defined(__ANDROID__)
#include <EGL/egl.h>
#else
#include <GL/glx.h>
#endif

namespace gl
{

	namespace internal
	{

		
		#if defined(__APPLE__)
		
		static void * get_proc_address(const char *func)
		{
			static void * image = dlopen("/System/Library/Frameworks/OpenGL.framework/Versions/Current/OpenGL", RTLD_LAZY);
		
			if (!image) return nullptr;
		
			return dlsym(image, func);
		}
		
		#elif defined(__sgi) || defined (__sun)
		
		static void * get_proc_address(const char *func)
		{
			static void * image = dlopen(nullptr, RTLD_LAZY | RTLD_LOCAL);
			static void * gpa = image ? dlsym(image, "glXGetProcAddress") : nullptr;
		
			if (gpa)
				return reinterpret_cast<void(*)(const GLubyte*)>(gpa)(reinterpret_cast<const char *>(name));
			else
				return dlsym(image, name);
		}
		
		#elif defined(_WIN32)
		
		#ifdef _MSC_VER
		#pragma warning(disable: 4055)
		#pragma warning(disable: 4054)
		#pragma warning(disable: 4996)
		#endif
		
		static int test_pointer(const PROC p)
		{
			ptrdiff_t i;
			if (!p) return 0;
			i = (ptrdiff_t)p;
		
			if(i == 1 || i == 2 || i == 3 || i == -1) return 0;
		
			return 1;
		}
		
		static void * get_proc_address(const char *name)
		{
			static HMODULE image = GetModuleHandleA("opengl32.dll");
		
			PROC func = wglGetProcAddress(reinterpret_cast<LPCSTR>(name));
			if (test_pointer(func))
			{
				return reinterpret_cast<void*>(func);
			}
		
			return reinterpret_cast<void*>(GetProcAddress(image, reinterpret_cast<LPCSTR>(name)));
		}
		
		#elif defined(__ANDROID__)
		
		static void * get_proc_address(const char *func)
		{
			return reinterpret_cast<void *>(eglGetProcAddress(func));
		}
			
		#else // GLX
		
		static void * get_proc_address(const char *func)
		{
			return reinterpret_cast<void *>(glXGetProcAddress(reinterpret_cast<const GLubyte*>(func)));
		}
		
		#endif

		// OpenGL ES 2.0

		void (*glActiveTexture)(GLenum texture) = nullptr;
		void (*glAttachShader)(GLuint program, GLuint shader) = nullptr;
		void (*glBindAttribLocation)(GLuint program, GLuint index, const GLchar *name) = nullptr;
		void (*glBindBuffer)(GLenum target, GLuint buffer) = nullptr;
		void (*glBindFramebuffer)(GLenum target, GLuint framebuffer) = nullptr;
		void (*glBindRenderbuffer)(GLenum target, GLuint renderbuffer) = nullptr;
		void (*glBindTexture)(GLenum target, GLuint texture) = nullptr;
		void (*glBlendColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) = nullptr;
		void (*glBlendEquation)(GLenum mode) = nullptr;
		void (*glBlendEquationSeparate)(GLenum modeRGB, GLenum modeAlpha) = nullptr;
		void (*glBlendFunc)(GLenum sfactor, GLenum dfactor) = nullptr;
		void (*glBlendFuncSeparate)(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha) = nullptr;
		void (*glBufferData)(GLenum target, GLsizeiptr size, const void *data, GLenum usage) = nullptr;
		void (*glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const void *data) = nullptr;
		GLenum (*glCheckFramebufferStatus)(GLenum target) = nullptr;
		void (*glClear)(GLbitfield mask) = nullptr;
		void (*glClearColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) = nullptr;
		void (*glClearDepthf)(GLfloat d) = nullptr;
		void (*glClearStencil)(GLint s) = nullptr;
		void (*glColorMask)(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) = nullptr;
		void (*glCompileShader)(GLuint shader) = nullptr;
		void (*glCompressedTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data) = nullptr;
		void (*glCompressedTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data) = nullptr;
		void (*glCopyTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border) = nullptr;
		void (*glCopyTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;
		GLuint (*glCreateProgram)() = nullptr;
		GLuint (*glCreateShader)(GLenum type) = nullptr;
		void (*glCullFace)(GLenum mode) = nullptr;
		void (*glDeleteBuffers)(GLsizei n, const GLuint *buffers) = nullptr;
		void (*glDeleteFramebuffers)(GLsizei n, const GLuint *framebuffers) = nullptr;
		void (*glDeleteProgram)(GLuint program) = nullptr;
		void (*glDeleteRenderbuffers)(GLsizei n, const GLuint *renderbuffers) = nullptr;
		void (*glDeleteShader)(GLuint shader) = nullptr;
		void (*glDeleteTextures)(GLsizei n, const GLuint *textures) = nullptr;
		void (*glDepthFunc)(GLenum func) = nullptr;
		void (*glDepthMask)(GLboolean flag) = nullptr;
		void (*glDepthRangef)(GLfloat n, GLfloat f) = nullptr;
		void (*glDetachShader)(GLuint program, GLuint shader) = nullptr;
		void (*glDisable)(GLenum cap) = nullptr;
		void (*glDisableVertexAttribArray)(GLuint index) = nullptr;
		void (*glDrawArrays)(GLenum mode, GLint first, GLsizei count) = nullptr;
		void (*glDrawElements)(GLenum mode, GLsizei count, GLenum type, const void *indices) = nullptr;
		void (*glEnable)(GLenum cap) = nullptr;
		void (*glEnableVertexAttribArray)(GLuint index) = nullptr;
		void (*glFinish)() = nullptr;
		void (*glFlush)() = nullptr;
		void (*glFramebufferRenderbuffer)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) = nullptr;
		void (*glFramebufferTexture2D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) = nullptr;
		void (*glFrontFace)(GLenum mode) = nullptr;
		void (*glGenBuffers)(GLsizei n, GLuint *buffers) = nullptr;
		void (*glGenerateMipmap)(GLenum target) = nullptr;
		void (*glGenFramebuffers)(GLsizei n, GLuint *framebuffers) = nullptr;
		void (*glGenRenderbuffers)(GLsizei n, GLuint *renderbuffers) = nullptr;
		void (*glGenTextures)(GLsizei n, GLuint *textures) = nullptr;
		void (*glGetActiveAttrib)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) = nullptr;
		void (*glGetActiveUniform)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) = nullptr;
		void (*glGetAttachedShaders)(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders) = nullptr;
		GLint (*glGetAttribLocation)(GLuint program, const GLchar *name) = nullptr;
		void (*glGetBooleanv)(GLenum pname, GLboolean *data) = nullptr;
		void (*glGetBufferParameteriv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		GLenum (*glGetError)() = nullptr;
		void (*glGetFloatv)(GLenum pname, GLfloat *data) = nullptr;
		void (*glGetFramebufferAttachmentParameteriv)(GLenum target, GLenum attachment, GLenum pname, GLint *params) = nullptr;
		void (*glGetIntegerv)(GLenum pname, GLint *data) = nullptr;
		void (*glGetProgramiv)(GLuint program, GLenum pname, GLint *params) = nullptr;
		void (*glGetProgramInfoLog)(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = nullptr;
		void (*glGetRenderbufferParameteriv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		void (*glGetShaderiv)(GLuint shader, GLenum pname, GLint *params) = nullptr;
		void (*glGetShaderInfoLog)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = nullptr;
		void (*glGetShaderPrecisionFormat)(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision) = nullptr;
		void (*glGetShaderSource)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source) = nullptr;
		const GLubyte *(*glGetString)(GLenum name) = nullptr;
		void (*glGetTexParameterfv)(GLenum target, GLenum pname, GLfloat *params) = nullptr;
		void (*glGetTexParameteriv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		void (*glGetUniformfv)(GLuint program, GLint location, GLfloat *params) = nullptr;
		void (*glGetUniformiv)(GLuint program, GLint location, GLint *params) = nullptr;
		GLint (*glGetUniformLocation)(GLuint program, const GLchar *name) = nullptr;
		void (*glGetVertexAttribfv)(GLuint index, GLenum pname, GLfloat *params) = nullptr;
		void (*glGetVertexAttribiv)(GLuint index, GLenum pname, GLint *params) = nullptr;
		void (*glGetVertexAttribPointerv)(GLuint index, GLenum pname, void **pointer) = nullptr;
		void (*glHint)(GLenum target, GLenum mode) = nullptr;
		GLboolean (*glIsBuffer)(GLuint buffer) = nullptr;
		GLboolean (*glIsEnabled)(GLenum cap) = nullptr;
		GLboolean (*glIsFramebuffer)(GLuint framebuffer) = nullptr;
		GLboolean (*glIsProgram)(GLuint program) = nullptr;
		GLboolean (*glIsRenderbuffer)(GLuint renderbuffer) = nullptr;
		GLboolean (*glIsShader)(GLuint shader) = nullptr;
		GLboolean (*glIsTexture)(GLuint texture) = nullptr;
		void (*glLineWidth)(GLfloat width) = nullptr;
		void (*glLinkProgram)(GLuint program) = nullptr;
		void (*glPixelStorei)(GLenum pname, GLint param) = nullptr;
		void (*glPolygonOffset)(GLfloat factor, GLfloat units) = nullptr;
		void (*glReadPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels) = nullptr;
		void (*glReleaseShaderCompiler)() = nullptr;
		void (*glRenderbufferStorage)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height) = nullptr;
		void (*glSampleCoverage)(GLfloat value, GLboolean invert) = nullptr;
		void (*glScissor)(GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;
		void (*glShaderBinary)(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length) = nullptr;
		void (*glShaderSource)(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length) = nullptr;
		void (*glStencilFunc)(GLenum func, GLint ref, GLuint mask) = nullptr;
		void (*glStencilFuncSeparate)(GLenum face, GLenum func, GLint ref, GLuint mask) = nullptr;
		void (*glStencilMask)(GLuint mask) = nullptr;
		void (*glStencilMaskSeparate)(GLenum face, GLuint mask) = nullptr;
		void (*glStencilOp)(GLenum fail, GLenum zfail, GLenum zpass) = nullptr;
		void (*glStencilOpSeparate)(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) = nullptr;
		void (*glTexImage2D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels) = nullptr;
		void (*glTexParameterf)(GLenum target, GLenum pname, GLfloat param) = nullptr;
		void (*glTexParameterfv)(GLenum target, GLenum pname, const GLfloat *params) = nullptr;
		void (*glTexParameteri)(GLenum target, GLenum pname, GLint param) = nullptr;
		void (*glTexParameteriv)(GLenum target, GLenum pname, const GLint *params) = nullptr;
		void (*glTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels) = nullptr;
		void (*glUniform1f)(GLint location, GLfloat v0) = nullptr;
		void (*glUniform1fv)(GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glUniform1i)(GLint location, GLint v0) = nullptr;
		void (*glUniform1iv)(GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glUniform2f)(GLint location, GLfloat v0, GLfloat v1) = nullptr;
		void (*glUniform2fv)(GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glUniform2i)(GLint location, GLint v0, GLint v1) = nullptr;
		void (*glUniform2iv)(GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glUniform3f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) = nullptr;
		void (*glUniform3fv)(GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glUniform3i)(GLint location, GLint v0, GLint v1, GLint v2) = nullptr;
		void (*glUniform3iv)(GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glUniform4f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) = nullptr;
		void (*glUniform4fv)(GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glUniform4i)(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) = nullptr;
		void (*glUniform4iv)(GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glUniformMatrix2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUseProgram)(GLuint program) = nullptr;
		void (*glValidateProgram)(GLuint program) = nullptr;
		void (*glVertexAttrib1f)(GLuint index, GLfloat x) = nullptr;
		void (*glVertexAttrib1fv)(GLuint index, const GLfloat *v) = nullptr;
		void (*glVertexAttrib2f)(GLuint index, GLfloat x, GLfloat y) = nullptr;
		void (*glVertexAttrib2fv)(GLuint index, const GLfloat *v) = nullptr;
		void (*glVertexAttrib3f)(GLuint index, GLfloat x, GLfloat y, GLfloat z) = nullptr;
		void (*glVertexAttrib3fv)(GLuint index, const GLfloat *v) = nullptr;
		void (*glVertexAttrib4f)(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w) = nullptr;
		void (*glVertexAttrib4fv)(GLuint index, const GLfloat *v) = nullptr;
		void (*glVertexAttribPointer)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer) = nullptr;
		void (*glViewport)(GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;

		// OpenGL ES 3.0

		void (*glReadBuffer)(GLenum src) = nullptr;
		void (*glDrawRangeElements)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices) = nullptr;
		void (*glTexImage3D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels) = nullptr;
		void (*glTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels) = nullptr;
		void (*glCopyTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;
		void (*glCompressedTexImage3D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data) = nullptr;
		void (*glCompressedTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data) = nullptr;
		void (*glGenQueries)(GLsizei n, GLuint *ids) = nullptr;
		void (*glDeleteQueries)(GLsizei n, const GLuint *ids) = nullptr;
		GLboolean (*glIsQuery)(GLuint id) = nullptr;
		void (*glBeginQuery)(GLenum target, GLuint id) = nullptr;
		void (*glEndQuery)(GLenum target) = nullptr;
		void (*glGetQueryiv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		void (*glGetQueryObjectuiv)(GLuint id, GLenum pname, GLuint *params) = nullptr;
		GLboolean (*glUnmapBuffer)(GLenum target) = nullptr;
		void (*glGetBufferPointerv)(GLenum target, GLenum pname, void **params) = nullptr;
		void (*glDrawBuffers)(GLsizei n, const GLenum *bufs) = nullptr;
		void (*glUniformMatrix2x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix3x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix2x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix4x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix3x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glUniformMatrix4x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glBlitFramebuffer)(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) = nullptr;
		void (*glRenderbufferStorageMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) = nullptr;
		void (*glFramebufferTextureLayer)(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer) = nullptr;
		void *(*glMapBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access) = nullptr;
		void (*glFlushMappedBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length) = nullptr;
		void (*glBindVertexArray)(GLuint array) = nullptr;
		void (*glDeleteVertexArrays)(GLsizei n, const GLuint *arrays) = nullptr;
		void (*glGenVertexArrays)(GLsizei n, GLuint *arrays) = nullptr;
		GLboolean (*glIsVertexArray)(GLuint array) = nullptr;
		void (*glGetIntegeri_v)(GLenum target, GLuint index, GLint *data) = nullptr;
		void (*glBeginTransformFeedback)(GLenum primitiveMode) = nullptr;
		void (*glEndTransformFeedback)() = nullptr;
		void (*glBindBufferRange)(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) = nullptr;
		void (*glBindBufferBase)(GLenum target, GLuint index, GLuint buffer) = nullptr;
		void (*glTransformFeedbackVaryings)(GLuint program, GLsizei count, const GLchar *const*varyings, GLenum bufferMode) = nullptr;
		void (*glGetTransformFeedbackVarying)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, GLchar *name) = nullptr;
		void (*glVertexAttribIPointer)(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) = nullptr;
		void (*glGetVertexAttribIiv)(GLuint index, GLenum pname, GLint *params) = nullptr;
		void (*glGetVertexAttribIuiv)(GLuint index, GLenum pname, GLuint *params) = nullptr;
		void (*glVertexAttribI4i)(GLuint index, GLint x, GLint y, GLint z, GLint w) = nullptr;
		void (*glVertexAttribI4ui)(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w) = nullptr;
		void (*glVertexAttribI4iv)(GLuint index, const GLint *v) = nullptr;
		void (*glVertexAttribI4uiv)(GLuint index, const GLuint *v) = nullptr;
		void (*glGetUniformuiv)(GLuint program, GLint location, GLuint *params) = nullptr;
		GLint (*glGetFragDataLocation)(GLuint program, const GLchar *name) = nullptr;
		void (*glUniform1ui)(GLint location, GLuint v0) = nullptr;
		void (*glUniform2ui)(GLint location, GLuint v0, GLuint v1) = nullptr;
		void (*glUniform3ui)(GLint location, GLuint v0, GLuint v1, GLuint v2) = nullptr;
		void (*glUniform4ui)(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) = nullptr;
		void (*glUniform1uiv)(GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glUniform2uiv)(GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glUniform3uiv)(GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glUniform4uiv)(GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glClearBufferiv)(GLenum buffer, GLint drawbuffer, const GLint *value) = nullptr;
		void (*glClearBufferuiv)(GLenum buffer, GLint drawbuffer, const GLuint *value) = nullptr;
		void (*glClearBufferfv)(GLenum buffer, GLint drawbuffer, const GLfloat *value) = nullptr;
		void (*glClearBufferfi)(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) = nullptr;
		const GLubyte *(*glGetStringi)(GLenum name, GLuint index) = nullptr;
		void (*glCopyBufferSubData)(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) = nullptr;
		void (*glGetUniformIndices)(GLuint program, GLsizei uniformCount, const GLchar *const*uniformNames, GLuint *uniformIndices) = nullptr;
		void (*glGetActiveUniformsiv)(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params) = nullptr;
		GLuint (*glGetUniformBlockIndex)(GLuint program, const GLchar *uniformBlockName) = nullptr;
		void (*glGetActiveUniformBlockiv)(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params) = nullptr;
		void (*glGetActiveUniformBlockName)(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName) = nullptr;
		void (*glUniformBlockBinding)(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding) = nullptr;
		void (*glDrawArraysInstanced)(GLenum mode, GLint first, GLsizei count, GLsizei instancecount) = nullptr;
		void (*glDrawElementsInstanced)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount) = nullptr;
		GLsync (*glFenceSync)(GLenum condition, GLbitfield flags) = nullptr;
		GLboolean (*glIsSync)(GLsync sync) = nullptr;
		void (*glDeleteSync)(GLsync sync) = nullptr;
		GLenum (*glClientWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout) = nullptr;
		void (*glWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout) = nullptr;
		void (*glGetInteger64v)(GLenum pname, GLint64 *data) = nullptr;
		void (*glGetSynciv)(GLsync sync, GLenum pname, GLsizei count, GLsizei *length, GLint *values) = nullptr;
		void (*glGetInteger64i_v)(GLenum target, GLuint index, GLint64 *data) = nullptr;
		void (*glGetBufferParameteri64v)(GLenum target, GLenum pname, GLint64 *params) = nullptr;
		void (*glGenSamplers)(GLsizei count, GLuint *samplers) = nullptr;
		void (*glDeleteSamplers)(GLsizei count, const GLuint *samplers) = nullptr;
		GLboolean (*glIsSampler)(GLuint sampler) = nullptr;
		void (*glBindSampler)(GLuint unit, GLuint sampler) = nullptr;
		void (*glSamplerParameteri)(GLuint sampler, GLenum pname, GLint param) = nullptr;
		void (*glSamplerParameteriv)(GLuint sampler, GLenum pname, const GLint *param) = nullptr;
		void (*glSamplerParameterf)(GLuint sampler, GLenum pname, GLfloat param) = nullptr;
		void (*glSamplerParameterfv)(GLuint sampler, GLenum pname, const GLfloat *param) = nullptr;
		void (*glGetSamplerParameteriv)(GLuint sampler, GLenum pname, GLint *params) = nullptr;
		void (*glGetSamplerParameterfv)(GLuint sampler, GLenum pname, GLfloat *params) = nullptr;
		void (*glVertexAttribDivisor)(GLuint index, GLuint divisor) = nullptr;
		void (*glBindTransformFeedback)(GLenum target, GLuint id) = nullptr;
		void (*glDeleteTransformFeedbacks)(GLsizei n, const GLuint *ids) = nullptr;
		void (*glGenTransformFeedbacks)(GLsizei n, GLuint *ids) = nullptr;
		GLboolean (*glIsTransformFeedback)(GLuint id) = nullptr;
		void (*glPauseTransformFeedback)() = nullptr;
		void (*glResumeTransformFeedback)() = nullptr;
		void (*glGetProgramBinary)(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary) = nullptr;
		void (*glProgramBinary)(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length) = nullptr;
		void (*glProgramParameteri)(GLuint program, GLenum pname, GLint value) = nullptr;
		void (*glInvalidateFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments) = nullptr;
		void (*glInvalidateSubFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;
		void (*glTexStorage2D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) = nullptr;
		void (*glTexStorage3D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) = nullptr;
		void (*glGetInternalformativ)(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint *params) = nullptr;

		// OpenGL ES 3.1

		void (*glDispatchCompute)(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z) = nullptr;
		void (*glDispatchComputeIndirect)(GLintptr indirect) = nullptr;
		void (*glDrawArraysIndirect)(GLenum mode, const void *indirect) = nullptr;
		void (*glDrawElementsIndirect)(GLenum mode, GLenum type, const void *indirect) = nullptr;
		void (*glFramebufferParameteri)(GLenum target, GLenum pname, GLint param) = nullptr;
		void (*glGetFramebufferParameteriv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		void (*glGetProgramInterfaceiv)(GLuint program, GLenum programInterface, GLenum pname, GLint *params) = nullptr;
		GLuint (*glGetProgramResourceIndex)(GLuint program, GLenum programInterface, const GLchar *name) = nullptr;
		void (*glGetProgramResourceName)(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) = nullptr;
		void (*glGetProgramResourceiv)(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei count, GLsizei *length, GLint *params) = nullptr;
		GLint (*glGetProgramResourceLocation)(GLuint program, GLenum programInterface, const GLchar *name) = nullptr;
		void (*glUseProgramStages)(GLuint pipeline, GLbitfield stages, GLuint program) = nullptr;
		void (*glActiveShaderProgram)(GLuint pipeline, GLuint program) = nullptr;
		GLuint (*glCreateShaderProgramv)(GLenum type, GLsizei count, const GLchar *const*strings) = nullptr;
		void (*glBindProgramPipeline)(GLuint pipeline) = nullptr;
		void (*glDeleteProgramPipelines)(GLsizei n, const GLuint *pipelines) = nullptr;
		void (*glGenProgramPipelines)(GLsizei n, GLuint *pipelines) = nullptr;
		GLboolean (*glIsProgramPipeline)(GLuint pipeline) = nullptr;
		void (*glGetProgramPipelineiv)(GLuint pipeline, GLenum pname, GLint *params) = nullptr;
		void (*glProgramUniform1i)(GLuint program, GLint location, GLint v0) = nullptr;
		void (*glProgramUniform2i)(GLuint program, GLint location, GLint v0, GLint v1) = nullptr;
		void (*glProgramUniform3i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2) = nullptr;
		void (*glProgramUniform4i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3) = nullptr;
		void (*glProgramUniform1ui)(GLuint program, GLint location, GLuint v0) = nullptr;
		void (*glProgramUniform2ui)(GLuint program, GLint location, GLuint v0, GLuint v1) = nullptr;
		void (*glProgramUniform3ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2) = nullptr;
		void (*glProgramUniform4ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) = nullptr;
		void (*glProgramUniform1f)(GLuint program, GLint location, GLfloat v0) = nullptr;
		void (*glProgramUniform2f)(GLuint program, GLint location, GLfloat v0, GLfloat v1) = nullptr;
		void (*glProgramUniform3f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2) = nullptr;
		void (*glProgramUniform4f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) = nullptr;
		void (*glProgramUniform1iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glProgramUniform2iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glProgramUniform3iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glProgramUniform4iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = nullptr;
		void (*glProgramUniform1uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glProgramUniform2uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glProgramUniform3uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glProgramUniform4uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = nullptr;
		void (*glProgramUniform1fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glProgramUniform2fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glProgramUniform3fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glProgramUniform4fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix2x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix3x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix2x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix4x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix3x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glProgramUniformMatrix4x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = nullptr;
		void (*glValidateProgramPipeline)(GLuint pipeline) = nullptr;
		void (*glGetProgramPipelineInfoLog)(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = nullptr;
		void (*glBindImageTexture)(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format) = nullptr;
		void (*glGetBooleani_v)(GLenum target, GLuint index, GLboolean *data) = nullptr;
		void (*glMemoryBarrier)(GLbitfield barriers) = nullptr;
		void (*glMemoryBarrierByRegion)(GLbitfield barriers) = nullptr;
		void (*glTexStorage2DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) = nullptr;
		void (*glGetMultisamplefv)(GLenum pname, GLuint index, GLfloat *val) = nullptr;
		void (*glSampleMaski)(GLuint maskNumber, GLbitfield mask) = nullptr;
		void (*glGetTexLevelParameteriv)(GLenum target, GLint level, GLenum pname, GLint *params) = nullptr;
		void (*glGetTexLevelParameterfv)(GLenum target, GLint level, GLenum pname, GLfloat *params) = nullptr;
		void (*glBindVertexBuffer)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) = nullptr;
		void (*glVertexAttribFormat)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset) = nullptr;
		void (*glVertexAttribIFormat)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) = nullptr;
		void (*glVertexAttribBinding)(GLuint attribindex, GLuint bindingindex) = nullptr;
		void (*glVertexBindingDivisor)(GLuint bindingindex, GLuint divisor) = nullptr;

		// OpenGL ES 3.2

		void (*glBlendBarrier)() = nullptr;
		void (*glCopyImageSubData)(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth) = nullptr;
		void (*glDebugMessageControl)(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled) = nullptr;
		void (*glDebugMessageInsert)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf) = nullptr;
		void (*glDebugMessageCallback)(GLDEBUGPROC callback, const void *userParam) = nullptr;
		GLuint (*glGetDebugMessageLog)(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog) = nullptr;
		void (*glPushDebugGroup)(GLenum source, GLuint id, GLsizei length, const GLchar *message) = nullptr;
		void (*glPopDebugGroup)() = nullptr;
		void (*glObjectLabel)(GLenum identifier, GLuint name, GLsizei length, const GLchar *label) = nullptr;
		void (*glGetObjectLabel)(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label) = nullptr;
		void (*glObjectPtrLabel)(const void *ptr, GLsizei length, const GLchar *label) = nullptr;
		void (*glGetObjectPtrLabel)(const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label) = nullptr;
		void (*glGetPointerv)(GLenum pname, void **params) = nullptr;
		void (*glEnablei)(GLenum target, GLuint index) = nullptr;
		void (*glDisablei)(GLenum target, GLuint index) = nullptr;
		void (*glBlendEquationi)(GLuint buf, GLenum mode) = nullptr;
		void (*glBlendEquationSeparatei)(GLuint buf, GLenum modeRGB, GLenum modeAlpha) = nullptr;
		void (*glBlendFunci)(GLuint buf, GLenum src, GLenum dst) = nullptr;
		void (*glBlendFuncSeparatei)(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) = nullptr;
		void (*glColorMaski)(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a) = nullptr;
		GLboolean (*glIsEnabledi)(GLenum target, GLuint index) = nullptr;
		void (*glDrawElementsBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex) = nullptr;
		void (*glDrawRangeElementsBaseVertex)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex) = nullptr;
		void (*glDrawElementsInstancedBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex) = nullptr;
		void (*glFramebufferTexture)(GLenum target, GLenum attachment, GLuint texture, GLint level) = nullptr;
		void (*glPrimitiveBoundingBox)(GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW) = nullptr;
		GLenum (*glGetGraphicsResetStatus)() = nullptr;
		void (*glReadnPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data) = nullptr;
		void (*glGetnUniformfv)(GLuint program, GLint location, GLsizei bufSize, GLfloat *params) = nullptr;
		void (*glGetnUniformiv)(GLuint program, GLint location, GLsizei bufSize, GLint *params) = nullptr;
		void (*glGetnUniformuiv)(GLuint program, GLint location, GLsizei bufSize, GLuint *params) = nullptr;
		void (*glMinSampleShading)(GLfloat value) = nullptr;
		void (*glPatchParameteri)(GLenum pname, GLint value) = nullptr;
		void (*glTexParameterIiv)(GLenum target, GLenum pname, const GLint *params) = nullptr;
		void (*glTexParameterIuiv)(GLenum target, GLenum pname, const GLuint *params) = nullptr;
		void (*glGetTexParameterIiv)(GLenum target, GLenum pname, GLint *params) = nullptr;
		void (*glGetTexParameterIuiv)(GLenum target, GLenum pname, GLuint *params) = nullptr;
		void (*glSamplerParameterIiv)(GLuint sampler, GLenum pname, const GLint *param) = nullptr;
		void (*glSamplerParameterIuiv)(GLuint sampler, GLenum pname, const GLuint *param) = nullptr;
		void (*glGetSamplerParameterIiv)(GLuint sampler, GLenum pname, GLint *params) = nullptr;
		void (*glGetSamplerParameterIuiv)(GLuint sampler, GLenum pname, GLuint *params) = nullptr;
		void (*glTexBuffer)(GLenum target, GLenum internalformat, GLuint buffer) = nullptr;
		void (*glTexBufferRange)(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) = nullptr;
		void (*glTexStorage3DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) = nullptr;

		// GL_ARB_compute_shader


		// GL_ARB_shader_image_load_store


		// GL_ARB_texture_filter_anisotropic


	} // namespace internal

	namespace sys
	{

		static bool ext_GL_ARB_compute_shader_loaded = false;
		static bool ext_GL_ARB_shader_image_load_store_loaded = false;
		static bool ext_GL_ARB_texture_filter_anisotropic_loaded = false;

		static bool load_core()
		{
			// OpenGL ES 2.0

			internal::glActiveTexture = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glActiveTexture"));
			if (!internal::glActiveTexture) return false;
			internal::glAttachShader = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glAttachShader"));
			if (!internal::glAttachShader) return false;
			internal::glBindAttribLocation = reinterpret_cast<void (*)(GLuint , GLuint , const GLchar *)>(internal::get_proc_address("glBindAttribLocation"));
			if (!internal::glBindAttribLocation) return false;
			internal::glBindBuffer = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBindBuffer"));
			if (!internal::glBindBuffer) return false;
			internal::glBindFramebuffer = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBindFramebuffer"));
			if (!internal::glBindFramebuffer) return false;
			internal::glBindRenderbuffer = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBindRenderbuffer"));
			if (!internal::glBindRenderbuffer) return false;
			internal::glBindTexture = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBindTexture"));
			if (!internal::glBindTexture) return false;
			internal::glBlendColor = reinterpret_cast<void (*)(GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glBlendColor"));
			if (!internal::glBlendColor) return false;
			internal::glBlendEquation = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glBlendEquation"));
			if (!internal::glBlendEquation) return false;
			internal::glBlendEquationSeparate = reinterpret_cast<void (*)(GLenum , GLenum )>(internal::get_proc_address("glBlendEquationSeparate"));
			if (!internal::glBlendEquationSeparate) return false;
			internal::glBlendFunc = reinterpret_cast<void (*)(GLenum , GLenum )>(internal::get_proc_address("glBlendFunc"));
			if (!internal::glBlendFunc) return false;
			internal::glBlendFuncSeparate = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLenum )>(internal::get_proc_address("glBlendFuncSeparate"));
			if (!internal::glBlendFuncSeparate) return false;
			internal::glBufferData = reinterpret_cast<void (*)(GLenum , GLsizeiptr , const void *, GLenum )>(internal::get_proc_address("glBufferData"));
			if (!internal::glBufferData) return false;
			internal::glBufferSubData = reinterpret_cast<void (*)(GLenum , GLintptr , GLsizeiptr , const void *)>(internal::get_proc_address("glBufferSubData"));
			if (!internal::glBufferSubData) return false;
			internal::glCheckFramebufferStatus = reinterpret_cast<GLenum (*)(GLenum )>(internal::get_proc_address("glCheckFramebufferStatus"));
			if (!internal::glCheckFramebufferStatus) return false;
			internal::glClear = reinterpret_cast<void (*)(GLbitfield )>(internal::get_proc_address("glClear"));
			if (!internal::glClear) return false;
			internal::glClearColor = reinterpret_cast<void (*)(GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glClearColor"));
			if (!internal::glClearColor) return false;
			internal::glClearDepthf = reinterpret_cast<void (*)(GLfloat )>(internal::get_proc_address("glClearDepthf"));
			if (!internal::glClearDepthf) return false;
			internal::glClearStencil = reinterpret_cast<void (*)(GLint )>(internal::get_proc_address("glClearStencil"));
			if (!internal::glClearStencil) return false;
			internal::glColorMask = reinterpret_cast<void (*)(GLboolean , GLboolean , GLboolean , GLboolean )>(internal::get_proc_address("glColorMask"));
			if (!internal::glColorMask) return false;
			internal::glCompileShader = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glCompileShader"));
			if (!internal::glCompileShader) return false;
			internal::glCompressedTexImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLenum , GLsizei , GLsizei , GLint , GLsizei , const void *)>(internal::get_proc_address("glCompressedTexImage2D"));
			if (!internal::glCompressedTexImage2D) return false;
			internal::glCompressedTexSubImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLsizei , GLsizei , GLenum , GLsizei , const void *)>(internal::get_proc_address("glCompressedTexSubImage2D"));
			if (!internal::glCompressedTexSubImage2D) return false;
			internal::glCopyTexImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLenum , GLint , GLint , GLsizei , GLsizei , GLint )>(internal::get_proc_address("glCopyTexImage2D"));
			if (!internal::glCopyTexImage2D) return false;
			internal::glCopyTexSubImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLint , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glCopyTexSubImage2D"));
			if (!internal::glCopyTexSubImage2D) return false;
			internal::glCreateProgram = reinterpret_cast<GLuint (*)()>(internal::get_proc_address("glCreateProgram"));
			if (!internal::glCreateProgram) return false;
			internal::glCreateShader = reinterpret_cast<GLuint (*)(GLenum )>(internal::get_proc_address("glCreateShader"));
			if (!internal::glCreateShader) return false;
			internal::glCullFace = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glCullFace"));
			if (!internal::glCullFace) return false;
			internal::glDeleteBuffers = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteBuffers"));
			if (!internal::glDeleteBuffers) return false;
			internal::glDeleteFramebuffers = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteFramebuffers"));
			if (!internal::glDeleteFramebuffers) return false;
			internal::glDeleteProgram = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glDeleteProgram"));
			if (!internal::glDeleteProgram) return false;
			internal::glDeleteRenderbuffers = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteRenderbuffers"));
			if (!internal::glDeleteRenderbuffers) return false;
			internal::glDeleteShader = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glDeleteShader"));
			if (!internal::glDeleteShader) return false;
			internal::glDeleteTextures = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteTextures"));
			if (!internal::glDeleteTextures) return false;
			internal::glDepthFunc = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glDepthFunc"));
			if (!internal::glDepthFunc) return false;
			internal::glDepthMask = reinterpret_cast<void (*)(GLboolean )>(internal::get_proc_address("glDepthMask"));
			if (!internal::glDepthMask) return false;
			internal::glDepthRangef = reinterpret_cast<void (*)(GLfloat , GLfloat )>(internal::get_proc_address("glDepthRangef"));
			if (!internal::glDepthRangef) return false;
			internal::glDetachShader = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glDetachShader"));
			if (!internal::glDetachShader) return false;
			internal::glDisable = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glDisable"));
			if (!internal::glDisable) return false;
			internal::glDisableVertexAttribArray = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glDisableVertexAttribArray"));
			if (!internal::glDisableVertexAttribArray) return false;
			internal::glDrawArrays = reinterpret_cast<void (*)(GLenum , GLint , GLsizei )>(internal::get_proc_address("glDrawArrays"));
			if (!internal::glDrawArrays) return false;
			internal::glDrawElements = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , const void *)>(internal::get_proc_address("glDrawElements"));
			if (!internal::glDrawElements) return false;
			internal::glEnable = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glEnable"));
			if (!internal::glEnable) return false;
			internal::glEnableVertexAttribArray = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glEnableVertexAttribArray"));
			if (!internal::glEnableVertexAttribArray) return false;
			internal::glFinish = reinterpret_cast<void (*)()>(internal::get_proc_address("glFinish"));
			if (!internal::glFinish) return false;
			internal::glFlush = reinterpret_cast<void (*)()>(internal::get_proc_address("glFlush"));
			if (!internal::glFlush) return false;
			internal::glFramebufferRenderbuffer = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLuint )>(internal::get_proc_address("glFramebufferRenderbuffer"));
			if (!internal::glFramebufferRenderbuffer) return false;
			internal::glFramebufferTexture2D = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLuint , GLint )>(internal::get_proc_address("glFramebufferTexture2D"));
			if (!internal::glFramebufferTexture2D) return false;
			internal::glFrontFace = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glFrontFace"));
			if (!internal::glFrontFace) return false;
			internal::glGenBuffers = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenBuffers"));
			if (!internal::glGenBuffers) return false;
			internal::glGenerateMipmap = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glGenerateMipmap"));
			if (!internal::glGenerateMipmap) return false;
			internal::glGenFramebuffers = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenFramebuffers"));
			if (!internal::glGenFramebuffers) return false;
			internal::glGenRenderbuffers = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenRenderbuffers"));
			if (!internal::glGenRenderbuffers) return false;
			internal::glGenTextures = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenTextures"));
			if (!internal::glGenTextures) return false;
			internal::glGetActiveAttrib = reinterpret_cast<void (*)(GLuint , GLuint , GLsizei , GLsizei *, GLint *, GLenum *, GLchar *)>(internal::get_proc_address("glGetActiveAttrib"));
			if (!internal::glGetActiveAttrib) return false;
			internal::glGetActiveUniform = reinterpret_cast<void (*)(GLuint , GLuint , GLsizei , GLsizei *, GLint *, GLenum *, GLchar *)>(internal::get_proc_address("glGetActiveUniform"));
			if (!internal::glGetActiveUniform) return false;
			internal::glGetAttachedShaders = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLuint *)>(internal::get_proc_address("glGetAttachedShaders"));
			if (!internal::glGetAttachedShaders) return false;
			internal::glGetAttribLocation = reinterpret_cast<GLint (*)(GLuint , const GLchar *)>(internal::get_proc_address("glGetAttribLocation"));
			if (!internal::glGetAttribLocation) return false;
			internal::glGetBooleanv = reinterpret_cast<void (*)(GLenum , GLboolean *)>(internal::get_proc_address("glGetBooleanv"));
			if (!internal::glGetBooleanv) return false;
			internal::glGetBufferParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetBufferParameteriv"));
			if (!internal::glGetBufferParameteriv) return false;
			internal::glGetError = reinterpret_cast<GLenum (*)()>(internal::get_proc_address("glGetError"));
			if (!internal::glGetError) return false;
			internal::glGetFloatv = reinterpret_cast<void (*)(GLenum , GLfloat *)>(internal::get_proc_address("glGetFloatv"));
			if (!internal::glGetFloatv) return false;
			internal::glGetFramebufferAttachmentParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetFramebufferAttachmentParameteriv"));
			if (!internal::glGetFramebufferAttachmentParameteriv) return false;
			internal::glGetIntegerv = reinterpret_cast<void (*)(GLenum , GLint *)>(internal::get_proc_address("glGetIntegerv"));
			if (!internal::glGetIntegerv) return false;
			internal::glGetProgramiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetProgramiv"));
			if (!internal::glGetProgramiv) return false;
			internal::glGetProgramInfoLog = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetProgramInfoLog"));
			if (!internal::glGetProgramInfoLog) return false;
			internal::glGetRenderbufferParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetRenderbufferParameteriv"));
			if (!internal::glGetRenderbufferParameteriv) return false;
			internal::glGetShaderiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetShaderiv"));
			if (!internal::glGetShaderiv) return false;
			internal::glGetShaderInfoLog = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetShaderInfoLog"));
			if (!internal::glGetShaderInfoLog) return false;
			internal::glGetShaderPrecisionFormat = reinterpret_cast<void (*)(GLenum , GLenum , GLint *, GLint *)>(internal::get_proc_address("glGetShaderPrecisionFormat"));
			if (!internal::glGetShaderPrecisionFormat) return false;
			internal::glGetShaderSource = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetShaderSource"));
			if (!internal::glGetShaderSource) return false;
			internal::glGetString = reinterpret_cast<const GLubyte *(*)(GLenum )>(internal::get_proc_address("glGetString"));
			if (!internal::glGetString) return false;
			internal::glGetTexParameterfv = reinterpret_cast<void (*)(GLenum , GLenum , GLfloat *)>(internal::get_proc_address("glGetTexParameterfv"));
			if (!internal::glGetTexParameterfv) return false;
			internal::glGetTexParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetTexParameteriv"));
			if (!internal::glGetTexParameteriv) return false;
			internal::glGetUniformfv = reinterpret_cast<void (*)(GLuint , GLint , GLfloat *)>(internal::get_proc_address("glGetUniformfv"));
			if (!internal::glGetUniformfv) return false;
			internal::glGetUniformiv = reinterpret_cast<void (*)(GLuint , GLint , GLint *)>(internal::get_proc_address("glGetUniformiv"));
			if (!internal::glGetUniformiv) return false;
			internal::glGetUniformLocation = reinterpret_cast<GLint (*)(GLuint , const GLchar *)>(internal::get_proc_address("glGetUniformLocation"));
			if (!internal::glGetUniformLocation) return false;
			internal::glGetVertexAttribfv = reinterpret_cast<void (*)(GLuint , GLenum , GLfloat *)>(internal::get_proc_address("glGetVertexAttribfv"));
			if (!internal::glGetVertexAttribfv) return false;
			internal::glGetVertexAttribiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetVertexAttribiv"));
			if (!internal::glGetVertexAttribiv) return false;
			internal::glGetVertexAttribPointerv = reinterpret_cast<void (*)(GLuint , GLenum , void **)>(internal::get_proc_address("glGetVertexAttribPointerv"));
			if (!internal::glGetVertexAttribPointerv) return false;
			internal::glHint = reinterpret_cast<void (*)(GLenum , GLenum )>(internal::get_proc_address("glHint"));
			if (!internal::glHint) return false;
			internal::glIsBuffer = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsBuffer"));
			if (!internal::glIsBuffer) return false;
			internal::glIsEnabled = reinterpret_cast<GLboolean (*)(GLenum )>(internal::get_proc_address("glIsEnabled"));
			if (!internal::glIsEnabled) return false;
			internal::glIsFramebuffer = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsFramebuffer"));
			if (!internal::glIsFramebuffer) return false;
			internal::glIsProgram = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsProgram"));
			if (!internal::glIsProgram) return false;
			internal::glIsRenderbuffer = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsRenderbuffer"));
			if (!internal::glIsRenderbuffer) return false;
			internal::glIsShader = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsShader"));
			if (!internal::glIsShader) return false;
			internal::glIsTexture = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsTexture"));
			if (!internal::glIsTexture) return false;
			internal::glLineWidth = reinterpret_cast<void (*)(GLfloat )>(internal::get_proc_address("glLineWidth"));
			if (!internal::glLineWidth) return false;
			internal::glLinkProgram = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glLinkProgram"));
			if (!internal::glLinkProgram) return false;
			internal::glPixelStorei = reinterpret_cast<void (*)(GLenum , GLint )>(internal::get_proc_address("glPixelStorei"));
			if (!internal::glPixelStorei) return false;
			internal::glPolygonOffset = reinterpret_cast<void (*)(GLfloat , GLfloat )>(internal::get_proc_address("glPolygonOffset"));
			if (!internal::glPolygonOffset) return false;
			internal::glReadPixels = reinterpret_cast<void (*)(GLint , GLint , GLsizei , GLsizei , GLenum , GLenum , void *)>(internal::get_proc_address("glReadPixels"));
			if (!internal::glReadPixels) return false;
			internal::glReleaseShaderCompiler = reinterpret_cast<void (*)()>(internal::get_proc_address("glReleaseShaderCompiler"));
			if (!internal::glReleaseShaderCompiler) return false;
			internal::glRenderbufferStorage = reinterpret_cast<void (*)(GLenum , GLenum , GLsizei , GLsizei )>(internal::get_proc_address("glRenderbufferStorage"));
			if (!internal::glRenderbufferStorage) return false;
			internal::glSampleCoverage = reinterpret_cast<void (*)(GLfloat , GLboolean )>(internal::get_proc_address("glSampleCoverage"));
			if (!internal::glSampleCoverage) return false;
			internal::glScissor = reinterpret_cast<void (*)(GLint , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glScissor"));
			if (!internal::glScissor) return false;
			internal::glShaderBinary = reinterpret_cast<void (*)(GLsizei , const GLuint *, GLenum , const void *, GLsizei )>(internal::get_proc_address("glShaderBinary"));
			if (!internal::glShaderBinary) return false;
			internal::glShaderSource = reinterpret_cast<void (*)(GLuint , GLsizei , const GLchar *const*, const GLint *)>(internal::get_proc_address("glShaderSource"));
			if (!internal::glShaderSource) return false;
			internal::glStencilFunc = reinterpret_cast<void (*)(GLenum , GLint , GLuint )>(internal::get_proc_address("glStencilFunc"));
			if (!internal::glStencilFunc) return false;
			internal::glStencilFuncSeparate = reinterpret_cast<void (*)(GLenum , GLenum , GLint , GLuint )>(internal::get_proc_address("glStencilFuncSeparate"));
			if (!internal::glStencilFuncSeparate) return false;
			internal::glStencilMask = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glStencilMask"));
			if (!internal::glStencilMask) return false;
			internal::glStencilMaskSeparate = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glStencilMaskSeparate"));
			if (!internal::glStencilMaskSeparate) return false;
			internal::glStencilOp = reinterpret_cast<void (*)(GLenum , GLenum , GLenum )>(internal::get_proc_address("glStencilOp"));
			if (!internal::glStencilOp) return false;
			internal::glStencilOpSeparate = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLenum )>(internal::get_proc_address("glStencilOpSeparate"));
			if (!internal::glStencilOpSeparate) return false;
			internal::glTexImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLsizei , GLsizei , GLint , GLenum , GLenum , const void *)>(internal::get_proc_address("glTexImage2D"));
			if (!internal::glTexImage2D) return false;
			internal::glTexParameterf = reinterpret_cast<void (*)(GLenum , GLenum , GLfloat )>(internal::get_proc_address("glTexParameterf"));
			if (!internal::glTexParameterf) return false;
			internal::glTexParameterfv = reinterpret_cast<void (*)(GLenum , GLenum , const GLfloat *)>(internal::get_proc_address("glTexParameterfv"));
			if (!internal::glTexParameterfv) return false;
			internal::glTexParameteri = reinterpret_cast<void (*)(GLenum , GLenum , GLint )>(internal::get_proc_address("glTexParameteri"));
			if (!internal::glTexParameteri) return false;
			internal::glTexParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , const GLint *)>(internal::get_proc_address("glTexParameteriv"));
			if (!internal::glTexParameteriv) return false;
			internal::glTexSubImage2D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLsizei , GLsizei , GLenum , GLenum , const void *)>(internal::get_proc_address("glTexSubImage2D"));
			if (!internal::glTexSubImage2D) return false;
			internal::glUniform1f = reinterpret_cast<void (*)(GLint , GLfloat )>(internal::get_proc_address("glUniform1f"));
			if (!internal::glUniform1f) return false;
			internal::glUniform1fv = reinterpret_cast<void (*)(GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glUniform1fv"));
			if (!internal::glUniform1fv) return false;
			internal::glUniform1i = reinterpret_cast<void (*)(GLint , GLint )>(internal::get_proc_address("glUniform1i"));
			if (!internal::glUniform1i) return false;
			internal::glUniform1iv = reinterpret_cast<void (*)(GLint , GLsizei , const GLint *)>(internal::get_proc_address("glUniform1iv"));
			if (!internal::glUniform1iv) return false;
			internal::glUniform2f = reinterpret_cast<void (*)(GLint , GLfloat , GLfloat )>(internal::get_proc_address("glUniform2f"));
			if (!internal::glUniform2f) return false;
			internal::glUniform2fv = reinterpret_cast<void (*)(GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glUniform2fv"));
			if (!internal::glUniform2fv) return false;
			internal::glUniform2i = reinterpret_cast<void (*)(GLint , GLint , GLint )>(internal::get_proc_address("glUniform2i"));
			if (!internal::glUniform2i) return false;
			internal::glUniform2iv = reinterpret_cast<void (*)(GLint , GLsizei , const GLint *)>(internal::get_proc_address("glUniform2iv"));
			if (!internal::glUniform2iv) return false;
			internal::glUniform3f = reinterpret_cast<void (*)(GLint , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glUniform3f"));
			if (!internal::glUniform3f) return false;
			internal::glUniform3fv = reinterpret_cast<void (*)(GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glUniform3fv"));
			if (!internal::glUniform3fv) return false;
			internal::glUniform3i = reinterpret_cast<void (*)(GLint , GLint , GLint , GLint )>(internal::get_proc_address("glUniform3i"));
			if (!internal::glUniform3i) return false;
			internal::glUniform3iv = reinterpret_cast<void (*)(GLint , GLsizei , const GLint *)>(internal::get_proc_address("glUniform3iv"));
			if (!internal::glUniform3iv) return false;
			internal::glUniform4f = reinterpret_cast<void (*)(GLint , GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glUniform4f"));
			if (!internal::glUniform4f) return false;
			internal::glUniform4fv = reinterpret_cast<void (*)(GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glUniform4fv"));
			if (!internal::glUniform4fv) return false;
			internal::glUniform4i = reinterpret_cast<void (*)(GLint , GLint , GLint , GLint , GLint )>(internal::get_proc_address("glUniform4i"));
			if (!internal::glUniform4i) return false;
			internal::glUniform4iv = reinterpret_cast<void (*)(GLint , GLsizei , const GLint *)>(internal::get_proc_address("glUniform4iv"));
			if (!internal::glUniform4iv) return false;
			internal::glUniformMatrix2fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix2fv"));
			if (!internal::glUniformMatrix2fv) return false;
			internal::glUniformMatrix3fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix3fv"));
			if (!internal::glUniformMatrix3fv) return false;
			internal::glUniformMatrix4fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix4fv"));
			if (!internal::glUniformMatrix4fv) return false;
			internal::glUseProgram = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glUseProgram"));
			if (!internal::glUseProgram) return false;
			internal::glValidateProgram = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glValidateProgram"));
			if (!internal::glValidateProgram) return false;
			internal::glVertexAttrib1f = reinterpret_cast<void (*)(GLuint , GLfloat )>(internal::get_proc_address("glVertexAttrib1f"));
			if (!internal::glVertexAttrib1f) return false;
			internal::glVertexAttrib1fv = reinterpret_cast<void (*)(GLuint , const GLfloat *)>(internal::get_proc_address("glVertexAttrib1fv"));
			if (!internal::glVertexAttrib1fv) return false;
			internal::glVertexAttrib2f = reinterpret_cast<void (*)(GLuint , GLfloat , GLfloat )>(internal::get_proc_address("glVertexAttrib2f"));
			if (!internal::glVertexAttrib2f) return false;
			internal::glVertexAttrib2fv = reinterpret_cast<void (*)(GLuint , const GLfloat *)>(internal::get_proc_address("glVertexAttrib2fv"));
			if (!internal::glVertexAttrib2fv) return false;
			internal::glVertexAttrib3f = reinterpret_cast<void (*)(GLuint , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glVertexAttrib3f"));
			if (!internal::glVertexAttrib3f) return false;
			internal::glVertexAttrib3fv = reinterpret_cast<void (*)(GLuint , const GLfloat *)>(internal::get_proc_address("glVertexAttrib3fv"));
			if (!internal::glVertexAttrib3fv) return false;
			internal::glVertexAttrib4f = reinterpret_cast<void (*)(GLuint , GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glVertexAttrib4f"));
			if (!internal::glVertexAttrib4f) return false;
			internal::glVertexAttrib4fv = reinterpret_cast<void (*)(GLuint , const GLfloat *)>(internal::get_proc_address("glVertexAttrib4fv"));
			if (!internal::glVertexAttrib4fv) return false;
			internal::glVertexAttribPointer = reinterpret_cast<void (*)(GLuint , GLint , GLenum , GLboolean , GLsizei , const void *)>(internal::get_proc_address("glVertexAttribPointer"));
			if (!internal::glVertexAttribPointer) return false;
			internal::glViewport = reinterpret_cast<void (*)(GLint , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glViewport"));
			if (!internal::glViewport) return false;

			// OpenGL ES 3.0

			internal::glReadBuffer = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glReadBuffer"));
			if (!internal::glReadBuffer) return false;
			internal::glDrawRangeElements = reinterpret_cast<void (*)(GLenum , GLuint , GLuint , GLsizei , GLenum , const void *)>(internal::get_proc_address("glDrawRangeElements"));
			if (!internal::glDrawRangeElements) return false;
			internal::glTexImage3D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLsizei , GLsizei , GLsizei , GLint , GLenum , GLenum , const void *)>(internal::get_proc_address("glTexImage3D"));
			if (!internal::glTexImage3D) return false;
			internal::glTexSubImage3D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLint , GLsizei , GLsizei , GLsizei , GLenum , GLenum , const void *)>(internal::get_proc_address("glTexSubImage3D"));
			if (!internal::glTexSubImage3D) return false;
			internal::glCopyTexSubImage3D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLint , GLint , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glCopyTexSubImage3D"));
			if (!internal::glCopyTexSubImage3D) return false;
			internal::glCompressedTexImage3D = reinterpret_cast<void (*)(GLenum , GLint , GLenum , GLsizei , GLsizei , GLsizei , GLint , GLsizei , const void *)>(internal::get_proc_address("glCompressedTexImage3D"));
			if (!internal::glCompressedTexImage3D) return false;
			internal::glCompressedTexSubImage3D = reinterpret_cast<void (*)(GLenum , GLint , GLint , GLint , GLint , GLsizei , GLsizei , GLsizei , GLenum , GLsizei , const void *)>(internal::get_proc_address("glCompressedTexSubImage3D"));
			if (!internal::glCompressedTexSubImage3D) return false;
			internal::glGenQueries = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenQueries"));
			if (!internal::glGenQueries) return false;
			internal::glDeleteQueries = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteQueries"));
			if (!internal::glDeleteQueries) return false;
			internal::glIsQuery = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsQuery"));
			if (!internal::glIsQuery) return false;
			internal::glBeginQuery = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBeginQuery"));
			if (!internal::glBeginQuery) return false;
			internal::glEndQuery = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glEndQuery"));
			if (!internal::glEndQuery) return false;
			internal::glGetQueryiv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetQueryiv"));
			if (!internal::glGetQueryiv) return false;
			internal::glGetQueryObjectuiv = reinterpret_cast<void (*)(GLuint , GLenum , GLuint *)>(internal::get_proc_address("glGetQueryObjectuiv"));
			if (!internal::glGetQueryObjectuiv) return false;
			internal::glUnmapBuffer = reinterpret_cast<GLboolean (*)(GLenum )>(internal::get_proc_address("glUnmapBuffer"));
			if (!internal::glUnmapBuffer) return false;
			internal::glGetBufferPointerv = reinterpret_cast<void (*)(GLenum , GLenum , void **)>(internal::get_proc_address("glGetBufferPointerv"));
			if (!internal::glGetBufferPointerv) return false;
			internal::glDrawBuffers = reinterpret_cast<void (*)(GLsizei , const GLenum *)>(internal::get_proc_address("glDrawBuffers"));
			if (!internal::glDrawBuffers) return false;
			internal::glUniformMatrix2x3fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix2x3fv"));
			if (!internal::glUniformMatrix2x3fv) return false;
			internal::glUniformMatrix3x2fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix3x2fv"));
			if (!internal::glUniformMatrix3x2fv) return false;
			internal::glUniformMatrix2x4fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix2x4fv"));
			if (!internal::glUniformMatrix2x4fv) return false;
			internal::glUniformMatrix4x2fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix4x2fv"));
			if (!internal::glUniformMatrix4x2fv) return false;
			internal::glUniformMatrix3x4fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix3x4fv"));
			if (!internal::glUniformMatrix3x4fv) return false;
			internal::glUniformMatrix4x3fv = reinterpret_cast<void (*)(GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glUniformMatrix4x3fv"));
			if (!internal::glUniformMatrix4x3fv) return false;
			internal::glBlitFramebuffer = reinterpret_cast<void (*)(GLint , GLint , GLint , GLint , GLint , GLint , GLint , GLint , GLbitfield , GLenum )>(internal::get_proc_address("glBlitFramebuffer"));
			if (!internal::glBlitFramebuffer) return false;
			internal::glRenderbufferStorageMultisample = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , GLsizei , GLsizei )>(internal::get_proc_address("glRenderbufferStorageMultisample"));
			if (!internal::glRenderbufferStorageMultisample) return false;
			internal::glFramebufferTextureLayer = reinterpret_cast<void (*)(GLenum , GLenum , GLuint , GLint , GLint )>(internal::get_proc_address("glFramebufferTextureLayer"));
			if (!internal::glFramebufferTextureLayer) return false;
			internal::glMapBufferRange = reinterpret_cast<void *(*)(GLenum , GLintptr , GLsizeiptr , GLbitfield )>(internal::get_proc_address("glMapBufferRange"));
			if (!internal::glMapBufferRange) return false;
			internal::glFlushMappedBufferRange = reinterpret_cast<void (*)(GLenum , GLintptr , GLsizeiptr )>(internal::get_proc_address("glFlushMappedBufferRange"));
			if (!internal::glFlushMappedBufferRange) return false;
			internal::glBindVertexArray = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glBindVertexArray"));
			if (!internal::glBindVertexArray) return false;
			internal::glDeleteVertexArrays = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteVertexArrays"));
			if (!internal::glDeleteVertexArrays) return false;
			internal::glGenVertexArrays = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenVertexArrays"));
			if (!internal::glGenVertexArrays) return false;
			internal::glIsVertexArray = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsVertexArray"));
			if (!internal::glIsVertexArray) return false;
			internal::glGetIntegeri_v = reinterpret_cast<void (*)(GLenum , GLuint , GLint *)>(internal::get_proc_address("glGetIntegeri_v"));
			if (!internal::glGetIntegeri_v) return false;
			internal::glBeginTransformFeedback = reinterpret_cast<void (*)(GLenum )>(internal::get_proc_address("glBeginTransformFeedback"));
			if (!internal::glBeginTransformFeedback) return false;
			internal::glEndTransformFeedback = reinterpret_cast<void (*)()>(internal::get_proc_address("glEndTransformFeedback"));
			if (!internal::glEndTransformFeedback) return false;
			internal::glBindBufferRange = reinterpret_cast<void (*)(GLenum , GLuint , GLuint , GLintptr , GLsizeiptr )>(internal::get_proc_address("glBindBufferRange"));
			if (!internal::glBindBufferRange) return false;
			internal::glBindBufferBase = reinterpret_cast<void (*)(GLenum , GLuint , GLuint )>(internal::get_proc_address("glBindBufferBase"));
			if (!internal::glBindBufferBase) return false;
			internal::glTransformFeedbackVaryings = reinterpret_cast<void (*)(GLuint , GLsizei , const GLchar *const*, GLenum )>(internal::get_proc_address("glTransformFeedbackVaryings"));
			if (!internal::glTransformFeedbackVaryings) return false;
			internal::glGetTransformFeedbackVarying = reinterpret_cast<void (*)(GLuint , GLuint , GLsizei , GLsizei *, GLsizei *, GLenum *, GLchar *)>(internal::get_proc_address("glGetTransformFeedbackVarying"));
			if (!internal::glGetTransformFeedbackVarying) return false;
			internal::glVertexAttribIPointer = reinterpret_cast<void (*)(GLuint , GLint , GLenum , GLsizei , const void *)>(internal::get_proc_address("glVertexAttribIPointer"));
			if (!internal::glVertexAttribIPointer) return false;
			internal::glGetVertexAttribIiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetVertexAttribIiv"));
			if (!internal::glGetVertexAttribIiv) return false;
			internal::glGetVertexAttribIuiv = reinterpret_cast<void (*)(GLuint , GLenum , GLuint *)>(internal::get_proc_address("glGetVertexAttribIuiv"));
			if (!internal::glGetVertexAttribIuiv) return false;
			internal::glVertexAttribI4i = reinterpret_cast<void (*)(GLuint , GLint , GLint , GLint , GLint )>(internal::get_proc_address("glVertexAttribI4i"));
			if (!internal::glVertexAttribI4i) return false;
			internal::glVertexAttribI4ui = reinterpret_cast<void (*)(GLuint , GLuint , GLuint , GLuint , GLuint )>(internal::get_proc_address("glVertexAttribI4ui"));
			if (!internal::glVertexAttribI4ui) return false;
			internal::glVertexAttribI4iv = reinterpret_cast<void (*)(GLuint , const GLint *)>(internal::get_proc_address("glVertexAttribI4iv"));
			if (!internal::glVertexAttribI4iv) return false;
			internal::glVertexAttribI4uiv = reinterpret_cast<void (*)(GLuint , const GLuint *)>(internal::get_proc_address("glVertexAttribI4uiv"));
			if (!internal::glVertexAttribI4uiv) return false;
			internal::glGetUniformuiv = reinterpret_cast<void (*)(GLuint , GLint , GLuint *)>(internal::get_proc_address("glGetUniformuiv"));
			if (!internal::glGetUniformuiv) return false;
			internal::glGetFragDataLocation = reinterpret_cast<GLint (*)(GLuint , const GLchar *)>(internal::get_proc_address("glGetFragDataLocation"));
			if (!internal::glGetFragDataLocation) return false;
			internal::glUniform1ui = reinterpret_cast<void (*)(GLint , GLuint )>(internal::get_proc_address("glUniform1ui"));
			if (!internal::glUniform1ui) return false;
			internal::glUniform2ui = reinterpret_cast<void (*)(GLint , GLuint , GLuint )>(internal::get_proc_address("glUniform2ui"));
			if (!internal::glUniform2ui) return false;
			internal::glUniform3ui = reinterpret_cast<void (*)(GLint , GLuint , GLuint , GLuint )>(internal::get_proc_address("glUniform3ui"));
			if (!internal::glUniform3ui) return false;
			internal::glUniform4ui = reinterpret_cast<void (*)(GLint , GLuint , GLuint , GLuint , GLuint )>(internal::get_proc_address("glUniform4ui"));
			if (!internal::glUniform4ui) return false;
			internal::glUniform1uiv = reinterpret_cast<void (*)(GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glUniform1uiv"));
			if (!internal::glUniform1uiv) return false;
			internal::glUniform2uiv = reinterpret_cast<void (*)(GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glUniform2uiv"));
			if (!internal::glUniform2uiv) return false;
			internal::glUniform3uiv = reinterpret_cast<void (*)(GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glUniform3uiv"));
			if (!internal::glUniform3uiv) return false;
			internal::glUniform4uiv = reinterpret_cast<void (*)(GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glUniform4uiv"));
			if (!internal::glUniform4uiv) return false;
			internal::glClearBufferiv = reinterpret_cast<void (*)(GLenum , GLint , const GLint *)>(internal::get_proc_address("glClearBufferiv"));
			if (!internal::glClearBufferiv) return false;
			internal::glClearBufferuiv = reinterpret_cast<void (*)(GLenum , GLint , const GLuint *)>(internal::get_proc_address("glClearBufferuiv"));
			if (!internal::glClearBufferuiv) return false;
			internal::glClearBufferfv = reinterpret_cast<void (*)(GLenum , GLint , const GLfloat *)>(internal::get_proc_address("glClearBufferfv"));
			if (!internal::glClearBufferfv) return false;
			internal::glClearBufferfi = reinterpret_cast<void (*)(GLenum , GLint , GLfloat , GLint )>(internal::get_proc_address("glClearBufferfi"));
			if (!internal::glClearBufferfi) return false;
			internal::glGetStringi = reinterpret_cast<const GLubyte *(*)(GLenum , GLuint )>(internal::get_proc_address("glGetStringi"));
			if (!internal::glGetStringi) return false;
			internal::glCopyBufferSubData = reinterpret_cast<void (*)(GLenum , GLenum , GLintptr , GLintptr , GLsizeiptr )>(internal::get_proc_address("glCopyBufferSubData"));
			if (!internal::glCopyBufferSubData) return false;
			internal::glGetUniformIndices = reinterpret_cast<void (*)(GLuint , GLsizei , const GLchar *const*, GLuint *)>(internal::get_proc_address("glGetUniformIndices"));
			if (!internal::glGetUniformIndices) return false;
			internal::glGetActiveUniformsiv = reinterpret_cast<void (*)(GLuint , GLsizei , const GLuint *, GLenum , GLint *)>(internal::get_proc_address("glGetActiveUniformsiv"));
			if (!internal::glGetActiveUniformsiv) return false;
			internal::glGetUniformBlockIndex = reinterpret_cast<GLuint (*)(GLuint , const GLchar *)>(internal::get_proc_address("glGetUniformBlockIndex"));
			if (!internal::glGetUniformBlockIndex) return false;
			internal::glGetActiveUniformBlockiv = reinterpret_cast<void (*)(GLuint , GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetActiveUniformBlockiv"));
			if (!internal::glGetActiveUniformBlockiv) return false;
			internal::glGetActiveUniformBlockName = reinterpret_cast<void (*)(GLuint , GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetActiveUniformBlockName"));
			if (!internal::glGetActiveUniformBlockName) return false;
			internal::glUniformBlockBinding = reinterpret_cast<void (*)(GLuint , GLuint , GLuint )>(internal::get_proc_address("glUniformBlockBinding"));
			if (!internal::glUniformBlockBinding) return false;
			internal::glDrawArraysInstanced = reinterpret_cast<void (*)(GLenum , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glDrawArraysInstanced"));
			if (!internal::glDrawArraysInstanced) return false;
			internal::glDrawElementsInstanced = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , const void *, GLsizei )>(internal::get_proc_address("glDrawElementsInstanced"));
			if (!internal::glDrawElementsInstanced) return false;
			internal::glFenceSync = reinterpret_cast<GLsync (*)(GLenum , GLbitfield )>(internal::get_proc_address("glFenceSync"));
			if (!internal::glFenceSync) return false;
			internal::glIsSync = reinterpret_cast<GLboolean (*)(GLsync )>(internal::get_proc_address("glIsSync"));
			if (!internal::glIsSync) return false;
			internal::glDeleteSync = reinterpret_cast<void (*)(GLsync )>(internal::get_proc_address("glDeleteSync"));
			if (!internal::glDeleteSync) return false;
			internal::glClientWaitSync = reinterpret_cast<GLenum (*)(GLsync , GLbitfield , GLuint64 )>(internal::get_proc_address("glClientWaitSync"));
			if (!internal::glClientWaitSync) return false;
			internal::glWaitSync = reinterpret_cast<void (*)(GLsync , GLbitfield , GLuint64 )>(internal::get_proc_address("glWaitSync"));
			if (!internal::glWaitSync) return false;
			internal::glGetInteger64v = reinterpret_cast<void (*)(GLenum , GLint64 *)>(internal::get_proc_address("glGetInteger64v"));
			if (!internal::glGetInteger64v) return false;
			internal::glGetSynciv = reinterpret_cast<void (*)(GLsync , GLenum , GLsizei , GLsizei *, GLint *)>(internal::get_proc_address("glGetSynciv"));
			if (!internal::glGetSynciv) return false;
			internal::glGetInteger64i_v = reinterpret_cast<void (*)(GLenum , GLuint , GLint64 *)>(internal::get_proc_address("glGetInteger64i_v"));
			if (!internal::glGetInteger64i_v) return false;
			internal::glGetBufferParameteri64v = reinterpret_cast<void (*)(GLenum , GLenum , GLint64 *)>(internal::get_proc_address("glGetBufferParameteri64v"));
			if (!internal::glGetBufferParameteri64v) return false;
			internal::glGenSamplers = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenSamplers"));
			if (!internal::glGenSamplers) return false;
			internal::glDeleteSamplers = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteSamplers"));
			if (!internal::glDeleteSamplers) return false;
			internal::glIsSampler = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsSampler"));
			if (!internal::glIsSampler) return false;
			internal::glBindSampler = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glBindSampler"));
			if (!internal::glBindSampler) return false;
			internal::glSamplerParameteri = reinterpret_cast<void (*)(GLuint , GLenum , GLint )>(internal::get_proc_address("glSamplerParameteri"));
			if (!internal::glSamplerParameteri) return false;
			internal::glSamplerParameteriv = reinterpret_cast<void (*)(GLuint , GLenum , const GLint *)>(internal::get_proc_address("glSamplerParameteriv"));
			if (!internal::glSamplerParameteriv) return false;
			internal::glSamplerParameterf = reinterpret_cast<void (*)(GLuint , GLenum , GLfloat )>(internal::get_proc_address("glSamplerParameterf"));
			if (!internal::glSamplerParameterf) return false;
			internal::glSamplerParameterfv = reinterpret_cast<void (*)(GLuint , GLenum , const GLfloat *)>(internal::get_proc_address("glSamplerParameterfv"));
			if (!internal::glSamplerParameterfv) return false;
			internal::glGetSamplerParameteriv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetSamplerParameteriv"));
			if (!internal::glGetSamplerParameteriv) return false;
			internal::glGetSamplerParameterfv = reinterpret_cast<void (*)(GLuint , GLenum , GLfloat *)>(internal::get_proc_address("glGetSamplerParameterfv"));
			if (!internal::glGetSamplerParameterfv) return false;
			internal::glVertexAttribDivisor = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glVertexAttribDivisor"));
			if (!internal::glVertexAttribDivisor) return false;
			internal::glBindTransformFeedback = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glBindTransformFeedback"));
			if (!internal::glBindTransformFeedback) return false;
			internal::glDeleteTransformFeedbacks = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteTransformFeedbacks"));
			if (!internal::glDeleteTransformFeedbacks) return false;
			internal::glGenTransformFeedbacks = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenTransformFeedbacks"));
			if (!internal::glGenTransformFeedbacks) return false;
			internal::glIsTransformFeedback = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsTransformFeedback"));
			if (!internal::glIsTransformFeedback) return false;
			internal::glPauseTransformFeedback = reinterpret_cast<void (*)()>(internal::get_proc_address("glPauseTransformFeedback"));
			if (!internal::glPauseTransformFeedback) return false;
			internal::glResumeTransformFeedback = reinterpret_cast<void (*)()>(internal::get_proc_address("glResumeTransformFeedback"));
			if (!internal::glResumeTransformFeedback) return false;
			internal::glGetProgramBinary = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLenum *, void *)>(internal::get_proc_address("glGetProgramBinary"));
			if (!internal::glGetProgramBinary) return false;
			internal::glProgramBinary = reinterpret_cast<void (*)(GLuint , GLenum , const void *, GLsizei )>(internal::get_proc_address("glProgramBinary"));
			if (!internal::glProgramBinary) return false;
			internal::glProgramParameteri = reinterpret_cast<void (*)(GLuint , GLenum , GLint )>(internal::get_proc_address("glProgramParameteri"));
			if (!internal::glProgramParameteri) return false;
			internal::glInvalidateFramebuffer = reinterpret_cast<void (*)(GLenum , GLsizei , const GLenum *)>(internal::get_proc_address("glInvalidateFramebuffer"));
			if (!internal::glInvalidateFramebuffer) return false;
			internal::glInvalidateSubFramebuffer = reinterpret_cast<void (*)(GLenum , GLsizei , const GLenum *, GLint , GLint , GLsizei , GLsizei )>(internal::get_proc_address("glInvalidateSubFramebuffer"));
			if (!internal::glInvalidateSubFramebuffer) return false;
			internal::glTexStorage2D = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , GLsizei , GLsizei )>(internal::get_proc_address("glTexStorage2D"));
			if (!internal::glTexStorage2D) return false;
			internal::glTexStorage3D = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , GLsizei , GLsizei , GLsizei )>(internal::get_proc_address("glTexStorage3D"));
			if (!internal::glTexStorage3D) return false;
			internal::glGetInternalformativ = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLsizei , GLint *)>(internal::get_proc_address("glGetInternalformativ"));
			if (!internal::glGetInternalformativ) return false;

			// OpenGL ES 3.1

			internal::glDispatchCompute = reinterpret_cast<void (*)(GLuint , GLuint , GLuint )>(internal::get_proc_address("glDispatchCompute"));
			if (!internal::glDispatchCompute) return false;
			internal::glDispatchComputeIndirect = reinterpret_cast<void (*)(GLintptr )>(internal::get_proc_address("glDispatchComputeIndirect"));
			if (!internal::glDispatchComputeIndirect) return false;
			internal::glDrawArraysIndirect = reinterpret_cast<void (*)(GLenum , const void *)>(internal::get_proc_address("glDrawArraysIndirect"));
			if (!internal::glDrawArraysIndirect) return false;
			internal::glDrawElementsIndirect = reinterpret_cast<void (*)(GLenum , GLenum , const void *)>(internal::get_proc_address("glDrawElementsIndirect"));
			if (!internal::glDrawElementsIndirect) return false;
			internal::glFramebufferParameteri = reinterpret_cast<void (*)(GLenum , GLenum , GLint )>(internal::get_proc_address("glFramebufferParameteri"));
			if (!internal::glFramebufferParameteri) return false;
			internal::glGetFramebufferParameteriv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetFramebufferParameteriv"));
			if (!internal::glGetFramebufferParameteriv) return false;
			internal::glGetProgramInterfaceiv = reinterpret_cast<void (*)(GLuint , GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetProgramInterfaceiv"));
			if (!internal::glGetProgramInterfaceiv) return false;
			internal::glGetProgramResourceIndex = reinterpret_cast<GLuint (*)(GLuint , GLenum , const GLchar *)>(internal::get_proc_address("glGetProgramResourceIndex"));
			if (!internal::glGetProgramResourceIndex) return false;
			internal::glGetProgramResourceName = reinterpret_cast<void (*)(GLuint , GLenum , GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetProgramResourceName"));
			if (!internal::glGetProgramResourceName) return false;
			internal::glGetProgramResourceiv = reinterpret_cast<void (*)(GLuint , GLenum , GLuint , GLsizei , const GLenum *, GLsizei , GLsizei *, GLint *)>(internal::get_proc_address("glGetProgramResourceiv"));
			if (!internal::glGetProgramResourceiv) return false;
			internal::glGetProgramResourceLocation = reinterpret_cast<GLint (*)(GLuint , GLenum , const GLchar *)>(internal::get_proc_address("glGetProgramResourceLocation"));
			if (!internal::glGetProgramResourceLocation) return false;
			internal::glUseProgramStages = reinterpret_cast<void (*)(GLuint , GLbitfield , GLuint )>(internal::get_proc_address("glUseProgramStages"));
			if (!internal::glUseProgramStages) return false;
			internal::glActiveShaderProgram = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glActiveShaderProgram"));
			if (!internal::glActiveShaderProgram) return false;
			internal::glCreateShaderProgramv = reinterpret_cast<GLuint (*)(GLenum , GLsizei , const GLchar *const*)>(internal::get_proc_address("glCreateShaderProgramv"));
			if (!internal::glCreateShaderProgramv) return false;
			internal::glBindProgramPipeline = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glBindProgramPipeline"));
			if (!internal::glBindProgramPipeline) return false;
			internal::glDeleteProgramPipelines = reinterpret_cast<void (*)(GLsizei , const GLuint *)>(internal::get_proc_address("glDeleteProgramPipelines"));
			if (!internal::glDeleteProgramPipelines) return false;
			internal::glGenProgramPipelines = reinterpret_cast<void (*)(GLsizei , GLuint *)>(internal::get_proc_address("glGenProgramPipelines"));
			if (!internal::glGenProgramPipelines) return false;
			internal::glIsProgramPipeline = reinterpret_cast<GLboolean (*)(GLuint )>(internal::get_proc_address("glIsProgramPipeline"));
			if (!internal::glIsProgramPipeline) return false;
			internal::glGetProgramPipelineiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetProgramPipelineiv"));
			if (!internal::glGetProgramPipelineiv) return false;
			internal::glProgramUniform1i = reinterpret_cast<void (*)(GLuint , GLint , GLint )>(internal::get_proc_address("glProgramUniform1i"));
			if (!internal::glProgramUniform1i) return false;
			internal::glProgramUniform2i = reinterpret_cast<void (*)(GLuint , GLint , GLint , GLint )>(internal::get_proc_address("glProgramUniform2i"));
			if (!internal::glProgramUniform2i) return false;
			internal::glProgramUniform3i = reinterpret_cast<void (*)(GLuint , GLint , GLint , GLint , GLint )>(internal::get_proc_address("glProgramUniform3i"));
			if (!internal::glProgramUniform3i) return false;
			internal::glProgramUniform4i = reinterpret_cast<void (*)(GLuint , GLint , GLint , GLint , GLint , GLint )>(internal::get_proc_address("glProgramUniform4i"));
			if (!internal::glProgramUniform4i) return false;
			internal::glProgramUniform1ui = reinterpret_cast<void (*)(GLuint , GLint , GLuint )>(internal::get_proc_address("glProgramUniform1ui"));
			if (!internal::glProgramUniform1ui) return false;
			internal::glProgramUniform2ui = reinterpret_cast<void (*)(GLuint , GLint , GLuint , GLuint )>(internal::get_proc_address("glProgramUniform2ui"));
			if (!internal::glProgramUniform2ui) return false;
			internal::glProgramUniform3ui = reinterpret_cast<void (*)(GLuint , GLint , GLuint , GLuint , GLuint )>(internal::get_proc_address("glProgramUniform3ui"));
			if (!internal::glProgramUniform3ui) return false;
			internal::glProgramUniform4ui = reinterpret_cast<void (*)(GLuint , GLint , GLuint , GLuint , GLuint , GLuint )>(internal::get_proc_address("glProgramUniform4ui"));
			if (!internal::glProgramUniform4ui) return false;
			internal::glProgramUniform1f = reinterpret_cast<void (*)(GLuint , GLint , GLfloat )>(internal::get_proc_address("glProgramUniform1f"));
			if (!internal::glProgramUniform1f) return false;
			internal::glProgramUniform2f = reinterpret_cast<void (*)(GLuint , GLint , GLfloat , GLfloat )>(internal::get_proc_address("glProgramUniform2f"));
			if (!internal::glProgramUniform2f) return false;
			internal::glProgramUniform3f = reinterpret_cast<void (*)(GLuint , GLint , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glProgramUniform3f"));
			if (!internal::glProgramUniform3f) return false;
			internal::glProgramUniform4f = reinterpret_cast<void (*)(GLuint , GLint , GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glProgramUniform4f"));
			if (!internal::glProgramUniform4f) return false;
			internal::glProgramUniform1iv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLint *)>(internal::get_proc_address("glProgramUniform1iv"));
			if (!internal::glProgramUniform1iv) return false;
			internal::glProgramUniform2iv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLint *)>(internal::get_proc_address("glProgramUniform2iv"));
			if (!internal::glProgramUniform2iv) return false;
			internal::glProgramUniform3iv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLint *)>(internal::get_proc_address("glProgramUniform3iv"));
			if (!internal::glProgramUniform3iv) return false;
			internal::glProgramUniform4iv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLint *)>(internal::get_proc_address("glProgramUniform4iv"));
			if (!internal::glProgramUniform4iv) return false;
			internal::glProgramUniform1uiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glProgramUniform1uiv"));
			if (!internal::glProgramUniform1uiv) return false;
			internal::glProgramUniform2uiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glProgramUniform2uiv"));
			if (!internal::glProgramUniform2uiv) return false;
			internal::glProgramUniform3uiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glProgramUniform3uiv"));
			if (!internal::glProgramUniform3uiv) return false;
			internal::glProgramUniform4uiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLuint *)>(internal::get_proc_address("glProgramUniform4uiv"));
			if (!internal::glProgramUniform4uiv) return false;
			internal::glProgramUniform1fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glProgramUniform1fv"));
			if (!internal::glProgramUniform1fv) return false;
			internal::glProgramUniform2fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glProgramUniform2fv"));
			if (!internal::glProgramUniform2fv) return false;
			internal::glProgramUniform3fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glProgramUniform3fv"));
			if (!internal::glProgramUniform3fv) return false;
			internal::glProgramUniform4fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , const GLfloat *)>(internal::get_proc_address("glProgramUniform4fv"));
			if (!internal::glProgramUniform4fv) return false;
			internal::glProgramUniformMatrix2fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix2fv"));
			if (!internal::glProgramUniformMatrix2fv) return false;
			internal::glProgramUniformMatrix3fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix3fv"));
			if (!internal::glProgramUniformMatrix3fv) return false;
			internal::glProgramUniformMatrix4fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix4fv"));
			if (!internal::glProgramUniformMatrix4fv) return false;
			internal::glProgramUniformMatrix2x3fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix2x3fv"));
			if (!internal::glProgramUniformMatrix2x3fv) return false;
			internal::glProgramUniformMatrix3x2fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix3x2fv"));
			if (!internal::glProgramUniformMatrix3x2fv) return false;
			internal::glProgramUniformMatrix2x4fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix2x4fv"));
			if (!internal::glProgramUniformMatrix2x4fv) return false;
			internal::glProgramUniformMatrix4x2fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix4x2fv"));
			if (!internal::glProgramUniformMatrix4x2fv) return false;
			internal::glProgramUniformMatrix3x4fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix3x4fv"));
			if (!internal::glProgramUniformMatrix3x4fv) return false;
			internal::glProgramUniformMatrix4x3fv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLboolean , const GLfloat *)>(internal::get_proc_address("glProgramUniformMatrix4x3fv"));
			if (!internal::glProgramUniformMatrix4x3fv) return false;
			internal::glValidateProgramPipeline = reinterpret_cast<void (*)(GLuint )>(internal::get_proc_address("glValidateProgramPipeline"));
			if (!internal::glValidateProgramPipeline) return false;
			internal::glGetProgramPipelineInfoLog = reinterpret_cast<void (*)(GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetProgramPipelineInfoLog"));
			if (!internal::glGetProgramPipelineInfoLog) return false;
			internal::glBindImageTexture = reinterpret_cast<void (*)(GLuint , GLuint , GLint , GLboolean , GLint , GLenum , GLenum )>(internal::get_proc_address("glBindImageTexture"));
			if (!internal::glBindImageTexture) return false;
			internal::glGetBooleani_v = reinterpret_cast<void (*)(GLenum , GLuint , GLboolean *)>(internal::get_proc_address("glGetBooleani_v"));
			if (!internal::glGetBooleani_v) return false;
			internal::glMemoryBarrier = reinterpret_cast<void (*)(GLbitfield )>(internal::get_proc_address("glMemoryBarrier"));
			if (!internal::glMemoryBarrier) return false;
			internal::glMemoryBarrierByRegion = reinterpret_cast<void (*)(GLbitfield )>(internal::get_proc_address("glMemoryBarrierByRegion"));
			if (!internal::glMemoryBarrierByRegion) return false;
			internal::glTexStorage2DMultisample = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , GLsizei , GLsizei , GLboolean )>(internal::get_proc_address("glTexStorage2DMultisample"));
			if (!internal::glTexStorage2DMultisample) return false;
			internal::glGetMultisamplefv = reinterpret_cast<void (*)(GLenum , GLuint , GLfloat *)>(internal::get_proc_address("glGetMultisamplefv"));
			if (!internal::glGetMultisamplefv) return false;
			internal::glSampleMaski = reinterpret_cast<void (*)(GLuint , GLbitfield )>(internal::get_proc_address("glSampleMaski"));
			if (!internal::glSampleMaski) return false;
			internal::glGetTexLevelParameteriv = reinterpret_cast<void (*)(GLenum , GLint , GLenum , GLint *)>(internal::get_proc_address("glGetTexLevelParameteriv"));
			if (!internal::glGetTexLevelParameteriv) return false;
			internal::glGetTexLevelParameterfv = reinterpret_cast<void (*)(GLenum , GLint , GLenum , GLfloat *)>(internal::get_proc_address("glGetTexLevelParameterfv"));
			if (!internal::glGetTexLevelParameterfv) return false;
			internal::glBindVertexBuffer = reinterpret_cast<void (*)(GLuint , GLuint , GLintptr , GLsizei )>(internal::get_proc_address("glBindVertexBuffer"));
			if (!internal::glBindVertexBuffer) return false;
			internal::glVertexAttribFormat = reinterpret_cast<void (*)(GLuint , GLint , GLenum , GLboolean , GLuint )>(internal::get_proc_address("glVertexAttribFormat"));
			if (!internal::glVertexAttribFormat) return false;
			internal::glVertexAttribIFormat = reinterpret_cast<void (*)(GLuint , GLint , GLenum , GLuint )>(internal::get_proc_address("glVertexAttribIFormat"));
			if (!internal::glVertexAttribIFormat) return false;
			internal::glVertexAttribBinding = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glVertexAttribBinding"));
			if (!internal::glVertexAttribBinding) return false;
			internal::glVertexBindingDivisor = reinterpret_cast<void (*)(GLuint , GLuint )>(internal::get_proc_address("glVertexBindingDivisor"));
			if (!internal::glVertexBindingDivisor) return false;

			// OpenGL ES 3.2

			internal::glBlendBarrier = reinterpret_cast<void (*)()>(internal::get_proc_address("glBlendBarrier"));
			if (!internal::glBlendBarrier) return false;
			internal::glCopyImageSubData = reinterpret_cast<void (*)(GLuint , GLenum , GLint , GLint , GLint , GLint , GLuint , GLenum , GLint , GLint , GLint , GLint , GLsizei , GLsizei , GLsizei )>(internal::get_proc_address("glCopyImageSubData"));
			if (!internal::glCopyImageSubData) return false;
			internal::glDebugMessageControl = reinterpret_cast<void (*)(GLenum , GLenum , GLenum , GLsizei , const GLuint *, GLboolean )>(internal::get_proc_address("glDebugMessageControl"));
			if (!internal::glDebugMessageControl) return false;
			internal::glDebugMessageInsert = reinterpret_cast<void (*)(GLenum , GLenum , GLuint , GLenum , GLsizei , const GLchar *)>(internal::get_proc_address("glDebugMessageInsert"));
			if (!internal::glDebugMessageInsert) return false;
			internal::glDebugMessageCallback = reinterpret_cast<void (*)(GLDEBUGPROC , const void *)>(internal::get_proc_address("glDebugMessageCallback"));
			if (!internal::glDebugMessageCallback) return false;
			internal::glGetDebugMessageLog = reinterpret_cast<GLuint (*)(GLuint , GLsizei , GLenum *, GLenum *, GLuint *, GLenum *, GLsizei *, GLchar *)>(internal::get_proc_address("glGetDebugMessageLog"));
			if (!internal::glGetDebugMessageLog) return false;
			internal::glPushDebugGroup = reinterpret_cast<void (*)(GLenum , GLuint , GLsizei , const GLchar *)>(internal::get_proc_address("glPushDebugGroup"));
			if (!internal::glPushDebugGroup) return false;
			internal::glPopDebugGroup = reinterpret_cast<void (*)()>(internal::get_proc_address("glPopDebugGroup"));
			if (!internal::glPopDebugGroup) return false;
			internal::glObjectLabel = reinterpret_cast<void (*)(GLenum , GLuint , GLsizei , const GLchar *)>(internal::get_proc_address("glObjectLabel"));
			if (!internal::glObjectLabel) return false;
			internal::glGetObjectLabel = reinterpret_cast<void (*)(GLenum , GLuint , GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetObjectLabel"));
			if (!internal::glGetObjectLabel) return false;
			internal::glObjectPtrLabel = reinterpret_cast<void (*)(const void *, GLsizei , const GLchar *)>(internal::get_proc_address("glObjectPtrLabel"));
			if (!internal::glObjectPtrLabel) return false;
			internal::glGetObjectPtrLabel = reinterpret_cast<void (*)(const void *, GLsizei , GLsizei *, GLchar *)>(internal::get_proc_address("glGetObjectPtrLabel"));
			if (!internal::glGetObjectPtrLabel) return false;
			internal::glGetPointerv = reinterpret_cast<void (*)(GLenum , void **)>(internal::get_proc_address("glGetPointerv"));
			if (!internal::glGetPointerv) return false;
			internal::glEnablei = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glEnablei"));
			if (!internal::glEnablei) return false;
			internal::glDisablei = reinterpret_cast<void (*)(GLenum , GLuint )>(internal::get_proc_address("glDisablei"));
			if (!internal::glDisablei) return false;
			internal::glBlendEquationi = reinterpret_cast<void (*)(GLuint , GLenum )>(internal::get_proc_address("glBlendEquationi"));
			if (!internal::glBlendEquationi) return false;
			internal::glBlendEquationSeparatei = reinterpret_cast<void (*)(GLuint , GLenum , GLenum )>(internal::get_proc_address("glBlendEquationSeparatei"));
			if (!internal::glBlendEquationSeparatei) return false;
			internal::glBlendFunci = reinterpret_cast<void (*)(GLuint , GLenum , GLenum )>(internal::get_proc_address("glBlendFunci"));
			if (!internal::glBlendFunci) return false;
			internal::glBlendFuncSeparatei = reinterpret_cast<void (*)(GLuint , GLenum , GLenum , GLenum , GLenum )>(internal::get_proc_address("glBlendFuncSeparatei"));
			if (!internal::glBlendFuncSeparatei) return false;
			internal::glColorMaski = reinterpret_cast<void (*)(GLuint , GLboolean , GLboolean , GLboolean , GLboolean )>(internal::get_proc_address("glColorMaski"));
			if (!internal::glColorMaski) return false;
			internal::glIsEnabledi = reinterpret_cast<GLboolean (*)(GLenum , GLuint )>(internal::get_proc_address("glIsEnabledi"));
			if (!internal::glIsEnabledi) return false;
			internal::glDrawElementsBaseVertex = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , const void *, GLint )>(internal::get_proc_address("glDrawElementsBaseVertex"));
			if (!internal::glDrawElementsBaseVertex) return false;
			internal::glDrawRangeElementsBaseVertex = reinterpret_cast<void (*)(GLenum , GLuint , GLuint , GLsizei , GLenum , const void *, GLint )>(internal::get_proc_address("glDrawRangeElementsBaseVertex"));
			if (!internal::glDrawRangeElementsBaseVertex) return false;
			internal::glDrawElementsInstancedBaseVertex = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , const void *, GLsizei , GLint )>(internal::get_proc_address("glDrawElementsInstancedBaseVertex"));
			if (!internal::glDrawElementsInstancedBaseVertex) return false;
			internal::glFramebufferTexture = reinterpret_cast<void (*)(GLenum , GLenum , GLuint , GLint )>(internal::get_proc_address("glFramebufferTexture"));
			if (!internal::glFramebufferTexture) return false;
			internal::glPrimitiveBoundingBox = reinterpret_cast<void (*)(GLfloat , GLfloat , GLfloat , GLfloat , GLfloat , GLfloat , GLfloat , GLfloat )>(internal::get_proc_address("glPrimitiveBoundingBox"));
			if (!internal::glPrimitiveBoundingBox) return false;
			internal::glGetGraphicsResetStatus = reinterpret_cast<GLenum (*)()>(internal::get_proc_address("glGetGraphicsResetStatus"));
			if (!internal::glGetGraphicsResetStatus) return false;
			internal::glReadnPixels = reinterpret_cast<void (*)(GLint , GLint , GLsizei , GLsizei , GLenum , GLenum , GLsizei , void *)>(internal::get_proc_address("glReadnPixels"));
			if (!internal::glReadnPixels) return false;
			internal::glGetnUniformfv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLfloat *)>(internal::get_proc_address("glGetnUniformfv"));
			if (!internal::glGetnUniformfv) return false;
			internal::glGetnUniformiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLint *)>(internal::get_proc_address("glGetnUniformiv"));
			if (!internal::glGetnUniformiv) return false;
			internal::glGetnUniformuiv = reinterpret_cast<void (*)(GLuint , GLint , GLsizei , GLuint *)>(internal::get_proc_address("glGetnUniformuiv"));
			if (!internal::glGetnUniformuiv) return false;
			internal::glMinSampleShading = reinterpret_cast<void (*)(GLfloat )>(internal::get_proc_address("glMinSampleShading"));
			if (!internal::glMinSampleShading) return false;
			internal::glPatchParameteri = reinterpret_cast<void (*)(GLenum , GLint )>(internal::get_proc_address("glPatchParameteri"));
			if (!internal::glPatchParameteri) return false;
			internal::glTexParameterIiv = reinterpret_cast<void (*)(GLenum , GLenum , const GLint *)>(internal::get_proc_address("glTexParameterIiv"));
			if (!internal::glTexParameterIiv) return false;
			internal::glTexParameterIuiv = reinterpret_cast<void (*)(GLenum , GLenum , const GLuint *)>(internal::get_proc_address("glTexParameterIuiv"));
			if (!internal::glTexParameterIuiv) return false;
			internal::glGetTexParameterIiv = reinterpret_cast<void (*)(GLenum , GLenum , GLint *)>(internal::get_proc_address("glGetTexParameterIiv"));
			if (!internal::glGetTexParameterIiv) return false;
			internal::glGetTexParameterIuiv = reinterpret_cast<void (*)(GLenum , GLenum , GLuint *)>(internal::get_proc_address("glGetTexParameterIuiv"));
			if (!internal::glGetTexParameterIuiv) return false;
			internal::glSamplerParameterIiv = reinterpret_cast<void (*)(GLuint , GLenum , const GLint *)>(internal::get_proc_address("glSamplerParameterIiv"));
			if (!internal::glSamplerParameterIiv) return false;
			internal::glSamplerParameterIuiv = reinterpret_cast<void (*)(GLuint , GLenum , const GLuint *)>(internal::get_proc_address("glSamplerParameterIuiv"));
			if (!internal::glSamplerParameterIuiv) return false;
			internal::glGetSamplerParameterIiv = reinterpret_cast<void (*)(GLuint , GLenum , GLint *)>(internal::get_proc_address("glGetSamplerParameterIiv"));
			if (!internal::glGetSamplerParameterIiv) return false;
			internal::glGetSamplerParameterIuiv = reinterpret_cast<void (*)(GLuint , GLenum , GLuint *)>(internal::get_proc_address("glGetSamplerParameterIuiv"));
			if (!internal::glGetSamplerParameterIuiv) return false;
			internal::glTexBuffer = reinterpret_cast<void (*)(GLenum , GLenum , GLuint )>(internal::get_proc_address("glTexBuffer"));
			if (!internal::glTexBuffer) return false;
			internal::glTexBufferRange = reinterpret_cast<void (*)(GLenum , GLenum , GLuint , GLintptr , GLsizeiptr )>(internal::get_proc_address("glTexBufferRange"));
			if (!internal::glTexBufferRange) return false;
			internal::glTexStorage3DMultisample = reinterpret_cast<void (*)(GLenum , GLsizei , GLenum , GLsizei , GLsizei , GLsizei , GLboolean )>(internal::get_proc_address("glTexStorage3DMultisample"));
			if (!internal::glTexStorage3DMultisample) return false;

			return true;
		}

		static bool load_ext_GL_ARB_compute_shader()
		{

			return true;
		}

		static bool load_ext_GL_ARB_shader_image_load_store()
		{

			return true;
		}

		static bool load_ext_GL_ARB_texture_filter_anisotropic()
		{

			return true;
		}

		static std::unordered_set<std::string> extensions;
		bool initialize()
		{
			if (!load_core()) return false;

			GLint num_extensions;
			internal::glGetIntegerv(0x821D, &num_extensions);
			for (GLint i = 0; i < num_extensions; ++i)
				extensions.insert(reinterpret_cast<const char *>(internal::glGetStringi(0x1F03, i)));

			if (extensions.count("GL_ARB_compute_shader") > 0)
				ext_GL_ARB_compute_shader_loaded = load_ext_GL_ARB_compute_shader();
			if (extensions.count("GL_ARB_shader_image_load_store") > 0)
				ext_GL_ARB_shader_image_load_store_loaded = load_ext_GL_ARB_shader_image_load_store();
			if (extensions.count("GL_ARB_texture_filter_anisotropic") > 0)
				ext_GL_ARB_texture_filter_anisotropic_loaded = load_ext_GL_ARB_texture_filter_anisotropic();

			return true;
		}

		const char * api(){ return "OpenGL ES"; }

		int major_version(){ return 3; }

		int minor_version(){ return 2; }

		bool has_extension(const char * name){ return extensions.contains(name); }

		bool ext_ARB_compute_shader(){ return ext_GL_ARB_compute_shader_loaded; }
		bool ext_ARB_shader_image_load_store(){ return ext_GL_ARB_shader_image_load_store_loaded; }
		bool ext_ARB_texture_filter_anisotropic(){ return ext_GL_ARB_texture_filter_anisotropic_loaded; }

		const char * shader_prefix()
		{
			return R"(#version 320 es

precision highp int;
precision highp float;
precision highp sampler2D;
precision highp usampler2D;

)";
		}

	} // namespace sys

} // namespace gl

