#pragma once

#include <psemek/gfx/texture.hpp>
#include <psemek/gfx/render_target.hpp>

#include <psemek/util/pimpl.hpp>

namespace psemek::gfx
{

	struct gamma_correction
	{
		gamma_correction();
		~gamma_correction();

		struct options
		{
			float gamma = 1.f / 2.2f;
		};

		void invoke(texture_2d const & src, render_target const & dst, options const & opts);

	private:
		psemek_declare_pimpl
	};

}
