#pragma once

#include <psemek/gfx/texture.hpp>
#include <psemek/gfx/render_target.hpp>

#include <psemek/util/pimpl.hpp>

namespace psemek::gfx
{

	struct overlay
	{
		overlay();
		~overlay();

		void invoke(texture_2d const & src, render_target const & dst);

	private:
		psemek_declare_pimpl
	};

}
