#pragma once

#include <psemek/gfx/texture.hpp>
#include <psemek/gfx/render_target.hpp>

#include <psemek/util/pimpl.hpp>

namespace psemek::gfx
{

	struct hblur
	{
		hblur(int size, float sigma, float gamma = 1.f);
		~hblur();

		void invoke(texture_2d const & src, render_target const & dst);

	private:
		psemek_declare_pimpl
	};

	struct vblur
	{
		vblur(int size, float sigma, float gamma = 1.f);
		~vblur();

		void invoke(texture_2d const & src, render_target const & dst);

	private:
		psemek_declare_pimpl
	};

}
