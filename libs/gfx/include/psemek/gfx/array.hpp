#pragma once

#include <psemek/gfx/gl.hpp>

#include <cstddef>

namespace psemek::gfx
{

	struct array
	{
		array();
		array(array &&);
		array & operator = (array &&);
		~array();

		array(array const &) = delete;
		array & operator = (array const &) = delete;

		static array null();

		GLuint id() const { return id_; }

		explicit operator bool() const { return id() != 0; }

		void bind() const;

		void reset();

	private:
		GLuint id_ = 0;

		explicit array(std::nullptr_t);
	};

}
