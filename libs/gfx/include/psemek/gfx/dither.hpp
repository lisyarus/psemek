#pragma once

#include <psemek/gfx/pixmap.hpp>

namespace psemek::gfx
{

	basic_pixmap<std::uint8_t> ordered_dither(std::size_t size);

}
