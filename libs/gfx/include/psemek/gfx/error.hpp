#pragma once

#include <string_view>

namespace psemek::gfx
{

	std::string_view gl_error_str(unsigned int e);

	void check_error(std::string_view context = "");

}
