#pragma once

#include <cstddef>
#include <optional>

namespace psemek::gfx
{

	std::optional<std::size_t> available_memory();

}
