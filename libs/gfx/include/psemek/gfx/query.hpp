#pragma once

#include <psemek/gfx/gl.hpp>
#include <psemek/util/function.hpp>

#include <vector>

namespace psemek::gfx
{

	struct query_pool
	{
		struct scope
		{
			GLenum target;

			~scope();
		};

		scope begin(GLenum target, util::function<void(GLint)> callback);

		void poll();

		std::size_t size() const { return ids_.size(); }

		~query_pool();

	private:
		std::vector<GLuint> ids_;
		std::vector<util::function<void(GLint)>> callbacks_;
	};

}
