#pragma once

#include <psemek/gfx/gltf_parser.hpp>
#include <psemek/gfx/drawable.hpp>
#include <psemek/gfx/texture.hpp>
#include <psemek/math/simplex.hpp>
#include <psemek/util/span.hpp>
#include <psemek/util/blob.hpp>

#include <functional>

namespace psemek::gfx
{

	struct gltf_mesh
	{
		struct primitive
		{
			gfx::drawable * drawable;
			std::optional<std::size_t> material;

			util::span<math::point<float, 3> const> vertices;
			util::span<math::triangle<std::uint32_t> const> triangles;
		};

		virtual gltf_asset::material const & material(std::size_t index) const = 0;
		virtual gfx::texture_2d const & texture(std::size_t index) const = 0;
		virtual util::span<primitive const> mesh(std::string_view name) const = 0;
		virtual gltf_asset::extra const * mesh_property(std::string_view mesh_name, std::string_view property_name) const = 0;

		virtual ~gltf_mesh() {}
	};

	std::unique_ptr<gltf_mesh> make_gltf_mesh(gltf_asset const & asset, std::function<util::blob(std::string const &)> uri_loader);

}
