#pragma once

#include <psemek/math/vector.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/simplex.hpp>

#include <iostream>
#include <vector>

namespace psemek::gfx
{

	struct obj_data
	{
		struct vertex
		{
			math::point<float, 3> position;
			math::vector<float, 2> texcoord;
			math::vector<float, 3> normal;
		};

		std::vector<math::triangle<vertex>> triangles;
	};

	obj_data parse_obj(std::istream & is);

}
