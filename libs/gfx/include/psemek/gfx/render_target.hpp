#pragma once

#include <psemek/gfx/framebuffer.hpp>

#include <psemek/math/box.hpp>

#include <psemek/util/assert.hpp>

namespace psemek::gfx
{

	struct render_target
	{
		gfx::framebuffer const * framebuffer;
		GLenum draw_buffer;
		math::box<int, 2> viewport;

		void bind() const
		{
			assert(framebuffer);
			framebuffer->bind();
			gl::DrawBuffers(1, &draw_buffer);
			gl::Viewport(viewport[0].min, viewport[1].min, viewport[0].length(), viewport[1].length());
		}
	};

}
