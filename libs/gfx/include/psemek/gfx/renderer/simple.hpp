#pragma once

#include <psemek/gfx/drawable.hpp>
#include <psemek/gfx/color.hpp>
#include <psemek/gfx/texture.hpp>

#include <psemek/math/matrix.hpp>

#include <psemek/util/pimpl.hpp>

#include <vector>
#include <memory>

namespace psemek::gfx
{

	struct simple_renderer
	{
		// Attributes:
		// 0: vec3 position
		// 1: vec4 color (if uniform color is not set)
		// 2: vec2 texcoord (if texture is present)

		struct render_state
		{
			drawable const * mesh = nullptr;
			std::optional<color_rgba> color = std::nullopt;
			texture_2d const * texture = nullptr;
		};

		struct render_object
		{
			virtual std::vector<render_state> get_render_states() = 0;

			virtual ~render_object() {}
		};

		struct render_options
		{
			math::matrix<float, 4, 4> transform;
		};

		simple_renderer();
		~simple_renderer();

		void add(render_object * o);
		void remove(render_object * o);

		void push(render_state s);

		void render(render_options const & opts);

	private:
		psemek_declare_pimpl
	};

}
