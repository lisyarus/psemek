#pragma once

namespace psemek::gfx
{

	struct drawable
	{
		virtual void draw() const = 0;

		virtual ~drawable() {}
	};

}
