#pragma once

#include <psemek/gfx/mesh.hpp>

namespace psemek::gfx
{

	mesh const & fullscreen_quad();

}
