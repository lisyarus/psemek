#include <psemek/gfx/memory.hpp>
#include <psemek/gfx/gl.hpp>

namespace psemek::gfx
{

	std::optional<std::size_t> available_memory()
	{
		static bool const nvx = gl::sys::has_extension("GL_NVX_gpu_memory_info");
		static bool const ati = gl::sys::has_extension("GL_ATI_meminfo");

		if (nvx)
		{
			GLint value = 0;
			gl::GetIntegerv(0x9049, &value);
			return static_cast<std::size_t>(value) * 1024;
		}

		if (ati)
		{
			GLint values[4]{};
			gl::GetIntegerv(0x87FC, values);
			return static_cast<std::size_t>(values[0]) * 1024;
		}

		return std::nullopt;
	}

}
