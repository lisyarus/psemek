#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <psemek/util/assert.hpp>
#define STBIW_ASSERT(x) assert(x)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Warray-bounds"
#include <psemek/gfx/detail/stb_image_write.h>
#pragma GCC diagnostic pop
