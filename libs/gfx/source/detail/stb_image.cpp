#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG

#include <psemek/util/assert.hpp>
#define STBI_ASSERT(x) assert(x)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#include <psemek/gfx/detail/stb_image.h>
#pragma GCC diagnostic pop
