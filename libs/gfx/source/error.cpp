#include <psemek/gfx/error.hpp>
#include <psemek/gfx/gl.hpp>
#include <psemek/util/to_string.hpp>
#include <psemek/util/exception.hpp>

#include <stdexcept>

namespace psemek::gfx
{

	static_assert(std::is_same_v<unsigned int, GLenum>);

	std::string_view gl_error_str(GLenum e)
	{
		switch (e)
		{
		case gl::NO_ERROR: return "GL_NO_ERROR";
		case gl::INVALID_ENUM: return "GL_INVALID_ENUM";
		case gl::INVALID_VALUE: return "GL_INVALID_VALUE";
		case gl::INVALID_OPERATION: return "GL_INVALID_OPERATION";
		case gl::INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case gl::OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
		}

		return "(unknown)";
	}

	void check_error(std::string_view context)
	{
		if (context.empty())
			context = "OpenGL error: ";

		auto e = gl::GetError();

		if (e != gl::NO_ERROR)
			throw util::exception(util::to_string(context, gl_error_str(e)));
	}

}
