#include <psemek/gfx/renderbuffer.hpp>

#include <utility>

namespace psemek::gfx
{

	renderbuffer::renderbuffer()
	{
		gl::GenRenderbuffers(1, &id_);
	}

	renderbuffer::renderbuffer(renderbuffer && other)
		: id_{other.id_}
	{
		other.id_ = 0;
	}

	renderbuffer & renderbuffer::operator = (renderbuffer && other)
	{
		if (this == &other) return *this;

		reset();
		std::swap(id_, other.id_);
		return *this;
	}

	renderbuffer::~renderbuffer()
	{
		reset();
	}

	renderbuffer renderbuffer::null()
	{
		return renderbuffer(nullptr);
	}

	void renderbuffer::bind() const
	{
		gl::BindRenderbuffer(target, id_);
	}

	void renderbuffer::reset()
	{
		if (id_ != 0)
			gl::DeleteRenderbuffers(1, &id_);
		id_ = 0;
	}

	void renderbuffer::storage(GLenum internal_format, math::vector<std::size_t, 2> const & size)
	{
		bind();
		gl::RenderbufferStorage(target, internal_format, size[0], size[1]);
		size_ = size;
		samples_ = std::nullopt;
	}

	void renderbuffer::storage(GLenum internal_format, math::vector<std::size_t, 2> const & size, int samples)
	{
		bind();
		gl::RenderbufferStorageMultisample(target, samples, internal_format, size[0], size[1]);
		size_ = size;
		samples_ = samples;
	}

	renderbuffer::renderbuffer(std::nullptr_t)
	{}

}
