#include <psemek/gfx/fullscreen.hpp>

namespace psemek::gfx
{

	static mesh create_fullscreen_quad()
	{
		mesh m;

		std::vector<math::point<float, 2>> vertices(4);
		std::vector<std::uint8_t> indices(6);

		vertices[0] = {-1.f, -1.f};
		vertices[1] = { 1.f, -1.f};
		vertices[2] = {-1.f,  1.f};
		vertices[3] = { 1.f,  1.f};

		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;
		indices[3] = 2;
		indices[4] = 1;
		indices[5] = 3;

		m.setup<math::point<float, 2>>();
		m.load(vertices, indices, gl::STATIC_DRAW);

		return m;
	}

	mesh const & fullscreen_quad()
	{
		static mesh m = create_fullscreen_quad();
		return m;
	}

}
