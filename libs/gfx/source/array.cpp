#include <psemek/gfx/array.hpp>

#include <utility>

namespace psemek::gfx
{

	array::array()
	{
		gl::GenVertexArrays(1, &id_);
	}

	array::array(array && other)
		: id_{other.id_}
	{
		other.id_ = 0;
	}

	array & array::operator = (array && other)
	{
		if (this == &other) return *this;

		reset();
		std::swap(id_, other.id_);
		return *this;
	}

	array::~array()
	{
		reset();
	}

	array array::null()
	{
		return array(nullptr);
	}

	void array::bind() const
	{
		gl::BindVertexArray(id_);
	}

	void array::reset()
	{
		if (id_ != 0)
			gl::DeleteVertexArrays(1, &id_);
		id_ = 0;
	}

	array::array(std::nullptr_t)
	{}

}
