#include <psemek/gfx/init.hpp>
#include <psemek/gfx/gl.hpp>
#include <psemek/log/log.hpp>
#include <psemek/util/exception.hpp>

namespace psemek::gfx
{

	void init()
	{
		if (!gl::sys::initialize())
			throw util::exception("Failed to load OpenGL functions");

		auto vendor = gl::GetString(gl::VENDOR);
		auto renderer = gl::GetString(gl::RENDERER);

		int major, minor;
		gl::GetIntegerv(gl::MAJOR_VERSION, &major);
		gl::GetIntegerv(gl::MINOR_VERSION, &minor);
		log::info() << "Initialized " << gl::sys::api() << ' ' << major << '.' << minor << ", " << vendor << ", " << renderer;
	}

}
