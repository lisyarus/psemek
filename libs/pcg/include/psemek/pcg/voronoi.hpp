#pragma once

#include <psemek/math/point.hpp>
#include <psemek/pcg/seamless.hpp>

#include <vector>

namespace psemek::pcg
{

	struct voronoi
	{
		voronoi(std::vector<math::point<float, 2>> points);
		voronoi(std::vector<math::point<float, 2>> points, seamless_tag);

		struct result_type
		{
			std::size_t index;
			float distance;
		};

		result_type operator()(float x, float y) const;

	private:
		std::vector<math::point<float, 2>> points_;
		bool seamless_ = false;
	};

}
