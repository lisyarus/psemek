#pragma once

namespace psemek::pcg
{

	struct seamless_tag{};

	constexpr seamless_tag seamless;

}
