#pragma once

#include <psemek/math/interval.hpp>
#include <psemek/gfx/pixmap.hpp>

#include <psemek/pcg/random/uniform_int.hpp>
#include <psemek/pcg/random/uniform_real.hpp>

namespace psemek::pcg
{

	template <typename T = float, typename RNG>
	gfx::basic_pixmap<T> white(std::size_t width, std::size_t height, RNG && rng, math::interval<T> const & range)
	{
		using dist = std::conditional_t<std::is_floating_point_v<T>, uniform_real_distribution<T>, uniform_int_distribution<T>>;

		dist d{range};

		gfx::basic_pixmap<T> result({width, height});

		for (std::size_t x = 0; x < width; ++x)
			for (std::size_t y = 0; y < height; ++y)
				result(x, y) = d(rng);

		return result;
	}

}
