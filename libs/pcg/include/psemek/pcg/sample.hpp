#pragma once

#include <psemek/gfx/pixmap.hpp>

namespace psemek::pcg
{

	template <typename Gen, typename R>
	void sample(Gen && gen, gfx::basic_pixmap<R> & result)
	{
		for (std::size_t j = 0; j < result.height(); ++j)
		{
			for (std::size_t i = 0; i < result.width(); ++i)
			{
				float ti = (i + 0.5f) / result.width();
				float tj = (j + 0.5f) / result.height();

				result(i, j) = gen(ti, tj);
			}
		}
	}

	template <typename Gen, typename R>
	void sample(Gen && gen, util::array<R, 3> & result)
	{
		for (std::size_t k = 0; k < result.height(); ++k)
		{
			for (std::size_t j = 0; j < result.height(); ++j)
			{
				for (std::size_t i = 0; i < result.width(); ++i)
				{
					float ti = (i + 0.5f) / result.width();
					float tj = (j + 0.5f) / result.height();
					float tk = (k + 0.5f) / result.depth();

					result(i, j, k) = gen(ti, tj, tk);
				}
			}
		}
	}

	template <typename Gen>
	auto sample(Gen && gen, std::size_t width, std::size_t height)
	{
		using R = decltype(gen(0.f, 0.f));

		gfx::basic_pixmap<R> result({width, height});

		sample(std::forward<Gen>(gen), result);
		return result;
	}

	template <typename Gen>
	auto sample(Gen && gen, std::size_t width, std::size_t height, std::size_t depth)
	{
		using R = decltype(gen(0.f, 0.f, 0.f));

		util::array<R, 3> result({width, height, depth});

		sample(std::forward<Gen>(gen), result);
		return result;
	}

}
