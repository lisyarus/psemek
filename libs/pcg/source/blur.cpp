#include <psemek/pcg/blur.hpp>
#include <psemek/gfx/color.hpp>

#include <cmath>
#include <vector>

namespace psemek::pcg
{

	namespace detail
	{

		std::vector<float> gauss_coeffs(int size, float sigma)
		{
			std::vector<float> res(2 * size + 1);
			float sum = 0.f;
			for (int i = -size; i <= size; ++i)
			{
				float x = (i / sigma);
				res[i + size] = std::exp(- x * x);
				sum += res[i + size];
			}
			for (auto & c : res)
				c /= sum;
			return res;
		}

	}

	template <typename Pixel>
	auto color_blur_impl(gfx::basic_pixmap<Pixel> const & p, int size, float sigma, seamless_tag)
	{
		auto to = [](auto const & c){ return gfx::to_colorf(c); };
		auto from = [](auto const & c){ return gfx::to_coloru8(c); };
		return blur(p, to, from, size, sigma, seamless);
	}

	gfx::pixmap_rgb blur(gfx::pixmap_rgb const & p, int size, float sigma, seamless_tag)
	{
		return color_blur_impl(p, size, sigma, seamless);
	}

	gfx::pixmap_rgba blur(gfx::pixmap_rgba const & p, int size, float sigma, seamless_tag)
	{
		return color_blur_impl(p, size, sigma, seamless);
	}

}
