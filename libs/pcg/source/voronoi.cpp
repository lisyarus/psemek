#include <psemek/pcg/voronoi.hpp>

namespace psemek::pcg
{

	voronoi::voronoi(std::vector<math::point<float, 2>> points)
		: points_(std::move(points))
		, seamless_{false}
	{}

	voronoi::voronoi(std::vector<math::point<float, 2>> points, seamless_tag)
		: points_(std::move(points))
		, seamless_{true}
	{}

	voronoi::result_type voronoi::operator()(float x, float y) const
	{
		float v = std::numeric_limits<float>::infinity();
		std::size_t index = 0;

		for (std::size_t i = 0; i < points_.size(); ++i)
		{
			auto const & p = points_[i];

			float dx = std::abs(p[0] - x);
			float dy = std::abs(p[1] - y);

			if (seamless_)
			{
				dx = std::min(dx, 1.f - dx);
				dy = std::min(dy, 1.f - dy);
			}

			float const d = std::sqrt(dx * dx + dy * dy);

			if (d < v)
			{
				v = d;
				index = i;
			}
		}

		return {index, v};
	}

}
