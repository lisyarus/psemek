#include <psemek/test/test.hpp>

#include <psemek/math/point.hpp>

using namespace psemek::math;

test_case(math_point_empty)
{
	point<int, 0> pi;
	point<float, 0> pf;

	static_assert(std::is_same_v<decltype(pi)::scalar_type, int>);
	static_assert(std::is_same_v<decltype(pf)::scalar_type, float>);

	expect_equal(pi.dimension(), 0);
	expect_equal(pf.dimension(), 0);
	expect_throw(pi[0], detail::empty_array_exception);
	expect_throw(pf[0], detail::empty_array_exception);
}

test_case(math_point_init)
{
	point<int, 2> pi{1, 2};
	point<float, 3> pf{3.f, 4.f, 5.f};

	static_assert(std::is_same_v<decltype(pi)::scalar_type, int>);
	static_assert(std::is_same_v<decltype(pf)::scalar_type, float>);

	expect_equal(pi[0], 1);
	expect_equal(pi[1], 2);
	expect_equal(pf[0], 3.f);
	expect_equal(pf[1], 4.f);
	expect_equal(pf[2], 5.f);
}

test_case(math_point_deduce)
{
	point pi{1, 2};
	point pf{3.f, 4.f, 5.f};

	static_assert(std::is_same_v<decltype(pi)::scalar_type, int>);
	static_assert(std::is_same_v<decltype(pf)::scalar_type, float>);

	expect_equal(pi[0], 1);
	expect_equal(pi[1], 2);
	expect_equal(pf[0], 3.f);
	expect_equal(pf[1], 4.f);
	expect_equal(pf[2], 5.f);
}

test_case(math_point_arithmetic)
{
	point pi1{1, 2};
	point pi2{3, 4};
	vector vi{5, 6};

	expect_equal(pi1 + vi, (point{6, 8}));
	expect_equal(pi2 + vi, (point{8, 10}));
	expect_equal(pi1 - vi, (point{-4, -4}));
	expect_equal(pi2 - vi, (point{-2, -2}));
	expect_equal(pi2 - pi1, (vector{2, 2}));
	expect_equal(pi1 - pi2, (vector{-2, -2}));

	point pf1{1.f, 2.f, 3.f};
	point pf2{4.f, 5.f, 6.f};
	vector vf{7.f, 8.f, 9.f};

	expect_equal(pf1 + vf, (point{8.f, 10.f, 12.f}));
	expect_equal(pf2 + vf, (point{11.f, 13.f, 15.f}));
	expect_equal(pf1 - vf, (point{-6.f, -6.f, -6.f}));
	expect_equal(pf2 - vf, (point{-3.f, -3.f, -3.f}));
	expect_equal(pf2 - pf1, (vector{3.f, 3.f, 3.f}));
	expect_equal(pf1 - pf2, (vector{-3.f, -3.f, -3.f}));
}

test_case(math_point_compare)
{
	point pi1{1, 2};
	point pi2{3, 4};

	expect_equal(pi1, pi1);
	expect_equal(pi2, pi2);
	expect_different(pi1, pi2);
	expect_different(pi2, pi1);
	expect_less(pi1, pi2);
	expect_lequal(pi1, pi2);
	expect_greater(pi2, pi1);
	expect_gequal(pi2, pi1);
}

test_case(math_point_cast)
{
	point pi{1, 2};
	point pf{3.f, 4.f, 5.f};

	expect_equal(cast<float>(pi), (point{1.f, 2.f}));
	expect_equal(cast<int>(pf), (point{3, 4, 5}));
}

test_case(math_point_distance)
{
	point p1{2.f, 5.f};
	point p2{7.f, 17.f};

	expect_equal(distance_sqr(p1, p2), 169.f);
	expect_close(distance(p1, p2), 13.f, 1e-6);
}

test_case(math_point_lerp)
{
	point p1{1.f, 2.f, 3.f};
	point p2{5.f, 6.f, 7.f};

	expect_equal((lerp(p1, p2, 0.25f)), (point{2.f, 3.f, 4.f}));
	expect_equal((lerp(p1, p2, 0.5f )), (point{3.f, 4.f, 5.f}));
	expect_equal((lerp(p1, p2, 0.75f)), (point{4.f, 5.f, 6.f}));
}
