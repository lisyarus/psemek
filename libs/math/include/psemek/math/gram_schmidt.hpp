#pragma once

#include <psemek/math/vector.hpp>

namespace psemek::math
{

	inline bool gram_schmidt()
	{
		return true;
	}

	template <typename V1, typename ... Vs>
	bool gram_schmidt(V1 & v1, Vs & ... vs)
	{
		if (auto l = length(v1); l != 0)
			v1 /= l;
		else
			return false;

		((vs -= dot(v1, vs) * v1), ...);

		return gram_schmidt(vs...);
	}

}
