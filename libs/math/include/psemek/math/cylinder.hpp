#pragma once

#include <psemek/math/vector.hpp>
#include <psemek/math/point.hpp>

namespace psemek::math
{

	template <typename T, std::size_t N>
	struct cylinder
	{
		point<T, N> center;
		vector<T, N> axis;
		T radius;
	};

	template <typename T, std::size_t N>
	cylinder(point<T, N>, vector<T, N>, T) -> cylinder<T, N>;

}
