#pragma once

#include <psemek/math/vector.hpp>

namespace psemek::math
{

	template <typename T, std::size_t ... Ns>
	auto concat(vector<T, Ns> const & ... vs)
	{
		vector<T, (Ns + ...)> result;

		std::size_t i = 0;

		auto apply = [&]<std::size_t N>(vector<T, N> const & v)
		{
			for (std::size_t j = 0; j < N;)
				result[i++] = v[j++];
		};

		(apply(vs), ...);

		return result;
	}

}
