#pragma once

namespace psemek::math
{

	constexpr long double pi = 3.141592653589793238462643383279502884l;

	constexpr long double phi = 1.61803398874989484820l;

}
