#pragma once

#include <psemek/math/point.hpp>

namespace psemek::math
{

	template <typename T, std::size_t N>
	struct sphere
	{
		point<T, N> center;
		T radius;
	};

	template <typename T, std::size_t N>
	sphere(point<T, N>, T) -> sphere<T, N>;

}
