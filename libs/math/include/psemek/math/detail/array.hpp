#pragma once

#include <psemek/math/dynamic.hpp>
#include <psemek/util/exception.hpp>

#include <vector>

namespace psemek::math::detail
{

	struct empty_array_exception
		: util::exception
	{
		empty_array_exception(util::stacktrace stacktrace = {})
			: util::exception("Indexing an empty array", std::move(stacktrace))
		{}
	};

	template <typename T, std::size_t N>
	struct array
	{
		using type = T[N];
	};

	template <typename T>
	struct array<T, 0>
	{
		struct type
		{
			T const & operator[](std::size_t) const { throw empty_array_exception{}; }
			T & operator[](std::size_t) { throw empty_array_exception{}; }

			type operator + (std::size_t) const { return *this; }
		};
	};

	template <typename T>
	struct array<T, dynamic>
	{
		using type = std::vector<T>;
	};

	template <typename T, typename ... Args>
	struct all_convertible_to;

	template <typename T>
	struct all_convertible_to<T>
		: std::true_type
	{};

	template <typename T, typename H, typename ... Args>
	struct all_convertible_to<T, H, Args...>
		: std::bool_constant<std::is_convertible_v<H, T>
			&& all_convertible_to<T, Args...>::value>
	{};

}
