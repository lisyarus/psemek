#pragma once

#include <psemek/math/math.hpp>

#include <psemek/util/assert.hpp>

namespace psemek::math
{

	enum class easing_type
	{
		nearest,
		constant_left,
		constant_right,
		linear,
		quadratic_in,
		quadratic_out,
		cubic,
	};

	template <typename T>
	T easing(easing_type type, T t)
	{
		switch (type) {
			case easing_type::nearest: return (2 * t < 1) ? 0 : 1;
			case easing_type::constant_left: return 0;
			case easing_type::constant_right: return 1;
			case easing_type::linear: return t;
			case easing_type::quadratic_in: return t * t;
			case easing_type::quadratic_out: return t * (2 - t);
			case easing_type::cubic: return smoothstep(t);
			default: assert(false);
		}

		return {};
	}

}
