#pragma once

namespace psemek::math
{

	template <typename T>
	struct cubic_hermite
	{
		T a[4];

		cubic_hermite() = default;
		cubic_hermite(T p0, T m0, T p1, T m1)
		{
			a[0] = p0;
			a[1] = m0;
			a[2] = - 3 * p0 - 2 * m0 + 3 * p1 - m1;
			a[3] = 2 * p0 + m0 - 2 * p1 + m1;
		}

		T operator() (T t) const
		{
			return a[0] + t * (a[1] + t * (a[2] + t * a[3]));
		}
	};

}
