#pragma once

#include <psemek/math/detail/array.hpp>
#include <psemek/math/vector.hpp>
#include <psemek/math/math.hpp>

#include <iostream>
#include <iomanip>
#include <algorithm>

namespace psemek::math
{

	template <typename T, std::size_t R, std::size_t C>
	struct matrix
	{
		using scalar_type = T;
		static constexpr std::size_t static_rows = R;
		static constexpr std::size_t static_columns = C;

		typename detail::array<T, R * C>::type coords;

		std::size_t rows() const
		{
			return R;
		}

		std::size_t columns() const
		{
			return C;
		}

		auto operator[](std::size_t i)
		{
			return coords + C * i;
		}

		auto operator[](std::size_t i) const
		{
			return coords + C * i;
		}

		matrix & operator *= (T const & s);
		matrix & operator /= (T const & s);

		matrix & operator += (matrix const & v);
		matrix & operator -= (matrix const & v);

		static matrix zero();
		static matrix identity();
		static matrix scalar(T const & s);
		static matrix diagonal(vector<T, std::min(R, C)> const & d);
	};

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::zero()
	{
		matrix<T, R, C> m;
		for (std::size_t i = 0; i < R * C; ++i)
			m.coords[i] = 0;
		return m;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::identity()
	{
		return scalar(T{1});
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::scalar(T const & s)
	{
		matrix<T, R, C> m = zero();
		for (std::size_t i = 0; i < std::min(R, C); ++i)
			m[i][i] = s;
		return m;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::diagonal(vector<T, std::min(R, C)> const & d)
	{
		matrix<T, R, C> m = zero();
		for (std::size_t i = 0; i < std::min(R, C); ++i)
			m[i][i] = d[i];
		return m;
	}

	template <typename T1, typename T, std::size_t R, std::size_t C>
	matrix<T1, R, C> cast(matrix<T, R, C> const & m)
	{
		matrix<T1, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = static_cast<T1>(m.coords[i]);
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	std::strong_ordering operator <=> (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		for (std::size_t i = 0; i < R * C; ++i)
		{
			if (m1.coords[i] < m2.coords[i])
				return std::strong_ordering::less;
			else if (m1.coords[i] > m2.coords[i])
				return std::strong_ordering::greater;
		}
		return std::strong_ordering::equal;
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator == (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return (m1 <=> m2) == std::strong_ordering::equal;
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator != (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return !(m1 == m2);
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator < (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return (m1 <=> m2) == std::strong_ordering::less;
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator > (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return (m2 < m1);
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator <= (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return !(m2 < m1);
	}

	template <typename T, std::size_t R, std::size_t C>
	bool operator >= (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return !(m1 < m2);
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator * (matrix<T, R, C> const & m, T const & s)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = m.coords[i] * s;
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator * (T const & s, matrix<T, R, C> const & m)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = s * m.coords[i];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator / (matrix<T, R, C> const & m, T const & s)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = m.coords[i] / s;
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> & matrix<T, R, C>::operator *= (T const & s)
	{
		*this = *this * s;
		return *this;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> & matrix<T, R, C>::operator /= (T const & s)
	{
		*this = *this / s;
		return *this;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator - (matrix<T, R, C> const & m)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = -m.coords[i];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator + (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = m1.coords[i] + m2.coords[i];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator - (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R * C; ++i)
			r.coords[i] = m1.coords[i] - m2.coords[i];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> & matrix<T, R, C>::operator += (matrix<T, R, C> const & m)
	{
		*this = *this + m;
		return *this;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> & matrix<T, R, C>::operator -= (matrix<T, R, C> const & m)
	{
		*this = *this - m;
		return *this;
	}

	template <typename T, std::size_t R, std::size_t C>
	vector<T, R> operator * (matrix<T, R, C> const & m, vector<T, C> const & v)
	{
		vector<T, R> r;
		for (std::size_t i = 0; i < R; ++i)
		{
			r[i] = T{};
			for (std::size_t j = 0; j < C; ++j)
				r[i] += m[i][j] * v[j];
		}
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	vector<T, C> operator * (vector<T, R> const & v, matrix<T, R, C> const & m)
	{
		vector<T, C> r;
		for (std::size_t j = 0; j < C; ++j)
		{
			r[j] = T{};
			for (std::size_t i = 0; i < R; ++i)
				r[i] += v[j] * m[i][j];
		}
		return r;
	}

	template <typename T, std::size_t R, std::size_t K, std::size_t C>
	matrix<T, R, C> operator * (matrix<T, R, K> const & m1, matrix<T, K, C> const & m2)
	{
		matrix<T, R, C> r;
		for (std::size_t i = 0; i < R; ++i)
		{
			for (std::size_t j = 0; j < C; ++j)
			{
				r[i][j] = T{};

				for (std::size_t k = 0; k < K; ++k)
					r[i][j] += m1[i][k] * m2[k][j];
			}
		}
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, C, R> transpose(matrix<T, R, C> const & m)
	{
		matrix<T, C, R> r;
		for (std::size_t i = 0; i < R; ++i)
			for (std::size_t j = 0; j < C; ++j)
				r[j][i] = m[i][j];
		return r;
	}

	template <typename T, std::size_t N, typename ... Rows>
	auto by_rows(vector<T, N> const & v, Rows const & ... rows)
	{
		vector<T, N> m[] = {v, rows...};

		matrix<T, sizeof...(Rows) + 1, N> result;
		for (std::size_t i = 0; i < result.rows(); ++i)
			for (std::size_t j = 0; j < result.columns(); ++j)
				result[i][j] = m[i][j];
		return result;
	}

	template <typename T, std::size_t N, typename ... Columns>
	auto by_columns(vector<T, N> const & v, Columns const & ... columns)
	{
		vector<T, N> m[] = {v, columns...};

		matrix<T, N, sizeof...(Columns) + 1> result;
		for (std::size_t i = 0; i < result.rows(); ++i)
			for (std::size_t j = 0; j < result.columns(); ++j)
				result[i][j] = m[j][i];
		return result;
	}

	template <typename T, std::size_t R, std::size_t C>
	vector<T, C> row(matrix<T, R, C> const & m, std::size_t i)
	{
		vector<T, C> r;
		for (std::size_t j = 0; j < C; ++j)
			r[j] = m[i][j];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	vector<T, R> column(matrix<T, R, C> const & m, std::size_t j)
	{
		vector<T, R> r;
		for (std::size_t i = 0; i < R; ++i)
			r[i] = m[i][j];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	T frobenius_norm_sqr(matrix<T, R, C> const & m)
	{
		T r{0};
		for (std::size_t i = 0; i < R; ++i)
			for (std::size_t j = 0; j < C; ++j)
				r += sqr(m[i][j]);
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	T frobenius_norm(matrix<T, R, C> const & m)
	{
		return std::sqrt(frobenius_norm_sqr(m));
	}

	template <typename T, std::size_t R, std::size_t C>
	T trace(matrix<T, R, C> const & m)
	{
		T r{0};
		for (std::size_t i = 0; i < std::min(R, C); ++i)
			r += m[i][i];
		return r;
	}

	template <std::size_t R1, std::size_t C1, std::size_t R2, std::size_t C2, typename T, std::size_t R, std::size_t C>
	matrix<T, R2 - R1, C2 - C1> submatrix(matrix<T, R, C> const & m)
	{
		static_assert(R1 < R2);
		static_assert(C1 < C2);
		static_assert(R2 <= R);
		static_assert(C2 <= C);

		matrix<T, R2 - R1, C2 - C1> result;
		for (std::size_t i = 0; i < R2 - R1; ++i)
		{
			for (std::size_t j = 0; j < C2 - C1; ++j)
			{
				result[i][j] = m[i + R1][j + C1];
			}
		}
		return result;
	}

	template <typename T>
	matrix<T, 3, 3> cross_product_matrix(vector<T, 3> const & v)
	{
		auto r = matrix<T, 3, 3>::zero();
		r[0][1] = -v[2];
		r[0][2] = v[1];
		r[1][0] = v[2];
		r[1][2] = -v[0];
		r[2][0] = -v[1];
		r[2][1] = v[0];
		return r;
	}

	template <typename T, std::size_t N>
	matrix<T, N, N> outer_product(vector<T, N> const & v1, vector<T, N> const & v2)
	{
		matrix<T, N, N> r;
		for (std::size_t i = 0; i < N; ++i)
			for (std::size_t j = 0; j < N; ++j)
				r[i][j] = v1[i] * v2[j];
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	std::ostream & operator << (std::ostream & os, matrix<T, R, C> const & m)
	{
		for (std::size_t i = 0; i < R; ++i)
		{
			for (std::size_t j = 0; j < C; ++j)
				os << m[i][j] << ' ';
			os << '\n';
		}
		return os;
	}

	template <typename T, std::size_t R, std::size_t C>
	struct setw
	{
		matrix<T, R, C> const & m;
		int w;
	};

	template <typename T, std::size_t R, std::size_t C>
	std::ostream & operator << (std::ostream & os, setw<T, R, C> const & w)
	{
		for (std::size_t i = 0; i < R; ++i)
		{
			for (std::size_t j = 0; j < C; ++j)
				os << std::fixed << std::right << std::setw(w.w) << w.m[i][j] << ' ';
			os << '\n';
		}
		return os;
	}

}
