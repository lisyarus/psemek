#pragma once

#include <psemek/util/exception.hpp>

#include <cstddef>
#include <format>

namespace psemek::math
{

	constexpr std::size_t dynamic = static_cast<std::size_t>(-1);

	struct dynamic_size_mismatch
		: util::exception
	{
		dynamic_size_mismatch(std::size_t size1, std::size_t size2, util::stacktrace stacktrace = {})
			: util::exception(std::format("Dynamic array size mismatch: {} != {}", size1, size2), std::move(stacktrace))
		{}
	};

	inline void check_dynamic_size(std::size_t size1, std::size_t size2)
	{
		if (size1 != size2)
			throw dynamic_size_mismatch(size1, size2);
	}

}
