#pragma once

#include <psemek/util/hash.hpp>

#include <psemek/math/detail/array.hpp>
#include <psemek/math/vector.hpp>

#include <iostream>

namespace psemek::math
{

	template <typename T, std::size_t N>
	struct point
	{
		using scalar_type = T;
		static constexpr std::size_t static_dimension = N;

		typename detail::array<T, N>::type coords;

		point() = default;

		template <typename ... Args>
		requires((sizeof...(Args) == N) && detail::all_convertible_to<T, Args...>::value)
		point(Args && ... args)
			: coords{ static_cast<T>(std::forward<Args>(args))... }
		{}

		std::size_t dimension() const
		{
			return N;
		}

		T & operator[](std::size_t i)
		{
			return coords[i];
		}

		T const & operator[](std::size_t i) const
		{
			return coords[i];
		}

		point & operator += (vector<T, N> const & v);
		point & operator -= (vector<T, N> const & v);

		static point zero();
	};

	template <typename ... Args>
	point(Args && ...) -> point<std::common_type_t<Args...>, sizeof...(Args)>;

	template <typename T, std::size_t N>
	point<T, N> point<T, N>::zero()
	{
		point<T, N> p;
		for (std::size_t i = 0; i < N; ++i)
			p[i] = 0;
		return p;
	}

	template <typename T1, typename T, std::size_t N>
	point<T1, N> cast(point<T, N> const & p)
	{
		point<T1, N> r;
		for (std::size_t i = 0; i < N; ++i)
			r[i] = T1(p[i]);
		return r;
	}

	template <typename T, std::size_t N>
	bool operator == (point<T, N> const & p1, point<T, N> const & p2)
	{
		return (p1 <=> p2) == std::strong_ordering::equal;
	}

	template <typename T, std::size_t N>
	bool operator != (point<T, N> const & p1, point<T, N> const & p2)
	{
		return !(p1 == p2);
	}

	template <typename T, std::size_t N>
	bool operator < (point<T, N> const & p1, point<T, N> const & p2)
	{
		return (p1 <=> p2) == std::strong_ordering::less;
	}

	template <typename T, std::size_t N>
	bool operator > (point<T, N> const & p1, point<T, N> const & p2)
	{
		return (p2 < p1);
	}

	template <typename T, std::size_t N>
	bool operator <= (point<T, N> const & p1, point<T, N> const & p2)
	{
		return !(p2 < p1);
	}

	template <typename T, std::size_t N>
	bool operator >= (point<T, N> const & p1, point<T, N> const & p2)
	{
		return !(p1 < p2);
	}

	template <typename T, std::size_t N>
	std::strong_ordering operator <=> (point<T, N> const & p1, point<T, N> const & p2)
	{
		for (std::size_t i = 0; i < N; ++i)
		{
			if (p1[i] < p2[i])
				return std::strong_ordering::less;
			else if (p1[i] > p2[i])
				return std::strong_ordering::greater;
		}
		return std::strong_ordering::equal;
	}

	template <typename T, std::size_t N>
	point<T, N> operator + (point<T, N> const & p, vector<T, N> const & v)
	{
		point<T, N> r;
		for (std::size_t i = 0; i < N; ++i)
			r[i] = p[i] + v[i];
		return r;
	}

	template <typename T, std::size_t N>
	point<T, N> operator + (vector<T, N> const & v, point<T, N> const & p)
	{
		point<T, N> r;
		for (std::size_t i = 0; i < N; ++i)
			r[i] = v[i] + p[i];
		return r;
	}

	template <typename T, std::size_t N>
	point<T, N> operator - (point<T, N> const & p, vector<T, N> const & v)
	{
		point<T, N> r;
		for (std::size_t i = 0; i < N; ++i)
			r[i] = p[i] - v[i];
		return r;
	}

	template <typename T, std::size_t N>
	vector<T, N> operator - (point<T, N> const & p1, point<T, N> const & p2)
	{
		vector<T, N> r;
		for (std::size_t i = 0; i < N; ++i)
			r[i] = p1[i] - p2[i];
		return r;
	}

	template <typename T, std::size_t N>
	point<T, N> & point<T, N>::operator += (vector<T, N> const & v)
	{
		return (*this) = (*this) + v;
	}

	template <typename T, std::size_t N>
	point<T, N> & point<T, N>::operator -= (vector<T, N> const & v)
	{
		return (*this) = (*this) - v;
	}

	template <typename T, std::size_t N>
	point<T, N> lerp(point<T, N> const & p0, point<T, N> const & p1, T const & t)
	{
		return p0 + t * (p1 - p0);
	}

	template <typename T, std::size_t N>
	T distance_sqr(point<T, N> const & p1, point<T, N> const & p2)
	{
		return length_sqr(p2 - p1);
	}

	template <typename T, std::size_t N>
	T distance(point<T, N> const & p1, point<T, N> const & p2)
	{
		return length(p2 - p1);
	}

	template <typename P, typename ... Points>
	auto volume(P const & p0, Points const & ... ps)
	{
		return det((ps - p0)...);
	}

	template <typename P, typename ... Points>
	auto normal(P const & p0, Points const & ... ps)
	{
		return normalized(ort((ps - p0)...));
	}

	template <typename T, std::size_t N>
	std::ostream & operator << (std::ostream & os, point<T, N> const & p)
	{
		os << '(' << p[0];
		for (std::size_t i = 1; i < N; ++i)
			os << ", " << p[i];
		os << ')';
		return os;
	}

	template <typename T, std::size_t N>
	bool isfinite(point<T, N> const & p)
	{
		for (std::size_t i = 0; i < N; ++i)
			if (!std::isfinite(p[i]))
				return false;
		return true;
	}

}

namespace std
{

	template <typename T, std::size_t N>
	struct hash<::psemek::math::point<T, N>>
	{
		std::uint64_t operator()(::psemek::math::point<T, N> const & v) const noexcept
		{
			hash<T> h;
			std::uint64_t r = 0;
			for (std::size_t i = 0; i < N; ++i)
				::psemek::util::hash_combine(r, h(v[i]));
			return r;
		}
	};

}
