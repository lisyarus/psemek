#pragma once

#include <psemek/util/assert.hpp>

#include <iostream>

namespace psemek::math
{

	enum class sign_t : int
	{
		positive = 1,
		zero = 0,
		negative = -1,
	};

	inline std::ostream & operator << (std::ostream & o, sign_t s)
	{
		switch (s)
		{
		case sign_t::positive: o << "positive"; break;
		case sign_t::zero: o << "zero"; break;
		case sign_t::negative: o << "negative"; break;
		default: o << "(unknown)"; break;
		}

		return o;
	}

	inline sign_t inverse(sign_t s)
	{
		switch (s)
		{
		case sign_t::positive: return sign_t::negative;
		case sign_t::zero: return sign_t::zero;
		case sign_t::negative: return sign_t::positive;
		}

		assert(false);
		return sign_t::zero;
	}

}
