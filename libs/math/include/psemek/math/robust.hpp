#pragma once

namespace psemek::math
{

	constexpr struct fast_predicate_tag{} fast;

	constexpr struct robust_predicate_tag{} robust;

	constexpr auto default_robust_tag = fast;

}
