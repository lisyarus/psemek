#include <psemek/audio/detail/white_noise.hpp>

#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

namespace psemek::audio::detail
{

	std::vector<float> white_noise(std::size_t sample_count)
	{
		random::generator rng{0x4b0a763ef6573bf2ull, 0};

		std::vector<float> result(sample_count);
		for (auto & v : result)
			v = random::uniform<float>(rng, {-1.f, 1.f});
		return result;
	}

}
