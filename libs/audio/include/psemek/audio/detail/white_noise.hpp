#pragma once

#include <vector>

namespace psemek::audio::detail
{

	std::vector<float> white_noise(std::size_t sample_count);

}
