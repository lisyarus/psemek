#pragma once

#include <psemek/audio/stream.hpp>

#include <atomic>
#include <memory>

namespace psemek::audio
{

	struct channel
	{
		channel() = default;

		channel(stream_ptr stream)
			: stream_(std::move(stream))
		{}

		stream_ptr stream() const
		{
			return stream_.load();
		}

		stream_ptr stream(stream_ptr new_stream)
		{
			return stream_.exchange(std::move(new_stream));
		}

		stream_ptr stop()
		{
			return stream(nullptr);
		}

		bool is_stopped() const
		{
			return stream() == nullptr;
		}

	private:
		std::atomic<stream_ptr> stream_;
	};

	using channel_ptr = std::shared_ptr<channel>;

}
