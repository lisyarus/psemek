#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/util/span.hpp>
#include <psemek/util/blob.hpp>

#include <optional>
#include <vector>
#include <filesystem>

namespace psemek::audio
{

	struct track
	{
		virtual stream_ptr stream() const = 0;
		virtual std::optional<std::size_t> length() const = 0;

		virtual ~track(){}
	};

	using track_ptr = std::shared_ptr<track>;

	track_ptr load_raw(util::span<float const> data);
	track_ptr load_raw(util::blob data);
	track_ptr load_raw(std::vector<float> data);

	track_ptr load_wav(util::span<char const> data);
	track_ptr load_wav(std::vector<char> const & data);

	track_ptr load_mp3(util::span<char const> data);
	track_ptr load_mp3(util::blob data);
	track_ptr load_mp3(std::vector<char> data);

	track_ptr load_lazy(std::filesystem::path const & path);

}
