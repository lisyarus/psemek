#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/audio/duration.hpp>

namespace psemek::audio
{

	// Comb filter
	// Turns x[n] into y[n] = x[n - delay] + gain * y[n - delay]
	stream_ptr echo(stream_ptr stream, duration delay, float gain);

}
