#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/audio/duration.hpp>

namespace psemek::audio
{

	// All-pass filter
	// Turns x[n] into y[n] = - gain * x[n] + x[n - delay] + gain * y[n - delay]
	stream_ptr all_pass(stream_ptr stream, duration delay, float gain);

}
