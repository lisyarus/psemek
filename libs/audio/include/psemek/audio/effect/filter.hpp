#pragma once

#include <psemek/audio/stream.hpp>

namespace psemek::audio
{

	// Turns x[n] into y[n] = a0*x[n] + a1*x[n-1]
	stream_ptr feedforward_filter(stream_ptr stream, float a0, float a1);

	// Turns x[n] into y[n] = a0*x[n] + b1*y[n-1]
	stream_ptr feedback_filter(stream_ptr stream, float a0, float b1);

	stream_ptr low_pass_filter(stream_ptr stream);
	stream_ptr high_pass_filter(stream_ptr stream);

}
