#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/audio/track.hpp>
#include <psemek/audio/recorder.hpp>

#include <utility>

namespace psemek::audio
{

	track_ptr make_duplicator(std::shared_ptr<recorder> recorder);
	track_ptr make_duplicator(stream_ptr stream);
	std::pair<stream_ptr, stream_ptr> duplicate(stream_ptr stream);

}
