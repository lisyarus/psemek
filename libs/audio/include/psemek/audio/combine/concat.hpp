#pragma once

#include <psemek/audio/stream.hpp>

#include <vector>

namespace psemek::audio
{

	stream_ptr concat(std::vector<stream_ptr> streams);

}
