#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/audio/track.hpp>

namespace psemek::audio
{

	stream_ptr loop(track_ptr track, std::optional<std::size_t> count = {});
	stream_ptr loop(stream_ptr stream, std::optional<std::size_t> count = {});

}
