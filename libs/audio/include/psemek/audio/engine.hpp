#pragma once

#include <psemek/audio/channel.hpp>

#include <memory>

namespace psemek::audio
{

	struct engine
	{
		virtual channel_ptr output() = 0;

		virtual ~engine() {}
	};

	// Implemented by platform backend
	std::unique_ptr<engine> make_engine();

}
