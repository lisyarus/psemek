#pragma once

#include <psemek/audio/stream.hpp>

namespace psemek::audio
{

	// True noise
	stream_ptr white_noise();

	// Fixed-buffer noise to simulate a specific frequency
	stream_ptr white_noise(float frequency);

}
