#pragma once

#include <cstdint>
#include <cmath>

namespace psemek::audio
{

	constexpr int frequency = 44100;
	constexpr float inv_frequency = 1.f / frequency;

	inline std::int64_t seconds_to_frames(float seconds)
	{
		return static_cast<std::int64_t>(std::round(seconds * frequency));
	}

	inline std::int64_t seconds_to_samples(float seconds)
	{
		return 2 * seconds_to_frames(seconds);
	}

	inline float frames_to_seconds(std::int64_t frames)
	{
		return static_cast<float>(frames) * inv_frequency;
	}

	inline float samples_to_seconds(std::int64_t samples)
	{
		return frames_to_seconds(samples / 2);
	}

	inline float to_db(float amplitude)
	{
		return 10.f * std::log10(amplitude);
	}

	inline float from_db(float db)
	{
		return std::pow(10.f, db / 10.f);
	}

}
