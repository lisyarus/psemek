#pragma once

#include <psemek/audio/stream.hpp>
#include <psemek/audio/track.hpp>
#include <psemek/audio/channel.hpp>
#include <psemek/audio/duration.hpp>
#include <psemek/util/span.hpp>

#include <vector>

namespace psemek::audio
{

	struct recorder
	{
		virtual channel_ptr channel() = 0;

		virtual std::size_t request(std::size_t samples) = 0;

		virtual util::span<float const> buffer() const = 0;

		virtual std::vector<float> grab_buffer() = 0;

		virtual ~recorder() {}
	};

	std::shared_ptr<recorder> make_recorder();
	std::shared_ptr<recorder> make_recorder(stream_ptr stream);

	std::shared_ptr<track> record(stream_ptr stream);
	std::shared_ptr<track> record(stream_ptr stream, duration duration);

}
