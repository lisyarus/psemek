#pragma once

#include <cmath>

namespace psemek::audio
{

	inline float midi_frequency(float note)
	{
		return 440.f * std::pow(2.f, (note - 69.f) / 12.f);
	}

}
