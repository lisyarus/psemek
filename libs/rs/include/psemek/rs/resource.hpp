#pragma once

#include <cstdint>
#include <string_view>

namespace psemek::rs
{

	using id = std::uint32_t;

	struct resource
	{
		rs::id id;
		std::string_view name;
		std::string_view data;
	};

}
