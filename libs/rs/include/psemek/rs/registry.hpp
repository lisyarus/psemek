#pragma once

#include <psemek/rs/resource.hpp>
#include <psemek/util/exception.hpp>

#include <stdexcept>

namespace psemek::rs
{

	struct unknown_id_error
		: util::exception
	{
		unknown_id_error(rs::id id, util::stacktrace stacktrace = {});

		rs::id id() const { return id_; }

	private:
		rs::id id_;
	};

	struct unknown_name_error
		: util::exception
	{
		unknown_name_error(std::string_view name, util::stacktrace stacktrace = {});

		std::string_view name() const { return name_; }

	private:
		std::string_view name_;
	};

	resource const * find(id id);
	resource const * find(std::string_view name);

	resource const & get(id id);
	resource const & get(std::string_view name);

	id add(std::string_view name, std::string_view data);

}
