#include <psemek/vecr/fill.hpp>
#include <psemek/math/orientation.hpp>

namespace psemek::vecr
{

	sdf_sample sdf(fill const & s, math::point<float, 2> const & p)
	{
		auto result = sdf(s.border, p, true);

		int inside = 0;
		for (std::size_t i = 0; i < s.border.points.size(); ++i)
		{
			auto const j = (i + 1) % s.border.points.size();
			bool const s0 = s.border.points[i][1] <= p[1];
			bool const s1 = p[1] < s.border.points[j][1];
			auto const sign = math::orientation(s.border.points[i], s.border.points[j], p);

			if (s0 && s1 && sign == math::sign_t::positive) inside += 1;
			if (!s0 && !s1 && sign == math::sign_t::negative) inside -= 1;
		}

		if (inside != 0)
		{
			result.value *= -1.f;
			result.gradient *= -1.f;
		}

		return result;
	}

	math::box<float, 2> bbox(fill const & s)
	{
		return bbox(s.border);
	}

}
