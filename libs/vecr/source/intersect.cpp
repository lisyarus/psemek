#include <psemek/vecr/intersect.hpp>
#include <psemek/math/math.hpp>

#include <vector>

namespace psemek::vecr
{

	sdf_sample sdf(intersect const & s, math::point<float, 2> const & p)
	{
		sdf_sample result;

		if (s.smooth == 0.f)
		{
			result.value = -std::numeric_limits<float>::infinity();
			for (auto const & ss : s.shapes)
			{
				auto r = sdf(ss, p);
				if (math::make_max(result.value, r.value))
					result.gradient = r.gradient;
			}
		}
		else
		{
			float v = 0.f;

			for (auto const & ss : s.shapes)
			{
				auto r = sdf(ss, p);
				float vs = std::exp(r.value / s.smooth);
				v += vs;
				result.gradient += r.gradient * vs;
			}

			result.value = std::log(v) * s.smooth;
			result.gradient /= v;
		}

		return result;
	}

	math::box<float, 2> bbox(intersect const & s)
	{
		auto result = math::box<float, 2>::full();
		for (auto const & ss : s.shapes)
			result &= bbox(ss);
		return result;
	}

}
