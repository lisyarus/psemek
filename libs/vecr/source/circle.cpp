#include <psemek/vecr/circle.hpp>

namespace psemek::vecr
{

	sdf_sample sdf(circle const & s, math::point<float, 2> const & p)
	{
		auto r = p - s.center;
		auto l = math::length(r);
		if (l > 0.f)
			return {l - s.radius, r / l};
		return {l - s.radius, {0.f, 0.f}};
	}

	math::box<float, 2> bbox(circle const & s)
	{
		return math::expand(math::box<float, 2>::singleton(s.center), s.radius);
	}

}
