#include <psemek/vecr/add.hpp>
#include <psemek/math/math.hpp>

#include <vector>

namespace psemek::vecr
{

	sdf_sample sdf(add const & s, math::point<float, 2> const & p)
	{
		sdf_sample result;

		if (s.smooth == 0.f)
		{
			for (auto const & ss : s.shapes)
			{
				auto r = sdf(ss, p);
				if (math::make_min(result.value, r.value))
					result.gradient = r.gradient;
			}
		}
		else
		{
			float v = 0.f;

			for (auto const & ss : s.shapes)
			{
				auto r = sdf(ss, p);
				float vs = std::exp(- r.value / s.smooth);
				v += vs;
				result.gradient += r.gradient * vs;
			}

			result.value = - std::log(v) * s.smooth;
			result.gradient /= v;
		}

		return result;
	}

	math::box<float, 2> bbox(add const & s)
	{
		math::box<float, 2> result;
		for (auto const & ss : s.shapes)
			result |= bbox(ss);
		return result;
	}

}
