#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	struct full
	{};

	inline sdf_sample sdf(full const &, math::point<float, 2> const &)
	{
		return {
			.value = 0.f,
			.gradient = {0.f, 0.f},
		};
	}

	inline math::box<float, 2> bbox(full const &)
	{
		return math::box<float, 2>::full();
	}

}
