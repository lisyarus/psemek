#pragma once

#include <psemek/math/vector.hpp>

namespace psemek::vecr
{

	struct sdf_sample
	{
		float value = std::numeric_limits<float>::infinity();
		math::vector<float, 2> gradient{0.f, 0.f};
	};

}
