#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/vecr/any.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/math.hpp>
#include <psemek/math/box.hpp>

#include <vector>

namespace psemek::vecr
{

	struct intersect
	{
		std::vector<any> shapes;
		float smooth = 0.f;
	};

	sdf_sample sdf(intersect const & s, math::point<float, 2> const & p);

	math::box<float, 2> bbox(intersect const & s);

}
