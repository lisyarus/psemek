#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	template <typename Shape>
	struct grow
	{
		Shape shape;
		float distance;
	};

	template <typename Shape>
	sdf_sample sdf(grow<Shape> const & s, math::point<float, 2> const & p)
	{
		auto result = sdf(s.shape, p);
		result.value -= s.distance;
		return result;
	}

	template <typename Shape>
	math::box<float, 2> bbox(grow<Shape> const & s)
	{
		return math::expand(bbox(s.shape), s.distance);
	}

}
