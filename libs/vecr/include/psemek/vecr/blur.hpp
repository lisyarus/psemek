#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	template <typename Shape>
	struct blur
	{
		Shape shape;
		float factor;
	};

	template <typename Shape>
	sdf_sample sdf(blur<Shape> const & s, math::point<float, 2> const & p)
	{
		auto result = sdf(s.shape, p);
		result.value /= s.factor;
		return result;
	}

	template <typename Shape>
	math::box<float, 2> bbox(blur<Shape> const & s)
	{
		return bbox(s.shape);
	}

}
