#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	struct circle
	{
		math::point<float, 2> center;
		float radius;
	};

	sdf_sample sdf(circle const & s, math::point<float, 2> const & p);

	math::box<float, 2> bbox(circle const & s);

}
