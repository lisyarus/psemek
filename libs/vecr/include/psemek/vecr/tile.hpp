#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

#include <cmath>

namespace psemek::vecr
{

	template <typename Shape>
	struct tile
	{
		Shape shape;
		math::box<float, 2> cell;
	};

	template <typename Shape>
	sdf_sample sdf(tile<Shape> const & s, math::point<float, 2> const & p)
	{
		auto q = p;
		for (std::size_t i : {0, 1})
			q[i] = s.cell[i].min + std::fmod(q[i] - s.cell[i].min, s.cell[i].length());
		return sdf(s.shape, q);
	}

	template <typename Shape>
	math::box<float, 2> bbox(tile<Shape> const &)
	{
		return math::box<float, 2>::full();
	}

}
