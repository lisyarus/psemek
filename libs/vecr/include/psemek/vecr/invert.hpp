#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	template <typename Shape>
	struct invert
	{
		Shape shape;
	};

	template <typename Shape>
	sdf_sample sdf(invert<Shape> const & s, math::point<float, 2> const & p)
	{
		auto result = sdf(s.shape, p);
		result.value *= -1.f;
		result.gradient *= -1.f;
		return result;
	}

	template <typename Shape>
	math::box<float, 2> bbox(invert<Shape> const &)
	{
		return math::box<float, 2>::full();
	}

}
