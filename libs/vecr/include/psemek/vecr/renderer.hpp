#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/vecr/any.hpp>
#include <psemek/vecr/full.hpp>
#include <psemek/vecr/colorizer.hpp>
#include <psemek/vecr/blend_mode.hpp>
#include <psemek/gfx/pixmap.hpp>

namespace psemek::vecr
{

	struct primitive
	{
		any mask = {};
		any filter = full{};
		float blur = 1.f;
		float alpha = 1.f;
		any_colorizer colorizer = {};
		blend_mode blend = vecr::blend;
	};

	struct renderer
	{
		void reset(math::vector<std::size_t, 2> const & size, std::size_t samples = 4, gfx::color_rgba const & color = {0, 0, 0, 0});

		math::vector<std::size_t, 2> size() const;
		std::size_t samples() const;
		gfx::pixmap_rgba const & result() const;
		gfx::pixmap_rgba release();

		gfx::pixmap_rgba const & canvas() const;
		gfx::pixmap_rgba release_canvas();

		void clear(gfx::color_rgba const & color = {0, 0, 0, 0});

		void draw(primitive const & primitive);

	private:
		std::size_t samples_ = 4;
		gfx::pixmap_rgba canvas_;
		mutable gfx::pixmap_rgba result_;
		mutable bool need_resolve_ = false;

		void resolve() const;
	};


}
