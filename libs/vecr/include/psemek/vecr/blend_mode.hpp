#pragma once

#include <psemek/gfx/color.hpp>

#include <functional>

namespace psemek::vecr
{

	using blend_mode = std::function<gfx::color_4f(gfx::color_4f const & dst, gfx::color_4f const & src)>;

	constexpr auto blend = [](gfx::color_4f const & dst, gfx::color_4f const & src)
	{
		return gfx::blend(dst, src);
	};

	constexpr auto overlay = [](gfx::color_4f const & dst, gfx::color_4f const & src)
	{
		return dst + src;
	};

	constexpr auto replace = [](gfx::color_4f const &, gfx::color_4f const & src)
	{
		return src;
	};

}
