#pragma once

#include <psemek/vecr/invert.hpp>
#include <psemek/vecr/intersect.hpp>

namespace psemek::vecr
{

	template <typename Shape1, typename Shape2>
	struct subtract
	{
		Shape1 shape1;
		Shape2 shape2;
		float smooth = 0.f;
	};

	template <typename Shape1, typename Shape2>
	sdf_sample sdf(subtract<Shape1, Shape2> const & s, math::point<float, 2> const & p)
	{
		return sdf(intersect{{s.shape1, invert{s.shape2}}, s.smooth}, p);
	}

	template <typename Shape1, typename Shape2>
	math::box<float, 2> bbox(subtract<Shape1, Shape2> const & s)
	{
		return bbox(s.shape1);
	}

}
