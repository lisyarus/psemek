#pragma once

#include <psemek/vecr/invert.hpp>
#include <psemek/vecr/intersect.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	struct add
	{
		std::vector<any> shapes;
		float smooth = 0.f;
	};

	sdf_sample sdf(add const & s, math::point<float, 2> const & p);

	math::box<float, 2> bbox(add const & s);

}
