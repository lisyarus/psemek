#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	struct halfspace
	{
		math::point<float, 2> origin;
		math::vector<float, 2> normal;
	};

	inline sdf_sample sdf(halfspace const & s, math::point<float, 2> const & p)
	{
		return {math::dot(p - s.origin, s.normal), s.normal};
	}

	inline math::box<float, 2> bbox(halfspace const &)
	{
		return math::box<float, 2>::full();
	}


}
