#pragma once

#include <psemek/vecr/sdf.hpp>
#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

#include <vector>

namespace psemek::vecr
{

	struct path
	{
		std::vector<math::point<float, 2>> points;
	};

	path closed(path path);

	sdf_sample sdf(path const & s, math::point<float, 2> const & p, bool closed = false);

	math::box<float, 2> bbox(path const & s);

}
