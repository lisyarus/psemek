#pragma once

#include <psemek/vecr/path.hpp>
#include <psemek/math/box.hpp>

namespace psemek::vecr
{

	struct fill
	{
		path border;
	};

	sdf_sample sdf(fill const & s, math::point<float, 2> const & p);

	math::box<float, 2> bbox(fill const & s);

}
