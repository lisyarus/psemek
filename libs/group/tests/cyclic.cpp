#include <psemek/test/test.hpp>

#include <psemek/group/cyclic.hpp>

#include <set>

using namespace psemek::group;

using group_type = cyclic<7, std::uint8_t>;

test_case(group_cyclic_construct)
{
	expect_equal(group_type::identity().value(), 0);

	int const n = group_type::size();

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::rotation(i).value(), i % n);

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::rotation(i - 4 * n).value(), i % n);
}

test_case(group_cyclic_values)
{
	std::set<group_type> values;
	for (auto g : group_type::values())
		values.insert(g);
	expect_equal(values.size(), group_type::size());
}

test_case(group_cyclic_multiply)
{
	for (auto g1 : group_type::values())
		for (auto g2 : group_type::values())
				expect_equal(g1 * g2, group_type::rotation(g1.value() + g2.value()));
}

test_case(group_cyclic_inverse)
{
	for (auto g : group_type::values())
		expect_equal(g * inverse(g), group_type::identity());
}
