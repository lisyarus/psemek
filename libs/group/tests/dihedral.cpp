#include <psemek/test/test.hpp>

#include <psemek/group/dihedral.hpp>

#include <set>

using namespace psemek::group;

using group_type = dihedral<7, std::uint8_t>;

test_case(group_dihedral_construct)
{
	expect_equal(group_type::identity().value(), 0);

	int const n = group_type::size() / 2;

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::rotation(i).value(), i % n);

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::reflection(i).value(), (i % n) + n);

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::rotation(i - 4 * n).value(), i % n);

	for (int i = 0; i < 4 * n; ++i)
		expect_equal(group_type::reflection(i - 4 * n).value(), (i % n) + n);
}

test_case(group_dihedral_values)
{
	std::set<group_type> values;
	for (auto g : group_type::values())
		values.insert(g);
	expect_equal(values.size(), group_type::size());
}

test_case(group_dihedral_multiply)
{
	for (auto g1 : group_type::values())
	{
		for (auto g2 : group_type::values())
		{
			auto value = g1.is_rotation() ? g1.value() + g2.value() : g1.value() + group_type::size() / 2 - g2.value();

			if (g1.is_reflection() ^ g2.is_reflection())
			{
				expect_equal(g1 * g2, group_type::reflection(value));
			}
			else
			{
				expect_equal(g1 * g2, group_type::rotation(value));
			}
		}
	}
}

test_case(group_dihedral_inverse)
{
	for (auto g : group_type::values())
		expect_equal(g * inverse(g), group_type::identity());
}
