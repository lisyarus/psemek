#include <psemek/test/test.hpp>

#include <psemek/ecs/container.hpp>
#include <psemek/ecs/declare_uuid.hpp>

using namespace psemek;
using namespace psemek::ecs;

namespace
{

	struct component_1
	{
		int value;

		psemek_ecs_declare_uuid("component_1")
	};

	struct component_2
	{
		int value;

		psemek_ecs_declare_uuid("component_2")
	};

}

test_case(ecs_callback_constructor_create)
{
	container container;

	int value = 0;
	container.constructor<component_1>([&value](component_1 const & c1){
		value = c1.value;
	});

	container.create(component_1{10});
	expect_equal(value, 10);

	container.create(component_1{20});
	expect_equal(value, 20);

	container.create(component_2{100});
	expect_equal(value, 20);

	container.create();
	expect_equal(value, 20);

	container.create(component_1{30});
	expect_equal(value, 30);

	container.create(component_1{40}, component_2{200});
	expect_equal(value, 40);
}

test_case(ecs_callback_destructor_destroy)
{
	container container;

	int value = 0;
	container.destructor<component_1>([&value](component_1 const & c1){
		value = c1.value;
	});

	auto h1 = container.create(component_1{10});
	auto h2 = container.create(component_1{20});
	auto h3 = container.create(component_2{100});
	auto h4 = container.create();
	auto h5 = container.create(component_1{30});
	auto h6 = container.create(component_1{40}, component_2{200});

	expect_equal(value, 0);
	container.destroy(h1);
	expect_equal(value, 10);
	container.destroy(h2);
	expect_equal(value, 20);
	container.destroy(h3);
	expect_equal(value, 20);
	container.destroy(h4);
	expect_equal(value, 20);
	container.destroy(h5);
	expect_equal(value, 30);
	container.destroy(h6);
	expect_equal(value, 40);
}
