#pragma once

#include <type_traits>

namespace psemek::ecs::detail
{

	template <typename T>
	constexpr bool is_empty_v = std::is_empty_v<T>;

	template <typename T>
	constexpr std::size_t stride()
	{
		return is_empty_v<T> ? 0 : sizeof(T);
	}

}
