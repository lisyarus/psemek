#pragma once

#include <psemek/util/type_name.hpp>

#include <string>

namespace psemek::ecs::detail
{

	template <typename Component>
	std::string describe()
	{
		if constexpr (requires {Component::to_string();})
		{
			return Component::to_string();
		}
		else
		{
			auto name = util::type_name<Component>();
			if (auto pos = name.find_last_of(':'); pos != std::string::npos)
				name = name.substr(pos + 1);
			return name;
		}
	}

	template <typename Component>
	std::string describe(Component const & component)
	{
		if constexpr (requires {component.to_string();})
		{
			return component.to_string();
		}
		else
		{
			return describe<Component>();
		}
	}

}
