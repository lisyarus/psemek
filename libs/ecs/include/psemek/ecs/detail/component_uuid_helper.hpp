#pragma once

#include <psemek/util/uuid.hpp>
#include <psemek/util/span.hpp>

namespace psemek::ecs::detail
{

	template <typename ... Components>
	struct component_uuid_helper
	{
		util::uuid uuids[sizeof...(Components)] { Components::uuid() ... };

		auto get() const
		{
			return util::span<util::uuid const>(uuids);
		}
	};

	template <>
	struct component_uuid_helper<>
	{
		auto get() const
		{
			return util::span<util::uuid const>();
		}
	};

}
