#pragma once

#include <type_traits>

namespace psemek::ecs::detail
{

	template <typename ... Ts>
	struct all_different_types;

	template <>
	struct all_different_types<>
		: std::true_type
	{};

	template <typename T, typename ... Ts>
	struct all_different_types<T, Ts...>
		: std::bool_constant<(!std::is_same_v<T, Ts> && ...) && all_different_types<Ts...>::value>
	{};

	template <typename ... Ts>
	constexpr bool all_different_types_v = all_different_types<Ts...>::value;

}
