#pragma once

#include <psemek/ecs/detail/id.hpp>
#include <psemek/util/span.hpp>

#include <vector>

namespace psemek::ecs::detail
{

	struct table;

	struct entity_data
	{
		struct table * table = nullptr;
		std::uint32_t row = 0;
		entity_epoch epoch = 0;
	};

	struct entity_list
	{
		entity_id create(table * table, std::uint32_t row);
		void destroy(entity_id id);

		util::span<entity_data const> get_entities() const
		{
			return entities_;
		}

		util::span<entity_data> get_entities()
		{
			return entities_;
		}

		std::size_t size() const
		{
			return entities_.size() - free_ids_.size();
		}

	private:
		std::vector<entity_data> entities_;
		std::vector<entity_id> free_ids_;

		void allocate_ids();
	};

}
