#pragma once

#include <cstdint>

namespace psemek::ecs::detail
{

	using entity_id = std::uint32_t;
	using entity_epoch = std::uint32_t;

}
