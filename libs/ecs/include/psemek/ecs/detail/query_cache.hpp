#pragma once

#include <psemek/ecs/detail/callback.hpp>
#include <psemek/util/uuid.hpp>

#include <vector>
#include <list>

namespace psemek::ecs::detail
{

	struct table;
	struct column;

	struct query_cache_entry
	{
		struct table * table = nullptr;
		std::vector<std::uint32_t> columns_indices;
	};

	struct query_cache
	{
		std::vector<util::uuid> with_uuids;
		std::vector<util::uuid> without_uuids;

		std::uint64_t hash;

		std::list<query_cache_entry> entries;

		using callback_factory = util::function<ordered_table_callback(std::vector<std::uint32_t> const &)>;

		std::vector<callback_factory> constructor_factories;
		std::vector<callback_factory> destructor_factories;

		void add(table * table);
	};

}
