#pragma once

#include <psemek/util/uuid.hpp>
#include <psemek/util/hash_table.hpp>
#include <psemek/util/type_name.hpp>
#include <psemek/util/exception.hpp>

#include <memory>

namespace psemek::ecs
{

	struct container;

}

namespace psemek::ecs::detail
{

	struct index_container
	{
		template <typename Index, typename ... Args>
		Index & get(container & container, Args && ... args)
		{
			auto uuid = Index::uuid();
			if (auto it = storage_.find(uuid); it != storage_.end())
				return *reinterpret_cast<Index *>(it->second.get());

			if constexpr (std::is_constructible_v<Index, ecs::container &, Args && ...>)
			{
				auto ptr = std::make_shared<Index>(container, std::forward<Args>(args)...);
				storage_.insert({uuid, ptr});
				return *ptr;
			}
			else if constexpr (std::is_constructible_v<Index, Args && ...>)
			{
				auto ptr = std::make_shared<Index>(std::forward<Args>(args)...);
				storage_.insert({uuid, ptr});
				return *ptr;
			}
			else
			{
				throw util::exception("Index " + util::type_name<Index>() + " is neither present nor constructible");
			}
		}

	private:
		util::hash_map<util::uuid, std::shared_ptr<void>> storage_;
	};

}
