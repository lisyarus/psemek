#pragma once

#include <psemek/ecs/handle.hpp>
#include <psemek/util/function.hpp>
#include <psemek/util/uuid.hpp>
#include <psemek/util/span.hpp>
#include <psemek/util/hash_table.hpp>

namespace psemek::ecs
{

	struct container;

	namespace detail
	{

		struct table;

		using table_callback = util::function<void(container &, table &, std::uint32_t, util::hash_set<util::uuid> const &, util::hash_set<util::uuid> const &, bool)>;

		struct ordered_table_callback
		{
			int id;
			table_callback callback;

			friend auto operator <=> (ordered_table_callback const & c1, ordered_table_callback const & c2)
			{
				return c1.id <=> c2.id;
			}

			friend bool operator == (ordered_table_callback const & c1, ordered_table_callback const & c2)
			{
				return c1.id == c2.id;
			}
		};

	}

}
