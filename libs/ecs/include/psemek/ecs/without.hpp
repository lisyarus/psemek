#pragma once

#include <type_traits>

namespace psemek::ecs
{

	template <typename Component>
	struct without
	{
		static_assert(!std::is_reference_v<Component>, "a component cannot be a reference");

		using component_type = std::remove_const_t<Component>;
	};

}
