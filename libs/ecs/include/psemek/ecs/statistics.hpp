#pragma once

#include <string>
#include <vector>

namespace psemek::ecs
{

	struct table_statistics
	{
		std::string description;
		std::size_t entity_count;
	};

	struct statistics
	{
		std::vector<table_statistics> tables;
	};

}
