#pragma once

#include <psemek/util/uuid.hpp>

#define psemek_ecs_declare_uuid(seed) \
	static ::psemek::util::uuid uuid() { static constexpr auto value = ::psemek::util::make_uuid(seed); return value; }
