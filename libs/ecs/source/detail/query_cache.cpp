#include <psemek/ecs/detail/query_cache.hpp>
#include <psemek/ecs/detail/table.hpp>

namespace psemek::ecs::detail
{

	void query_cache::add(table * table)
	{
		auto & entry = entries.emplace_back();
		entry.table = table;

		for (auto const & uuid : with_uuids)
			entry.columns_indices.push_back(*table->column_index(uuid));

		for (auto const & factory : constructor_factories)
			entry.table->add_constructor(factory(entry.columns_indices));

		for (auto const & factory : destructor_factories)
			entry.table->add_destructor(factory(entry.columns_indices));
	}

}
