#pragma once

#include <psemek/fonts/font.hpp>
#include <psemek/fonts/kerned_font.hpp>

namespace psemek::fonts
{

	struct msdf_font
		: kerned_font
	{
		msdf_font(bmfont_data data, gfx::texture_2d atlas);

		font_type type() const override { return font_type::msdf; }

		float sdf_scale() const override { return data_.sdf_scale; }
	};

}
