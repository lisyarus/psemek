#pragma once

#include <psemek/fonts/font.hpp>
#include <psemek/fonts/bmfont.hpp>

namespace psemek::fonts
{

	struct kerned_font
		: font
	{
		kerned_font(bmfont_data data, gfx::texture_2d atlas);

		font_type type() const override { return font_type::bitmap; }

		std::string_view name() const override { return data_.name; }

		math::vector<int, 2> size() const override { return data_.size; }

		int baseline() const override { return data_.baseline; }

		bool supports_character(char32_t c) const override;
		util::span<character_range const> supported_characters() const override { return ranges_; }

		std::vector<glyph> shape(std::string_view str, shape_options const & options, math::point<float, 2> & pen) const override;
		std::vector<glyph> shape(std::u32string_view str, shape_options const & options, math::point<float, 2> & pen) const override;

		gfx::texture_2d const & atlas() const override { return atlas_; };

		std::optional<math::box<float, 2>> texcoords(char32_t character) const override;

	protected:
		std::vector<character_range> ranges_;
		bmfont_data data_;
		gfx::texture_2d atlas_;

		template <typename String>
		std::vector<glyph> shape_impl(String const & str, shape_options const & options, math::point<float, 2> & pen) const;
	};

}
