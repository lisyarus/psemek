#pragma once

#include <psemek/fonts/font.hpp>

namespace psemek::fonts
{

	struct monospace_font
		: font
	{
		monospace_font(character_range range, std::string_view name, math::vector<int, 2> size, gfx::texture_2d atlas, std::vector<math::box<float, 2>> texcoords);

		font_type type() const override { return font_type::bitmap; }

		std::string_view name() const override { return name_; }

		math::vector<int, 2> size() const override { return size_; }

		bool supports_character(char32_t c) const override;
		util::span<character_range const> supported_characters() const override { return {&range_, &range_ + 1}; }

		std::vector<glyph> shape(std::string_view str, shape_options const & options, math::point<float, 2> & pen) const override;
		std::vector<glyph> shape(std::u32string_view str, shape_options const & options, math::point<float, 2> & pen) const override;

		gfx::texture_2d const & atlas() const override { return atlas_; };

		std::optional<math::box<float, 2>> texcoords(char32_t character) const override;

	private:
		character_range range_;
		std::string_view name_;
		math::vector<int, 2> size_;
		gfx::texture_2d atlas_;
		std::vector<math::box<float, 2>> texcoords_;

		template <typename String>
		std::vector<glyph> shape_impl(String const & str, shape_options const & options, math::point<float, 2> & pen) const;
	};

}
