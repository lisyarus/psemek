#include <psemek/fonts/msdf_font.hpp>
#include <psemek/gfx/pixmap.hpp>

namespace psemek::fonts
{

	std::unique_ptr<font> make_msdf_font(io::istream && description, io::istream && texture)
	{
		auto data = bmfont_data::parse(std::move(description));

		gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
		gl::PixelStorei(gl::PACK_ALIGNMENT, 1);

		auto pixmap = gfx::read_image<gfx::color_rgb>(std::move(texture));
		gfx::texture_2d atlas;
		atlas.load(pixmap);
		atlas.linear_mipmap_filter();
		atlas.anisotropy();
		atlas.generate_mipmap();

		return std::make_unique<msdf_font>(std::move(data), std::move(atlas));
	}

	msdf_font::msdf_font(bmfont_data data, gfx::texture_2d atlas)
		: kerned_font(std::move(data), std::move(atlas))
	{}

}
