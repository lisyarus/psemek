#pragma once

#include <psemek/async/executor.hpp>

namespace psemek::async
{

	struct synchronous_executor
		: executor
	{
		void post(task t) override;

		void post_at(clock::time_point time, task t) override;

		void stop() override;

		void wait() override;

		void wait_for(clock::duration period) override;

		void wait_until(clock::time_point time) override;

		std::size_t task_count() const override;
	};

}
