#pragma once

#include <psemek/async/executor.hpp>
#include <psemek/util/synchronyzed_queue.hpp>

#include <optional>

namespace psemek::async
{

	struct event_loop
		: executor
	{
		void post(task t) override;

		void post_at(clock::time_point time, task t) override;

		void stop() override;

		void wait() override;

		void wait_for(clock::duration period) override;

		void wait_until(clock::time_point time) override;

		std::size_t task_count() const override;

		~event_loop() override { stop(); }

		// wait all non-delayed events
		std::size_t pump(std::optional<std::size_t> max_events = std::nullopt);

	private:
		util::synchronized_queue<task> queue_;
	};


}
