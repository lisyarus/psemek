#include <psemek/async/synchronous_executor.hpp>
#include <psemek/util/exception.hpp>

#include <stdexcept>

namespace psemek::async
{

	void synchronous_executor::post(task t)
	{
		t();
	}

	void synchronous_executor::post_at(clock::time_point, task)
	{
		throw util::exception("Synchronous_executor doesn't support executor::post_at");
	}

	void synchronous_executor::stop()
	{}

	void synchronous_executor::wait()
	{}

	void synchronous_executor::wait_for(clock::duration)
	{}

	void synchronous_executor::wait_until(clock::time_point)
	{}

	std::size_t synchronous_executor::task_count() const
	{
		return 0;
	}

}
