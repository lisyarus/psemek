#pragma once

#include <psemek/bt/repeat.hpp>
#include <psemek/bt/negate.hpp>

namespace psemek::bt
{

	template <typename Tree>
	node_ptr<Tree> retry(node_ptr<Tree> child, std::optional<std::size_t> max_count = std::nullopt)
	{
		return repeat<Tree>(negate<Tree>(std::move(child)), max_count);
	}

}
