#pragma once

#include <psemek/bt/success.hpp>
#include <psemek/bt/repeat.hpp>

namespace psemek::bt
{

	template <typename Tree>
	node_ptr<Tree> forever(node_ptr<Tree> child)
	{
		return repeat<Tree>(success<Tree>(std::move(child)));
	}

}
