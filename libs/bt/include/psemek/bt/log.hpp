#pragma once

#ifdef PSEMEK_BT_LOG
#include <psemek/log/log.hpp>
#define bt_log ::psemek::log::debug
#else
#include <psemek/util/null_ostream.hpp>
#define bt_log ::psemek::util::null_ostream
#endif
