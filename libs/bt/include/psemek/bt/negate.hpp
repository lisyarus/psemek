#pragma once

#include <psemek/bt/node.hpp>

#include <optional>

namespace psemek::bt
{

	template <typename Tree>
	struct negate_node;

	template <typename Time, typename Event, typename ... Args>
	struct negate_node<tree<Time, Event, Args...>>
		: node<tree<Time, Event, Args...>>
	{
		using tree_type = tree<Time, Event, Args...>;
		using node_type = node<tree_type>;
		using typename node_type::finished;
		using typename node_type::status;

		negate_node(node_ptr<tree_type> child)
			: child_(std::move(child))
		{}

		void start(Args ... args) override
		{
			child_->start(args...);
		}

		status update(Time dt, Args ... args) override
		{
			auto result = child_->update(dt, args...);
			if (auto f = std::get_if<finished>(&result))
			{
				if (f->result)
					return finished{false};
				else
					return finished{true};
			}
			return result;
		}

		bool event(Event const & event, Args ... args) override
		{
			return child_->event(event, args...);
		}

	private:
		node_ptr<tree<Time, Event, Args...>> child_;
	};

	template <typename Tree>
	node_ptr<Tree> negate(node_ptr<Tree> child)
	{
		return std::make_unique<negate_node<Tree>>(std::move(child));
	}

}
