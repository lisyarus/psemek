#pragma once

#include <psemek/bt/node.hpp>
#include <psemek/bt/sequence.hpp>
#include <psemek/bt/success.hpp>
#include <psemek/bt/trampoline.hpp>

namespace psemek::bt
{

	template <typename Tree>
	node_ptr<Tree> finally(node_ptr<Tree> child, node_ptr<Tree> finalizator)
	{
		trampoline<Tree> t;

		return
			t.rethrow(
				bt::sequence(
					bt::success(t.intercept(std::move(child))),
					std::move(finalizator)
				)
			);
	}

}
