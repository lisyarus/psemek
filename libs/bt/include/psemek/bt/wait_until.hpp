#pragma once

#include <psemek/bt/retry.hpp>

namespace psemek::bt
{

	template <typename Tree>
	node_ptr<Tree> wait_until(node_ptr<Tree> child)
	{
		return retry<Tree>(std::move(child));
	}

}
