#pragma once

#include <psemek/bt/status.hpp>
#include <psemek/log/log.hpp>
#include <psemek/util/exception.hpp>
#include <psemek/util/to_string.hpp>

namespace psemek::bt
{

	struct assertion_failed_exception
		: util::exception
	{
		assertion_failed_exception(std::string message, util::stacktrace stacktrace = {})
			: util::exception(std::move(message), std::move(stacktrace))
		{}
	};

	template <typename ... Args>
	inline bool assertion_failed(Args const & ... args)
	{
		(::psemek::log::error() << ... << args);
		throw assertion_failed_exception(util::to_string(args...));
	}

}

#define bt_assert(cond, ...) (void)(!(cond) && ::psemek::bt::assertion_failed("BT assertion failed: ", #cond, " ", ##__VA_ARGS__, " (", __FILE__, ":", __LINE__, ")"))
