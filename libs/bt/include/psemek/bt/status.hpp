#pragma once

#include <variant>

namespace psemek::bt::detail
{

	struct running
	{};

	struct finished
	{
		bool result;
	};

	template <typename Time>
	struct suspended
	{
		Time duration;
	};

	template <typename Time>
	using status = std::variant<running, finished, suspended<Time>>;

}
