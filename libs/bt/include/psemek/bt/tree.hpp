#pragma once

#include <tuple>

namespace psemek::bt
{

	template <typename Time, typename Event, typename ... Args>
	struct tree
	{
		using time_type = Time;
		using event_type = Event;
		using args_type = std::tuple<Args...>;
	};

}
