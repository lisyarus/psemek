#pragma once

#include <psemek/bt/tree.hpp>
#include <psemek/bt/status.hpp>

#include <memory>

namespace psemek::bt
{

	template <typename Tree>
	struct node;

	template <typename Time, typename Event, typename ... Args>
	struct node<tree<Time, Event, Args...>>
	{
		using tree_type = tree<Time, Event, Args...>;

		using running = detail::running;
		using finished = detail::finished;
		using suspended = detail::suspended<Time>;

		using status = detail::status<Time>;

		virtual void start(Args ...) {}
		virtual status update(Time dt, Args ... args) = 0;
		virtual bool event(Event const &, Args ...) { return false; }

		virtual ~node() {}
	};

	template <typename Tree>
	using node_ptr = std::unique_ptr<node<Tree>>;

}
