#pragma once

#include <psemek/bt/action.hpp>
#include <psemek/util/functional.hpp>

namespace psemek::bt
{

	template <typename Tree>
	node_ptr<Tree> nop()
	{
		return action<Tree>(util::nop);
	}

}
