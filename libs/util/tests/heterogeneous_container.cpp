#include <psemek/test/test.hpp>

#include <psemek/util/heterogeneous_container.hpp>

using namespace psemek::util;

using container = heterogeneous_container<std::uint32_t, char, int, float, double>;

test_case(util_heterogeneous__container_empty)
{
	container c;

	expect(c.empty());
	expect_equal(c.size(), 0);
}

test_case(util_heterogeneous__container_max__size)
{
	container c;

	expect_gequal(c.max_single_size(), 1 << 20);
}

test_case(util_heterogeneous__container_size)
{
	container c;

	auto c0 = c.insert('0');
	expect_equal(c.size(), 1);
	expect(!c.empty());

	auto c1 = c.insert('1');
	expect_equal(c.size(), 2);
	expect(!c.empty());

	auto c2 = c.insert('2');
	expect_equal(c.size(), 3);
	expect(!c.empty());

	auto d0 = c.insert(0.0);
	expect_equal(c.size(), 4);
	expect(!c.empty());

	auto d1 = c.insert(1.0);
	expect_equal(c.size(), 5);
	expect(!c.empty());

	auto d2 = c.insert(2.0);
	expect_equal(c.size(), 6);
	expect(!c.empty());

	c.erase(c2);
	expect_equal(c.size(), 5);
	expect(!c.empty());

	c.erase(d1);
	expect_equal(c.size(), 4);
	expect(!c.empty());

	c.erase(d0);
	expect_equal(c.size(), 3);
	expect(!c.empty());

	c.erase(c0);
	expect_equal(c.size(), 2);
	expect(!c.empty());

	c.erase(d2);
	expect_equal(c.size(), 1);
	expect(!c.empty());

	c.erase(c1);
	expect_equal(c.size(), 0);
	expect(c.empty());
}

test_case(util_heterogeneous__container_get)
{
	container c;

	auto c0 = c.insert('0');
	expect_equal(c.get<char>(c0), '0');
	expect_equal(c.get(c0).index(), 0);
	expect_equal_deref(std::get<0>(c.get(c0)), '0');
	expect_equal_deref(std::get<char *>(c.get(c0)), '0');

	auto c1 = c.insert('1');
	expect_equal(c.get<char>(c1), '1');
	expect_equal(c.get(c1).index(), 0);
	expect_equal_deref(std::get<0>(c.get(c1)), '1');
	expect_equal_deref(std::get<char *>(c.get(c1)), '1');

	auto f0 = c.insert(3.14f);
	expect_equal(c.get<float>(f0), 3.14f);
	expect_equal(c.get(f0).index(), 2);
	expect_equal_deref(std::get<2>(c.get(f0)), 3.14f);
	expect_equal_deref(std::get<float *>(c.get(f0)), 3.14f);

	expect_throw(c.get<float>(c0), std::exception);
	expect_throw(c.get<float>(c1), std::exception);
	expect_throw(c.get<char>(f0), std::exception);
}

test_case(util_heterogeneous__container_visit)
{
	container c;

	auto c0 = c.insert('0');
	auto f0 = c.insert(3.14f);

	c.visit([](auto const & v){
		if constexpr (std::is_same_v<std::decay_t<decltype(v)>, char>)
		{
			expect_equal(v, '0');
		}
		else
		{
			expect(false);
		}
	}, c0);

	c.visit([](auto const & v){
		if constexpr (std::is_same_v<std::decay_t<decltype(v)>, float>)
		{
			expect_equal(v, 3.14f);
		}
		else
		{
			expect(false);
		}
	}, f0);

	c.visit([](auto const & vc, auto const & vf){
		if constexpr (std::is_same_v<std::decay_t<decltype(vc)>, char> && std::is_same_v<std::decay_t<decltype(vf)>, float>)
		{
			expect_equal(vc, '0');
			expect_equal(vf, 3.14f);
		}
		else
		{
			expect(false);
		}
	}, c0, f0);

	c.visit([](auto const & vf, auto const & vc){
		if constexpr (std::is_same_v<std::decay_t<decltype(vc)>, char> && std::is_same_v<std::decay_t<decltype(vf)>, float>)
		{
			expect_equal(vc, '0');
			expect_equal(vf, 3.14f);
		}
		else
		{
			expect(false);
		}
	}, f0, c0);
}
