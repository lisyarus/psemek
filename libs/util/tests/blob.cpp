#include <psemek/test/test.hpp>

#include <psemek/util/blob.hpp>

using namespace psemek::util;

test_case(util_blob_empty)
{
	blob b;
	expect(!b);
	expect_equal(b.size(), 0);
	expect_equal_ptr(b.data(), nullptr);
}

test_case(util_blob_alloc)
{
	blob b(16);
	expect(b);
	expect_equal(b.size(), 16);
	expect_different_ptr(b.data(), nullptr);

	for (int i = 0; i < b.size(); ++i)
		b[i] = static_cast<char>(i);

	for (int i = 0; i < b.size(); ++i)
		expect_equal((int)b[i], i);
}

test_case(util_blob_release)
{
	blob b;
	expect(!b);
	expect_equal(b.size(), 0);
	expect_equal_ptr(b.data(), nullptr);

	std::unique_ptr<char[]> p(new char[16]);
	auto q = p.get();

	b = blob(16, std::move(p));
	expect(b);
	expect_equal(b.size(), 16);
	expect_equal_ptr(b.data(), q);

	p = b.release();

	expect(!b);
	expect_equal(b.size(), 0);
	expect_equal_ptr(b.data(), nullptr);
	expect_equal(p.get(), q);
}

test_case(util_blob_copy)
{
	blob b(16);
	expect(b);
	expect_equal(b.size(), 16);
	expect_different_ptr(b.data(), nullptr);

	blob c(b);
	expect(c);
	expect_equal(c.size(), 16);
	expect_different_ptr(c.data(), nullptr);

	blob d;
	expect(!d);
	expect_equal(d.size(), 0);
	expect_equal_ptr(d.data(), nullptr);

	d = c;
	expect(d);
	expect_equal(d.size(), 16);
	expect_different_ptr(d.data(), nullptr);
}

test_case(util_blob_move)
{
	blob b(16);
	expect(b);
	expect_equal(b.size(), 16);
	expect_different_ptr(b.data(), nullptr);

	auto p = b.data();

	blob c(std::move(b));
	expect(c);
	expect_equal(c.size(), 16);
	expect_equal_ptr(c.data(), p);

	blob d;
	expect(!d);
	expect_equal(d.size(), 0);
	expect_equal_ptr(d.data(), nullptr);

	d = std::move(c);
	expect(d);
	expect_equal(d.size(), 16);
	expect_equal_ptr(d.data(), p);
}
