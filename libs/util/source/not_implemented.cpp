#include <psemek/util/not_implemented.hpp>

namespace psemek::util
{

	not_implemented_error::not_implemented_error(util::stacktrace stacktrace)
		: exception("Not implemented", std::move(stacktrace))
	{}

	void not_implemented()
	{
		throw not_implemented_error{};
	}

}
