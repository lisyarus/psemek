#include <psemek/util/terminal_color.hpp>

#include <cstdlib>

namespace psemek::util::terminal_color
{

	bool supported()
	{
		std::string_view term;
		if (auto t = std::getenv("TERM"))
			term = t;

		return term == "linux" || term.starts_with("xterm");
	}

	std::string_view bold()
	{
		return "\033[1m";
	}

	std::string_view red()
	{
		return "\033[1;31m";
	}

	std::string_view green()
	{
		return "\033[1;32m";
	}

	std::string_view yellow()
	{
		return "\033[33m";
	}

	std::string_view cyan()
	{
		return "\033[36m";
	}

	std::string_view normal()
	{
		return "\033[0m";
	}

}
