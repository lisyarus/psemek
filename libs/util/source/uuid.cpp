#include <psemek/util/uuid.hpp>

#include <iomanip>

namespace psemek::util
{

	std::ostream & operator << (std::ostream & os, uuid const & uuid)
	{
		os << std::hex << std::setfill('0') << std::left;
		os << std::setw(8)  <<  (uuid[0] & 0x00000000ffffffffull);
		os << std::setw(1) << '-';
		os << std::setw(4)  << ((uuid[0] & 0x0000ffff00000000ull) >> 32);
		os << std::setw(1) << '-';
		os << std::setw(4)  << ((uuid[0] & 0xffff000000000000ull) >> 48);
		os << std::setw(1) << '-';
		os << std::setw(4)  <<  (uuid[1] & 0x000000000000ffffull);
		os << std::setw(1) << '-';
		os << std::setw(12) << ((uuid[1] & 0xffffffffffff0000ull) >> 16);

		return os;
	}

}
