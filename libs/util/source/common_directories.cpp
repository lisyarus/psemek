#include <psemek/util/common_directories.hpp>

#include <cstdlib>

#ifdef _WIN32
#include <fileapi.h>
#endif

namespace psemek::util
{

	std::vector<std::pair<std::string, std::filesystem::path>> common_directories()
	{
		std::vector<std::pair<std::string, std::filesystem::path>> result;

#ifdef __linux__
		{
			std::filesystem::path path = std::getenv("HOME");
			if (!path.empty())
				result.push_back({"Home", path});
		}

		result.push_back({"Root", "/"});
#endif

#ifdef _WIN32
		{
			std::filesystem::path path = std::getenv("USERPROFILE");
			if (!path.empty())
				result.push_back({"Home", path});
		}

		{
			auto drives = GetLogicalDrives();
			for (int i = 0; i < 26; ++i)
			{
				if ((drives & (1 << i)) == 0) continue;

				char name[3] = "A:";
				char path[4] = "A:\\";
				name[0] = 'A' + i;
				path[0] = name[0];
				result.push_back({name, path});
			}
		}
#endif

		return result;
	}

}
