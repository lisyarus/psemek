#include <psemek/util/type_name.hpp>

#include <boost/core/demangle.hpp>

namespace psemek::util
{

	std::string type_name(std::type_info const & type)
	{
		return boost::core::demangle(type.name());
	}

	std::string type_name(std::type_index const & type)
	{
		return boost::core::demangle(type.name());
	}

}
