#include <psemek/util/open_url.hpp>

#include <cstdlib>

namespace psemek::util
{

	[[maybe_unused]] static void ignore(int) {}

	static void do_open_url(std::string url)
	{
		if (!url.starts_with("http"))
			url = "https://" + url;

#ifdef _WIN32
		url = "start " + url;
		ignore(std::system(url.data()));
#endif

#ifdef __linux__
		url = "xdg-open " + url;
		ignore(std::system(url.data()));
#endif
	}

	void open_url(std::string url)
	{
		do_open_url(std::move(url));
	}

}
