#pragma once

#include <memory>

namespace psemek::util
{

	template <typename T>
	std::shared_ptr<T> to_shared(T && x)
	{
		return std::make_shared<T>(std::move(x));
	}

}
