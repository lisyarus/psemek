#pragma once

#include <psemek/util/exception.hpp>
#undef assert

#ifdef PSEMEK_DEBUG
#define assert(x) ((void)(!(x) && ::psemek::util::assertion_handler("Assertion failed: " #x)))
#else
#define assert(x) ((void)(x))
#endif

namespace psemek::util
{

	[[noreturn]] inline bool assertion_handler(char const * assertion, stacktrace stacktrace = {})
	{
		 throw ::psemek::util::exception(assertion, std::move(stacktrace));
	}

}
