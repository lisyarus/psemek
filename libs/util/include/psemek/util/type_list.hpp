#pragma once

#include <tuple>
#include <variant>

namespace psemek::util
{

	template <typename ...>
	struct type_list;

	namespace detail
	{

		template <template <typename> typename F, typename ... Args>
		struct type_list_map_impl
		{
			using type = type_list<typename F<Args>::type ...>;
		};

	}

	template <typename ... Args>
	struct type_list
	{
		template <template <typename> typename F>
		using map = detail::type_list_map_impl<F, Args...>::type;

		using tuple = std::tuple<Args...>;
		using variant = std::variant<Args...>;
	};

}
