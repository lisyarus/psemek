#pragma once

#include <utility>

namespace psemek::util
{

	struct null_ostream
	{
		null_ostream & write(char const *, std::size_t)
		{
			return *this;
		}
	};

	template <typename T>
	null_ostream & operator << (null_ostream & os, T const &)
	{
		return os;
	}

	template <typename T>
	null_ostream && operator << (null_ostream && os, T const &)
	{
		return std::move(os);
	}

}
