#pragma once

#include <psemek/util/exception.hpp>

namespace psemek::util
{

	struct not_implemented_error
		: exception
	{
		not_implemented_error(util::stacktrace stacktrace = {});
	};

	[[noreturn]] void not_implemented();

}
