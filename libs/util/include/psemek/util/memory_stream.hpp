#pragma once

#include <iostream>

namespace psemek::util
{

	struct memory_istream
		: std::istream
	{
		memory_istream(std::string_view data)
			: std::istream(&b_)
			, b_(data)
		{}

	private:
		struct buf
			: std::streambuf
		{
			buf(std::string_view data)
			{
				char * p = const_cast<char *>(data.data());
				setg(p, p, p + data.size());
			}
		};

		buf b_;
	};

}
