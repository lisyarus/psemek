#pragma once

namespace psemek::util
{

	constexpr auto nop = [](auto const & ...){};

	constexpr auto id = [](auto && x) -> decltype(auto) { return x; };

	template <typename T, T value>
	constexpr auto always = [](auto && ...){ return value; };

	constexpr auto always_true  = always<bool, true>;
	constexpr auto always_false = always<bool, false>;

	template <typename T>
	auto constant(T const & x)
	{
		return [x](auto const & ...){ return x; };
	}

	template <typename F1, typename F2>
	auto bind_and(F1 && f1, F2 && f2)
	{
		return [=](auto const &... args){
			return f1(args...) && f2(args...);
		};
	}

}
