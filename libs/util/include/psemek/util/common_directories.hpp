#pragma once

#include <filesystem>
#include <vector>

namespace psemek::util
{

	std::vector<std::pair<std::string, std::filesystem::path>> common_directories();

}
