#pragma once

#include <psemek/util/clock.hpp>

namespace psemek::util
{

	template <typename Duration = std::chrono::duration<double>, typename Clock = std::chrono::system_clock>
	struct timer
		: clock<Duration, Clock>
	{
		timer(Duration duration)
			: duration_(duration)
		{}

		explicit operator bool()
		{
			if (this->duration() >= duration_)
			{
				this->start_ += std::chrono::duration_cast<typename Clock::duration>(duration_);
				return true;
			}

			return false;
		}

	private:
		Duration duration_;
	};

}
