#pragma once

#include <filesystem>

namespace psemek::util
{

	std::filesystem::path executable_path();

}
