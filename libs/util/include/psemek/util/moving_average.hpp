#pragma once

#include <vector>

namespace psemek::util
{

	template <typename T>
	struct moving_average
	{
		moving_average(std::size_t max)
			: data_(max, T())
			, begin_(0)
			, size_(0)
			, sum_(T())
		{}

		void clear()
		{
			begin_ = 0;
			size_ = 0;
			sum_ = T(0);
		}

		void push(T x)
		{
			if (size_ >= data_.size())
			{
				sum_ -= data_[begin_];
			}
			else
				size_++;

			data_[begin_++] = x;
			sum_ += x;

			if (begin_ == data_.size())
				begin_ = 0;
		}

		T sum() const
		{
			return sum_;
		}

		std::size_t count() const
		{
			return size_;
		}

		std::size_t max_count() const
		{
			return data_.size();
		}

		T average() const
		{
			return sum() / count();
		}

	private:
		std::vector<T> data_;
		std::size_t begin_, size_;
		T sum_;
	};

}
