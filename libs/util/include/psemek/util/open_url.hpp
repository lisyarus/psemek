#pragma once

#include <string>

namespace psemek::util
{

	void open_url(std::string url);

}
