#pragma once

#include <string_view>

namespace psemek::util::terminal_color
{

	bool supported();
	std::string_view bold();
	std::string_view red();
	std::string_view green();
	std::string_view yellow();
	std::string_view cyan();
	std::string_view normal();

}
