#pragma once

namespace psemek::util
{

	template <typename Time>
	struct updater
	{
		virtual void update(Time dt) = 0;

		virtual ~updater() {}
	};

}
