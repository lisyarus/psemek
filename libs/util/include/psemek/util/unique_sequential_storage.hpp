#pragma once

#include <psemek/util/hash_table.hpp>

#include <vector>
#include <cstdint>

namespace psemek::util
{

	template <typename T, typename Hash = std::hash<T>>
	struct unique_sequential_storage
	{
		using index = std::uint32_t;

		template <typename H>
		index insert(H && value)
		{
			if (auto it = value_to_index_.find(value); it != value_to_index_.end())
				return it->second;

			index result = index_to_value_.size();
			index_to_value_.push_back(value);
			value_to_index_[std::forward<H>(value)] = result;
			return result;
		}

		T const & get(index i) const
		{
			return index_to_value_[i];
		}

	private:
		util::hash_map<T, index, Hash> value_to_index_;
		std::vector<T> index_to_value_;
	};

}
