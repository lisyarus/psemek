#pragma once

#define _UTIL_CAT2(x,y) x##y
#define _UTIL_CAT(x,y) _UTIL_CAT2(x,y)
#define autoname _UTIL_CAT(autonamed_,__COUNTER__)
