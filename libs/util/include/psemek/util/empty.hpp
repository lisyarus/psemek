#pragma once

#include <iostream>

namespace psemek::util
{

	struct empty{};

	inline std::ostream & operator << (std::ostream & os, empty)
	{
		os << "empty";
		return os;
	}

}
