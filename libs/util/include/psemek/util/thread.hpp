#pragma once

#include <thread>

namespace psemek::util
{

	struct thread
		: std::thread
	{
		template <typename ... Args>
		thread(Args && ... args)
			: std::thread(std::forward<Args>(args)...)
		{}

		thread(thread &&) = default;
		thread & operator = (thread &&) = default;

		~ thread()
		{
			if (joinable())
				join();
		}
	};

}
