#pragma once

namespace psemek::util
{

	template <typename F>
	struct at_scope_exit
	{
		F f;

		at_scope_exit(F f)
			: f(f)
		{}

		~at_scope_exit()
		{
			f();
		}
	};

}
