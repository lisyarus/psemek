#pragma once

#include <psemek/util/exception.hpp>

namespace psemek::util
{

	struct system_error
		: exception
	{
		system_error(std::error_code error_code, std::string message, util::stacktrace stacktrace = {})
			: exception(message + ": " + error_code.message(), std::move(stacktrace))
			, error_code_(error_code)
		{}

		std::error_code error_code() const
		{
			return error_code_;
		}

	private:
		std::error_code error_code_;
	};

}
