#include <psemek/react/value.hpp>
#include <psemek/util/unused.hpp>

namespace psemek::react
{

	namespace detail
	{

		void on_node_destroyed(std::string const & debug_marker)
		{
			// Can put a breakpoint here to track destruction of specific nodes
			unused(debug_marker);
		}

	}

}
