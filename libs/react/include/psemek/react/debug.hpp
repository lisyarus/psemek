#pragma once

#include <psemek/react/value.hpp>

namespace psemek::react
{

	template <typename T>
	value<T> debug_marker(std::string marker, value<T> value)
	{
		value.node(detail::internal_tag{})->debug_marker = std::move(marker);
		return value;
	}

}
