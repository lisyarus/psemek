#include <psemek/app/resource.hpp>
#include <psemek/io/file_stream.hpp>
#include <psemek/log/log.hpp>

namespace psemek::app
{

	std::unique_ptr<io::istream> open_resource(std::filesystem::path const & relative_path)
	{
		log::info() << "Opening resource " << relative_path;
		return std::make_unique<io::file_istream>(app::resource_root() / relative_path);
	}

}
