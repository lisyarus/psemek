#pragma once

#include <psemek/gfx/pixmap.hpp>
#include <psemek/math/point.hpp>

#include <memory>

namespace psemek::sdl2
{

	enum class default_cursor_type
	{
		arrow,
		beam,
		wait,
		crosshair,
		waitarrow,
		sizenwse,
		sizenesw,
		sizewe,
		sizens,
		sizeall,
		no,
		hand,
	};

	struct cursor;

	std::shared_ptr<cursor> get_default_cursor(default_cursor_type type);
	std::shared_ptr<cursor> make_cursor(gfx::pixmap_rgba const & image, math::point<int, 2> const & pivot);

	void set_cursor(cursor const & cursor);

}
