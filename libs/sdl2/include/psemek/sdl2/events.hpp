#pragma once

#include <psemek/app/event_handler.hpp>

namespace psemek::sdl2
{

	// Returns true is quit is requested
	bool poll_events(app::event_handler & handler);

}
