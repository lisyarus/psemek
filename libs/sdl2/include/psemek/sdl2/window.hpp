#pragma once

#include <psemek/app/application.hpp>

#if defined(PSEMEK_GRAPHICS_API_WEBGPU)
#include <psemek/wgpu/surface.hpp>
#include <psemek/wgpu/adapter.hpp>
#include <psemek/wgpu/device.hpp>
#endif

#include <SDL2/SDL.h>

namespace psemek::sdl2
{

	struct window
	{
		window(psemek::app::application::options const & options);
		~window();

		math::vector<int, 2> size() const;

		void show();
		void swap();
		void show_cursor(bool show);
		void relative_mouse_mode(bool mode);
		void vsync(bool on);
		void windowed(bool on);

#if defined(PSEMEK_GRAPHICS_API_WEBGPU)
		wgpu::adapter wgpu_adapter() const { return wgpu_adapter_; }
		wgpu::surface wgpu_surface() const { return wgpu_surface_; }
		wgpu::device wgpu_device() const { return wgpu_device_; }
#endif

	private:
		std::shared_ptr<void> sdl_init_;
		SDL_Window * window_ = nullptr;

#if defined(PSEMEK_GRAPHICS_API_OPENGL)
		SDL_GLContext gl_context_ = nullptr;
#elif defined(PSEMEK_GRAPHICS_API_WEBGPU)
		wgpu::adapter wgpu_adapter_;
		wgpu::surface wgpu_surface_;
		wgpu::device wgpu_device_;
#endif
	};

}
