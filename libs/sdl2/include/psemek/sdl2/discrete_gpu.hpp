#pragma once

#ifdef _WIN32

#define PSEMEK_SDL2_DISCRETE_GPU \
extern "C" { __declspec(dllexport) unsigned long NvOptimusEnablement = 1; } \
extern "C" { __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1; }

#else

#define PSEMEK_SDL2_DISCRETE_GPU

#endif
