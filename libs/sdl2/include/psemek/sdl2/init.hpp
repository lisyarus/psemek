#pragma once

#include <memory>
#include <cstdint>

namespace psemek::sdl2
{

	[[noreturn]] void fail(std::string const & message);

	std::shared_ptr<void> init(std::uint32_t subsystems);

}
