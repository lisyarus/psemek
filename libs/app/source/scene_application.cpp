#include <psemek/app/scene_application.hpp>

namespace psemek::app
{

	void scene_application::on_event(resize_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(focus_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(mouse_move_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(mouse_wheel_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(mouse_button_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(touch_down_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(touch_up_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(touch_move_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::on_event(key_event const & event)
	{
		on_event_impl(event);
	}

	void scene_application::stop()
	{
		if (auto scene = current_scene())
			scene->on_exit();
		application_base::stop();
	}

	void scene_application::update()
	{
		if (auto scene = current_scene())
			scene->update();
	}

	void scene_application::present()
	{
		if (auto scene = current_scene())
			scene->present();
	}

	template <typename Event>
	void scene_application::on_event_impl(Event const & event)
	{
		application_base::on_event(event);
		if (auto scene = current_scene())
			scene->on_event(event);
	}

}
