#include <psemek/app/resource.hpp>
#include <psemek/util/executable_path.hpp>

namespace psemek::app
{

	namespace
	{

		static std::filesystem::path global_resource_root = util::executable_path().parent_path();

	}

	std::filesystem::path resource_root()
	{
		return global_resource_root;
	}

	void set_resource_root(std::filesystem::path const & root)
	{
		global_resource_root = root;
	}

}
