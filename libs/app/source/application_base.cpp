#include <psemek/app/application_base.hpp>

namespace psemek::app
{

	void application_base::on_event(resize_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(focus_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(mouse_move_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(mouse_wheel_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(mouse_button_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(touch_down_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(touch_up_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(touch_move_event const & event)
	{
		apply(state_, event);
	}

	void application_base::on_event(key_event const & event)
	{
		apply(state_, event);
	}

	void application_base::stop()
	{
		running_ = false;
	}

	bool application_base::running() const
	{
		return running_;
	}

}
