#pragma once

#include <psemek/app/events.hpp>
#include <psemek/util/hash_table.hpp>

namespace psemek::app
{

	struct event_state
	{
		math::vector<int, 2> size = {0, 0};
		bool focus = true;
		math::point<int, 2> mouse = {0, 0};
		int wheel = 0;
		util::hash_set<mouse_button> mouse_button_down;
		util::hash_set<keycode> key_down;
	};

	inline void apply(event_state & state, resize_event const & event)
	{
		state.size = event.size;
	}

	inline void apply(event_state & state, focus_event const & event)
	{
		state.focus = event.gained;
	}

	inline void apply(event_state & state, mouse_move_event const & event)
	{
		state.mouse = event.position;
	}

	inline void apply(event_state & state, mouse_wheel_event const & event)
	{
		state.wheel += event.delta;
	}

	inline void apply(event_state & state, mouse_button_event const & event)
	{
		if (event.down)
			state.mouse_button_down.insert(event.button);
		else
			state.mouse_button_down.erase(event.button);
	}

	inline void apply(event_state &, touch_down_event const &)
	{}

	inline void apply(event_state &, touch_up_event const &)
	{}

	inline void apply(event_state &, touch_move_event const &)
	{}

	inline void apply(event_state & state, key_event const & event)
	{
		if (event.down)
			state.key_down.insert(event.key);
		else
			state.key_down.erase(event.key);
	}

}
