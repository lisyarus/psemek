#pragma once

#include <psemek/io/stream.hpp>

#include <filesystem>
#include <memory>

namespace psemek::app
{

	std::filesystem::path resource_root();
	void set_resource_root(std::filesystem::path const & root);

	// Implemented by a backend library
	std::unique_ptr<io::istream> open_resource(std::filesystem::path const & relative_path);

}
