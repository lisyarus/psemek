#pragma once

#include <psemek/app/application.hpp>

#include <memory>

namespace psemek::app
{

	template <typename Factory>
	std::unique_ptr<application::factory> default_application_factory(application::options const & options, Factory && factory)
	{
		struct factory_impl
			: application::factory
		{
			application::options opts;
			Factory factory;

			factory_impl(application::options const & options, Factory && factory)
				: opts(options)
				, factory(std::move(factory))
			{}

			application::options options() override
			{
				return opts;
			}

			std::unique_ptr<application> create(application::options const & options, application::context const & context) override
			{
				return factory(options, context);
			}
		};

		return std::make_unique<factory_impl>(options, std::move(factory));
	}

	template <typename Application>
	std::unique_ptr<application::factory> default_application_factory(application::options const & options)
	{
		return default_application_factory(options, [](auto const & options, auto const & context){
			return std::make_unique<Application>(options, context);
		});
	}

}

