#pragma once

#include <psemek/math/point.hpp>

#include <string>

namespace psemek::app
{

	struct resize_event
	{
		math::vector<int, 2> size;
	};

	struct focus_event
	{
		bool gained;
	};

	struct mouse_move_event
	{
		math::point<int, 2> position;
		math::vector<int, 2> relative;
	};

	struct mouse_wheel_event
	{
		int delta;
	};

	enum class mouse_button
	{
		left,
		middle,
		right,
	};

	struct mouse_button_event
	{
		mouse_button button;
		bool down;
	};

	struct touch_down_event
	{
		math::point<int, 2> position;
	};

	struct touch_up_event
	{
		math::point<int, 2> position;
	};

	struct touch_move_event
	{
		math::point<int, 2> position;
	};

	enum class keycode
	{
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		NUM_1,
		NUM_2,
		NUM_3,
		NUM_4,
		NUM_5,
		NUM_6,
		NUM_7,
		NUM_8,
		NUM_9,
		NUM_0,
		RETURN,
		ESCAPE,
		BACKSPACE,
		TAB,
		SPACE,
		MINUS,
		EQUALS,
		LEFTBRACKET,
		RIGHTBRACKET,
		BACKSLASH,
		NONUSHASH,
		SEMICOLON,
		APOSTROPHE,
		BACKQUOTE,
		COMMA,
		PERIOD,
		SLASH,
		CAPSLOCK,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		PRINTSCREEN,
		SCROLLLOCK,
		PAUSE,
		INSERT,
		HOME,
		PAGEUP,
		DELETE,
		END,
		PAGEDOWN,
		RIGHT,
		LEFT,
		DOWN,
		UP,
		NUMLOCKCLEAR,
		KP_DIVIDE,
		KP_MULTIPLY,
		KP_MINUS,
		KP_PLUS,
		KP_ENTER,
		KP_1,
		KP_2,
		KP_3,
		KP_4,
		KP_5,
		KP_6,
		KP_7,
		KP_8,
		KP_9,
		KP_0,
		KP_PERIOD,
		APPLICATION,
		MUTE,
		VOLUMEUP,
		VOLUMEDOWN,
		LCTRL,
		LSHIFT,
		LALT,
		LGUI,
		RCTRL,
		RSHIFT,
		RALT,
		RGUI,
	};

	struct key_event
	{
		keycode key;
		bool down;
	};

	struct text_input_event
	{
		std::string input;
	};

}
