#pragma once

#include <psemek/app/application_base.hpp>
#include <psemek/app/scene.hpp>

namespace psemek::app
{

	struct scene_application
		: application_base
	{
		void on_event(resize_event const &) override;
		void on_event(focus_event const &) override;
		void on_event(mouse_move_event const &) override;
		void on_event(mouse_wheel_event const &) override;
		void on_event(mouse_button_event const &) override;
		void on_event(touch_down_event const &) override;
		void on_event(touch_up_event const &) override;
		void on_event(touch_move_event const &) override;
		void on_event(key_event const &) override;

		void stop() override;

		void update() override;
		void present() override;

		virtual std::shared_ptr<scene> current_scene() = 0;

	private:
		template <typename Event>
		void on_event_impl(Event const & event);
	};

}
