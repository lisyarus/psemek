#pragma once

#include <psemek/app/event_handler.hpp>
#include <psemek/app/event_state.hpp>

namespace psemek::app
{

	struct scene
		: event_handler
	{
		virtual void on_enter(event_state const &) {}
		virtual void on_exit() {}

		virtual void update() {}
		virtual void present() {}
	};

}
