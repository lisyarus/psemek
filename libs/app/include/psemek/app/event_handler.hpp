#pragma once

#include <psemek/app/events.hpp>

namespace psemek::app
{

	struct event_handler
	{
		virtual void on_event(resize_event const &) {}
		virtual void on_event(focus_event const &) {}
		virtual void on_event(mouse_move_event const &) {}
		virtual void on_event(mouse_wheel_event const &) {}
		virtual void on_event(mouse_button_event const &) {}
		virtual void on_event(touch_down_event const &) {}
		virtual void on_event(touch_up_event const &) {}
		virtual void on_event(touch_move_event const &) {}
		virtual void on_event(key_event const &) {}
		virtual void on_event(text_input_event const &) {}

		virtual ~event_handler() {}
	};

}
