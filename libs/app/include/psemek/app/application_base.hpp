#pragma once

#include <psemek/app/application.hpp>
#include <psemek/app/event_state.hpp>

namespace psemek::app
{

	struct application_base
		: application
	{
		void on_event(resize_event const &) override;
		void on_event(focus_event const &) override;
		void on_event(mouse_move_event const &) override;
		void on_event(mouse_wheel_event const &) override;
		void on_event(mouse_button_event const &) override;
		void on_event(touch_down_event const &) override;
		void on_event(touch_up_event const &) override;
		void on_event(touch_move_event const &) override;
		void on_event(key_event const &) override;

		void stop() override;
		bool running() const override;

		event_state const & state() const { return state_; }

	private:
		bool running_ = true;
		event_state state_;
	};

}
