#pragma once

#include <psemek/ui/asymmetric.hpp>

namespace psemek::ui
{

	struct single_container
		: asymmetric
	{
		children_range children() const override { return children_; }

		virtual std::shared_ptr<element> set_child(std::shared_ptr<element> c);
		virtual std::shared_ptr<element> child() const { return child_; }

		~single_container() override;

	protected:
		std::shared_ptr<element> child_;
		element * children_[1]{nullptr};
	};

}
