#pragma once

#include <psemek/ui/element.hpp>

namespace psemek::ui
{

	struct asymmetric
		: element
	{
		virtual void set_width_first(bool value);
		virtual bool width_first() const { return width_first_; }

	protected:
		bool width_first_ = true;
	};

}
