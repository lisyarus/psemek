#pragma once

#include <psemek/gfx/color.hpp>
#include <psemek/gfx/texture.hpp>
#include <psemek/gfx/texture_view.hpp>
#include <psemek/math/box.hpp>
#include <psemek/math/simplex.hpp>

namespace psemek::ui
{

	namespace detail
	{

		enum class color_mode
		{
			mix,
			multiply,
		};

		// Have to define this struct outside of ui::painter to workaround
		//   https://gcc.gnu.org/bugzilla/show_bug.cgi?id=88165
		struct image_options
		{
			gfx::color_rgba color{0, 0, 0, 0};
			color_mode mode = color_mode::mix;
			float rotation{0.f};
		};

	}

	struct painter
	{
		virtual void draw_rect(math::box<float, 2> const & rect, gfx::color_rgba const & color) = 0;

		virtual void draw_triangle(math::triangle<math::point<float, 2>> const & tri, gfx::color_rgba const & color)
		{
			draw_triangle(tri, color, color, color);
		}

		virtual void draw_triangle(math::triangle<math::point<float, 2>> const & tri, gfx::color_rgba const & c0, gfx::color_rgba const & c1, gfx::color_rgba const & c2) = 0;

		using color_mode = detail::color_mode;
		using image_options = detail::image_options;

		virtual void draw_image(math::box<float, 2> const & rect, gfx::texture_view_2d const & tex, image_options const & opts = image_options{}) = 0;
		virtual void draw_msdf_glyph(math::box<float, 2> const & rect, gfx::texture_view_2d const & tex, float sdf_scale, image_options const & opts = image_options{}) = 0;

		virtual void begin_stencil() = 0;
		virtual void commit_stencil() = 0;
		virtual void end_stencil() = 0;

		virtual ~painter() {}
	};

}
