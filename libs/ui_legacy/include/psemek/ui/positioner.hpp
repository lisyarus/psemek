#pragma once

#include <psemek/ui/single_container.hpp>
#include <psemek/ui/box_shape.hpp>

namespace psemek::ui
{

	struct positioner
		: single_container
	{
		enum class x_align
		{
			left,
			center,
			right,
		};

		enum class y_align
		{
			top,
			center,
			bottom,
		};

		struct shape const & shape() const override;
		void reshape(math::box<float, 2> const & box) override;

		virtual void set_position(math::point<float, 2> const & p, x_align x, y_align y, bool clamp = true);

		bool transparent() const override { return true; }

		void draw(ui::painter &) const override {}

	private:
		box_shape shape_;

		math::point<float, 2> position_{0.f, 0.f};
		x_align x_;
		y_align y_;
		bool clamp_;
	};

}
