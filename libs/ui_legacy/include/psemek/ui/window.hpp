#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/label.hpp>

#include <functional>

namespace psemek::ui
{

	struct window
		: element
	{
		using on_close_callback = std::function<void()>;

		virtual label * caption() = 0;
		virtual std::shared_ptr<element> set_child(std::shared_ptr<element> c) = 0;
		virtual on_close_callback on_close() const = 0;
		virtual void on_close(on_close_callback callback) = 0;
		virtual void close() = 0;
	};

}
