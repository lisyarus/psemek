#pragma once

#include <psemek/ui/single_container.hpp>
#include <psemek/ui/label.hpp>
#include <psemek/ui/image_view.hpp>

#include <functional>

namespace psemek::ui
{

	struct button
		: single_container
	{
		struct label * label() const;

		struct image_view * icon() const;

		bool on_event(mouse_move const & e) override;
		bool on_event(mouse_click const & e) override;

		void update(float dt) override;

		void set_enabled(bool value) override;
		void set_hidden(bool value) override;

		using callback = std::function<void()>;

		void on_click(callback callback)
		{
			on_click_callback_ = std::move(callback);
		}

		void on_release(callback callback)
		{
			on_release_callback_ = std::move(callback);
		}

		virtual void set_repeat(float wait, float period);
		virtual void set_no_repeat();

	protected:
		enum class state_t
		{
			normal,
			mouseover,
			mousedown,
		};

		virtual state_t state() const { return state_; }

		virtual void on_state_changed(state_t old);
		virtual void post_on_click();
		virtual void post_on_release();

	private:
		state_t state_ = state_t::normal;

		callback on_click_callback_;
		callback on_release_callback_;

		struct repeat
		{
			float wait;
			float period;

			enum class state_t
			{
				idle,
				wait,
				repeat,
			} state = state_t::idle;

			float timer = 0.f;
		};

		std::optional<repeat> repeat_;
	};

}
