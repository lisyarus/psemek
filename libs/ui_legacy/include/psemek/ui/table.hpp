#pragma once

#include <psemek/ui/grid_layout.hpp>

namespace psemek::ui
{

	struct table
		: grid_layout
	{
		table();

		void reshape(math::box<float, 2> const & box) override;

		void set_size(std::size_t rows, std::size_t columns) override;

		virtual bool horizontal_border(std::size_t i) const;
		virtual void set_horizontal_border(std::size_t i, bool visible);

		virtual bool vertical_border(std::size_t i) const;
		virtual void set_vertical_border(std::size_t i, bool visible);

	protected:
		std::vector<float> line_x_;
		std::vector<float> line_y_;

	private:
		std::vector<bool> horizontal_border_;
		std::vector<bool> vertical_border_;
	};

}
