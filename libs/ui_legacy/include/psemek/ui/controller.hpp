#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/gfx/render_target.hpp>
#include <psemek/util/pimpl.hpp>
#include <psemek/util/function.hpp>

namespace psemek::ui
{

	struct controller
	{
		controller(async::event_loop * loop);
		~controller();

		async::event_loop * loop() const;

		std::shared_ptr<element> set_root(std::shared_ptr<element> r);
		element * root();

		void set_hint_delay(float seconds);
		void on_hint(util::function<void(element *)> callback);

		void set_post_draw_effect(util::function<void(element *, painter &)> effect);

		void reshape(math::box<float, 2> const & shape);

		bool event(mouse_move const & e);
		bool event(mouse_click const & e);
		bool event(mouse_wheel const & e);
		bool event(key_press const & e);
		bool event(text_input const & e);

		void update(float dt);

		void render(gfx::render_target const & rt);

	private:
		psemek_declare_pimpl
	};

}
