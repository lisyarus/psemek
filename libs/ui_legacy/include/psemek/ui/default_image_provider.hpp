#pragma once

#include <psemek/ui/image_provider.hpp>
#include <psemek/util/hstring.hpp>

#include <unordered_map>

namespace psemek::ui
{

	struct default_image_provider
		: image_provider
	{
		void put(std::string id, gfx::texture_view_2d view)
		{
			images_[std::move(id)] = view;
		}

		gfx::texture_view_2d get(std::string_view const & id) const override
		{
			if (auto it = images_.find(id); it != images_.end())
				return it->second;
			return {};
		}

	private:
		std::unordered_map<util::hstring, gfx::texture_view_2d> images_;
	};

}
