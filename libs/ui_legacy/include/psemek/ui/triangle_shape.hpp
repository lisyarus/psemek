#pragma once

#include <psemek/ui/shape.hpp>

#include <psemek/math/simplex.hpp>

namespace psemek::ui
{

	struct triangle_shape
		: shape
	{
		math::triangle<math::point<float, 2>> triangle{{{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}}};

		triangle_shape() = default;

		triangle_shape(math::triangle<math::point<float, 2>> triangle)
			: triangle{triangle}
		{}

		bool contains(math::point<float, 2> const & point) const override;
		math::box<float, 2> bbox() const override;
	};

}
