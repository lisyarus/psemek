#pragma once

#include <psemek/ui/shape.hpp>

namespace psemek::ui
{

	struct box_shape
		: shape
	{
		math::box<float, 2> box{{{0.f, 0.f}, {0.f, 0.f}}};

		box_shape() = default;

		box_shape(math::box<float, 2> box)
			: box{box}
		{}

		bool contains(math::point<float, 2> const & point) const override;
		math::box<float, 2> bbox() const override { return box; }
	};

}
