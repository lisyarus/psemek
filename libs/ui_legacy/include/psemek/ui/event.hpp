#pragma once

#include <SDL2/SDL_keycode.h>

#include <psemek/math/point.hpp>

#include <tuple>

namespace psemek::ui
{

	struct mouse_move
	{
		math::point<int, 2> position;
	};

	enum class mouse_button
	{
		left,
		middle,
		right,
	};

	struct mouse_click
	{
		mouse_button button;
		bool down;
	};

	struct mouse_wheel
	{
		int delta;
	};

	struct key_press
	{
		SDL_Keycode key;
		bool down;
	};

	struct text_input
	{
		std::string_view text;
	};

	using event_type_list = std::tuple<mouse_move, mouse_click, mouse_wheel, key_press, text_input>;

}
