#pragma once

#include <psemek/ui/element.hpp>

#include <functional>

namespace psemek::ui
{

	struct label;
	struct button;

	struct spinbox
		: element
	{
		virtual int value() const { return value_; }
		virtual void set_value(int v, bool notify = true);

		virtual math::interval<int> value_range() const { return value_range_; }
		virtual void set_value_range(math::interval<int> i);

		virtual bool wrap() const { return wrap_; }
		virtual void set_wrap(bool w);

		virtual void inc();
		virtual void dec();

		using on_value_changed_callback = std::function<void(int)>;

		virtual void on_value_changed(on_value_changed_callback callback, bool notify = true);

		virtual struct label * label() const { return nullptr; }
		virtual button * dec_button() const { return nullptr; }
		virtual button * inc_button() const { return nullptr; }

	protected:
		virtual void post_value_changed();

	private:
		int value_ = 0;
		math::interval<int> value_range_ = math::interval<int>::full();
		bool wrap_ = false;
		on_value_changed_callback on_value_changed_;
	};

}
