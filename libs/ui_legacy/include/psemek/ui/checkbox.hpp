#pragma once

#include <psemek/ui/single_container.hpp>

#include <functional>

namespace psemek::ui
{

	struct checkbox
		: single_container
	{
		struct label * label() const;

		struct image_view * icon() const;

		bool on_event(mouse_move const & e) override;
		bool on_event(mouse_click const & e) override;

		bool value() const { return value_; }
		void set_value(bool value, bool signal = true);

		using on_value_changed_callback = std::function<void(bool)>;

		void on_value_changed(on_value_changed_callback callback)
		{
			callback_ = std::move(callback);
		}

	protected:
		enum class state_t
		{
			normal,
			mouseover,
			mousedown,
		};

		virtual state_t state() const { return state_; }

		virtual void on_state_changed(state_t old);

	private:
		state_t state_ = state_t::normal;
		bool value_ = false;

		on_value_changed_callback callback_;

		void post_value_changed();
	};

}
