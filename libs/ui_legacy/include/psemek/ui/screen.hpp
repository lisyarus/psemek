#pragma once

#include <psemek/ui/container.hpp>
#include <psemek/ui/container_impl.hpp>
#include <psemek/ui/box_shape.hpp>

namespace psemek::ui
{

	struct screen
		: container
	{
		enum class x_policy
		{
			left,
			center,
			right,
			fill,
			floating,
		};

		enum class y_policy
		{
			top,
			center,
			bottom,
			fill,
			floating,
		};

		screen();

		children_range children() const override;

		bool add_child(std::shared_ptr<element> c, x_policy x, y_policy y);
		bool add_child(std::shared_ptr<element> c) override;
		bool has_child(element * c) const override;
		std::shared_ptr<element> remove_child(element * c) override;
		bool move_to_top(element * c);

		struct shape const & shape() const override { return shape_; }
		void reshape(math::box<float, 2> const & bbox) override;

		math::box<float, 2> size_constraints() const override;

		bool transparent() const override { return !hint(); }

		void draw(painter &) const override {}

		~screen() override;

	private:
		container_impl container_;
		box_shape shape_;

		struct policy
		{
			x_policy x;
			y_policy y;
		};

		std::vector<policy> policies_;
	};

}
