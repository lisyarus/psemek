#pragma once

#include <psemek/ui/element.hpp>

#include <psemek/gfx/texture.hpp>
#include <psemek/gfx/texture_view.hpp>

namespace psemek::ui
{

	struct button;
	struct label;
	struct frame;
	struct window;
	struct screen;
	struct grid_layout;
	struct image_view;
	struct rich_image_view;
	struct checkbox;
	struct slider;
	struct spinbox;
	struct scroller;
	struct progress_bar;
	struct selector;
	struct table;
	struct edit;

	struct element_factory
	{
		virtual std::shared_ptr<button> make_button();
		virtual std::shared_ptr<button> make_button(std::string text);
		virtual std::shared_ptr<button> make_button(gfx::texture_view_2d icon);
		virtual std::shared_ptr<label> make_label(std::string text, bool tagged = false);
		virtual std::shared_ptr<frame> make_frame();
		virtual std::shared_ptr<window> make_window(std::string caption);
		virtual std::shared_ptr<screen> make_screen();
		virtual std::shared_ptr<grid_layout> make_grid_layout();
		virtual std::shared_ptr<image_view> make_image_view(gfx::texture_view_2d image);
		virtual std::shared_ptr<rich_image_view> make_rich_image_view(std::shared_ptr<gfx::texture_2d> image);
		virtual std::shared_ptr<checkbox> make_checkbox(bool value);
		virtual std::shared_ptr<checkbox> make_toggle_button();
		virtual std::shared_ptr<checkbox> make_toggle_button(std::string text);
		virtual std::shared_ptr<checkbox> make_toggle_button(gfx::texture_view_2d icon);
		virtual std::shared_ptr<slider> make_slider();
		virtual std::shared_ptr<spinbox> make_spinbox();
		// directions:
		// 0 - up
		// 1 - down
		// 2 - left
		// 3 - right
		virtual std::shared_ptr<button> make_arrow_button(int direction);
		virtual std::shared_ptr<scroller> make_scroller();
		virtual std::shared_ptr<progress_bar> make_progress_bar();
		virtual std::shared_ptr<progress_bar> make_progress_bar(std::string text);
		virtual std::shared_ptr<selector> make_selector();
		virtual std::shared_ptr<table> make_table();
		virtual std::shared_ptr<edit> make_edit();

		virtual ~element_factory() {}
	};

}
