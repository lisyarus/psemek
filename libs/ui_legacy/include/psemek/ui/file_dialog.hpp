#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/window.hpp>
#include <psemek/ui/element_factory.hpp>

#include <filesystem>
#include <functional>

namespace psemek::ui
{

	enum class file_dialog_type
	{
		save,
		load
	};

	struct file_dialog_options
	{
		struct element_factory & element_factory;
		file_dialog_type type;
		std::string caption;
		std::filesystem::path path;
		std::function<void(std::filesystem::path const &)> on_visited = nullptr;
		std::function<void(std::filesystem::path const &)> on_selected = nullptr;
		std::function<void()> on_canceled = nullptr;
		std::shared_ptr<ui::element> extra_widget = nullptr;
	};

	std::shared_ptr<window> make_file_dialog(file_dialog_options const & options);

}
