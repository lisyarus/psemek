#pragma once

#include <psemek/ui/single_container.hpp>

#include <functional>

namespace psemek::ui
{

	struct event_interceptor
		: single_container
	{
		math::box<float, 2> size_constraints() const override;
		math::interval<float> width_constraints(float height) const override;
		math::interval<float> height_constraints(float width) const override;

		struct shape const & shape() const override;
		void reshape(math::box<float, 2> const & bbox) override;

		void on_mouse_move(std::function<bool(mouse_move const &)> callback);
		void on_mouse_click(std::function<bool(mouse_click const &)> callback);
		void on_mouse_wheel(std::function<bool(mouse_wheel const &)> callback);
		void on_key_press(std::function<bool(key_press const &)> callback);
		void on_update(std::function<void(float)> callback);

		bool on_event(mouse_move  const & event) override;
		bool on_event(mouse_click const & event) override;
		bool on_event(mouse_wheel const & event) override;
		bool on_event(key_press   const & event) override;

		bool transparent() const override { return true; }

		void update(float dt) override;

		void draw(painter &) const override {}

	private:
		bool mouseover_ = false;

		std::function<bool(mouse_move const &)> mouse_move_callback_;
		std::function<bool(mouse_click const &)> mouse_click_callback_;
		std::function<bool(mouse_wheel const &)> mouse_wheel_callback_;
		std::function<bool(key_press const &)> key_press_callback_;
		std::function<void(float)> update_callback_;
	};

}
