#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/box_shape.hpp>

#include <functional>

namespace psemek::ui
{

	struct edit
		: element
	{
		enum font_type
		{
			normal,
			bold,
		};

		enum class halignment
		{
			left,
			center,
			right,
		};

		enum class valignment
		{
			top,
			center,
			bottom,
		};

		virtual bool set_text(std::string_view text, bool signal = true);
		virtual bool set_text(std::u32string text, bool signal = true);
		virtual std::u32string_view text() const { return text_; }

		virtual void set_font(font_type f);
		virtual font_type font() const { return font_; }

		virtual void set_halign(halignment value);
		virtual halignment halign() const { return halign_; }

		virtual void set_valign(valignment value);
		virtual valignment valign() const { return valign_; }

		using validator_type = std::function<bool(std::u32string_view)>;
		virtual bool set_validator(validator_type validator);
		static validator_type numeric_nonnegative(int max_length);
		static validator_type numeric(int max_length);

		using callback_type = std::function<void(std::u32string_view)>;
		virtual void on_text_entered(callback_type callback, bool signal = true);

		virtual void on_start_input(std::function<void()> callback);
		virtual void on_text_changed(callback_type callback);

		bool on_event(mouse_move const & e) override;
		bool on_event(mouse_click const & e) override;
		bool on_event(key_press const & e) override;
		bool on_event(text_input const & e) override;

		bool focused() const override;
		virtual bool editing() const;

		struct shape const & shape() const override { return shape_; }
		void reshape(math::box<float, 2> const & bbox) override;

		math::box<float, 2> size_constraints() const override;

		void update(float dt) override;

		void draw(painter & p) const override;

	protected:

		void set_text_shape(math::box<float, 2> const & box);

	private:
		std::u32string text_;
		std::u32string old_text_;
		font_type font_ = font_type::normal;
		halignment halign_ = halignment::left;
		valignment valign_ = valignment::top;

		bool editing_ = false;
		bool mouseover_ = false;
		std::optional<float> mouse_x_;
		bool ctrl_down_ = false;

		std::size_t caret_ = 0;
		float caret_blink_period_ = 0.5f;
		float caret_blink_timer_ = 0.f;
		bool caret_visible_ = true;

		box_shape shape_;
		math::box<float, 2> text_box_;

		validator_type validator_;

		callback_type on_text_entered_;
		std::function<void()> on_start_input_;
		callback_type on_text_changed_;

		struct cached_state
		{
			struct image
			{
				math::box<float, 2> texcoords;
				math::box<float, 2> position;
			};

			gfx::texture_2d const * texture = nullptr;
			std::vector<image> images;
			math::vector<float, 2> size{0.f, 0.f};
		};

		mutable std::optional<cached_state> cached_state_;

		void on_state_changed();
		void reset_caret();
		void post_text_entered() const;
		void post_start_input() const;
		void post_text_changed() const;
	};

}
