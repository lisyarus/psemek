#pragma once

#include <psemek/ui/element.hpp>

namespace psemek::ui
{

	bool spawn(element * root, std::shared_ptr<element> element, math::point<float, 2> const & position);

}
