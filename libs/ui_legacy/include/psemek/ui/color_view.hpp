#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/box_shape.hpp>

namespace psemek::ui
{

	struct color_view
		: ui::element
	{
		virtual gfx::color_rgba color() const { return color_; }
		virtual void set_color(gfx::color_rgba value) { color_ = value; }

		virtual bool square() const { return square_; }
		virtual void set_square(bool value) { square_ = value; }

		struct shape const & shape() const override { return shape_; }
		void reshape(math::box<float, 2> const & box) override { shape_.box = box; }

		math::interval<float> width_constraints(float height) const override;
		math::interval<float> height_constraints(float width) const override;

		void draw(painter & p) const override;

	private:
		box_shape shape_;
		gfx::color_rgba color_ = {255, 0, 0, 255};
		bool square_ = true;
	};

}
