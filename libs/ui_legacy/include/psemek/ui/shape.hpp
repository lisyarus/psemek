#pragma once

#include <psemek/math/point.hpp>
#include <psemek/math/box.hpp>

namespace psemek::ui
{

	struct shape
	{
		virtual bool contains(math::point<float, 2> const & point) const = 0;
		virtual math::box<float, 2> bbox() const = 0;

		virtual ~shape() {}
	};

}
