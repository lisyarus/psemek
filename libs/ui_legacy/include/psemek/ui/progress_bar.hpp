#pragma once

#include <psemek/ui/single_container.hpp>
#include <psemek/ui/label.hpp>

namespace psemek::ui
{

	struct progress_bar
		: single_container
	{
		virtual float value() const { return value_; }
		virtual void set_value(float value);

		virtual struct label * label() const;

	private:
		float value_ = 0.f;
	};

}
