#pragma once

#include <psemek/ui/asymmetric.hpp>

namespace psemek::ui
{

	struct container
		: asymmetric
	{
		virtual bool add_child(std::shared_ptr<element> c) = 0;
		virtual bool has_child(element * c) const = 0;
		virtual std::shared_ptr<element> remove_child(element * c) = 0;
	};

}
