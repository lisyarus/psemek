#pragma once

#include <psemek/ui/element.hpp>

#include <vector>

namespace psemek::ui
{

	struct container_impl
	{
		container_impl(element * parent)
			: parent_{parent}
		{}

		element::children_range children() const;

		bool empty() const;
		std::size_t size() const;
		void resize(std::size_t new_size);

		std::size_t add(std::shared_ptr<element> c);
		void add(std::shared_ptr<element> c, std::size_t index);

		std::shared_ptr<element> get(std::size_t index) const;
		std::optional<std::size_t> find(element * c) const;

		std::shared_ptr<element> remove(element * c);
		std::shared_ptr<element> remove(std::size_t index);

		void clear();

	protected:
		element * parent_;
		std::vector<std::shared_ptr<element>> children_;
		std::vector<element *> children_range_;
	};

}
