#pragma once

#include <psemek/ui/element_factory.hpp>

#include <psemek/util/pimpl.hpp>
#include <psemek/gfx/pixmap.hpp>

namespace psemek::ui
{

	struct default_element_factory
		: element_factory
	{
		default_element_factory();
		~default_element_factory();

		void set_close_icon(gfx::pixmap_rgba const & icon);

		std::shared_ptr<button> make_button() override;
		using element_factory::make_button;
		std::shared_ptr<frame> make_frame() override;
		std::shared_ptr<window> make_window(std::string caption) override;
		std::shared_ptr<checkbox> make_checkbox(bool value) override;
		std::shared_ptr<checkbox> make_toggle_button() override;
		std::shared_ptr<slider> make_slider() override;
		std::shared_ptr<spinbox> make_spinbox() override;
		std::shared_ptr<button> make_arrow_button(int direction) override;
		std::shared_ptr<scroller> make_scroller() override;
		std::shared_ptr<selector> make_selector() override;
		std::shared_ptr<edit> make_edit() override;

	private:
		psemek_declare_pimpl
	};

}
