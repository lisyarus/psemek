#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/box_shape.hpp>

#include <psemek/gfx/texture_view.hpp>

namespace psemek::ui
{

	struct image_view
		: element
	{
		virtual bool upscale() const { return upscale_; }
		virtual void set_upscale(bool value);

		virtual bool downscale() const { return downscale_; }
		virtual void set_downscale(bool value);

		virtual bool keep_aspect_ratio() const { return keep_aspect_ratio_; }
		virtual void set_keep_aspect_ratio(bool value);

		virtual gfx::texture_view_2d image() const { return image_; }
		virtual void set_image(gfx::texture_view_2d image);

		virtual gfx::color_rgba const & color() const { return color_; }
		virtual void set_color(gfx::color_rgba const & c) { color_ = c; }

		struct shape const & shape() const override { return shape_; }
		void reshape(math::box<float, 2> const & bbox) override { shape_.box = bbox; }

		math::box<float, 2> size_constraints() const override;

		bool transparent() const override { return !hint(); }

		void draw(painter & p) const override;

	private:
		bool upscale_ = true;
		bool downscale_ = true;
		bool keep_aspect_ratio_ = true;
		gfx::texture_view_2d image_;
		gfx::color_rgba color_{0, 0, 0, 0};
		box_shape shape_;
	};

}
