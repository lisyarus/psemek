#pragma once

#include <psemek/gfx/texture_view.hpp>

#include <string_view>

namespace psemek::ui
{

	struct image_provider
	{
		virtual gfx::texture_view_2d get(std::string_view const & id) const = 0;

		virtual ~ image_provider() {}
	};

}
