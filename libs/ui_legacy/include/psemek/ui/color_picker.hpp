#pragma once

#include <psemek/ui/element.hpp>
#include <psemek/ui/single_container.hpp>
#include <psemek/ui/element_factory.hpp>

#include <functional>

namespace psemek::ui
{

	struct color_picker
		: single_container
	{
		virtual gfx::color_rgba color() const = 0;
		virtual void set_color(gfx::color_rgba value, bool notify = true) = 0;

		using on_value_changed_callback = std::function<void(gfx::color_rgba)>;

		virtual void on_value_changed(on_value_changed_callback callback) { on_value_changed_callback_ = callback; }

	protected:
		on_value_changed_callback on_value_changed_callback_;
	};

	std::shared_ptr<color_picker> make_color_picker(element_factory & factory, bool alpha = false);

}
