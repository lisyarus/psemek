#pragma once

#include <psemek/ui/painter.hpp>

#include <psemek/util/pimpl.hpp>

#include <psemek/math/matrix.hpp>

namespace psemek::ui
{

	struct painter_impl
		: painter
	{
		painter_impl();
		~painter_impl();

		void draw_rect(math::box<float, 2> const & rect, gfx::color_rgba const & color) override;
		void draw_triangle(math::triangle<math::point<float, 2>> const & tri, gfx::color_rgba const & c0, gfx::color_rgba const & c1, gfx::color_rgba const & c2) override;

		void draw_image(math::box<float, 2> const & rect, gfx::texture_view_2d const & tex, image_options const & opts) override;
		void draw_msdf_glyph(math::box<float, 2> const & rect, gfx::texture_view_2d const & tex, float sdf_scale, image_options const & opts) override;

		void begin_stencil() override;
		void commit_stencil() override;
		void end_stencil() override;

		void start_frame(math::box<float, 2> const & bbox);
		void render(math::matrix<float, 4, 4> const & transform);
		math::box<float, 2> current_bbox() const;

	private:
		psemek_declare_pimpl
	};

}
