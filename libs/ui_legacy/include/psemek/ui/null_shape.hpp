#pragma once

#include <psemek/ui/shape.hpp>

namespace psemek::ui
{

	struct null_shape
		: shape
	{
		bool contains(math::point<float, 2> const & point) const override;
		math::box<float, 2> bbox() const override;
	};

}
