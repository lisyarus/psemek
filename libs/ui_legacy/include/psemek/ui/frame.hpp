#pragma once

#include <psemek/ui/single_container.hpp>

namespace psemek::ui
{

	struct frame
		: single_container
	{
		virtual void set_min_size(std::optional<math::vector<float, 2>> const & size);
		virtual void set_max_size(std::optional<math::vector<float, 2>> const & size);
		virtual void set_fixed_size(math::vector<float, 2> const & size);

		virtual std::optional<math::vector<float, 2>> min_size() const { return min_size_; }
		virtual std::optional<math::vector<float, 2>> max_size() const { return max_size_; }

		struct shape const & shape() const override;
		void reshape(math::box<float, 2> const & box) override;
		math::box<float, 2> size_constraints() const override;
		math::interval<float> width_constraints(float height) const override;
		math::interval<float> height_constraints(float width) const override;

		void draw(painter &) const override {}

	private:
		std::optional<math::vector<float, 2>> min_size_;
		std::optional<math::vector<float, 2>> max_size_;
	};

}
