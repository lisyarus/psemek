#include <psemek/ui/null_shape.hpp>

namespace psemek::ui
{

	bool null_shape::contains(math::point<float, 2> const &) const
	{
		return false;
	}

	math::box<float, 2> null_shape::bbox() const
	{
		return math::box<float, 2>{{{0.f, 0.f}, {0.f, 0.f}}};
	}

}
