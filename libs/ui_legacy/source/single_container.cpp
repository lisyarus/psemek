#include <psemek/ui/single_container.hpp>

namespace psemek::ui
{

	std::shared_ptr<element> single_container::set_child(std::shared_ptr<element> c)
	{
		auto old = std::move(child_);
		if (old) old->set_parent(nullptr);

		child_ = std::move(c);
		if (child_) child_->set_parent(this);
		children_[0] = child_.get();

		post_reshape();

		return old;
	}

	single_container::~single_container()
	{
		release_children();
	}

}
