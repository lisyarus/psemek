#include <psemek/ui/table.hpp>

namespace psemek::ui
{

	table::table()
	{
		set_size(0, 0);
	}

	void table::reshape(math::box<float, 2> const & box)
	{
		grid_layout::reshape(box);

		auto st = merged_own_style();

		auto offset = *(st->outer_margin) / 2.f;

		line_y_.resize(row_count() + 1);
		line_x_.resize(column_count() + 1);

		if (row_count() == 0)
			line_y_[0] = box[1].center();
		else for (std::size_t r = 0; r <= row_count(); ++r)
		{
			if (r == 0)
				line_y_[r] = row_shape_[0].min - offset;
			else if (r == row_count())
				line_y_[r] = row_shape_[r - 1].max + offset;
			else
				line_y_[r] = (row_shape_[r - 1].max + row_shape_[r].min) / 2.f;
			line_y_[r] = std::round(line_y_[r]);
		}

		if (column_count() == 0)
			line_x_[0] = box[0].center();
		else for (std::size_t c = 0; c <= column_count(); ++c)
		{
			if (c == 0)
				line_x_[c] = column_shape_[0].min - offset;
			else if (c == column_count())
				line_x_[c] = column_shape_[c - 1].max + offset;
			else
				line_x_[c] = (column_shape_[c - 1].max + column_shape_[c].min) / 2.f;
			line_x_[c] = std::round(line_x_[c]);
		}
	}

	void table::set_size(std::size_t rows, std::size_t columns)
	{
		grid_layout::set_size(rows, columns);

		horizontal_border_.resize(rows + 1, true);
		vertical_border_.resize(columns + 1, true);
	}

	bool table::horizontal_border(std::size_t i) const
	{
		return horizontal_border_[i];
	}

	void table::set_horizontal_border(std::size_t i, bool visible)
	{
		horizontal_border_[i] = visible;
	}

	bool table::vertical_border(std::size_t i) const
	{
		return vertical_border_[i];
	}

	void table::set_vertical_border(std::size_t i, bool visible)
	{
		vertical_border_[i] = visible;
	}

}
