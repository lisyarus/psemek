#include <psemek/ui/default_element_factory.hpp>

#include <psemek/ui/button.hpp>
#include <psemek/ui/frame.hpp>
#include <psemek/ui/window.hpp>
#include <psemek/ui/checkbox.hpp>
#include <psemek/ui/slider.hpp>
#include <psemek/ui/spinbox.hpp>
#include <psemek/ui/box_shape.hpp>
#include <psemek/ui/container.hpp>
#include <psemek/ui/grid_layout.hpp>
#include <psemek/ui/scroller.hpp>
#include <psemek/ui/selector.hpp>
#include <psemek/ui/edit.hpp>

#include <psemek/io/memory_stream.hpp>

#include <psemek/ui/resources/cross_red_16x16_png.hpp>

namespace psemek::ui
{

	namespace
	{

		struct button_impl
			: button
		{
			struct shape const & shape() const override
			{
				return shape_;
			}

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = bbox;
				auto st = merged_own_style();

				math::vector offset{0.f, 0.f};
				if (state() == state_t::mousedown)
					offset = math::cast<float>(*st->action_offset);

				auto c = child_.get();
				if (c) c->reshape(math::shrink(bbox, math::vector<float, 2>{*st->border_width + (*st->inner_margin)[0], *st->border_width + (*st->inner_margin)[1]}) + offset);
			}

			void on_state_changed(state_t old) override
			{
				button::on_state_changed(old);

				gfx::color_rgba color{0, 0, 0, 0};

				switch (state()) {
				case state_t::mouseover:
					color = {255, 255, 255, 63};
					break;
				case state_t::mousedown:
					color = {0, 0, 0, 63};
					break;
				default:
					break;
				}

				if (icon())
					icon()->set_color(color);
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();
				if (!st) return;

				if (st->shadow_offset != math::vector{0, 0})
					p.draw_rect(shape_.box + math::cast<float>(*st->shadow_offset), *st->shadow_color);

				auto offset = math::vector{0.f, 0.f};
				if (state() == state_t::mousedown)
					offset = math::cast<float>(*st->action_offset);

				if (st->border_width > 0)
					p.draw_rect(shape_.box + offset, *st->border_color);

				gfx::color_rgba color = *st->fg_color;
				if (state() == state_t::mouseover)
					color = *st->highlight_color;
				else if (state() == state_t::mousedown)
					color = *st->action_color;
				p.draw_rect(math::shrink(shape_.box, 1.f * (*st->border_width)) + offset, color);
			}

			math::box<float, 2> size_constraints() const override
			{
				auto sc = element::size_constraints();

				if (child_)
				{
					auto csc = child_->size_constraints();
					sc[0].min = csc[0].min;
					sc[1].min = csc[1].min;
				}

				if (auto st = merged_own_style())
				{
					sc[0] += 2.f * (*st->border_width);
					sc[1] += 2.f * (*st->border_width);

					sc += 2.f * math::cast<float>(*st->inner_margin);
				}

				return sc;
			}

			math::interval<float> width_constraints(float height) const override
			{
				auto wc = element::width_constraints(height);

				auto st = merged_own_style();

				float extra = 2.f * (*st->border_width) + 2.f * math::cast<float>(*st->inner_margin)[0];

				if (child_)
				{
					auto cwc = child_->width_constraints(height - extra);
					wc.min = cwc.min;
				}

				wc += extra;

				return wc;
			}

			math::interval<float> height_constraints(float width) const override
			{
				auto hc = element::height_constraints(width);

				auto st = merged_own_style();

				float extra = 2.f * (*st->border_width) + 2.f * math::cast<float>(*st->inner_margin)[1];

				if (child_)
				{
					auto chc = child_->height_constraints(width - extra);
					hc.min = chc.min;
				}

				hc += extra;

				return hc;
			}

		private:
			box_shape shape_;
		};

		struct frame_impl
			: frame
		{
			struct shape const & shape() const override { return shape_; }

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = bbox;
				auto st = merged_own_style();
				if (!st) return;
				for (auto c : children())
					if (c) c->reshape(math::shrink(bbox, 1.f * (*st->border_width + *st->bevel_width + *st->outer_margin)));
			}

			math::box<float, 2> size_constraints() const override
			{
				math::box<float, 2> sc = frame::size_constraints();

				auto st = merged_own_style();
				if (st)
				{
					float extra = 2.f * (*st->border_width + *st->bevel_width + *st->outer_margin);
					sc[0] += extra;
					sc[1] += extra;
				}

				return sc;
			}

			math::interval<float> width_constraints(float height) const override
			{
				auto wc = frame::width_constraints(height);

				auto st = merged_own_style();
				if (st)
				{
					float extra = 2.f * (*st->border_width + *st->bevel_width + *st->outer_margin);
					wc += extra;
				}

				return wc;
			}

			math::interval<float> height_constraints(float width) const override
			{
				auto hc = frame::height_constraints(width);

				auto st = merged_own_style();
				if (st)
				{
					float extra = 2.f * (*st->border_width + *st->bevel_width + *st->outer_margin);
					hc += extra;
				}

				return hc;
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();
				if (!st) return;

				if (st->shadow_offset != math::vector{0, 0})
					p.draw_rect(shape_.box + math::cast<float>(*st->shadow_offset), *st->shadow_color);

				if (st->border_width > 0)
					p.draw_rect(shape_.box, *st->border_color);

				if (st->bevel_width > 0)
				{
					math::point<float, 2> corners[4];
					math::point<float, 2> corners_in[4];

					auto box = math::shrink<float>(shape_.box, *st->border_width);

					corners[0] = box.corner(0, 0);
					corners[1] = box.corner(1, 0);
					corners[2] = box.corner(1, 1);
					corners[3] = box.corner(0, 1);

					auto box_in = math::shrink<float>(box, *st->bevel_width);

					corners_in[0] = box_in.corner(0, 0);
					corners_in[1] = box_in.corner(1, 0);
					corners_in[2] = box_in.corner(1, 1);
					corners_in[3] = box_in.corner(0, 1);

					auto c1 = *st->bevel_light_color;
					auto c2 = *st->bevel_dark_color;

					if (*st->bevel_type == bevel_type::down)
						std::swap(c1, c2);

					p.draw_triangle({corners[0], corners_in[0], corners[1]}, c1);
					p.draw_triangle({corners[1], corners_in[0], corners_in[1]}, c1);
					p.draw_triangle({corners[1], corners_in[1], corners[2]}, c1);
					p.draw_triangle({corners[2], corners_in[1], corners_in[2]}, c1);

					p.draw_triangle({corners[2], corners_in[2], corners[3]}, c2);
					p.draw_triangle({corners[3], corners_in[2], corners_in[3]}, c2);
					p.draw_triangle({corners[3], corners_in[3], corners[0]}, c2);
					p.draw_triangle({corners[0], corners_in[3], corners_in[0]}, c2);
				}

				p.draw_rect(math::shrink(shape_.box, 1.f * (*st->border_width + *st->bevel_width)), *st->bg_color);
			}

		private:
			box_shape shape_;
		};

		struct window_impl
			: window
		{
			window_impl(std::shared_ptr<gfx::texture_2d> close_icon)
				: close_texture_(std::move(close_icon))
				, caption_(std::make_shared<label>())
				, close_button_(std::make_shared<button_impl>())
			{
				close_button_->set_child(std::make_shared<image_view>());
				close_button_->icon()->set_image({close_texture_.get()});
				close_button_->icon()->set_downscale(false);

				caption_->set_parent(this);
				close_button_->set_parent(this);

				caption_->set_valign(label::valignment::center);
				caption_->set_overflow(label::overflow_mode::ellipsis);

				auto close_button_style = std::make_shared<struct style>();
				close_button_style->shadow_offset = {0, 0};
				close_button_->set_style(close_button_style);

				children_[0] = caption_.get();
				children_[1] = close_button_.get();
				children_[2] = nullptr;

				on_close_ = [this]
				{
					if (!parent()) return;

					if (auto p = dynamic_cast<container *>(parent()))
						p->remove_child(this);
					else
						throw std::runtime_error("Cannot remove window from non-container parent");
				};
			}

			void setup_close()
			{
				auto weak_self = weak_from_this();
				close_button_->on_click([weak_self]{
					if (auto self = std::dynamic_pointer_cast<window_impl>(weak_self.lock()))
					{
						if (self->on_close_)
							self->on_close_();
					}
				});
			}

			label * caption() override
			{
				return caption_.get();
			}

			std::shared_ptr<element> set_child(std::shared_ptr<element> c) override
			{
				if (child_) child_->set_parent(nullptr);
				std::swap(child_, c);
				if (child_) child_->set_parent(this);
				children_[2] = child_.get();
				return c;
			}

			children_range children() const override
			{
				return children_range{children_};
			}

			on_close_callback on_close() const override
			{
				return on_close_;
			}

			void on_close(on_close_callback callback) override
			{
				on_close_ = std::move(callback);
			}

			void close() override
			{
				if (on_close_)
					on_close_();
			}

			bool on_event(mouse_move  const & e) override
			{
				mouse_ = e.position;

				if (drag_ && *drag_ != e.position)
				{
					shape_.box += math::cast<float>(e.position - *drag_);
					drag_ = e.position;
					post_reshape();
				}

				return shape().contains(math::cast<float>(*mouse_));
			}

			bool on_event(mouse_click const & e) override
			{
				if (!mouse_)
					return false;

				if (e.button == mouse_button::left && mouse_)
				{
					if (e.down && shape().contains(math::cast<float>(*mouse_)))
					{
						drag_ = *mouse_;
						return true;
					}

					if (!e.down && drag_)
					{
						drag_ = std::nullopt;
						return true;
					}
				}

				if (e.button == mouse_button::left && mouse_)
				{
					if (shape().contains(math::cast<float>(*mouse_)))
						return true;
				}

				return false;
			}

			bool on_event(mouse_wheel const &) override
			{
				return mouse_ && shape().contains(math::cast<float>(*mouse_));
			}

			struct shape const & shape() const override
			{
				return shape_;
			}

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = bbox;

				auto lc = caption_->size_constraints();
				auto bc = close_button_->size_constraints();

				auto st = merged_own_style();

				float const header_height = std::max(lc[1].min + 2.f * (*st->inner_margin)[1] + 2.f * (*st->border_width), bc[1].min);

				{
					math::box<float, 2> b;
					b[0].min = bbox[0].max - bc[0].min;
					b[0].max = bbox[0].max;
					b[1].min = bbox[1].min;
					b[1].max = bbox[1].min + header_height;
					close_button_->reshape(b);
				}

				{
					math::box<float, 2> b;
					b[0].min = bbox[0].min + (*st->inner_margin)[0] + (*st->border_width);
					b[0].max = bbox[0].max - (*st->inner_margin)[0] - bc[0].min;
					b[1].min = bbox[1].min + (*st->inner_margin)[1] + (*st->border_width);
					b[1].max = bbox[1].min + header_height - (*st->inner_margin)[1] - (*st->border_width);
					caption_->reshape(b);
				}

				if (child_)
				{
					math::box<float, 2> b;
					b[0].min = bbox[0].min + (*st->border_width);
					b[0].max = bbox[0].max - (*st->border_width);
					b[1].min = bbox[1].min + header_height;
					b[1].max = bbox[1].max - (*st->border_width);
					child_->reshape(b);
				}
			}

			math::box<float, 2> size_constraints() const override
			{
				math::box<float, 2> r = element::size_constraints();
				if (child_) r = child_->size_constraints();

				auto lc = caption_->size_constraints();
				auto bc = close_button_->size_constraints();

				auto st = merged_own_style();

				float const header_height = std::max(lc[1].min + 2.f * (*st->inner_margin)[1] + 2.f * (*st->border_width), bc[1].min);

				r[0].min = std::max(r[0].min + 2.f * (*st->inner_margin)[0] + 2.f * (*st->border_width), (*st->border_width) + lc[0].min + bc[0].min);
				r[1] += (*st->border_width) * 1.f + header_height;

				return r;
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();

				auto lc = caption_->size_constraints();
				auto bc = close_button_->size_constraints();

				auto const & bb = shape_.box;

				if (*st->shadow_offset != math::vector{0, 0})
					p.draw_rect(bb + math::cast<float>(*st->shadow_offset), *st->shadow_color);
				p.draw_rect(bb, *st->border_color);

				float const header_height = std::max(lc[1].min + 2.f * (*st->inner_margin)[1] + 2.f * (*st->border_width), bc[1].min);
				p.draw_rect({{{bb[0].min + (*st->border_width), bb[0].max - bc[0].min}, {bb[1].min + (*st->border_width), bb[1].min + header_height - (*st->border_width)}}},
					*st->fg_color);
				p.draw_rect({{{bb[0].min + (*st->border_width), bb[0].max - (*st->border_width)}, {bb[1].min + header_height, bb[1].max - (*st->border_width)}}},
					*st->bg_color);
			}

		private:
			std::shared_ptr<gfx::texture_2d> close_texture_;
			std::shared_ptr<label> caption_;
			std::shared_ptr<button> close_button_;
			std::shared_ptr<element> child_;

			on_close_callback on_close_;

			std::optional<math::point<int, 2>> mouse_;
			std::optional<math::point<int, 2>> drag_;

			box_shape shape_;

			element * children_[3];
		};

		struct checkbox_impl
			: checkbox
		{
			std::shared_ptr<element> set_child(std::shared_ptr<element>) override
			{
				throw std::runtime_error("default checkbox doesn't support children");
			}

			struct shape const & shape() const override
			{
				return shape_;
			}

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = math::expand(math::box<float, 2>::singleton(bbox.center()), size() / 2.f);
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();
				if (!st) return;

				math::box<float, 2> boxes[5];

				boxes[0] = {{shape_.box[0], {shape_.box[1].min, shape_.box[1].min + outer_width()}}};
				boxes[1] = {{shape_.box[0], {shape_.box[1].max - outer_width(), shape_.box[1].max}}};
				boxes[2] = {{{shape_.box[0].min, shape_.box[0].min + outer_width()}, {shape_.box[1].min + outer_width(), shape_.box[1].max - outer_width()}}};
				boxes[3] = {{{shape_.box[0].max - outer_width(), shape_.box[0].max}, {shape_.box[1].min + outer_width(), shape_.box[1].max - outer_width()}}};
				boxes[4] = math::shrink(shape_.box, 0.f + outer_width() + outer_margin());

				int const box_count = value() ? 5 : 4;

				if (st->shadow_offset != math::vector{0, 0})
				{
					auto const offset = math::cast<float>(*st->shadow_offset);
					for (int b = 0; b < box_count; ++b)
						p.draw_rect(boxes[b] + offset, *st->shadow_color);
				}

				auto offset = math::vector{0.f, 0.f};
				if (state() == state_t::mousedown)
					offset = math::cast<float>(*st->action_offset);

				gfx::color_rgba color = *st->fg_color;
				if (state() == state_t::mouseover)
					color = *st->highlight_color;
				else if (state() == state_t::mousedown)
					color = *st->action_color;

				for (int b = 0; b < box_count; ++b)
					p.draw_rect(boxes[b] + offset, color);
			}

			math::box<float, 2> size_constraints() const override
			{
				auto sc = element::size_constraints();

				sc[0] &= math::interval<float>{size(), std::numeric_limits<float>::infinity()};
				sc[1] &= math::interval<float>{size(), std::numeric_limits<float>::infinity()};

				return sc;
			}

		private:
			box_shape shape_;

			int size() const
			{
				return *merged_own_style()->ref_height;
			}

			int outer_width() const
			{
				return *merged_own_style()->ref_height / 8;
			}

			int outer_margin() const
			{
				return *merged_own_style()->ref_height / 8;
			}
		};

		struct toggle_button_impl
			: checkbox
		{
			struct shape const & shape() const override
			{
				return shape_;
			}

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = bbox;
				auto st = merged_own_style();

				math::vector offset{0.f, 0.f};
				if (state() == state_t::mousedown || value())
					offset = math::cast<float>(*st->action_offset);

				auto c = child_.get();
				if (c) c->reshape(math::shrink(bbox, math::vector<float, 2>{*st->border_width + (*st->inner_margin)[0], *st->border_width + (*st->inner_margin)[1]}) + offset);
			}

			void on_state_changed(state_t old) override
			{
				checkbox::on_state_changed(old);

				gfx::color_rgba color{0, 0, 0, 0};

				switch (state()) {
				case state_t::mouseover:
					color = {255, 255, 255, 63};
					break;
				case state_t::mousedown:
					color = {0, 0, 0, 63};
					break;
				default:
					break;
				}

				if (value())
					color = {0, 0, 0, 63};

				if (icon())
					icon()->set_color(color);
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();
				if (!st) return;

				if (st->shadow_offset != math::vector{0, 0})
					p.draw_rect(shape_.box + math::cast<float>(*st->shadow_offset), *st->shadow_color);

				auto offset = math::vector{0.f, 0.f};
				if (state() == state_t::mousedown || value())
					offset = math::cast<float>(*st->action_offset);

				if (st->border_width > 0)
					p.draw_rect(shape_.box + offset, *st->border_color);

				gfx::color_rgba color = *st->fg_color;
				if (state() == state_t::mouseover)
					color = *st->highlight_color;
				else if (state() == state_t::mousedown || value())
					color = *st->action_color;
				p.draw_rect(math::shrink(shape_.box, 1.f * (*st->border_width)) + offset, color);
			}

			math::box<float, 2> size_constraints() const override
			{
				auto sc = element::size_constraints();

				if (child_)
				{
					auto csc = child_->size_constraints();
					sc[0].min = csc[0].min;
					sc[1].min = csc[1].min;
				}

				if (auto st = merged_own_style())
				{
					sc[0] += 2.f * (*st->border_width);
					sc[1] += 2.f * (*st->border_width);

					sc += 2.f * math::cast<float>(*st->inner_margin);
				}

				return sc;
			}

			math::interval<float> width_constraints(float height) const override
			{
				auto wc = element::width_constraints(height);

				auto st = merged_own_style();

				float extra = 2.f * (*st->border_width) + 2.f * math::cast<float>(*st->inner_margin)[0];

				if (child_)
				{
					auto cwc = child_->width_constraints(height - extra);
					wc.min = cwc.min;
				}

				wc += extra;

				return wc;
			}

			math::interval<float> height_constraints(float width) const override
			{
				auto hc = element::height_constraints(width);

				auto st = merged_own_style();

				float extra = 2.f * (*st->border_width) + 2.f * math::cast<float>(*st->inner_margin)[1];

				if (child_)
				{
					auto chc = child_->height_constraints(width - extra);
					hc.min = chc.min;
				}

				hc += extra;

				return hc;
			}

		private:
			box_shape shape_;
		};

		struct slider_impl
			: slider
		{
			struct shape const & shape() const override { return shape_; }

			void reshape(math::box<float, 2> const & bbox) override
			{
				shape_.box = bbox;
			}

			math::box<float, 2> size_constraints() const override
			{
				static float const inf = std::numeric_limits<float>::infinity();
				return {{{button_width(), inf}, {button_height(), inf}}};
			}

			void draw(painter & p) const override
			{
				auto st = merged_own_style();

				math::box<float, 2> ab;
				ab[0] = shape_.box[0];
				ab[1] = math::expand(math::interval<float>::singleton(shape_.box[1].center()), axis_width() / 2.f);

				gfx::color_rgba c = *st->fg_color;
				if (state() == state_t::mouseover)
					c = *st->highlight_color;
				else if (state() == state_t::mousedown)
					c = *st->action_color;

				auto const r = slider_range();

				float x = math::lerp(r, math::unlerp<float>(math::cast<float>(value_range()), value()));

				math::box<float, 2> sb;
				sb[0] = math::expand(math::interval<float>::singleton(x), button_width() / 2.f);
				sb[1] = math::expand(math::interval<float>::singleton(shape_.box[1].center()), button_height() / 2.f);

				if (*st->shadow_offset != math::vector{0, 0})
				{
					auto off = math::cast<float>(*st->shadow_offset);
					p.draw_rect(ab + off, *st->shadow_color);
					p.draw_rect(sb + off, *st->shadow_color);
				}

				p.draw_rect(ab, *st->axis_color);
				if (*st->border_width != 0)
					p.draw_rect(sb, *st->border_color);
				p.draw_rect(math::shrink(sb, 1.f * (*st->border_width)), c);
			}

		protected:

			math::interval<float> slider_range() const override
			{
				return math::shrink(shape_.box[0], button_width() / 2.f);
			}

		private:
			box_shape shape_;

			int button_width() const
			{
				auto st = merged_own_style();
				return *st->ref_height / 3;
			}

			int button_height() const
			{
				auto st = merged_own_style();
				return *st->ref_height;
			}

			int axis_width() const
			{
				auto st = merged_own_style();
				return *st->ref_height / 5;
			}
		};

		struct arrow_button_impl
			: button
		{
			arrow_button_impl(int direction)
				: direction_{direction}
			{}

			std::shared_ptr<element> set_child(std::shared_ptr<element>) override
			{
				throw std::runtime_error("default arrow button doesn't support children");
			}

			math::box<float, 2> size_constraints() const override
			{
				auto b = button::size_constraints();

				auto st = merged_own_style();
				b[0].min = *st->ref_height / 2.f;
				b[1].min = *st->ref_height / 2.f;

				return b;
			}

			struct shape const & shape() const override
			{
				return shape_;
			}

			void reshape(math::box<float, 2> const & box) override
			{
				shape_.box = box;

				switch (direction_)
				{
				case 0:
					triangle_[0] = box.corner(0.f, 1.f);
					triangle_[1] = box.corner(1.f, 1.f);
					triangle_[2] = box.corner(0.5f, 0.f);
					break;
				case 1:
					triangle_[0] = box.corner(0.f, 0.f);
					triangle_[1] = box.corner(1.f, 0.f);
					triangle_[2] = box.corner(0.5f, 1.f);
					break;
				case 2:
					triangle_[0] = box.corner(1.f, 0.f);
					triangle_[1] = box.corner(1.f, 1.f);
					triangle_[2] = box.corner(0.f, 0.5f);
					break;
				case 3:
					triangle_[0] = box.corner(0.f, 0.f);
					triangle_[1] = box.corner(0.f, 1.f);
					triangle_[2] = box.corner(1.f, 0.5f);
					break;
				default:
					throw std::runtime_error("Unknown arrow button direction");
				}

				if (math::volume(triangle_[0], triangle_[1], triangle_[2]) < 0.f)
					std::swap(triangle_[1], triangle_[2]);
			}

			void draw(painter &p) const override
			{
				auto st = merged_own_style();

				if (*st->shadow_offset != math::vector{0, 0})
					p.draw_triangle(triangle_ + math::cast<float>(*st->shadow_offset), *st->shadow_color);

				math::vector off{0.f, 0.f};
				if (state() == state_t::mousedown)
					off = math::cast<float>(*st->action_offset);

				gfx::color_rgba color = *st->fg_color;
				if (state() == state_t::mouseover)
					color = *st->highlight_color;
				else if (state() == state_t::mousedown)
					color = *st->action_color;
				p.draw_triangle(triangle_ + off, color);
			}

		private:
			int const direction_;
			box_shape shape_;
			math::triangle<math::point<float, 2>> triangle_;
		};

		struct spinbox_impl
			: spinbox
		{
			spinbox_impl()
			{
				auto layout = std::make_shared<grid_layout>();
				layout->set_row_count(2);

				auto inc_button = std::make_shared<arrow_button_impl>(0);
				auto dec_button = std::make_shared<arrow_button_impl>(1);

				inc_button->on_click([this]{
					inc();
				});

				dec_button->on_click([this]{
					dec();
				});

				layout->set(0, 0, inc_button);
				layout->set(1, 0, dec_button);
				layout->set_outer_margin(false);

				child_ = layout;
				children_range_[0] = child_.get();
				child_->set_parent(this);

				inc_button_ = inc_button.get();
				dec_button_ = dec_button.get();
			}

			math::box<float, 2> size_constraints() const override
			{
				return child_->size_constraints();
			}

			children_range children() const override
			{
				return {children_range_};
			}

			struct shape const & shape() const override
			{
				return child_->shape();
			}

			void reshape(math::box<float, 2> const & box) override
			{
				child_->reshape(box);
			}

			void draw(painter &) const override
			{}

			button * dec_button() const override { return dec_button_; }
			button * inc_button() const override { return inc_button_; }

			~spinbox_impl()
			{
				release_children();
			}

		private:
			std::shared_ptr<element> child_;
			element * children_range_[1] {nullptr};

			button * inc_button_;
			button * dec_button_;
		};

		struct scroller_impl
			: scroller
		{
			void draw(struct painter & painter) const override
			{
				auto st = merged_own_style();

				auto draw_box = [&](math::box<float, 2> box, state_t state, float position, int dimension)
				{
					float h = height();

					box[dimension].max -= h;
					float origin = math::lerp(box[dimension], position);
					box[dimension] = {origin, origin + h};

					gfx::color_rgba color;
					if (state == state_t::normal)
						color = *st->fg_color;
					else if (state == state_t::mouseover)
						color = *st->highlight_color;
					else
					{
						color = *st->action_color;
						box += math::cast<float>(*st->action_offset);
					}

					if (*st->shadow_offset != math::vector{0, 0})
						painter.draw_rect(box + math::cast<float>(*st->shadow_offset), *st->shadow_color);
					painter.draw_rect(box, color);
				};

				if (horizontal_scroll())
					draw_box(horizontal_box(), horizontal_state_, position(direction::horizontal), 0);

				if (vertical_scroll())
					draw_box(vertical_box(), vertical_state_, position(direction::vertical), 1);

				scroller::draw(painter);
			}

		protected:
			float width() const override
			{
				return 10.f;
			}

			float height() const
			{
				return 50.f;
			}
		};

		struct selector_impl
			: selector
		{
			void draw(painter & p) const override
			{
				auto st = merged_own_style();

				auto bbox = shape().bbox();

				if (st->shadow_offset)
					p.draw_rect(bbox + math::cast<float>(*st->shadow_offset), *st->shadow_color);

				p.draw_rect(bbox, *st->fg_color);

				if (auto i = selected())
					p.draw_rect(item_box(*i), *st->highlight_color);

				for (int i = 0; i < size(); ++i)
				{
					if (submenu(i))
					{
						auto box = item_box(i);

						auto c = box.corner(1.f, 0.5f);
						c[0] -= (*st->inner_margin)[0];

						int w = st->font->size()[1];

						auto p0 = c;
						p0[0] -= w / 2.f;
						p0[1] -= w / 2.f;

						auto p1 = c;
						p1[0] -= w / 2.f;
						p1[1] += w / 2.f;

						auto sh = math::cast<float>(*st->shadow_offset);

						p.draw_triangle({p0 + sh, p1 + sh, c + sh}, *st->shadow_color);
						p.draw_triangle({p0, p1, c}, *st->text_color);
					}
				}
			}

		protected:
			int submenu_extra() const override
			{
				auto st = merged_own_style();
				return 2 * (*st->inner_margin)[0] + st->font->size()[1] / 2;
			}
		};

		struct edit_impl
			: edit
		{
			void draw(painter & p) const override
			{
				auto st = merged_own_style();
				p.draw_rect(shape().bbox(), *st->action_color);

				edit::draw(p);
			}
		};

	}

	struct default_element_factory::impl
	{
		std::shared_ptr<gfx::texture_2d> close_icon;

		impl();
	};

	default_element_factory::impl::impl()
	{
		close_icon = std::make_shared<gfx::texture_2d>(gfx::texture_2d::from_pixmap(gfx::read_image<gfx::color_rgba>(io::memory_istream{resources::cross_red_16x16_png.data})));
		close_icon->nearest_filter();
	}

	default_element_factory::default_element_factory()
		: pimpl_{make_impl()}
	{}

	default_element_factory::~default_element_factory() = default;

	void default_element_factory::set_close_icon(gfx::pixmap_rgba const & icon)
	{
		impl().close_icon->load(icon);
	}

	std::shared_ptr<button> default_element_factory::make_button()
	{
		return std::make_shared<button_impl>();
	}

	std::shared_ptr<frame> default_element_factory::make_frame()
	{
		return std::make_shared<frame_impl>();
	}

	std::shared_ptr<window> default_element_factory::make_window(std::string caption)
	{
		auto r = std::make_shared<window_impl>(impl().close_icon);
		r->setup_close();
		r->caption()->set_text(std::move(caption));
		return r;
	}

	std::shared_ptr<spinbox> default_element_factory::make_spinbox()
	{
		return std::make_shared<spinbox_impl>();
	}

	std::shared_ptr<checkbox> default_element_factory::make_checkbox(bool value)
	{
		auto c = std::make_shared<checkbox_impl>();
		c->set_value(value);
		return c;
	}

	std::shared_ptr<checkbox> default_element_factory::make_toggle_button()
	{
		return std::make_shared<toggle_button_impl>();
	}

	std::shared_ptr<slider> default_element_factory::make_slider()
	{
		return std::make_shared<slider_impl>();
	}

	std::shared_ptr<button> default_element_factory::make_arrow_button(int direction)
	{
		return std::make_shared<arrow_button_impl>(direction);
	}

	std::shared_ptr<scroller> default_element_factory::make_scroller()
	{
		return std::make_shared<scroller_impl>();
	}

	std::shared_ptr<selector> default_element_factory::make_selector()
	{
		return std::make_shared<selector_impl>();
	}

	std::shared_ptr<edit> default_element_factory::make_edit()
	{
		return std::make_shared<edit_impl>();
	}

}
