#include <psemek/ui/progress_bar.hpp>

#include <psemek/math/interval.hpp>

namespace psemek::ui
{

	void progress_bar::set_value(float value)
	{
		value_ = math::clamp(value, {0.f, 1.f});
	}

	label * progress_bar::label() const
	{
		return dynamic_cast<struct label *>(child().get());
	}

}
