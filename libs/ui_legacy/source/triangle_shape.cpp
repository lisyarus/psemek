#include <psemek/ui/triangle_shape.hpp>

#include <psemek/math/contains.hpp>

namespace psemek::ui
{

	bool triangle_shape::contains(math::point<float, 2> const & point) const
	{
		return math::contains(triangle, point);
	}

	math::box<float, 2> triangle_shape::bbox() const
	{
		math::box<float, 2> b;
		b |= triangle[0];
		b |= triangle[1];
		b |= triangle[2];
		return b;
	}

}
