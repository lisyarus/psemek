#include <psemek/ui/box_shape.hpp>

#include <psemek/math/contains.hpp>

namespace psemek::ui
{

	bool box_shape::contains(math::point<float, 2> const & point) const
	{
		return math::contains(box, point);
	}

}
