#include <psemek/ui/asymmetric.hpp>

namespace psemek::ui
{

	void asymmetric::set_width_first(bool value)
	{
		width_first_ = value;
		post_reshape();
	}

}
