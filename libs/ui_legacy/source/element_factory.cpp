#include <psemek/ui/element_factory.hpp>

#include <psemek/ui/label.hpp>
#include <psemek/ui/button.hpp>
#include <psemek/ui/checkbox.hpp>
#include <psemek/ui/image_view.hpp>
#include <psemek/ui/rich_image_view.hpp>
#include <psemek/ui/grid_layout.hpp>
#include <psemek/ui/screen.hpp>
#include <psemek/ui/progress_bar.hpp>

namespace psemek::ui
{

	std::shared_ptr<button> element_factory::make_button() { return nullptr; }

	std::shared_ptr<button> element_factory::make_button(std::string text)
	{
		auto b = make_button();
		if (b)
		{
			auto l = make_label(std::move(text));
			if (l)
			{
				l->set_valign(label::valignment::center);
				l->set_halign(label::halignment::center);
				l->set_overflow(label::overflow_mode::ellipsis);
				b->set_child(l);
			}
		}
		return b;
	}

	std::shared_ptr<button> element_factory::make_button(gfx::texture_view_2d icon)
	{
		auto b = make_button();
		if (b)
		{
			auto i = make_image_view(icon);
			if (i)
			{
				i->set_downscale(false);
				i->set_upscale(false);
				b->set_child(i);
			}
		}
		return b;
	}

	std::shared_ptr<label> element_factory::make_label(std::string text, bool tagged)
	{
		auto result = std::make_shared<label>();
		if (tagged)
			result->set_tagged_text(std::move(text));
		else
			result->set_text(std::move(text));
		return result;
	}

	std::shared_ptr<frame> element_factory::make_frame() { return nullptr; }

	std::shared_ptr<window> element_factory::make_window(std::string) { return nullptr; }

	std::shared_ptr<screen> element_factory::make_screen()
	{
		return std::make_shared<screen>();
	}

	std::shared_ptr<grid_layout> element_factory::make_grid_layout()
	{
		return std::make_shared<grid_layout>();
	}

	std::shared_ptr<image_view> element_factory::make_image_view(gfx::texture_view_2d image)
	{
		auto i = std::make_shared<image_view>();
		i->set_image(image);
		return i;
	}

	std::shared_ptr<rich_image_view> element_factory::make_rich_image_view(std::shared_ptr<gfx::texture_2d> image)
	{
		auto i = std::make_shared<rich_image_view>();
		i->set_image(std::move(image));
		return i;
	}

	std::shared_ptr<checkbox> element_factory::make_checkbox(bool)
	{
		return nullptr;
	}

	std::shared_ptr<checkbox> element_factory::make_toggle_button() { return nullptr; }

	std::shared_ptr<checkbox> element_factory::make_toggle_button(std::string text)
	{
		auto b = make_toggle_button();
		if (b)
		{
			auto l = make_label(std::move(text));
			if (l)
			{
				l->set_valign(label::valignment::center);
				l->set_halign(label::halignment::center);
				l->set_overflow(label::overflow_mode::ellipsis);
				b->set_child(l);
			}
		}
		return b;
	}

	std::shared_ptr<checkbox> element_factory::make_toggle_button(gfx::texture_view_2d icon)
	{
		auto b = make_toggle_button();
		if (b)
		{
			auto i = make_image_view(icon);
			if (i)
			{
				i->set_downscale(false);
				i->set_upscale(false);
				b->set_child(i);
			}
		}
		return b;
	}

	std::shared_ptr<slider> element_factory::make_slider() { return nullptr; }

	std::shared_ptr<spinbox> element_factory::make_spinbox() { return nullptr; }

	std::shared_ptr<button> element_factory::make_arrow_button(int) { return nullptr; }

	std::shared_ptr<scroller> element_factory::make_scroller() { return nullptr; }

	std::shared_ptr<progress_bar> element_factory::make_progress_bar() { return nullptr; }

	std::shared_ptr<progress_bar> element_factory::make_progress_bar(std::string text)
	{
		auto result = make_progress_bar();

		if (result)
		{
			auto label = make_label(std::move(text));
			if (label)
			{
				label->set_halign(ui::label::halignment::center);
				label->set_valign(ui::label::valignment::center);
				result->set_child(label);
			}
		}

		return result;
	}

	std::shared_ptr<selector> element_factory::make_selector() { return nullptr; }

	std::shared_ptr<table> element_factory::make_table() { return nullptr; }

	std::shared_ptr<edit> element_factory::make_edit() { return nullptr; }

}
