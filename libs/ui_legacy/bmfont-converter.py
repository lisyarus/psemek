#!/usr/bin/env python3

import json
import sys

result = dict()
result["info"] = dict()
result["common"] = dict()
result["chars"] = list()

with open(sys.argv[1], "rt") as in_file:
    for line in in_file:
        #print("Line:", line)
        parts = line.strip("\n").split(" ")
        #print("Parts:", parts)
        key = parts[0]
        obj = {p.split("=")[0]: p.split("=")[1] for p in parts[1:] if len(p) > 0}
        if key == "info":
            result[key]["face"] = obj["face"].strip("\"")
            result[key]["size"] = abs(int(obj["size"]))
        if key == "common":
            result[key]["base"] = int(obj["base"])
        if key == "char":
            char = {k: int(v) for k, v in obj.items()}
            result["chars"].append(char)

print(json.dumps(result))
