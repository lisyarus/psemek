#pragma once

#include <psemek/math/box.hpp>
#include <psemek/util/range.hpp>

namespace psemek::cg
{

	template <typename Iterator>
	auto bbox(Iterator begin, Iterator end)
	{
		using point_type = std::decay_t<decltype(*begin)>;

		math::box<typename point_type::scalar_type, point_type::static_dimension> result;
		for (; begin != end; ++begin)
			result |= *begin;
		return result;
	}

	template <typename Container>
	auto bbox(Container const & container)
	{
		return bbox(util::xbegin(container), util::xend(container));
	}

}
