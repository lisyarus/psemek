#pragma once

#include <psemek/math/simplex.hpp>
#include <psemek/cg/body/body.hpp>

#include <vector>

namespace psemek::cg
{

	template <typename T>
	struct polygon
	{
		polygon(std::vector<math::point<T, 2>> vertices);

		std::vector<math::point<T, 2>> vertices;
		std::vector<math::segment<std::size_t>> edges;
	};

	template <typename T>
	polygon<T>::polygon(std::vector<math::point<T, 2>> vertices)
		: vertices(std::move(vertices))
	{
		edges.resize(vertices.size());
		for (std::size_t i = 0; i < vertices.size(); ++i)
			edges[i].points = {i, (i + 1) % vertices.size()};
	}

	template <typename T>
	auto const & vertices(polygon<T> const & p)
	{
		return p.vertices;
	}

	template <typename T>
	auto const & edges(polygon<T> const & p)
	{
		return p.edges;
	}

}
