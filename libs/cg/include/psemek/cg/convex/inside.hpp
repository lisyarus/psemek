#pragma once

#include <psemek/cg/body/body.hpp>

#include <psemek/math/orientation.hpp>

namespace psemek::cg
{

	template <typename RobustTag, typename T, std::size_t N, typename Body>
	bool inside(RobustTag robust_tag, math::point<T, N> const & p, Body const & body)
	{
		auto const & vs = vertices(body);
		auto const & fs = faces(body);

		for (auto const & f : fs)
		{
			if (math::orientation(robust_tag, vs[f[0]], vs[f[1]], vs[f[2]], p) == math::sign_t::negative)
				return false;
		}
		return true;
	}

	template <typename T, std::size_t N, typename Body>
	bool inside(math::point<T, N> const & p, Body const & body)
	{
		return inside(math::default_robust_tag, p, body);
	}

}
