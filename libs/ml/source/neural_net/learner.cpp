#include <psemek/ml/neural_net/learner.hpp>

namespace psemek::ml
{

	template struct neural_net_learner<float>;
	template struct neural_net_learner<double>;

}
