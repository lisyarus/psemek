#include <psemek/ml/neural_net/evaluator.hpp>

namespace psemek::ml
{

	template struct neural_net_evaluator<float>;
	template struct neural_net_evaluator<double>;

}
