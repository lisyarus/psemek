#include <psemek/ml/neural_net/neural_net.hpp>

#include <psemek/util/to_string.hpp>

namespace psemek::ml
{

	template struct neural_net<float>;
	template struct neural_net<double>;

}
