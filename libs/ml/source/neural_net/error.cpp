#include <psemek/ml/neural_net/error.hpp>

#include <psemek/util/to_string.hpp>

namespace psemek::ml
{

	empty_neural_net_error::empty_neural_net_error(util::stacktrace stacktrace)
		: neural_net_error("Neural net must have at least a single layer", std::move(stacktrace))
	{}

	wrong_activation_types_count_error::wrong_activation_types_count_error(std::size_t expected, std::size_t actual, util::stacktrace stacktrace)
		: neural_net_error(util::to_string("Wrong number of activation types: expected ", expected, ", got ", actual), std::move(stacktrace))
	{}

	wrong_neural_net_input_size::wrong_neural_net_input_size(std::size_t expected, std::size_t actual, util::stacktrace stacktrace)
		: neural_net_error(util::to_string("Wrong neural net input size: expected ", expected, ", got ", actual), std::move(stacktrace))
	{}

	wrong_neural_net_output_size::wrong_neural_net_output_size(std::size_t expected, std::size_t actual, util::stacktrace stacktrace)
		: neural_net_error(util::to_string("Wrong neural net output size: expected ", expected, ", got ", actual), std::move(stacktrace))
	{}

}
