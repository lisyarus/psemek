#include <psemek/ml/neural_net/randomize.hpp>

namespace psemek::ml
{

	template void randomize_uniform<float, random::generator>(neural_net<float> &, random::generator &&);
	template void randomize_uniform<double, random::generator>(neural_net<double> &, random::generator &&);
	template void randomize_normal<float, random::generator>(neural_net<float> &, random::generator &&);
	template void randomize_normal<double, random::generator>(neural_net<double> &, random::generator &&);

}
