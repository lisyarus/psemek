#pragma once

#include <vector>

#include <psemek/math/math.hpp>

namespace psemek::ml
{

	template <typename T>
	T l2_loss(std::vector<T> const & x1, std::vector<T> const & x2)
	{
		T value = T{0};
		for (std::size_t i = 0; i < x1.size(); ++i)
			value += math::sqr(x1[i] - x2[i]) / T{2};
		return value;
	}

}
