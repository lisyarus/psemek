#pragma once

#include <psemek/util/exception.hpp>

namespace psemek::ml
{

	struct neural_net_error
		: util::exception
	{
		using util::exception::exception;
	};

	struct empty_neural_net_error
		: neural_net_error
	{
		empty_neural_net_error(util::stacktrace stacktrace = {});
	};

	struct wrong_activation_types_count_error
		: neural_net_error
	{
		wrong_activation_types_count_error(std::size_t expected, std::size_t actual, util::stacktrace stacktrace = {});
	};

	struct wrong_neural_net_input_size
		: neural_net_error
	{
		wrong_neural_net_input_size(std::size_t expected, std::size_t actual, util::stacktrace stacktrace = {});
	};

	struct wrong_neural_net_output_size
		: neural_net_error
	{
		wrong_neural_net_output_size(std::size_t expected, std::size_t actual, util::stacktrace stacktrace = {});
	};

}
