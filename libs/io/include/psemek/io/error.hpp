#pragma once

#include <psemek/util/exception.hpp>

namespace psemek::io
{

	struct null_stream
		: util::exception
	{
		using util::exception::exception;
	};

	struct null_istream
		: null_stream
	{
		null_istream(util::stacktrace stacktrace = {});
	};

	struct null_ostream
		: null_stream
	{
		null_ostream(util::stacktrace stacktrace = {});
	};

	struct stream_end
		: util::exception
	{
		using util::exception::exception;
	};

	struct istream_end
		: stream_end
	{
		istream_end(util::stacktrace stacktrace = {});
	};

	struct ostream_end
		: stream_end
	{
		ostream_end(util::stacktrace stacktrace = {});
	};

}
