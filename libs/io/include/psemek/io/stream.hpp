#pragma once

#include <psemek/io/error.hpp>
#include <psemek/util/blob.hpp>

#include <cstddef>
#include <memory>

namespace psemek::io
{

	struct istream
	{
		virtual std::size_t read(char * p, std::size_t size) = 0;
		virtual std::size_t read_all(char * p, std::size_t size);
		virtual bool finished() const = 0;

		virtual ~istream() {}
	};

	struct ostream
	{
		virtual std::size_t write(char const * p, std::size_t size) = 0;
		virtual std::size_t write_all(char const * p, std::size_t size);
		virtual void flush() {}

		virtual ~ostream() {}
	};

	std::unique_ptr<istream> std_in();
	std::unique_ptr<ostream> std_out();
	std::unique_ptr<ostream> std_err();

	util::blob read_full(istream && stream);

}
