#pragma once

#include <psemek/io/stream.hpp>

#include <vector>

namespace psemek::io
{

	// Convert a set of output streams to a set of mutually
	// synchronized streams: writing to any of the streams
	// flushes all the other streams
	std::vector<std::unique_ptr<ostream>> synchronized(std::vector<std::unique_ptr<ostream>> streams);

}
