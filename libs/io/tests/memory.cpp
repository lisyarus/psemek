#include <psemek/test/test.hpp>

#include <psemek/io/memory_stream.hpp>

using namespace psemek::io;

test_case(io_memory_null)
{
	char buffer[256]{};

	memory_istream is;
	expect_throw(is.read(buffer, std::size(buffer)), null_istream);

	memory_ostream os;
	expect_throw(os.write(buffer, std::size(buffer)), null_ostream);
}

test_case(io_memory_write)
{
	char const test_str[] = "Hello, world!";
	auto const length = std::size(test_str);
	char buffer_out[256]{};

	memory_ostream os(buffer_out, buffer_out + std::size(buffer_out));
	expect_equal(os.write(test_str, length), length);
	expect_equal(std::string_view(test_str, length), std::string_view(buffer_out, length));
}

test_case(io_memory_read)
{
	char const test_str[] = "Hello, world!";
	auto const length = std::size(test_str);
	char buffer_in[256]{};

	memory_istream is(test_str, test_str + length);
	expect_equal(is.read(buffer_in, length), length);
	expect_equal(std::string_view(test_str, length), std::string_view(buffer_in, length));
}

test_case(io_memory_write__overflow)
{
	char const test_str[] = "Hello, world!";
	char buffer_out[8]{};
	auto const length = std::size(buffer_out);

	expect_less(length, std::size(test_str));

	memory_ostream os(buffer_out, buffer_out + length);
	expect_equal(os.write(test_str, std::size(test_str)), length);
	expect_equal(std::string_view(test_str, length), std::string_view(buffer_out, length));
}

test_case(io_memory_read__overflow)
{
	char const test_str[] = "Hello, world!";
	char buffer_in[8]{};
	auto const length = std::size(buffer_in);

	expect_less(length, std::size(test_str));

	memory_istream is(test_str, test_str + std::size(test_str));
	expect_equal(is.read(buffer_in, length), length);
	expect_equal(std::string_view(test_str, length), std::string_view(buffer_in, length));
}
