#include <psemek/io/memory_stream.hpp>

#include <algorithm>

namespace psemek::io
{

	std::size_t memory_istream::read(char * p, std::size_t size)
	{
		if (!begin_) throw null_istream{};
		size = std::min<std::size_t>(size, end_ - begin_);
		std::copy(begin_, begin_ + size, p);
		begin_ += size;
		return size;
	}

	std::size_t memory_ostream::write(char const * p, std::size_t size)
	{
		if (!begin_) throw null_ostream{};
		size = std::min<std::size_t>(size, end_ - begin_);
		std::copy(p, p + size, begin_);
		begin_ += size;
		return size;
	}

}
