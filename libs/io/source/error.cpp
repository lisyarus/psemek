#include <psemek/io/error.hpp>

namespace psemek::io
{

	null_istream::null_istream(util::stacktrace stacktrace)
		: null_stream("Attempt to read from null input stream", std::move(stacktrace))
	{}

	null_ostream::null_ostream(util::stacktrace stacktrace)
		: null_stream("Attempt to write to null output stream", std::move(stacktrace))
	{}

	istream_end::istream_end(util::stacktrace stacktrace)
		: stream_end("Unexpected input stream end", std::move(stacktrace))
	{}

	ostream_end::ostream_end(util::stacktrace stacktrace)
		: stream_end("Unexpected output stream end", std::move(stacktrace))
	{}

}
