#!/usr/bin/env bash

set -e

mkdir /home/build
cd /home/build
cmake ../source -DCMAKE_TOOLCHAIN_FILE=/home/toolchain.cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo
cmake --build . -j
