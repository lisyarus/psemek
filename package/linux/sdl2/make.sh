#!/usr/bin/env bash

set -e

mkdir build
cd build

cat ../options | grep '  --' | awk '{ print $1 }' | xargs ../source/configure -srcdir=../source --prefix=`realpath ../install`

make -j16
make install

cd ..
strip -g install/lib/libSDL2.so

SONAME=`objdump -p install/lib/libSDL2.so | grep SONAME | awk '{print $2}'`
mv -v `readlink -f install/lib/libSDL2.so` "install/lib/${SONAME}"
ln -svf "${SONAME}" "install/lib/libSDL2.so"
