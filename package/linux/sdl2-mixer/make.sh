#!/usr/bin/env bash

set -e

rm -rf build
mkdir build
cd build

cat ../options | grep '  --' | awk '{ print $1 }' | xargs ../source/configure -srcdir=../source --prefix=`realpath ../../sdl2/install`

make -j16
make install

cd ../../sdl2/
strip -g install/lib/libSDL2_mixer.so

SONAME=`objdump -p install/lib/libSDL2_mixer.so | grep SONAME | awk '{print $2}'`
mv -v `readlink -f install/lib/libSDL2_mixer.so` "install/lib/${SONAME}"
ln -svf "${SONAME}" "install/lib/libSDL2_mixer.so"
