set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc-posix)
set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++-posix)
set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSROOT /usr/x86_64-w64-mingw32)
set(CMAKE_BUILD_TYPE Release)
set(CMAKE_RC_COMPILER  /usr/bin/x86_64-w64-mingw32-windres)

set(PSEMEK_PACKAGE_MODE ON)
set(PSEMEK_PACKAGE_ROOT "${CMAKE_CURRENT_LIST_DIR}")
set(PSEMEK_PACKAGE_HOST OFF)
set(PSEMEK_PACKAGE_TARGET ON)
set(PSEMEK_PACKAGE_TOOLS_PATH "${PSEMEK_PACKAGE_ROOT}/tools")

set(PSEMEK_PACKAGE_COPY_FILES
	${PSEMEK_PACKAGE_ROOT}/sdl2/install/bin/SDL2.dll
	/usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll
)

set(PSEMEK_PACKAGE_COPY_FILES_WEBGPU
	${PSEMEK_PACKAGE_ROOT}/wgpu-native/install/target/wgpu_native.dll
)

set(PSEMEK_PACKAGE_COPY_FILES_FREETYPE
	${PSEMEK_PACKAGE_ROOT}/freetype/install/bin/*
)

list(APPEND CMAKE_PREFIX_PATH
	"${PSEMEK_PACKAGE_ROOT}/sdl2/install"
	"${PSEMEK_PACKAGE_ROOT}/boost/install"
	"${PSEMEK_PACKAGE_ROOT}/freetype/install"
)

set(WGPU_NATIVE_ROOT
	"${PSEMEK_PACKAGE_ROOT}/wgpu-native/install/target"
)
