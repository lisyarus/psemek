#!/usr/bin/env bash

set -e

mkdir -p /home/build-host
cd /home/build-host
cmake ../source -DCMAKE_TOOLCHAIN_FILE=/home/toolchain-host.cmake -DCMAKE_INSTALL_PREFIX=/home/tools
cmake --build . -t install -j

mkdir -p /home/build-target
cd /home/build-target
rm -rf ./*
cmake ../source -DCMAKE_TOOLCHAIN_FILE=/home/toolchain-target.cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo
cmake --build . -j
