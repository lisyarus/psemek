#!/usr/bin/env bash

set -e

if [ "$#" -ne 3 ]; then
    echo "Usage: package-helper <target-name> <application-name> <apk-path>"
    exit -1
fi

echo "$1" > /home/target-name
echo "$2" > /home/application-name
echo "$3" > /home/apk-name