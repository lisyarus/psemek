* geom
	* Primitives of dynamic runtime size (Eigen-like)
* parser
	* overflow checks for number parsers
	* recursive parsers
* ui
	* screen children sorting
* audio
    * noise generators
    * linear, quadratic & any-order feedforward (finite impulse response), feedback (infinite impulse response = resonance) & mixed filters, see https://www.music.mcgill.ca/~gary/618/week1/node11.html
    * all-pass, band-pass, band-stop filters
    * echo
    * delay
    * overdrive
    * reverb
    * spline-based envelopes
    * drum sounds, maybe more instruments
    * midi player with effects
    * 3D listener
    

