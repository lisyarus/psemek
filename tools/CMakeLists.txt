file(GLOB PSEMEK_TOOLS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/*")
list(REMOVE_ITEM PSEMEK_TOOLS "CMakeLists.txt")

foreach(tool ${PSEMEK_TOOLS})
	add_subdirectory(${tool})
endforeach()
