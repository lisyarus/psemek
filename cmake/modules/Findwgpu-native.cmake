if(wgpu-native_FOUND)
	set(wgpu-native_FIND_QUIETLY TRUE)
endif()

# Don't search for include files - these are bundled with psemek-wgpu lib
find_library(wgpu-native_LIBRARIES NAMES libwgpu_native.a wgpu_native.dll wgpu_native PATHS "${WGPU_NATIVE_ROOT}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(wgpu-native DEFAULT_MSG wgpu-native_LIBRARIES)

if(wgpu-native_FOUND AND NOT TARGET wgpu-native)
	add_library(wgpu-native STATIC IMPORTED)
	set_target_properties(wgpu-native PROPERTIES
		IMPORTED_LOCATION "${wgpu-native_LIBRARIES}"
	)
endif()

mark_as_advanced(wgpu-native_LIBRARIES)
