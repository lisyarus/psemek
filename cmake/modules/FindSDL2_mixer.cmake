if(SDL2_mixer_FOUND)
	set(SDL2_mixer_FIND_QUIETLY TRUE)
endif()

find_path(SDL2_mixer_INCLUDE_DIRS NAMES "SDL2/SDL_mixer.h" PATHS "${SDL2_mixer_ROOT}/include")
find_library(SDL2_mixer_LIBRARIES NAMES "SDL2_mixer" PATHS "${SDL2_mixer_ROOT}/lib")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SDL2_mixer DEFAULT_MSG SDL2_mixer_INCLUDE_DIRS SDL2_mixer_LIBRARIES)

if(SDL2_mixer_FOUND AND (NOT (TARGET SDL2_mixer)))
	if(WIN32)
		add_library(SDL2_mixer STATIC IMPORTED)
	else()
		add_library(SDL2_mixer SHARED IMPORTED)
	endif()
	set_target_properties(SDL2_mixer PROPERTIES
		IMPORTED_LOCATION "${SDL2_mixer_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${SDL2_mixer_INCLUDE_DIRS}"
	)
endif()

mark_as_advanced(SDL2_mixer_INCLUDE_DIRS SDL2_mixer_LIBRARIES)
