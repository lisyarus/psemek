if(GMP_FOUND)
	set(GMP_FIND_QUIETLY TRUE)
endif()

find_path(GMP_INCLUDE_DIRS NAMES "gmp.h" PATHS "${GMP_ROOT}/include")
find_library(GMP_LIBRARIES NAMES "gmp" PATHS "${GMP_ROOT}/lib")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GMP DEFAULT_MSG GMP_INCLUDE_DIRS GMP_LIBRARIES)

if(GMP_FOUND AND NOT TARGET GMP)
	if(WIN32)
		add_library(GMP STATIC IMPORTED)
	else()
		add_library(GMP SHARED IMPORTED)
	endif()
	set_target_properties(GMP PROPERTIES
		IMPORTED_LOCATION "${GMP_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${GMP_INCLUDE_DIRS}"
	)
endif()

mark_as_advanced(GMP_INCLUDE_DIRS GMP_LIBRARIES)
