#!/usr/bin/env bash

echo "Total commits:" $(git rev-list --all --count)
echo "Source files: " $(find tools/ libs/ examples/ -name '*.cpp' -or -name '*.hpp' | wc -l)
echo "Source lines: " $(find tools/ libs/ examples/ -name '*.cpp' -or -name '*.hpp' | xargs cat | wc -l)
